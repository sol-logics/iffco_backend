PGDMP     6            
    
    y            fams    13.2    13.2 �    �           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            �           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            �           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            �           1262    16571    fams    DATABASE     `   CREATE DATABASE fams WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE = 'English_India.1252';
    DROP DATABASE fams;
                postgres    false            �            1259    16572    amc    TABLE     d  CREATE TABLE public.amc (
    id bigint NOT NULL,
    created_by bigint,
    created_date timestamp(6) with time zone,
    updated_by bigint,
    updated_date timestamp(6) with time zone,
    is_deleted boolean,
    deleted_date timestamp with time zone,
    deleted_by bigint,
    ref_vendor bigint,
    ref_asset bigint,
    start_date timestamp(6) with time zone,
    expiry_date timestamp(6) with time zone,
    cost numeric(20,8),
    amc_unique_code character varying,
    s_number_count bigint,
    previous_amc_id bigint,
    renew_uuid character varying,
    is_latest boolean,
    ref_status bigint
);
    DROP TABLE public.amc;
       public         heap    postgres    false            �            1259    16578 
   amc_id_seq    SEQUENCE     s   CREATE SEQUENCE public.amc_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 !   DROP SEQUENCE public.amc_id_seq;
       public          postgres    false    200            �           0    0 
   amc_id_seq    SEQUENCE OWNED BY     9   ALTER SEQUENCE public.amc_id_seq OWNED BY public.amc.id;
          public          postgres    false    201            �            1259    16580    amc_services    TABLE     �  CREATE TABLE public.amc_services (
    id bigint NOT NULL,
    ref_amc bigint,
    description character varying,
    ref_status bigint,
    created_by bigint,
    created_date timestamp(6) with time zone,
    updated_by bigint,
    updated_date timestamp(6) with time zone,
    is_deleted boolean,
    deleted_by bigint,
    deleted_date timestamp(6) with time zone,
    year bigint,
    month bigint,
    day bigint
);
     DROP TABLE public.amc_services;
       public         heap    postgres    false            �            1259    16586    amc_services_id_seq    SEQUENCE     |   CREATE SEQUENCE public.amc_services_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.amc_services_id_seq;
       public          postgres    false    202            �           0    0    amc_services_id_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE public.amc_services_id_seq OWNED BY public.amc_services.id;
          public          postgres    false    203            �            1259    16588    asset    TABLE     �  CREATE TABLE public.asset (
    id bigint NOT NULL,
    name character varying NOT NULL,
    manufacturer character varying,
    model_no character varying NOT NULL,
    serial_no character varying NOT NULL,
    uom character varying,
    capacity character varying,
    unique_code character varying,
    ref_location bigint,
    ref_type bigint,
    ref_category bigint,
    department character varying,
    warranty_expiry_date timestamp(6) with time zone,
    ref_status bigint,
    purchase_date timestamp(6) with time zone,
    purchase_price numeric(20,8),
    description character varying,
    sub_component bigint,
    useful_life character varying,
    s_no_count bigint,
    updated_date timestamp(6) with time zone,
    updated_by bigint,
    deleted_date timestamp(6) with time zone,
    deleted_by bigint,
    created_by bigint,
    created_date timestamp(6) with time zone,
    is_deleted boolean,
    ref_asset_group bigint
);
    DROP TABLE public.asset;
       public         heap    postgres    false            �            1259    16594    asset_group    TABLE     Y  CREATE TABLE public.asset_group (
    id bigint NOT NULL,
    name character varying,
    description character varying,
    created_date timestamp with time zone,
    created_by bigint,
    updated_by bigint,
    updated_date timestamp with time zone,
    is_deleted boolean,
    deleted_by bigint,
    deleted_date timestamp with time zone
);
    DROP TABLE public.asset_group;
       public         heap    postgres    false            �            1259    16600    asset_group_id_seq    SEQUENCE     {   CREATE SEQUENCE public.asset_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.asset_group_id_seq;
       public          postgres    false    205            �           0    0    asset_group_id_seq    SEQUENCE OWNED BY     I   ALTER SEQUENCE public.asset_group_id_seq OWNED BY public.asset_group.id;
          public          postgres    false    206            �            1259    16602    asset_id_seq    SEQUENCE     u   CREATE SEQUENCE public.asset_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public.asset_id_seq;
       public          postgres    false    204            �           0    0    asset_id_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE public.asset_id_seq OWNED BY public.asset.id;
          public          postgres    false    207            �            1259    16767    attachment_association    TABLE       CREATE TABLE public.attachment_association (
    id bigint NOT NULL,
    ref_user bigint,
    ref_amc bigint,
    ref_asset bigint,
    ref_vendor bigint,
    file_name character varying,
    path character varying,
    created_date timestamp(6) with time zone,
    created_by bigint,
    updated_date timestamp(6) with time zone,
    updated_by bigint,
    is_deleted boolean,
    deleted_date timestamp(6) with time zone,
    deleted_by bigint,
    file_download_uri character varying,
    file_type character varying,
    size bigint
);
 *   DROP TABLE public.attachment_association;
       public         heap    postgres    false            �            1259    16765    attachment_association_id_seq    SEQUENCE     �   CREATE SEQUENCE public.attachment_association_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 4   DROP SEQUENCE public.attachment_association_id_seq;
       public          postgres    false    237            �           0    0    attachment_association_id_seq    SEQUENCE OWNED BY     _   ALTER SEQUENCE public.attachment_association_id_seq OWNED BY public.attachment_association.id;
          public          postgres    false    236            �            1259    16834    audit_trail    TABLE       CREATE TABLE public.audit_trail (
    id bigint NOT NULL,
    ref_amc bigint,
    ref_assets bigint,
    ref_vendor bigint,
    ref_user bigint,
    action_by bigint,
    action_date timestamp(6) with time zone,
    action character varying,
    data character varying
);
    DROP TABLE public.audit_trail;
       public         heap    postgres    false            �            1259    16832    audit_trail_id_seq    SEQUENCE     {   CREATE SEQUENCE public.audit_trail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.audit_trail_id_seq;
       public          postgres    false    239            �           0    0    audit_trail_id_seq    SEQUENCE OWNED BY     I   ALTER SEQUENCE public.audit_trail_id_seq OWNED BY public.audit_trail.id;
          public          postgres    false    238            �            1259    16604    category    TABLE     ^   CREATE TABLE public.category (
    id bigint NOT NULL,
    name character varying NOT NULL
);
    DROP TABLE public.category;
       public         heap    postgres    false            �            1259    16610    category_id_seq    SEQUENCE     x   CREATE SEQUENCE public.category_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.category_id_seq;
       public          postgres    false    208            �           0    0    category_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE public.category_id_seq OWNED BY public.category.id;
          public          postgres    false    209            �            1259    16612    company    TABLE     �   CREATE TABLE public.company (
    id bigint NOT NULL,
    name character varying NOT NULL,
    address character varying NOT NULL,
    website character varying,
    contact character varying
);
    DROP TABLE public.company;
       public         heap    postgres    false            �            1259    16618    company_id_seq    SEQUENCE     w   CREATE SEQUENCE public.company_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.company_id_seq;
       public          postgres    false    210            �           0    0    company_id_seq    SEQUENCE OWNED BY     A   ALTER SEQUENCE public.company_id_seq OWNED BY public.company.id;
          public          postgres    false    211            �            1259    16620    location    TABLE     r   CREATE TABLE public.location (
    id bigint NOT NULL,
    floor character varying,
    area character varying
);
    DROP TABLE public.location;
       public         heap    postgres    false            �            1259    16626    location_id_seq    SEQUENCE     x   CREATE SEQUENCE public.location_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.location_id_seq;
       public          postgres    false    212            �           0    0    location_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE public.location_id_seq OWNED BY public.location.id;
          public          postgres    false    213            �            1259    16628    notification_alert_details    TABLE     n  CREATE TABLE public.notification_alert_details (
    id bigint NOT NULL,
    mail_body character varying,
    mail_subject character varying,
    alert_times bigint,
    alert_intervals bigint,
    end_times bigint,
    object_id bigint,
    stop_on_action boolean,
    cc_to character varying,
    alert_left bigint,
    created_by bigint,
    created_date timestamp(6) with time zone,
    alert_sent bigint,
    notification_name character varying,
    alert_status boolean,
    alert_start_time timestamp(6) with time zone,
    "alert_end_time " timestamp(6) with time zone,
    attachment_details character varying
);
 .   DROP TABLE public.notification_alert_details;
       public         heap    postgres    false            �            1259    16634 !   notification_alert_details_id_seq    SEQUENCE     �   CREATE SEQUENCE public.notification_alert_details_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 8   DROP SEQUENCE public.notification_alert_details_id_seq;
       public          postgres    false    214            �           0    0 !   notification_alert_details_id_seq    SEQUENCE OWNED BY     g   ALTER SEQUENCE public.notification_alert_details_id_seq OWNED BY public.notification_alert_details.id;
          public          postgres    false    215            �            1259    16636    notification_config    TABLE     �  CREATE TABLE public.notification_config (
    id bigint NOT NULL,
    alert_times bigint,
    alert_intervals bigint,
    before_days bigint,
    end_time bigint,
    stop_on_action boolean,
    created_by bigint,
    created_date timestamp(6) with time zone,
    updated_by bigint,
    updated_date timestamp(6) with time zone,
    is_deleted boolean,
    deleted_by bigint,
    deleted_date timestamp(6) with time zone,
    ref_notification_meta bigint
);
 '   DROP TABLE public.notification_config;
       public         heap    postgres    false            �            1259    16639    notification_config_id_seq    SEQUENCE     �   CREATE SEQUENCE public.notification_config_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 1   DROP SEQUENCE public.notification_config_id_seq;
       public          postgres    false    216            �           0    0    notification_config_id_seq    SEQUENCE OWNED BY     Y   ALTER SEQUENCE public.notification_config_id_seq OWNED BY public.notification_config.id;
          public          postgres    false    217            �            1259    16641    notification_meta    TABLE     R  CREATE TABLE public.notification_meta (
    id bigint NOT NULL,
    notification_name character varying,
    mail_body character varying,
    mail_subject character varying,
    created_date timestamp(6) with time zone,
    updated_date timestamp(6) with time zone,
    is_deleted boolean,
    deleted_date timestamp(6) with time zone
);
 %   DROP TABLE public.notification_meta;
       public         heap    postgres    false            �            1259    16647    notification_meta_id_seq    SEQUENCE     �   CREATE SEQUENCE public.notification_meta_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 /   DROP SEQUENCE public.notification_meta_id_seq;
       public          postgres    false    218            �           0    0    notification_meta_id_seq    SEQUENCE OWNED BY     U   ALTER SEQUENCE public.notification_meta_id_seq OWNED BY public.notification_meta.id;
          public          postgres    false    219            �            1259    16649    notification_receiver    TABLE     f  CREATE TABLE public.notification_receiver (
    id bigint NOT NULL,
    ref_notification_config bigint,
    ref_user bigint,
    created_by bigint,
    created_date timestamp(6) with time zone,
    updated_by bigint,
    updated_date timestamp(6) with time zone,
    is_deleted boolean,
    deleted_by bigint,
    deleted_date timestamp(6) with time zone
);
 )   DROP TABLE public.notification_receiver;
       public         heap    postgres    false            �            1259    16652    notification_receiver_id_seq    SEQUENCE     �   CREATE SEQUENCE public.notification_receiver_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 3   DROP SEQUENCE public.notification_receiver_id_seq;
       public          postgres    false    220            �           0    0    notification_receiver_id_seq    SEQUENCE OWNED BY     ]   ALTER SEQUENCE public.notification_receiver_id_seq OWNED BY public.notification_receiver.id;
          public          postgres    false    221            �            1259    16654    profile    TABLE     �  CREATE TABLE public.profile (
    id bigint NOT NULL,
    ref_user bigint NOT NULL,
    first_name character varying NOT NULL,
    last_name character varying,
    ref_role bigint NOT NULL,
    created_on timestamp(6) with time zone,
    updated_on timestamp(6) with time zone,
    ref_company bigint NOT NULL,
    emp_id character varying,
    designation character varying,
    departement character varying,
    updated_by bigint,
    created_by bigint,
    deleted_by bigint,
    is_deleted boolean
);
    DROP TABLE public.profile;
       public         heap    postgres    false            �            1259    16660    profile_id_seq    SEQUENCE     w   CREATE SEQUENCE public.profile_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.profile_id_seq;
       public          postgres    false    222            �           0    0    profile_id_seq    SEQUENCE OWNED BY     A   ALTER SEQUENCE public.profile_id_seq OWNED BY public.profile.id;
          public          postgres    false    223            �            1259    16662    roles    TABLE     [   CREATE TABLE public.roles (
    id bigint NOT NULL,
    type character varying NOT NULL
);
    DROP TABLE public.roles;
       public         heap    postgres    false            �            1259    16668    roles_id_seq    SEQUENCE     u   CREATE SEQUENCE public.roles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public.roles_id_seq;
       public          postgres    false    224            �           0    0    roles_id_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE public.roles_id_seq OWNED BY public.roles.id;
          public          postgres    false    225            �            1259    16670    status    TABLE     \   CREATE TABLE public.status (
    id bigint NOT NULL,
    name character varying NOT NULL
);
    DROP TABLE public.status;
       public         heap    postgres    false            �            1259    16676    status_id_seq    SEQUENCE     v   CREATE SEQUENCE public.status_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE public.status_id_seq;
       public          postgres    false    226            �           0    0    status_id_seq    SEQUENCE OWNED BY     ?   ALTER SEQUENCE public.status_id_seq OWNED BY public.status.id;
          public          postgres    false    227            �            1259    16678    type    TABLE     Z   CREATE TABLE public.type (
    id bigint NOT NULL,
    name character varying NOT NULL
);
    DROP TABLE public.type;
       public         heap    postgres    false            �            1259    16684    type_id_seq    SEQUENCE     t   CREATE SEQUENCE public.type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 "   DROP SEQUENCE public.type_id_seq;
       public          postgres    false    228            �           0    0    type_id_seq    SEQUENCE OWNED BY     ;   ALTER SEQUENCE public.type_id_seq OWNED BY public.type.id;
          public          postgres    false    229            �            1259    16686    user_otp    TABLE     �   CREATE TABLE public.user_otp (
    id bigint NOT NULL,
    otp character varying NOT NULL,
    created_on timestamp(6) with time zone NOT NULL,
    valid_upto timestamp(6) with time zone NOT NULL,
    ref_user bigint NOT NULL
);
    DROP TABLE public.user_otp;
       public         heap    postgres    false            �            1259    16692    user_otp_id_seq    SEQUENCE     x   CREATE SEQUENCE public.user_otp_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.user_otp_id_seq;
       public          postgres    false    230            �           0    0    user_otp_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE public.user_otp_id_seq OWNED BY public.user_otp.id;
          public          postgres    false    231            �            1259    16694    users    TABLE     �  CREATE TABLE public.users (
    id bigint NOT NULL,
    email character varying NOT NULL,
    password character varying,
    created_on timestamp(6) with time zone,
    updated_on timestamp(6) with time zone,
    active boolean,
    mobile character varying,
    updated_by bigint,
    is_deleted boolean,
    deleted_by bigint,
    deleted_on timestamp with time zone,
    created_by bigint,
    reset_password boolean
);
    DROP TABLE public.users;
       public         heap    postgres    false            �            1259    16700    users_id_seq    SEQUENCE     u   CREATE SEQUENCE public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public.users_id_seq;
       public          postgres    false    232            �           0    0    users_id_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;
          public          postgres    false    233            �            1259    16702    vendors    TABLE     �  CREATE TABLE public.vendors (
    id bigint NOT NULL,
    name character varying NOT NULL,
    contact_person character varying NOT NULL,
    created_by bigint,
    created_date timestamp(6) with time zone NOT NULL,
    updated_date timestamp(6) with time zone,
    is_deleted boolean,
    deleted_on timestamp(6) with time zone,
    deleted_by bigint,
    discontinue boolean,
    discontinue_on timestamp(6) with time zone,
    discontinue_by bigint,
    updated_by bigint,
    address character varying,
    email character varying,
    mobile_number character varying,
    landline character varying,
    gstin character varying,
    designation character varying,
    website character varying,
    description character varying
);
    DROP TABLE public.vendors;
       public         heap    postgres    false            �            1259    16708    vendors_id_seq    SEQUENCE     w   CREATE SEQUENCE public.vendors_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.vendors_id_seq;
       public          postgres    false    234            �           0    0    vendors_id_seq    SEQUENCE OWNED BY     A   ALTER SEQUENCE public.vendors_id_seq OWNED BY public.vendors.id;
          public          postgres    false    235            �           2604    16710    amc id    DEFAULT     `   ALTER TABLE ONLY public.amc ALTER COLUMN id SET DEFAULT nextval('public.amc_id_seq'::regclass);
 5   ALTER TABLE public.amc ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    201    200            �           2604    16711    amc_services id    DEFAULT     r   ALTER TABLE ONLY public.amc_services ALTER COLUMN id SET DEFAULT nextval('public.amc_services_id_seq'::regclass);
 >   ALTER TABLE public.amc_services ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    203    202            �           2604    16712    asset id    DEFAULT     d   ALTER TABLE ONLY public.asset ALTER COLUMN id SET DEFAULT nextval('public.asset_id_seq'::regclass);
 7   ALTER TABLE public.asset ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    207    204            �           2604    16713    asset_group id    DEFAULT     p   ALTER TABLE ONLY public.asset_group ALTER COLUMN id SET DEFAULT nextval('public.asset_group_id_seq'::regclass);
 =   ALTER TABLE public.asset_group ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    206    205            �           2604    16770    attachment_association id    DEFAULT     �   ALTER TABLE ONLY public.attachment_association ALTER COLUMN id SET DEFAULT nextval('public.attachment_association_id_seq'::regclass);
 H   ALTER TABLE public.attachment_association ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    236    237    237            �           2604    16837    audit_trail id    DEFAULT     p   ALTER TABLE ONLY public.audit_trail ALTER COLUMN id SET DEFAULT nextval('public.audit_trail_id_seq'::regclass);
 =   ALTER TABLE public.audit_trail ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    239    238    239            �           2604    16714    category id    DEFAULT     j   ALTER TABLE ONLY public.category ALTER COLUMN id SET DEFAULT nextval('public.category_id_seq'::regclass);
 :   ALTER TABLE public.category ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    209    208            �           2604    16715 
   company id    DEFAULT     h   ALTER TABLE ONLY public.company ALTER COLUMN id SET DEFAULT nextval('public.company_id_seq'::regclass);
 9   ALTER TABLE public.company ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    211    210            �           2604    16716    location id    DEFAULT     j   ALTER TABLE ONLY public.location ALTER COLUMN id SET DEFAULT nextval('public.location_id_seq'::regclass);
 :   ALTER TABLE public.location ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    213    212            �           2604    16717    notification_alert_details id    DEFAULT     �   ALTER TABLE ONLY public.notification_alert_details ALTER COLUMN id SET DEFAULT nextval('public.notification_alert_details_id_seq'::regclass);
 L   ALTER TABLE public.notification_alert_details ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    215    214            �           2604    16718    notification_config id    DEFAULT     �   ALTER TABLE ONLY public.notification_config ALTER COLUMN id SET DEFAULT nextval('public.notification_config_id_seq'::regclass);
 E   ALTER TABLE public.notification_config ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    217    216            �           2604    16719    notification_meta id    DEFAULT     |   ALTER TABLE ONLY public.notification_meta ALTER COLUMN id SET DEFAULT nextval('public.notification_meta_id_seq'::regclass);
 C   ALTER TABLE public.notification_meta ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    219    218            �           2604    16720    notification_receiver id    DEFAULT     �   ALTER TABLE ONLY public.notification_receiver ALTER COLUMN id SET DEFAULT nextval('public.notification_receiver_id_seq'::regclass);
 G   ALTER TABLE public.notification_receiver ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    221    220            �           2604    16721 
   profile id    DEFAULT     h   ALTER TABLE ONLY public.profile ALTER COLUMN id SET DEFAULT nextval('public.profile_id_seq'::regclass);
 9   ALTER TABLE public.profile ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    223    222            �           2604    16722    roles id    DEFAULT     d   ALTER TABLE ONLY public.roles ALTER COLUMN id SET DEFAULT nextval('public.roles_id_seq'::regclass);
 7   ALTER TABLE public.roles ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    225    224            �           2604    16723 	   status id    DEFAULT     f   ALTER TABLE ONLY public.status ALTER COLUMN id SET DEFAULT nextval('public.status_id_seq'::regclass);
 8   ALTER TABLE public.status ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    227    226            �           2604    16724    type id    DEFAULT     b   ALTER TABLE ONLY public.type ALTER COLUMN id SET DEFAULT nextval('public.type_id_seq'::regclass);
 6   ALTER TABLE public.type ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    229    228            �           2604    16725    user_otp id    DEFAULT     j   ALTER TABLE ONLY public.user_otp ALTER COLUMN id SET DEFAULT nextval('public.user_otp_id_seq'::regclass);
 :   ALTER TABLE public.user_otp ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    231    230            �           2604    16726    users id    DEFAULT     d   ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);
 7   ALTER TABLE public.users ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    233    232            �           2604    16727 
   vendors id    DEFAULT     h   ALTER TABLE ONLY public.vendors ALTER COLUMN id SET DEFAULT nextval('public.vendors_id_seq'::regclass);
 9   ALTER TABLE public.vendors ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    235    234            d          0    16572    amc 
   TABLE DATA             COPY public.amc (id, created_by, created_date, updated_by, updated_date, is_deleted, deleted_date, deleted_by, ref_vendor, ref_asset, start_date, expiry_date, cost, amc_unique_code, s_number_count, previous_amc_id, renew_uuid, is_latest, ref_status) FROM stdin;
    public          postgres    false    200   (�       f          0    16580    amc_services 
   TABLE DATA           �   COPY public.amc_services (id, ref_amc, description, ref_status, created_by, created_date, updated_by, updated_date, is_deleted, deleted_by, deleted_date, year, month, day) FROM stdin;
    public          postgres    false    202   ߲       h          0    16588    asset 
   TABLE DATA           {  COPY public.asset (id, name, manufacturer, model_no, serial_no, uom, capacity, unique_code, ref_location, ref_type, ref_category, department, warranty_expiry_date, ref_status, purchase_date, purchase_price, description, sub_component, useful_life, s_no_count, updated_date, updated_by, deleted_date, deleted_by, created_by, created_date, is_deleted, ref_asset_group) FROM stdin;
    public          postgres    false    204   D�       i          0    16594    asset_group 
   TABLE DATA           �   COPY public.asset_group (id, name, description, created_date, created_by, updated_by, updated_date, is_deleted, deleted_by, deleted_date) FROM stdin;
    public          postgres    false    205   c�       �          0    16767    attachment_association 
   TABLE DATA           �   COPY public.attachment_association (id, ref_user, ref_amc, ref_asset, ref_vendor, file_name, path, created_date, created_by, updated_date, updated_by, is_deleted, deleted_date, deleted_by, file_download_uri, file_type, size) FROM stdin;
    public          postgres    false    237   ��       �          0    16834    audit_trail 
   TABLE DATA           z   COPY public.audit_trail (id, ref_amc, ref_assets, ref_vendor, ref_user, action_by, action_date, action, data) FROM stdin;
    public          postgres    false    239   �       l          0    16604    category 
   TABLE DATA           ,   COPY public.category (id, name) FROM stdin;
    public          postgres    false    208   ��       n          0    16612    company 
   TABLE DATA           F   COPY public.company (id, name, address, website, contact) FROM stdin;
    public          postgres    false    210   �       p          0    16620    location 
   TABLE DATA           3   COPY public.location (id, floor, area) FROM stdin;
    public          postgres    false    212   U�       r          0    16628    notification_alert_details 
   TABLE DATA           (  COPY public.notification_alert_details (id, mail_body, mail_subject, alert_times, alert_intervals, end_times, object_id, stop_on_action, cc_to, alert_left, created_by, created_date, alert_sent, notification_name, alert_status, alert_start_time, "alert_end_time ", attachment_details) FROM stdin;
    public          postgres    false    214   ��       t          0    16636    notification_config 
   TABLE DATA           �   COPY public.notification_config (id, alert_times, alert_intervals, before_days, end_time, stop_on_action, created_by, created_date, updated_by, updated_date, is_deleted, deleted_by, deleted_date, ref_notification_meta) FROM stdin;
    public          postgres    false    216   ��       v          0    16641    notification_meta 
   TABLE DATA           �   COPY public.notification_meta (id, notification_name, mail_body, mail_subject, created_date, updated_date, is_deleted, deleted_date) FROM stdin;
    public          postgres    false    218   ��       x          0    16649    notification_receiver 
   TABLE DATA           �   COPY public.notification_receiver (id, ref_notification_config, ref_user, created_by, created_date, updated_by, updated_date, is_deleted, deleted_by, deleted_date) FROM stdin;
    public          postgres    false    220   �       z          0    16654    profile 
   TABLE DATA           �   COPY public.profile (id, ref_user, first_name, last_name, ref_role, created_on, updated_on, ref_company, emp_id, designation, departement, updated_by, created_by, deleted_by, is_deleted) FROM stdin;
    public          postgres    false    222   +�       |          0    16662    roles 
   TABLE DATA           )   COPY public.roles (id, type) FROM stdin;
    public          postgres    false    224   r�       ~          0    16670    status 
   TABLE DATA           *   COPY public.status (id, name) FROM stdin;
    public          postgres    false    226   ��       �          0    16678    type 
   TABLE DATA           (   COPY public.type (id, name) FROM stdin;
    public          postgres    false    228   �       �          0    16686    user_otp 
   TABLE DATA           M   COPY public.user_otp (id, otp, created_on, valid_upto, ref_user) FROM stdin;
    public          postgres    false    230   E�       �          0    16694    users 
   TABLE DATA           �   COPY public.users (id, email, password, created_on, updated_on, active, mobile, updated_by, is_deleted, deleted_by, deleted_on, created_by, reset_password) FROM stdin;
    public          postgres    false    232   ��       �          0    16702    vendors 
   TABLE DATA             COPY public.vendors (id, name, contact_person, created_by, created_date, updated_date, is_deleted, deleted_on, deleted_by, discontinue, discontinue_on, discontinue_by, updated_by, address, email, mobile_number, landline, gstin, designation, website, description) FROM stdin;
    public          postgres    false    234   ,�       �           0    0 
   amc_id_seq    SEQUENCE SET     :   SELECT pg_catalog.setval('public.amc_id_seq', 171, true);
          public          postgres    false    201            �           0    0    amc_services_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.amc_services_id_seq', 161, true);
          public          postgres    false    203            �           0    0    asset_group_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.asset_group_id_seq', 1, false);
          public          postgres    false    206            �           0    0    asset_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('public.asset_id_seq', 91, true);
          public          postgres    false    207            �           0    0    attachment_association_id_seq    SEQUENCE SET     M   SELECT pg_catalog.setval('public.attachment_association_id_seq', 275, true);
          public          postgres    false    236            �           0    0    audit_trail_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.audit_trail_id_seq', 27, true);
          public          postgres    false    238            �           0    0    category_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.category_id_seq', 7, true);
          public          postgres    false    209            �           0    0    company_id_seq    SEQUENCE SET     <   SELECT pg_catalog.setval('public.company_id_seq', 2, true);
          public          postgres    false    211            �           0    0    location_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.location_id_seq', 4, true);
          public          postgres    false    213            �           0    0 !   notification_alert_details_id_seq    SEQUENCE SET     P   SELECT pg_catalog.setval('public.notification_alert_details_id_seq', 1, false);
          public          postgres    false    215            �           0    0    notification_config_id_seq    SEQUENCE SET     I   SELECT pg_catalog.setval('public.notification_config_id_seq', 21, true);
          public          postgres    false    217            �           0    0    notification_meta_id_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.notification_meta_id_seq', 1, true);
          public          postgres    false    219            �           0    0    notification_receiver_id_seq    SEQUENCE SET     L   SELECT pg_catalog.setval('public.notification_receiver_id_seq', 201, true);
          public          postgres    false    221            �           0    0    profile_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.profile_id_seq', 87, true);
          public          postgres    false    223            �           0    0    roles_id_seq    SEQUENCE SET     :   SELECT pg_catalog.setval('public.roles_id_seq', 3, true);
          public          postgres    false    225            �           0    0    status_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('public.status_id_seq', 6, true);
          public          postgres    false    227            �           0    0    type_id_seq    SEQUENCE SET     9   SELECT pg_catalog.setval('public.type_id_seq', 5, true);
          public          postgres    false    229            �           0    0    user_otp_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.user_otp_id_seq', 2, true);
          public          postgres    false    231            �           0    0    users_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('public.users_id_seq', 91, true);
          public          postgres    false    233            �           0    0    vendors_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.vendors_id_seq', 43, true);
          public          postgres    false    235            �           2606    16729    amc amc_pkey 
   CONSTRAINT     J   ALTER TABLE ONLY public.amc
    ADD CONSTRAINT amc_pkey PRIMARY KEY (id);
 6   ALTER TABLE ONLY public.amc DROP CONSTRAINT amc_pkey;
       public            postgres    false    200            �           2606    16731    amc_services amc_services_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.amc_services
    ADD CONSTRAINT amc_services_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY public.amc_services DROP CONSTRAINT amc_services_pkey;
       public            postgres    false    202            �           2606    16733    asset_group asset_group_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.asset_group
    ADD CONSTRAINT asset_group_pkey PRIMARY KEY (id);
 F   ALTER TABLE ONLY public.asset_group DROP CONSTRAINT asset_group_pkey;
       public            postgres    false    205            �           2606    16735    asset asset_pkey 
   CONSTRAINT     N   ALTER TABLE ONLY public.asset
    ADD CONSTRAINT asset_pkey PRIMARY KEY (id);
 :   ALTER TABLE ONLY public.asset DROP CONSTRAINT asset_pkey;
       public            postgres    false    204            �           2606    16775 2   attachment_association attachment_association_pkey 
   CONSTRAINT     p   ALTER TABLE ONLY public.attachment_association
    ADD CONSTRAINT attachment_association_pkey PRIMARY KEY (id);
 \   ALTER TABLE ONLY public.attachment_association DROP CONSTRAINT attachment_association_pkey;
       public            postgres    false    237            �           2606    16842    audit_trail audit_trail_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.audit_trail
    ADD CONSTRAINT audit_trail_pkey PRIMARY KEY (id);
 F   ALTER TABLE ONLY public.audit_trail DROP CONSTRAINT audit_trail_pkey;
       public            postgres    false    239            �           2606    16737    category category_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY public.category
    ADD CONSTRAINT category_pkey PRIMARY KEY (id);
 @   ALTER TABLE ONLY public.category DROP CONSTRAINT category_pkey;
       public            postgres    false    208            �           2606    16739    company company_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY public.company
    ADD CONSTRAINT company_pkey PRIMARY KEY (id);
 >   ALTER TABLE ONLY public.company DROP CONSTRAINT company_pkey;
       public            postgres    false    210            �           2606    16741    location location_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY public.location
    ADD CONSTRAINT location_pkey PRIMARY KEY (id);
 @   ALTER TABLE ONLY public.location DROP CONSTRAINT location_pkey;
       public            postgres    false    212            �           2606    16743 :   notification_alert_details notification_alert_details_pkey 
   CONSTRAINT     x   ALTER TABLE ONLY public.notification_alert_details
    ADD CONSTRAINT notification_alert_details_pkey PRIMARY KEY (id);
 d   ALTER TABLE ONLY public.notification_alert_details DROP CONSTRAINT notification_alert_details_pkey;
       public            postgres    false    214            �           2606    16745 ,   notification_config notification_config_pkey 
   CONSTRAINT     j   ALTER TABLE ONLY public.notification_config
    ADD CONSTRAINT notification_config_pkey PRIMARY KEY (id);
 V   ALTER TABLE ONLY public.notification_config DROP CONSTRAINT notification_config_pkey;
       public            postgres    false    216            �           2606    16747 (   notification_meta notification_meta_pkey 
   CONSTRAINT     f   ALTER TABLE ONLY public.notification_meta
    ADD CONSTRAINT notification_meta_pkey PRIMARY KEY (id);
 R   ALTER TABLE ONLY public.notification_meta DROP CONSTRAINT notification_meta_pkey;
       public            postgres    false    218            �           2606    16749 0   notification_receiver notification_receiver_pkey 
   CONSTRAINT     n   ALTER TABLE ONLY public.notification_receiver
    ADD CONSTRAINT notification_receiver_pkey PRIMARY KEY (id);
 Z   ALTER TABLE ONLY public.notification_receiver DROP CONSTRAINT notification_receiver_pkey;
       public            postgres    false    220            �           2606    16751    profile profile_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY public.profile
    ADD CONSTRAINT profile_pkey PRIMARY KEY (id);
 >   ALTER TABLE ONLY public.profile DROP CONSTRAINT profile_pkey;
       public            postgres    false    222            �           2606    16753    roles roles_pkey 
   CONSTRAINT     N   ALTER TABLE ONLY public.roles
    ADD CONSTRAINT roles_pkey PRIMARY KEY (id);
 :   ALTER TABLE ONLY public.roles DROP CONSTRAINT roles_pkey;
       public            postgres    false    224            �           2606    16755    status status_pkey 
   CONSTRAINT     P   ALTER TABLE ONLY public.status
    ADD CONSTRAINT status_pkey PRIMARY KEY (id);
 <   ALTER TABLE ONLY public.status DROP CONSTRAINT status_pkey;
       public            postgres    false    226            �           2606    16757    type type_pkey 
   CONSTRAINT     L   ALTER TABLE ONLY public.type
    ADD CONSTRAINT type_pkey PRIMARY KEY (id);
 8   ALTER TABLE ONLY public.type DROP CONSTRAINT type_pkey;
       public            postgres    false    228            �           2606    16759    user_otp user_otp_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY public.user_otp
    ADD CONSTRAINT user_otp_pkey PRIMARY KEY (id);
 @   ALTER TABLE ONLY public.user_otp DROP CONSTRAINT user_otp_pkey;
       public            postgres    false    230            �           2606    16761    users users_pkey 
   CONSTRAINT     N   ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);
 :   ALTER TABLE ONLY public.users DROP CONSTRAINT users_pkey;
       public            postgres    false    232            �           2606    16763    vendors vendors_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY public.vendors
    ADD CONSTRAINT vendors_pkey PRIMARY KEY (id);
 >   ALTER TABLE ONLY public.vendors DROP CONSTRAINT vendors_pkey;
       public            postgres    false    234            d   �   x����
�0Eg�+�]Y~�[��������J��"��r�*$,�\=�ĥ��4�hQ-^8��#bMr�\�rG�Q	���Q�L�eB݂�܉O":&�]�eG!��A�ϵ�:��Ԓ�$yxu'�ZB�%BSG^{����J�gxɿ	�L�/il�pspνG�Ks      f   U   x�mʻ�0�ڙ�a=��8C0A�`Ѥ��O���P8���*ƅ:�3�tc��!'b���v����%Wv��(�ddmqk��;|      h     x�e�]k� ��O~E�G�|h�^2zU��B鶌�f$�����1Qsޣ�� ������_���4��@H�I����K7 ���pbd:�>�g7���*��o�o�n�G����N;`��1bD���|H_ZR���(J0������q�jSH��:�.d�ꪀО���.���� I��1�-�BDM�z�{"Y�t�4��m�e�8�кV	�$�`,c�:����wLUn( ����!������JV���X��h��`�֛z4UU�&UuF      i      x������ � �      �   Z  x����n�@�5<����o���.�vժ�v�	&��@R��;&��x�D�d!�>|�;?)���	�h^�г�t���7����|-������Z��z��<��8��+�X�P 9��I?�/&�Z�r�~~~��y-YT>��ַO�?�n3_U��r�o���/w��<S�����{�n�y��x�)�БZ+�A5��T��w��	R�:/?V�+?+�
�RC�@t�4�K�
`�"��J{�g��#�F�$��E�S�G�6-/���޽tx��V"��q�Y��i$����)�:	>P�Z~N������nO� ��щ���m���f�G�X��޷A{�Y("XŶǠlB�88���1h.݇z.���QgUIUU�hl8���y}�Tb�����ֵ�]���5��F3Rn�|�����e��]�n�nM���6����v���`��p����[�D��M�R��Z�@z��W�����f�+�9ƽ�k�H��8=�F6���0��V�r�:�� Í�,V�":�=*�n���A��A�4���*Dv�;����S��	�qw<Y��}h��3�5��W�����qy�ȋ�i���!`��      �   �  x��\ms�6���
�f�ӥ^	���8q����⦝�K�K��T"u$�����%��,+q�PI$`���ł�����/#�x@�? �"#�=D<&�%���(zp�:��9�c��G�P/��7>[����x�����~�D!�'P�z:��$�����2������2����3�pi�s��jq��p�d�3��pm���<M���"I�
VGGO��)�ҷ����+S?	.B�Y/������h|�'Aj�_]]9Y��f��$�E��3(���Na<�?L����${����!뒛!�1�a�`ƱP�ܖ��Ch����!��Z&���'�oO�ş�i����AZ�#F�!̆'aP*��G�4Xd�]F+����pe�B���Qa�FP(&>7Pw��޲E�jQ�RU�(�9�2�7$�&>49�fz���� 1�'�UYѭB,�T�ҫeԮeLr&����T��:v�˵��2��?�Fq�M33�\��K .L"�v(u(��r��<w����{��Z�#=5��2���6�<��BS�]��.�t�x��Fxr�g�͜�"��p���L/��Ӣ�	T~ly��Zk���F����8	�E&���"C�TQJ����g:v��������f ~/�q�"ST1$�oC��	�B���ȷ�> ���MȯTQ �R.��w��O=�互���L����Ԑ�x@����r�RUV#JJ�!Ue6~��ty��4,t����Xř?�t��64
�mn>��1R��@7˲�Ud� Ӣ�z�0�1A�r�w���r�z�2�Z���=�GF���hNG�W�#�)(=�a:�7Y�|v�ϻp� ��[�A�Q|ݾ�  	�nx�:N>��30�L�t�0�~�i�׷P�6��a�z�J=���|��t�4$R��h�}���*�\�Ŀ��ީ?eS�q01�!h��3��?[�_3���^�#Pv�Q��i��-���e��;;Ll�9ٓj��Q�P&g����B�8�~�#�S�Թ.������ő�f�c��0FZT�f3��H,V#�����n`E6ǳ��n��;�L�*l���"���G����#mCb�F�:?�KP��0mے���ab�l���9n��
O�� }�v�wgӷ7ǒ+dg�73`+�o�q�0�b�&
Uz�L���$ؠ��d�*P�Ġ�h� 5�O�8Iwq��o�������00#k!��	�X
���9[%K�ֹ]��
�t���#��� 7Y�Q�6+paUG�C]����-Y��r7+Кi�)Ű�܁4��h'V ��ppZ�&�,t���yF���y��B��R�X�5m�]QG
��e1�>X.B�W[��U��#�a�mX�I):��=��Q0�}�
&�ToW+�E��a��3��ڿ}�g�	΁�p �}��OA�y�M�D�8d����۠�PT�t��1�[D���P���XIZu�]��y�9�9n)�wL�oۈ)]�ͫW��,��N_��f��4xUW�1�fY�~��b.H���\쓤��v%jvk� IP{ X�$��-̤k�	��b���DF��WAr���K��E��b�]#���-q��4^�%�ȹg���Ὅ�~�:@��\ܯt�Θ��X<�_�*��1�:�nT+�\4�G�$Y��S���&i9���ǥ3�o��	xԴg��H� /Ž��Q�*ńk���KY�J� ��BK?\�����R��j�"J�䥥�K�1����Rh��mO���e��q+)�Y/>:�с�|��|�܍��AG�:ś*��	Tt�|y:��Cf���J0ٕ\�MG�C�[��r(�J��T&�{.e�Z�uhp7rY�[�58�.�>0���e`(C�ʾ
]2E9zu�[r8������{��жª�,��ջ����m=A��c& ���l����&@�X�B�A�����늶��3���,�+��*6����:I�4�G�~��?��s�5pU���p�:*�s�����a�VMΰ�*�d��x�[�\�v���v�J�����P��t� �u�9^�H��j}��� �u\_E��;�x��M�pg.�J+2žː�sR�7�� m�G����ʕ�ٺ}8Ps{@�3)�s�4o�'+���j3�u�
�駧��K�v+4 ��ٷ�^�ھ899~����xu�1�$iA��|Z�>[��-�'pᖽ�h�\�@fāE��z�,�O�
j�ݪ��{�V���m�t�����t<f�N���u3���[Ӗ�Z�nm�q!�$�b���%3wV��\CA���`�Ķ͵����,��{�-YfɵS��hSv|#ʻ�I#�`��af �)�̲�#ź�������:�8X���p�Ǥ$[�	��ml��a��i}�'���۴\/~��&����'.W�Xr���wi�������#��'|���T���̝a��t�fQ[��6�1VyI�B{z�-r۪�)b��YHNJ=�j�����e�z;�AHy挶�&�%����G����L����iv�Gf��!��қ�������(�g�J{��F�^X�5��6�t�	�ᨽ�q;!�5db�E���=��+�����-�$��7��6��O��^��,��v_�:�zt
P�<N)����w�S�n��y��.┉�Q�ڀ	u�:ŻȚ4���zH8X
������ʾ�0mƨ"
JI�Z2(5���&�����]kr�2�!选�ܖ/�>��K3���q����O.����H�	�|��p�h8i4�MyC��_5o�Γĥ�wʛ��4��&pGބ}oﲷo{����>r ������{'W���b�܁�``;������`[��=�� JaWR�s���9Ձ���D��r&��2�%ca��;g,�ļx��VJ������y�����V��      l   Y   x�3��H,J)O,J�2��O+3�9}�32�R�*�L8�J��2KJ�⦜���)�y\f��9��%E�y��\���)�y��\1z\\\ ���      n   5   x�3��tss��t/-*M/J���".#���������bN��̔D�x� �W[      p   <   x�3�t/�/�KQH���/�t+��+�2�t�,*.A3�*�tJL��2��B
R��b���� �K      r      x������ � �      t      x������ � �      v   #   x�3�t�u�w	u�wqq��C�4 ����� ��f      x      x������ � �      z   7  x���QK�0ǟ�����r�5�%o�|pl2�����ʹ��u�7���b� ����O��UՖ��~_n<�(E�2�"�þBe	a}�o�E��;a�8�w��]�:�G��(���d�r�J5f������u�V�CU�M�x<%\ 3�j�W0w]����w9C�"[�u>���>$�Q��A�U�������9j)�F�����~'T#J��B#��a�EqԜ=0�8'�̐�"��7�o�Vq�`�`<������3��XQX*rb"s�'GsE����c�Q�lw��O�q�!����w�'I����M      |   '   x�3�tL����2��M�KLO-�2�.ILK����� �A      ~   K   x�3�tL.�,K�2���K�0�9�K�R�RRS�L��
��Ee�ɩ\��!E�y�i�EE@)3N����b 3F��� u��      �   1   x�3�J�+IM�2�tˬ �Ɯ�y��@�	�y�)�Si%����� $�      �   7   x�3�46404�4202�50�54S00�21�26�343�60��dM�d͹b���� 
��      �   �  x�e��n1Eך�Ⱦ��Q$��t�M6�)qk׀���wƏ֙B� "�yM��}�Y�O����O�x؇J���r������	4g�^@�Hf���9x�A�u��r͡�ܙ����gݗ���m�w+'RM�Ƅ��p�&�b��m���2h� �����Wp����\��a���-M��i�bC��l��r��Dɤ=(�#qF������0e��� �V��q����X�v�7Y� �`��
�����YV�P�к�]d�X������z����)EQ�(�hy�c��Q��V.�ae5W@.�~jm�!�L�8��3�̱gc��$��5������O�	;��T-�<��ko�6��6i���bm�J���l�I�E�K<n�
x�s��w]��͸�      �   �  x����n�0���)�W�l����t{��C�j�U/ֆm���(y��@�MӨH����eS�7ct�(��]+�+�Zy�%#ѕ��x~SX|�{Wm7�إCn^�Po埮L�d��мZ���Ʌ�'��>V�m��z��Q��}�cu�U���2��"�BNb��w�������t���kc����D��W ��E�պ����[�[ZvN�cK�}IH�dQ�Э��	ʒ�����Pc�6�!,��-ZFK*3Q�P�԰���H�]WS���g�^�4NC�nD��9W�C�c7Cؘ�?IA���*:B�s�*�;`��`�dё�8+��
�qd�MZ����Go��J�)�4�p�OD����ނ�BY�c��t�h�z�p��Ϛ���8*2�,��/�M_�;p�˞e�e�H�     