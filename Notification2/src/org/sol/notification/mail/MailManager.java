package org.sol.notification.mail;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.BodyPart;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;
import javax.mail.internet.MimeMultipart;

import org.apache.axiom.attachments.ByteArrayDataSource;
import org.apache.log4j.Logger;
import org.sol.notification.model.Constants;
import org.sol.notification.model.NotificationAlertDetails;
import org.sol.notification.model.NotificationMailSender;

import org.apache.commons.io.IOUtils;
import com.sollogics.alfresco.AlfrescoProjectManager;
import com.sollogics.alfresco.DmsDocument;
import com.sollogics.alfresco.ProjectSpace;
import com.sollogics.alfresco.common.DmsConstants;

public class MailManager {
	
	private static Logger logger = Logger.getLogger("notification");
	
	private NotificationMailSender[] mailSenders;
	private int maxQuota;
	private int maxMailHr;
	private int secPerMail;
	private String alfrescoPropertiesPath;
	
	public MailManager(NotificationMailSender[] mailSenders, int maxQuota, int maxMailHr, String alfrescoPropertiesPath ) {
		this.alfrescoPropertiesPath = alfrescoPropertiesPath;
		this.mailSenders = mailSenders;
		this.maxQuota = maxQuota;
		this.maxMailHr = maxMailHr;
		secPerMail = 3600/maxMailHr;
		if (secPerMail <= 0) secPerMail = 1;
	}

	public synchronized boolean sendMail(NotificationAlertDetails alertDetails, Set<String> emailIds, String fromAddress) {
		
		try {
			NotificationMailSender ms = selectBestSender();
			if (ms == null) {
				logger.warn("No mail sender found");
				return false;
			}
			
			int qSecs = ms.getQuotaSec();
			if (qSecs < 0) {
				int sleepSec = -qSecs;
				logger.info("Throttling mail. Sender sleeping for " + sleepSec + " seconds");
				Thread.sleep( sleepSec * 1000 );
				addQuotaSeconds(sleepSec);
			}
			
			boolean sent = sendViaSender(ms, alertDetails, emailIds, fromAddress);
			if (sent) {
				int qUsed = emailIds.size() * secPerMail;
				ms.setQuotaSec(ms.getQuotaSec() - qUsed);
				ms.setAccessTime(new Date());
			}
			
			return sent;
		}
		catch(Exception ex) {
			logger.error("Exception sending mail", ex);
		}
		
		return false;
	}

	private boolean sendViaSender(NotificationMailSender mailSender,
			NotificationAlertDetails alertDetails, Set<String> emailIds, String fromAddress) {
		try {
			List<DmsDocument> documents = null;
			String nodeIds = null;
			String attachmentDetails = alertDetails.getAttachmentDetails();
			if(attachmentDetails==null || attachmentDetails.isEmpty()){
				try {				
					logger.debug("MailSender::sendMailWithAttachment - Call simpleMail sendMail method");
					simpleSendMail(mailSender.isUseSSL(), alertDetails.getMailSubject(), alertDetails.getMailBody(), mailSender.getSender(), 
							mailSender.getHost(), mailSender.getSmtpPort(), mailSender.getSockPort(), 
							mailSender.getPasswd(), emailIds, documents, fromAddress);
					logger.info("MailSender::sendMailWithAttachment - Mail Sent successfully on " + new Timestamp(new Date().getTime()));
					logger.debug("MailSender::sendMailWithAttachment - Sent to Mail Recipient: " + emailIds.size());
					return true;
				}catch (Exception e) {
					logger.error("MailSender::sendMailWithAttachment - There was an exception. The alert parameters have not been set. Will try sending again later - " + e.getStackTrace());
					return false;
				}
			}
			
			// write new code for attachment
			
		/*	else {
				if(attachmentDetails.contains(Constants.ALFRESCO_DOCUMENT_CONTEXTID)){
					Map<String, String> detailsMap = getAttachmentDetails(attachmentDetails);
					String projectId = detailsMap.get(Constants.PROJECT_ID);
					//String row = detailsMap.get(Constants.ALFRESCO_DOCUMENT_ROW);
					//String context = detailsMap.get(Constants.ALFRESCO_DOCUMENT_CONTEXT);
					String contextId = detailsMap.get(Constants.ALFRESCO_DOCUMENT_CONTEXTID);
					
					documents = getDocList(contextId, projectId, detailsMap);
					try {				
						logger.debug("MailSender::sendMailWithAttachment - Call simpleMail sendMail method");
						simpleSendMail(mailSender.isUseSSL(), alertDetails.getMailSubject(), alertDetails.getMailBody(), mailSender.getSender(), 
								mailSender.getHost(), mailSender.getSmtpPort(), mailSender.getSockPort(), 
								mailSender.getPasswd(), emailIds, documents, fromAddress);
						logger.info("MailSender::sendMailWithAttachment - Mail Sent successfully on " + new Timestamp(new Date().getTime()));
						logger.debug("MailSender::sendMailWithAttachment - Sent to Mail Recipient: " + emailIds.size());
						return true;
					}catch (Exception e) {
						logger.error("MailSender::sendMailWithAttachment - There was an exception. The alert parameters have not been set. Will try sending again later - " + e.getStackTrace());
						return false;
					}
				}else{
					List<FileDetails> filedetailLst = new ArrayList<>();
					List<String> nodeIdList = new ArrayList<>();
					FileController filecontroller = new FileController();
					nodeIds = filecontroller.searchChildsByNodeId(attachmentDetails);
					if(nodeIds!=null){
						nodeIdList = new ArrayList<String>(Arrays.asList(nodeIds.split(",")));
						for (String stringNodeid : nodeIdList) {
							FileDetails fileDetail = filecontroller.getFileWithContentByNodeId(stringNodeid);
							if(fileDetail!=null){
								filedetailLst.add(fileDetail);
							}
						}
						try {				
							logger.debug("MailSender::sendMailWithAttachment - Call simpleMail sendMail method");
							simpleSendMail2(mailSender.isUseSSL(), alertDetails.getMailSubject(), alertDetails.getMailBody(), mailSender.getSender(), 
									mailSender.getHost(), mailSender.getSmtpPort(), mailSender.getSockPort(), 
									mailSender.getPasswd(), emailIds, filedetailLst, fromAddress);
							logger.info("MailSender::sendMailWithAttachment - Mail Sent successfully on " + new Timestamp(new Date().getTime()));
							logger.debug("MailSender::sendMailWithAttachment - Sent to Mail Recipient: " + emailIds.size());
							return true;
						}catch (Exception e) {
							logger.error("MailSender::sendMailWithAttachment - There was an exception. The alert parameters have not been set. Will try sending again later - " + e.getStackTrace());
							return false;
						}
					}
	
				}
			}*/
		}catch (Exception e) {
			logger.error("MailSender::sendMailWithAttachment - There was an exception. The alert parameters have not been set. Will try sending again later - " + e.getStackTrace());
			return false;
		}
		return false;
	}

	public synchronized void simpleSendMail(boolean useSSL, String subject, String body,
			final String sender, final String host, final int hostPort, final int sockPort, final String passwd,
			Set<String> recipients, List<DmsDocument> attachmentDocuments, String fromAddress) throws Exception {

		logger.debug("SimpleMail::sendMail - Enter");
		
		if(body==null){
			logger.error("SimpleMail::sendMail - Body is null. Can't send mail");
			return;
		}
		if(sender==null){
			logger.error("SimpleMail::sendMail - Sender is null. Can't send mail");
			return;
		}
		if(recipients==null || recipients.size()==0){
			logger.error("SimpleMail::sendMail - There are no mail recipients. Can't send email. Returning.");
			return;
		}
		
		if( subject==null ){
			logger.info("SimpleMail::sendMail - subject is null, setting to empty string");
			subject = "No Subject";
		}
		logger.info("SimpleMail::sendMail - Subect: " + subject + "; Sender: " + sender);
		logger.debug("SimpleMail::sendMail - Host = " + host + "; hostPort = " + hostPort + "; sockPort = " + sockPort);
		
		Properties props = new Properties();
		props.setProperty("mail.transport.protocol", "smtp");
		props.setProperty("mail.host", host);
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", ""+hostPort);
		props.setProperty("mail.smtp.quitwait", "false");

		logger.debug("SimpleMail::sendMail - Creating session");
		
		if(useSSL){
			props.put("mail.smtp.socketFactory.fallback", "false");
			props.put("mail.smtp.socketFactory.port", ""+sockPort);
			props.put("mail.smtp.socketFactory.class",
					"javax.net.ssl.SSLSocketFactory");
		}
		
		Session session = Session.getInstance(props,
					new javax.mail.Authenticator() {
						protected PasswordAuthentication getPasswordAuthentication() {
							return new PasswordAuthentication(sender, passwd);
						}
					});

		if( session==null){
			logger.error("SimpleMail::sendMail - Critical Error. Could not create session object");
			return;
		}
		logger.debug("SimpleMail::sendMail - Session created. Now creating MimeMessage");
		MimeMessage message = new MimeMessage(session);
		logger.debug("SimpleMail::sendMail - Created mime message");
		message.setSender(new InternetAddress(sender));
		logger.debug("SimpleMail::sendMail - set sender done");
		message.setSubject(subject);
		logger.debug("SimpleMail::sendMail - set subject done");
	
		Multipart mimeMultiPart = new MimeMultipart();
		logger.debug("SimpleMail::sendMail - new MimeMultipart created");
		
		// Create the message part 1
        BodyPart messageBodyPart = new MimeBodyPart();
		logger.debug("SimpleMail::sendMail - new Mimebodypart created");
        body = body.replace("</html>", Constants.DISCLAIMER);
		logger.debug("SimpleMail::sendMail - Body replace 1");
        body = body.replace("</HTML>", Constants.DISCLAIMER);
		logger.debug("SimpleMail::sendMail - Body replace 2");
        messageBodyPart.setText(body);
		logger.debug("SimpleMail::sendMail - Text sent");
        messageBodyPart.setContent(body, "text/html");
        
		logger.debug("SimpleMail::sendMail - Adding body part to mime..");
        mimeMultiPart.addBodyPart(messageBodyPart);

        if((attachmentDocuments != null) && (attachmentDocuments.size() > 0)) {
        	
    		logger.debug("SimpleMail::sendMail - There are attachments. Adding attachments");
    		
			AlfrescoProjectManager mgrInstance = AlfrescoProjectManager.getInstance(alfrescoPropertiesPath);
			if (mgrInstance==null) {
				logger.error("SimpleMail::sendMail - Unable to create alfresco manager instance");
				System.out.println("Unable to create session...");
				return;
			}
    		logger.debug("SimpleMail::sendMail - There are attachments. Adding attachments");
			
	        for(DmsDocument attachmentDocument : attachmentDocuments){
		        if(attachmentDocument==null){
		        	logger.debug("SimpleMail::sendMail - attachment document is null. Continue.");
		        	continue;
		        }
		        
				// Create the message part 2, which is attachment
		        messageBodyPart = new MimeBodyPart();
	
		        logger.debug("SimpleMail::sendMail - Getting Alfresco project space - " + attachmentDocument.getParentProjectSpace().getProjectId());
		        ProjectSpace projectSpace = attachmentDocument.getParentProjectSpace();

		        logger.debug("SimpleMail::sendMail - Getting document content");
		        if( projectSpace==null){
		        	logger.debug("SimpleMail::sendMail - project space is null. Continue.");
		        	continue;
		        }
				byte[] bytes = projectSpace.getContent(attachmentDocument);
	
				if(bytes==null){
		        	logger.debug("SimpleMail::sendMail - attachment document content is null. Continue.");
					continue;
				}
		        DataSource source = new ByteArrayDataSource(bytes, "application/octet-stream");
		        messageBodyPart.setDataHandler(new DataHandler(source));
		        logger.debug("SimpleMail::sendMail - add attachment title: " + attachmentDocument.getTitle());
		        messageBodyPart.setFileName(attachmentDocument.getTitle());
		        logger.debug("SimpleMail::sendMail -Adding body part.");
		        mimeMultiPart.addBodyPart(messageBodyPart);
	        }
        }
		logger.debug("SimpleMail::sendMail - Set mime content");
		message.setContent(mimeMultiPart);

		boolean hasRecipient = false;
		for(String recipient : recipients) {
			if((recipient == null) || recipient.isEmpty()) {
				
	        	logger.debug("SimpleMail::sendMail - Null recipient. Continue and look for others.");
				continue;
			}
	        logger.debug("SimpleMail::sendMail - Adding recipient: " + recipient);
			message.addRecipient(RecipientType.TO, new InternetAddress(recipient));
			hasRecipient = true;
			

		}

		try{
			if(hasRecipient) {
		        logger.debug("SimpleMail::sendMail - Call Tranport sendMail: " );
				if(!fromAddress.isEmpty()){
					message.setFrom(new InternetAddress(fromAddress));
				}
				Transport.send(message);	
		        logger.debug("SimpleMail::sendMail - Mail sent" );
				
			}else{
				logger.debug("SimpleMail::sendMail - There are no recipients");
			}
		}
		catch (Exception e) {
			logger.error("Exception while sending email: " + e.toString());
			throw e;
		}
    	logger.debug("SimpleMail::sendMail - Done sending. Returning...");
	}
	
	/*public synchronized void simpleSendMail2(boolean useSSL, String subject, String body,
			final String sender, final String host, final int hostPort, final int sockPort, final String passwd,
			Set<String> recipients, List<FileDetails> filedetaillst, String fromAddress) throws Exception {

		logger.debug("SimpleMail::sendMail - Enter");
		
		if(body==null){
			logger.error("SimpleMail::sendMail - Body is null. Can't send mail");
			return;
		}
		if(sender==null){
			logger.error("SimpleMail::sendMail - Sender is null. Can't send mail");
			return;
		}
		if(recipients==null || recipients.size()==0){
			logger.error("SimpleMail::sendMail - There are no mail recipients. Can't send email. Returning.");
			return;
		}
		
		if( subject==null ){
			logger.info("SimpleMail::sendMail - subject is null, setting to empty string");
			subject = "No Subject";
		}
		logger.info("SimpleMail::sendMail - Subect: " + subject + "; Sender: " + sender);
		logger.debug("SimpleMail::sendMail - Host = " + host + "; hostPort = " + hostPort + "; sockPort = " + sockPort);
		
		Properties props = new Properties();
		props.setProperty("mail.transport.protocol", "smtp");
		props.setProperty("mail.host", host);
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", ""+hostPort);
		props.setProperty("mail.smtp.quitwait", "false");

		logger.debug("SimpleMail::sendMail - Creating session");
		
		if(useSSL){
			props.put("mail.smtp.socketFactory.fallback", "false");
			props.put("mail.smtp.socketFactory.port", ""+sockPort);
			props.put("mail.smtp.socketFactory.class",
					"javax.net.ssl.SSLSocketFactory");
		}
		
		Session session = Session.getInstance(props,
					new javax.mail.Authenticator() {
						protected PasswordAuthentication getPasswordAuthentication() {
							return new PasswordAuthentication(sender, passwd);
						}
					});

		if( session==null){
			logger.error("SimpleMail::sendMail - Critical Error. Could not create session object");
			return;
		}
		logger.debug("SimpleMail::sendMail - Session created. Now creating MimeMessage");
		MimeMessage message = new MimeMessage(session);
		logger.debug("SimpleMail::sendMail - Created mime message");
		message.setSender(new InternetAddress(sender));
		logger.debug("SimpleMail::sendMail - set sender done");
		message.setSubject(subject);
		logger.debug("SimpleMail::sendMail - set subject done");
	
		Multipart mimeMultiPart = new MimeMultipart();
		logger.debug("SimpleMail::sendMail - new MimeMultipart created");
		
		// Create the message part 1
        BodyPart messageBodyPart = new MimeBodyPart();
		logger.debug("SimpleMail::sendMail - new Mimebodypart created");
        body = body.replace("</html>", Constants.DISCLAIMER);
		logger.debug("SimpleMail::sendMail - Body replace 1");
        body = body.replace("</HTML>", Constants.DISCLAIMER);
		logger.debug("SimpleMail::sendMail - Body replace 2");
        messageBodyPart.setText(body);
		logger.debug("SimpleMail::sendMail - Text sent");
        messageBodyPart.setContent(body, "text/html");
        
		logger.debug("SimpleMail::sendMail - Adding body part to mime..");
        mimeMultiPart.addBodyPart(messageBodyPart);

        if((filedetaillst != null) && (filedetaillst.size() > 0)) {
        	
    		logger.debug("SimpleMail::sendMail - There are attachments. Adding attachments");
    		
	        for(FileDetails fileDetails : filedetaillst){
		        if(fileDetails==null){
		        	logger.debug("SimpleMail::sendMail - attachment document is null. Continue.");
		        	continue;
		        }
		        
				// Create the message part 2, which is attachment
		        messageBodyPart = new MimeBodyPart();

		        logger.debug("SimpleMail::sendMail - Getting document content");
		        if( fileDetails.getInputStream()==null){
		        	logger.debug("SimpleMail::sendMail - project space is null. Continue.");
		        	continue;
		        }
				byte[] bytes = IOUtils.toByteArray(fileDetails.getInputStream());
	
				if(bytes==null){
		        	logger.debug("SimpleMail::sendMail - attachment document content is null. Continue.");
					continue;
				}
		        DataSource source = new ByteArrayDataSource(bytes, "application/octet-stream");
		        messageBodyPart.setDataHandler(new DataHandler(source));
		        logger.debug("SimpleMail::sendMail - add attachment title: " + fileDetails.getTitle());
		        messageBodyPart.setFileName(fileDetails.getTitle());
		        logger.debug("SimpleMail::sendMail -Adding body part.");
		        mimeMultiPart.addBodyPart(messageBodyPart);
	        }
        }
		logger.debug("SimpleMail::sendMail - Set mime content");
		message.setContent(mimeMultiPart);

		boolean hasRecipient = false;
		for(String recipient : recipients) {
			if((recipient == null) || recipient.isEmpty()) {
				
	        	logger.debug("SimpleMail::sendMail - Null recipient. Continue and look for others.");
				continue;
			}
	        logger.debug("SimpleMail::sendMail - Adding recipient: " + recipient);
			message.addRecipient(RecipientType.TO, new InternetAddress(recipient));
			hasRecipient = true;
			

		}

		try{
			if(hasRecipient) {
		        logger.debug("SimpleMail::sendMail - Call Tranport sendMail: " );
				if(!fromAddress.isEmpty()){
					message.setFrom(new InternetAddress(fromAddress));
				}
				Transport.send(message);	
		        logger.debug("SimpleMail::sendMail - Mail sent" );
				
			}else{
				logger.debug("SimpleMail::sendMail - There are no recipients");
			}
		}
		catch (Exception e) {
			logger.error("Exception while sending email: " + e.toString());
			throw e;
		}
    	logger.debug("SimpleMail::sendMail - Done sending. Returning...");
	}
	*/
	
	public void addQuotaSeconds(int seconds) {
	
		for(NotificationMailSender ms: mailSenders) {
			
			int qs = ms.getQuotaSec();
			if ((seconds < 0) || (qs < maxQuota)) {
				qs += seconds;
				if (qs > maxQuota) qs = maxQuota;
				ms.setQuotaSec(qs);
			}
		}
		
	}
	
	private NotificationMailSender selectBestSender() {
		if ((mailSenders == null) || (mailSenders.length == 0))
			return null;
		
		Arrays.sort(mailSenders, Collections.reverseOrder());
		return mailSenders[0];
	}
	
	private Map<String,String> getAttachmentDetails(String attachmentDetails) {
		
		//ProjectId=abc,RowId=-1,Context=quote,ContextId=
		String[] detailsNV = attachmentDetails.split(";");
		Map<String,String> details = new HashMap<String, String>();
		
		for(String s : detailsNV){
			if(s==null || s.isEmpty())
				continue;
			String[] temp = s.split("=");
			if (temp[0] == null || temp[0].isEmpty() || temp[1] == null
					|| temp[1].isEmpty())
				continue;
			details.put(temp[0], temp[1]);
		}
		return details;
	}
	
	public List<DmsDocument> getDocList(String contextId, String projectId, 
			Map<String,String> filterMap ) {
		try {
			logger.debug("MailSender::getDocList - Enter" );
			
			if(projectId==null || projectId.isEmpty())
				return new ArrayList<DmsDocument>();
			if(contextId==null)
				contextId = "";
			
			List<DmsDocument> finalDocList = getDocumentList(contextId, projectId, filterMap);
			return finalDocList;
		} catch (NumberFormatException e) {
			logger.error("MailSender::getDocList - Exception in get doc list. Number format exception " );

		}
		catch (Exception e) {
			logger.error("MailSender::getDocList - Exception in get doc list. " + e.getMessage() );

		}			
		return new ArrayList<DmsDocument>();
	}
	
	private List<DmsDocument> getDocumentList( String contextId, String project, Map<String, String> propertyMapFilter) {
		List<DmsDocument> list = new ArrayList<DmsDocument>();
		try {
			AlfrescoProjectManager mgrInstance = AlfrescoProjectManager.getInstance( alfrescoPropertiesPath );
			
			if (mgrInstance==null) {
				logger.error("MailSender::getDocumentList - Unable to create alfresco manager instance");
				System.out.println("Unable to create session...");
				return list;
			}
			
			logger.debug("MailSender::getDocumentList - Getting project: " + project );

			ProjectSpace projectSpace = mgrInstance.GetProjectByRelativePath(project);

			List<DmsDocument> docLst = projectSpace.getDocuments(contextId, null);
			for (DmsDocument dmsDocument : docLst) {
				
				//logic to red from model the custom types and filter 
				List<String> custPropNameList = DmsConstants.getPropertyList();
				boolean isFilteredDoc = true;
				for (String tempPropName : custPropNameList) {
					
					if(propertyMapFilter.containsKey(tempPropName)==false)
						continue;
					
					String propertyToFilter = propertyMapFilter.get(tempPropName);
					if(propertyToFilter==null || propertyToFilter.isEmpty())
						continue;
					
					String customDocProperty = dmsDocument.getCustomPropertyByName(tempPropName);
					if(customDocProperty==null || customDocProperty.isEmpty())
						continue;
					
					if(customDocProperty.equals(propertyToFilter))
						continue;
					else{
						isFilteredDoc = false;
						break;
					}
						
				}
				
				if(isFilteredDoc)
					list.add(dmsDocument);
				
			}
			return list;
		} catch (Exception e) {
			logger.error("MailSender::getDocumentList - Exception >>>>>>>: ", e);
			return list;
		}
	}
}
