package org.sol.notification.service;

import java.io.InputStream;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.log4j.Logger;

import org.sol.notification.model.*;

public class NotificationServlet extends HttpServlet {

	private static final long serialVersionUID = 2739291794887870452L;
	private static Logger logger = Logger.getLogger(Constants.NOTIFIER_LOGGER_NAME);
	
	private NotificationApp notifyApp;
	private Thread notifyThread;

	//private ProcedureCallApp procApp;
	//private Thread procThread;

	
	public NotificationServlet() {
        super();
	}
	
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
	
		try {
			PropertiesConfiguration props = new PropertiesConfiguration();
			InputStream pin = config.getServletContext().getResourceAsStream(Constants.PROPERTIES_RESOURCE_NAME);
			if (pin == null) throw new ServletException("notification.properties not found");
			props.load(pin);
			// create Notification application class and run in a thread
			notifyApp = new NotificationApp(new NotificationConfig(props));
			notifyThread = new Thread(notifyApp, Constants.NOTIFIER_THREAD_NAME);
			notifyThread.start();

			//procApp = new ProcedureCallApp(new NotificationConfig(props));
			//procThread = new Thread(procApp, Constants.NOTIFIER_THREAD_NAME);
			//procThread.start();

		}
		catch(Exception ex) {
			logger.fatal("Exception initializing Notification servlet", ex);
			throw new ServletException(ex);
		}
	}
	
	@Override
	public void destroy() {
		
		try {
			notifyApp.stop();
			notifyApp.stop();
			notifyThread.join(30000);
			//procThread.join(30000);
			super.destroy();
			
		}
		catch(Exception ex) {
			logger.error("Exception destroying Notification servlet", ex);
		}
	}

}
