package org.sol.notification.service;

import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.sol.notification.db.DBUtils;
import org.sol.notification.db.NotificationManager;
import org.sol.notification.mail.MailManager;
import org.sol.notification.model.Constants;
import org.sol.notification.model.DnmUser;
import org.sol.notification.model.NotificationAlertDetails;
import org.sol.notification.model.NotificationConfig;

public class NotificationApp implements Runnable {
	
	private static Logger logger = Logger.getLogger(Constants.NOTIFIER_LOGGER_NAME);
	private boolean running = false;
	private NotificationConfig config;
	
	public NotificationApp(NotificationConfig config) {
		this.config = config;
		running = true;
	}
	
	public void run() {
		
		try {
			
			boolean emailsCleaned = false;
			int poll = config.getPollSeconds();
			NotificationManager notifyManager = new NotificationManager(config.getDbSchema(), config.getDbConnection(),
																config.getDbUser(), config.getDbPassword());
			DBUtils.loadDBDriver(config.getDbDriverName());
			MailManager mailManager = new MailManager(config.getMailSenders(), config.getMaxQuotaSeconds(),
											config.getMaxMailPerHr(), config.getAlfrescoPropertiesPath());
			
			while(running) {
				try {
					boolean isOfficeTime = true;
					logger.info("Starting run loop now");
					Calendar cal = Calendar.getInstance();
					
					if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
						isOfficeTime = false;
						if (!emailsCleaned) {
							logger.info("Cleaning up notification table");
							if (notifyManager.doCleanup())
								emailsCleaned = true;
						}
					}
					else {
						// reset for next week;
						emailsCleaned = false;
					}
					
					if(cal.get(Calendar.HOUR_OF_DAY)>=config.getEndHour() || cal.get(Calendar.HOUR_OF_DAY)<config.getStartHour())		
						isOfficeTime = false;
					
					List<NotificationAlertDetails> currentAlerts = notifyManager.getNotificationList(isOfficeTime);
					
					for(NotificationAlertDetails notificationDetails : currentAlerts) {
						
						//In case one of the notification results in an exception. move to the next...
						try
						{
							//Integer phaseNotificationRecipientId = notificationDetails.getPhaseNotificationRecipientRefId();
							//This will contain all the email ids
							Set<String> emailIds = new HashSet<String>();					
							//we need only point and location to get the recipients...
							//notifyManager.prepareNotificationRecipientsList( notificationDetails, emailIds );					
							String ccToList = notificationDetails.getCcTo();
							/*
							 * String accCCList = null; if(notificationDetails.getAlertTimes() >1 &&
							 * notificationDetails.getAlertSent() >=1){
							 * notifyManager.getCcListFromNotificationAccPoint(notificationDetails);
							 * accCCList = notificationDetails.getEscalatedUserList(); if(accCCList != null
							 * && !accCCList.equals("")){ String[] ccToArr = accCCList.split(","); for(
							 * String s : ccToArr){ if( s!=null && !s.isEmpty()) emailIds.add(s); } } }
							 */

							if( ccToList!=null && !ccToList.isEmpty()){
								String[] ccToArr = ccToList.split(",");
								for( String s : ccToArr){
									if( s!=null && !s.isEmpty())
										emailIds.add(s);
								}
							}
							
//							DnmUser user  = notifyManager.getPhaseNotificationRecipient(phaseNotificationRecipientId );

							//String email = "";
//							if(user != null){
//								email = user.getEmailId();
//								if(email != null && !email.isEmpty())
//									emailIds.add(user.getEmailId());
//							}
							
							/*
							 * if((notificationDetails.getIsEscalted()!=null &&
							 * notificationDetails.getIsEscalted()==true) ||
							 * (notificationDetails.getAlertTimes() >1 && notificationDetails.getAlertSent()
							 * >=1)) { if(emailIds.contains("snarayan@rimhub.com")){
							 * emailIds.remove("snarayan@rimhub.com"); emailIds.add("nsharma@rimhub.com"); }
							 * }
							 */
						//if(notificationDetails.getAlertType().equals("email") ){
							logger.debug("Notificationthread::run - Calling mailSender send method. " );
							int iAlertsSent = notificationDetails.getAlertSent();
							String mailSubject = notificationDetails.getMailSubject();
							if( iAlertsSent>0 ){
								if( notificationDetails.getIsEscalted())
									mailSubject += "   " + "[Reminder no "+ iAlertsSent +" ESCALATED ]";
								else									
									mailSubject += "   " + "[Reminder no "+ iAlertsSent +"]";
								
							}
							
							/*
							 * if( notificationDetails.getIsEscalted()){ String escalationMessage =
							 * "Reminder numbmer " + iAlertsSent + "    ESCALATED "; //add html tags to mail
							 * body String escHTML =
							 * "<font style=\"color:red;text-align:center;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; "
							 * + escalationMessage + "</font><br>"; String newMailBody = escHTML +
							 * notificationDetails.getMailBody();
							 * notificationDetails.setMailBody(newMailBody); }
							 */
							notificationDetails.setMailSubject(mailSubject);
							
							if (mailManager.sendMail(notificationDetails, emailIds, config.getFromAddress()))
								notifyManager.updateNotificationAlertParameters(notificationDetails);
						}
					//	}
						catch(Exception e)
						{
							logger.error("Notificationthread::run - Exception while processing a notification point. " + e.toString());
							//No error handing needed immediately. Need to review if this happens
						}
					}
					
					logger.info("Going to sleep for " + poll + " seconds");
					Thread.sleep(poll * 1000);
					mailManager.addQuotaSeconds(poll);
				} catch (Exception e) {
					
				}
			}
		}
		catch(Exception ex) {
			logger.fatal("Exception in Notifier run loop", ex);
		}
	}
	
	public void stop() {
		running = false;
	}
	

}
