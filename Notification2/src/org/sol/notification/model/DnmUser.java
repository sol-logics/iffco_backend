package org.sol.notification.model;

public class DnmUser implements java.io.Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer id;
	private String emailId;
	private String mobileNumber; 
	private String fName;
	private String lName;
	private String mName;
	
	public String getfName() {
		return fName;
	}

	public void setfName(String fName) {
		this.fName = fName;
	}

	public String getlName() {
		return lName;
	}

	public void setlName(String lName) {
		this.lName = lName;
	}

	public String getmName() {
		return mName;
	}

	public void setmName(String mName) {
		this.mName = mName;
	}

	public DnmUser(Integer id){
		this.id = id;
	}
	
	public DnmUser(final Integer id, final String emailId, final String mobileNumber){
		this.id = id;
		this.emailId = emailId;
		this.mobileNumber = mobileNumber;
	}
	
	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * @return the emailId
	 */
	public String getEmailId() {
		return emailId;
	}
	/**
	 * @param emailId the emailId to set
	 */
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}


	/**
	 * @return the mobile
	 */
	public String getMobile() {
		return mobileNumber;
	}


	/**
	 * @param mobile the mobile to set
	 */
	public void setMobile(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
}
