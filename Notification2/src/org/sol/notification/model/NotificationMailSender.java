package org.sol.notification.model;

import java.util.Date;

public class NotificationMailSender implements Comparable<NotificationMailSender> {
	
	private String sender = "";
	private String passwd = "";
	private String host = "localhost";
	private int smtpPort = 465;
	private int sockPort = 465;
	private int quotaSec = 300;
	private boolean useSSL = true;
	
	private Date accessTime = new Date();

	public NotificationMailSender(int initialQuota){
		quotaSec = initialQuota;
	}
	
	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public String getPasswd() {
		return passwd;
	}

	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public int getSmtpPort() {
		return smtpPort;
	}

	public void setSmtpPort(int smtpPort) {
		this.smtpPort = smtpPort;
	}

	public int getSockPort() {
		return sockPort;
	}

	public void setSockPort(int sockPort) {
		this.sockPort = sockPort;
	}

	public boolean isUseSSL() {
		return useSSL;
	}

	public void setUseSSL(boolean useSSL) {
		this.useSSL = useSSL;
	}
	
	public int getQuotaSec() {
		return quotaSec;
	}

	public void setQuotaSec(int quotaSec) {
		this.quotaSec = quotaSec;
	}

	public Date getAccessTime() {
		return accessTime;
	}

	public void setAccessTime(Date accessTime) {
		this.accessTime = accessTime;
	}

	@Override
	public int compareTo(NotificationMailSender that) {
		int qd = this.quotaSec - that.quotaSec;
		if (qd != 0) return qd;
		return that.accessTime.compareTo(this.accessTime);
	}
	
	@Override
	public boolean equals(Object that) {
	    if (that == null) return false;
	    if (that == this) return true;
	    if (!(that instanceof NotificationMailSender)) return false;
	    NotificationMailSender thatThisClass = (NotificationMailSender)that;
	    if ((this.quotaSec == thatThisClass.quotaSec) && (this.accessTime == thatThisClass.accessTime))
	    	return true;
	    return false;
	}

}
