package org.sol.notification.model;

import java.util.HashMap;
import java.util.Iterator;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.log4j.Logger;
import org.sol.notification.model.NotificationMailSender;

public class NotificationConfig {
	
	private static Logger logger = Logger.getLogger(Constants.NOTIFIER_LOGGER_NAME);
	
	private String dbConnection = "";
	private String dbUser = "";
	private String dbPassword = "";
	private String dbSchema = "";
	private String dbDriverName = "";
	private NotificationMailSender[] mailSenders = new NotificationMailSender[1];
	private int startHour = 9;
	private int endHour = 18;
	private int pollSeconds = 60;
	private int maxQuotaSeconds = 900; // 15 minutes
	private int maxMailPerHr = 100;
	private String alfrescoPropertiesPath="";
	private String fromAddress;
	

	public NotificationConfig(Configuration config) {
		
		try {
			Configuration dbConfig = config.subset(Constants.PROP_DB_CONFIG);
			dbConnection = dbConfig.getString(Constants.PROP_DB_CONNECTION);
			dbUser = dbConfig.getString(Constants.PROP_DB_USER);
			dbPassword = dbConfig.getString(Constants.PROP_DB_PASSWORD);
			dbSchema = dbConfig.getString(Constants.PROP_DB_SCHEMA);
			dbDriverName = dbConfig.getString(Constants.PROP_DB_DRIVER_NAME);

			Configuration alfrescoConfig = config.subset(Constants.PROP_ALFRESCO);
			alfrescoPropertiesPath = alfrescoConfig.getString(Constants.PROP_ALFRESCO_PROPERTIES_PATH);
			
			Configuration mailConfig = config.subset(Constants.PROP_MAIL_CONFIG);
			
			startHour = mailConfig.getInt(Constants.PROP_MAIL_START_HOUR, 9);
			endHour = mailConfig.getInt(Constants.PROP_MAIL_END_HOUR, 18);
			maxQuotaSeconds = mailConfig.getInt(Constants.PROP_MAIL_MAX_QUOTA, 900);
			maxMailPerHr = mailConfig.getInt(Constants.PROP_MAIL_LIMIT_HOUR, 100);
			fromAddress = mailConfig.getString(Constants.PROP_MAIL_FROM_ADDRESS, "");

			
			Configuration mailSendersConfig = mailConfig.subset(Constants.PROP_MAIL_SENDER);
			Iterator<String> sKeys = mailSendersConfig.getKeys();
			HashMap<String, NotificationMailSender> msList = new HashMap<String, NotificationMailSender>();
			while(sKeys.hasNext()) {
				String k = sKeys.next();
				NotificationMailSender ms = new NotificationMailSender(maxQuotaSeconds);
				ms.setSender(mailSendersConfig.getString(k));
				msList.put(k, ms);
			}
			
			mailSendersConfig = mailConfig.subset(Constants.PROP_MAIL_PASSWD);
			sKeys = mailSendersConfig.getKeys();
			while(sKeys.hasNext()) {
				String k = sKeys.next();
				if (msList.containsKey(k)) {
					NotificationMailSender ms = msList.get(k);
					ms.setPasswd(mailSendersConfig.getString(k));
				}
			}
			
			mailSendersConfig = mailConfig.subset(Constants.PROP_MAIL_HOST);
			sKeys = mailSendersConfig.getKeys();
			while(sKeys.hasNext()) {
				String k = sKeys.next();
				if (msList.containsKey(k)) {
					NotificationMailSender ms = msList.get(k);
					ms.setHost(mailSendersConfig.getString(k));
				}
			}
			
			mailSendersConfig = mailConfig.subset(Constants.PROP_MAIL_SMTP_PORT);
			sKeys = mailSendersConfig.getKeys();
			while(sKeys.hasNext()) {
				String k = sKeys.next();
				if (msList.containsKey(k)) {
					NotificationMailSender ms = msList.get(k);
					ms.setSmtpPort(mailSendersConfig.getInt(k));
				}
			}
			
			mailSendersConfig = mailConfig.subset(Constants.PROP_MAIL_SOCK_PORT);
			sKeys = mailSendersConfig.getKeys();
			while(sKeys.hasNext()) {
				String k = sKeys.next();
				if (msList.containsKey(k)) {
					NotificationMailSender ms = msList.get(k);
					ms.setSockPort(mailSendersConfig.getInt(k));
				}
			}
			
			mailSendersConfig = mailConfig.subset(Constants.PROP_MAIL_USE_SSL);
			sKeys = mailSendersConfig.getKeys();
			while(sKeys.hasNext()) {
				String k = sKeys.next();
				if (msList.containsKey(k)) {
					NotificationMailSender ms = msList.get(k);
					ms.setUseSSL(mailSendersConfig.getBoolean(k));
				}
			}
			
			mailSenders = msList.values().toArray(mailSenders);
			
					
			pollSeconds = config.getInt(Constants.PROP_POLL_SECONDS, 60);
		}
		catch (Exception ex) {
			logger.error("Exception setting configuration", ex);
		}
		
		
	}

	public String getDbConnection() {
		return dbConnection;
	}

	public void setDbConnection(String dbConnection) {
		this.dbConnection = dbConnection;
	}

	public String getDbUser() {
		return dbUser;
	}

	public void setDbUser(String dbUser) {
		this.dbUser = dbUser;
	}

	public String getDbPassword() {
		return dbPassword;
	}

	public void setDbPassword(String dbPassword) {
		this.dbPassword = dbPassword;
	}

	public NotificationMailSender[] getMailSenders() {
		return mailSenders;
	}

	public void setMailSenders(NotificationMailSender[] mailSenders) {
		this.mailSenders = mailSenders;
	}

	public int getStartHour() {
		return startHour;
	}

	public void setStartHour(int startHour) {
		this.startHour = startHour;
	}

	public int getEndHour() {
		return endHour;
	}

	public void setEndHour(int endHour) {
		this.endHour = endHour;
	}

	public String getDbSchema() {
		return dbSchema;
	}

	public void setDbSchema(String dbSchema) {
		this.dbSchema = dbSchema;
	}

	public int getPollSeconds() {
		return pollSeconds;
	}

	public void setPollSeconds(int pollSeconds) {
		this.pollSeconds = pollSeconds;
	}

	public int getMaxQuotaSeconds() {
		return maxQuotaSeconds;
	}

	public void setMaxQuotaSeconds(int maxQuotaSeconds) {
		this.maxQuotaSeconds = maxQuotaSeconds;
	}

	public int getMaxMailPerHr() {
		return maxMailPerHr;
	}

	public void setMaxMailPerHr(int maxMailPerHr) {
		this.maxMailPerHr = maxMailPerHr;
	}

	public String getAlfrescoPropertiesPath() {
		return alfrescoPropertiesPath;
	}
	public String getDbDriverName() {
		return dbDriverName;
	}

	public String getFromAddress() {
		return fromAddress;
	}

}
