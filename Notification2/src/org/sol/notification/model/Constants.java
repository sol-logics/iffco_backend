package org.sol.notification.model;

public class Constants {

	public static final String PROPERTIES_RESOURCE_NAME = "/WEB-INF/notification.properties";
	public static final String PROP_DB_CONFIG = "notification.db";
	public static final String PROP_DB_CONNECTION = "connection";
	public static final String PROP_DB_USER = "user";
	public static final String PROP_DB_PASSWORD = "password";
	public static final String PROP_DB_SCHEMA = "schema";
	public static final String PROP_DB_DRIVER_NAME = "driver.name";

	public static final String PROP_ALFRESCO = "alfresco.properties";
	public static final String PROP_ALFRESCO_PROPERTIES_PATH = "path";

	
	public static final String PROP_MAIL_CONFIG = "notification.mail";
	public static final String PROP_MAIL_SENDER = "sender";
	public static final String PROP_MAIL_PASSWD = "password";
	public static final String PROP_MAIL_HOST = "host";
	public static final String PROP_MAIL_SMTP_PORT = "smtp.port";
	public static final String PROP_MAIL_SOCK_PORT = "socket.port";
	public static final String PROP_MAIL_USE_SSL = "useSSL";
	public static final String PROP_MAIL_START_HOUR = "start.hour";
	public static final String PROP_MAIL_END_HOUR = "end.hour";
	public static final String PROP_MAIL_MAX_QUOTA = "max.quota";
	public static final String PROP_MAIL_LIMIT_HOUR = "limit.hour";
	public static final String PROP_MAIL_FROM_ADDRESS = "from";
	
	public static final String PROP_POLL_SECONDS = "notification.poll.seconds";
	
	public static final String NOTIFIER_THREAD_NAME = "Notifier";
	public static final String NOTIFIER_LOGGER_NAME = "notification";
	
	public static final String NV_SEPERATOR = "=";
	public static final String NV_PAIR_SEPERATOR = ";";
	
	public static final String PROJECT_ID = "ProjectId";
	public static final String ALFRESCO_DOCUMENT_ROW = "Row";
	public static final String ALFRESCO_DOCUMENT_CONTEXT = "Context";
	public static final String ALFRESCO_DOCUMENT_CONTEXTID = "ContextId";
	
	public static final String DISCLAIMER = "<br><br><sub>Notice: <i>The information contained in this e-mail message and/or attachments to it may contain confidential or privileged information. If you are not the intended recipient, any dissemination, use, review, distribution, printing or copying of the information contained in this e-mail message and/or attachments to it are strictly prohibited. If you have received this communication in error, please notify us by reply e-mail or telephone and immediately and permanently delete the message and any attachments.</i> Thank you</sub></html>";
}
