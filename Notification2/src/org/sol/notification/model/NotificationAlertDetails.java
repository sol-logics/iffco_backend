package org.sol.notification.model;

import java.sql.Date;
import java.sql.Timestamp;

public class NotificationAlertDetails implements java.io.Serializable {
	private static final long serialVersionUID = 1L;

	private Integer id;
	private Integer dnmUserTrackRefId;
	private Integer phaseNotificationRecipientRefId;
	private Integer notificationPointsRefId;
	private String alertType;
	private Boolean alertStatus;
	private Date alertDate;
	private Timestamp alertStartTime;
	private Timestamp alertLastSentTime;
	private Timestamp alertEndTime;
	private Integer alertSent;
	private Integer alertLeft;
	private Integer alertTimes;
	private Integer alertSeverity;
	private Integer alertFrequency;
	private String attachmentDetails;
	private String mailSubject;
	private String mailBody;
	private String ccTo;
	private String branch;
	transient private Boolean isEscalted = false;
	transient private String escalatedUserList = null;

	public String getCcTo() {
		return ccTo;
	}

	public void setCcTo(String ccTo) {
		this.ccTo = ccTo;
	}

	public String getMailSubject() {
		return mailSubject;
	}

	public void setMailSubject(String mailSubject) {
		this.mailSubject = mailSubject;
	}

	public String getMailBody() {

		return mailBody;
	}

	public void setMailBody(String mailBody) {

		this.mailBody = mailBody;
	}

	public String getAttachmentDetails() {
		return attachmentDetails;
	}

	public void setAttachmentDetails(String attachmentDetails) {
		this.attachmentDetails = attachmentDetails;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the dnmUserTrackRefId
	 */
	public Integer getDnmUserTrackRefId() {
		return dnmUserTrackRefId;
	}

	/**
	 * @param dnmUserTrackRefId
	 *            the dnmUserTrackRefId to set
	 */
	public void setDnmUserTrackRefId(Integer dnmUserTrackRefId) {
		this.dnmUserTrackRefId = dnmUserTrackRefId;
	}

	/**
	 * @return the alertDate
	 */
	public Date getAlertDate() {
		return alertDate;
	}

	/**
	 * @param alertDate
	 *            the alertDate to set
	 */
	public void setAlertDate(Date alertDate) {
		this.alertDate = alertDate;
	}

	/**
	 * @return the alertStartTime
	 */
	public Timestamp getAlertStartTime() {
		return alertStartTime;
	}

	/**
	 * @param alertStartTime
	 *            the alertStartTime to set
	 */
	public void setAlertStartTime(Timestamp alertStartTime) {
		this.alertStartTime = alertStartTime;
	}

	/**
	 * @return the alertLastSentTime
	 */
	public Timestamp getAlertLastSentTime() {
		return alertLastSentTime;
	}

	/**
	 * @param alertLastSentTime
	 *            the alertLastSentTime to set
	 */
	public void setAlertLastSentTime(Timestamp alertLastSentTime) {
		this.alertLastSentTime = alertLastSentTime;
	}

	/**
	 * @return the alertEndTime
	 */
	public Timestamp getAlertEndTime() {
		return alertEndTime;
	}

	/**
	 * @param alertEndTime
	 *            the alertEndTime to set
	 */
	public void setAlertEndTime(Timestamp alertEndTime) {
		this.alertEndTime = alertEndTime;
	}

	/**
	 * @return the alertSent
	 */
	public Integer getAlertSent() {
		return alertSent;
	}

	/**
	 * @param alertSent
	 *            the alertSent to set
	 */
	public void setAlertSent(Integer alertSent) {
		this.alertSent = alertSent;
	}

	/**
	 * @return the alertLeft
	 */
	public Integer getAlertLeft() {
		return alertLeft;
	}

	/**
	 * @param alertLeft
	 *            the alertLeft to set
	 */
	public void setAlertLeft(Integer alertLeft) {
		this.alertLeft = alertLeft;
	}

	/**
	 * @return the alertType
	 */
	public String getAlertType() {
		return alertType;
	}

	/**
	 * @param alertType
	 *            the alertType to set
	 */
	public void setAlertType(String alertType) {
		this.alertType = alertType;
	}

	/**
	 * @return the alertStatus
	 */
	public Boolean getAlertStatus() {
		return alertStatus;
	}

	/**
	 * @param alertStatus
	 *            the alertStatus to set
	 */
	public void setAlertStatus(Boolean alertStatus) {
		this.alertStatus = alertStatus;
	}

	/**
	 * @return the alertTimes
	 */
	public Integer getAlertTimes() {
		return alertTimes;
	}

	/**
	 * @param alertTimes
	 *            the alertTimes to set
	 */
	public void setAlertTimes(Integer alertTimes) {
		this.alertTimes = alertTimes;
	}

	/**
	 * @return the alertSeverity
	 */
	public Integer getAlertSeverity() {
		return alertSeverity;
	}

	/**
	 * @param alertSeverity
	 *            the alertSeverity to set
	 */
	public void setAlertSeverity(Integer alertSeverity) {
		this.alertSeverity = alertSeverity;
	}

	/**
	 * @return the alertFrequency
	 */
	public Integer getAlertFrequency() {
		return alertFrequency;
	}

	/**
	 * @param alertFrequency
	 *            the alertFrequency to set
	 */
	public void setAlertFrequency(Integer alertFrequency) {
		this.alertFrequency = alertFrequency;
	}

	public Integer getNotificationPointsRefId() {
		return notificationPointsRefId;
	}

	public void setNotificationPointsRefId(Integer notificationPointsRefId) {
		this.notificationPointsRefId = notificationPointsRefId;
	}

	public Integer getPhaseNotificationRecipientRefId() {
		return phaseNotificationRecipientRefId;
	}

	public void setPhaseNotificationRecipientRefId(
			Integer phaseNotificationRecipientRefId) {
		this.phaseNotificationRecipientRefId = phaseNotificationRecipientRefId;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public Boolean getIsEscalted() {
		return isEscalted;
	}

	public void setIsEscalted(Boolean isEscalted) {
		this.isEscalted = isEscalted;
	}

	public String getEscalatedUserList() {
		return escalatedUserList;
	}

	public void setEscalatedUserList(String escalatedUserList) {
		this.escalatedUserList = escalatedUserList;
	}
}
