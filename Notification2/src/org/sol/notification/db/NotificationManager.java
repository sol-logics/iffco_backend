package org.sol.notification.db;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.sol.notification.model.*;

public class NotificationManager {
	
	private static Logger logger = Logger.getLogger("notification");
	List<NotificationAlertDetails> alertDetailsList = new ArrayList<NotificationAlertDetails>();
	private final String DETAILS_SELECT_QUERY = "SELECT * FROM `SCHEMA_NAME`.`notification_alert_details` WHERE `alert_status`=1 and `alert_left`>0 and `alert_start_time`<=Now() and `alert_end_time`>Now()";
	private final String CLEANUP_QUERY = "DELETE FROM `SCHEMA_NAME`.`notification_alert_details` WHERE (`alert_status`!=1 or `alert_left`<=0) and alert_last_sent_time < DATE_SUB(CURDATE(),INTERVAL 7 DAY)";
	private static Integer globalGroupLocationId = -1;
	
	private String schema;
	private String connection;
	private String user;
	private String passwd;
	
	public NotificationManager(String schema, String connection, String user, String passwd) {
		this.schema = schema;
		this.connection = connection;
		this.user = user;
		this.passwd = passwd;
	}
	
	
	private ResultSet executeQuery(String sql) {
		Connection c;
		try {
			c = DBUtils.getConnection(connection, user, passwd);
			Statement st = c.createStatement();
			ResultSet rs = st.executeQuery(sql);
			return rs;
		} catch (SQLException e) {
			logger.error("SQL Exception executing query: " + sql, e);
			return null;
		}
	}

	private Integer executeUpdate(String sql) {
		Connection c;
		try {
			c = DBUtils.getConnection(connection, user, passwd);
			Statement st = c.createStatement();
			Integer rowsUpdated = st.executeUpdate(sql);
			return rowsUpdated;
		} catch (SQLException e) {
			logger.error("SQL Exception update query: " + sql, e);
			return null;
		}
	}
	
	private void closeResultSetStatement(ResultSet rs){
		
		if( rs!=null){
			try{
				Statement st = rs.getStatement();
				if( st!=null )
					st.close();
			}catch(Exception ex){
				ex.printStackTrace();
			}
			
			try{
				rs.close();
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}

	}

	/***
	 * This method is used to get the current list of Notifications details that are ready to be sent...	
	 * @param schemaName
	 * @param followBusinessHours - if false then send the notification anyways. For now it is 
	 * true for Notification point 11 and 30
	 * @return
	 */
	public synchronized List<NotificationAlertDetails> getNotificationList(boolean followBusinessHours) {
		
		alertDetailsList.clear();
		String query = DETAILS_SELECT_QUERY;
		if(followBusinessHours==false ) {
			return new ArrayList<NotificationAlertDetails>();
		}
		//if(followBusinessHours==false )
			//query += " and (notification_points_ref_id=11 or notification_points_ref_id=30)";
		query = query.replace("SCHEMA_NAME", schema);
        logger.info("SQL Query: " + query);
		ResultSet rs = executeQuery(query);
		if(rs==null){
			return alertDetailsList;
		}

		try {
			while(rs.next()){
				Timestamp lastSentTimeStamp;
				Integer alertsSent = rs.getInt("alert_sent");
				int frequency = rs.getInt("alert_intervals");
				lastSentTimeStamp = rs.getTimestamp("alert_last_sent_time");
				if(alertsSent > 0) {			
					Date currentDateTime =  new Date();
					Timestamp now = new Timestamp(currentDateTime.getTime());
					
					//discard if the alert was sent within frequency minutes....
					if(lastSentTimeStamp.getTime() > now.getTime() - frequency*60*1000)
						continue;
				}
				
				NotificationAlertDetails notificationAlertDetails = new NotificationAlertDetails();
				notificationAlertDetails.setId(rs.getInt("id"));
				//notificationAlertDetails.setDnmUserTrackRefId(rs.getInt("dnm_user_track_ref_id"));
				//notificationAlertDetails.setPhaseNotificationRecipientRefId(rs.getInt("phase_notification_recipient_ref_id"));
				//notificationAlertDetails.setNotificationPointsRefId(rs.getInt("notification_points_ref_id"));
				//notificationAlertDetails.setAlertType(rs.getString("alert_type"));
				notificationAlertDetails.setAlertStatus(rs.getBoolean("alert_status"));
				notificationAlertDetails.setAlertDate(rs.getDate("alert_date"));
				notificationAlertDetails.setAlertStartTime(rs.getTimestamp("alert_start_time"));
				notificationAlertDetails.setAlertLastSentTime(lastSentTimeStamp);
				notificationAlertDetails.setAlertEndTime(rs.getTimestamp("alert_end_time"));
				notificationAlertDetails.setAlertSent(rs.getInt("alert_sent"));
				notificationAlertDetails.setAlertLeft(rs.getInt("alert_left"));
				notificationAlertDetails.setAlertTimes(rs.getInt("alert_times"));
				//notificationAlertDetails.setAlertSeverity(rs.getInt("alert_severity"));
				notificationAlertDetails.setAlertFrequency(frequency);
				notificationAlertDetails.setAttachmentDetails(rs.getString("attachment_details"));
				notificationAlertDetails.setMailSubject(rs.getString("mail_subject"));
				notificationAlertDetails.setMailBody(rs.getString("mail_body"));
				notificationAlertDetails.setCcTo(rs.getString("cc_to"));
				//notificationAlertDetails.setBranch(rs.getString("branch"));
	
				alertDetailsList.add(notificationAlertDetails);
			}
		} 
		catch (SQLException e1) {
			logger.error("SQL exception in getting one of the notification alert details record.", e1);
		}
		catch (Exception e2) {
			logger.error("Exception getting notification details", e2);
		}
		
		return alertDetailsList;
	}
	
	public void getCcListFromNotificationAccPoint(NotificationAlertDetails notificationDetails){
		Integer npId = notificationDetails.getNotificationPointsRefId();
		Integer sent = notificationDetails.getAlertSent();
		Integer userId = notificationDetails.getPhaseNotificationRecipientRefId();
		//String branch = notificationDetails.getBranch();
		try{
			
			String parentQuery = "select parent from `SCHEMA_NAME`.`hr_user` where (`isDeleted` is null or `isDeleted` !=1) and `id` ="+userId;
			
			ResultSet rs = executeQuery(parentQuery.replace("SCHEMA_NAME", schema));
			Integer parentId = -1;
			if(rs!=null){
				if( rs.first() ){
					parentId = rs.getInt("parent");
				}
			}
			
			
			String queryAcc = "select * from `SCHEMA_NAME`.`escalation_points` where (`isDeleted` is null or `isDeleted` !=1) and `refNotificationPoints` ="+npId;
			String ccListEsclation = null;

			queryAcc = queryAcc.replace("SCHEMA_NAME", schema);
			rs = executeQuery(queryAcc);
			if(rs==null){
				return;
			}
			try{
				while(rs.next()){
					Integer startPoint = rs.getInt("startPoint");
					if(startPoint == null || startPoint >= (sent + 1) )
						continue;
					notificationDetails.setIsEscalted(true);
					String roleIds = rs.getString("selectedRoles");
					String userIds = rs.getString("selectedUser");
					
					String userIDFromRoles = null;
					String finalIds = null;
					
					if(roleIds !=null && !roleIds.equals("")){
						String[] roleArray = roleIds.split(",");
						for(int i=0;i<roleArray.length;i++){
							ResultSet rsRole = executeQuery(("select branchWise from `SCHEMA_NAME`.hr_user_roles where id ="+roleArray[i]+"").replace("SCHEMA_NAME", schema));
							if(rsRole != null && rsRole.next()){
								Boolean bracnhWise = false;
								try{
									bracnhWise = rsRole.getBoolean("branchWise");
								}catch (Exception e) {
								}
								String userIDFromRolesQuery = null;
								//String userIdFromGroupQuery = null;
								ResultSet temp = null;
								if(bracnhWise == null || bracnhWise.equals(false)){
									userIDFromRolesQuery = "select id from `SCHEMA_NAME`.hr_user where id in (select association.refuser from `SCHEMA_NAME`.hr_user_roles_association as association where association.refuserrole in (select id from `SCHEMA_NAME`.hr_user_roles where (isDeleted is null or isDeleted !=1) and id ="+roleArray[i]+"))";
									//userIdFromGroupQuery = "select id from `SCHEMA_NAME`.dnm_user where id in(select association.userID from `SCHEMA_NAME`.user_group_association as association where association.groupID in (select groupID from `SCHEMA_NAME`.group_role_association where roleID = "+roleArray[i]+"))";								
								}else{
									
									//For Rimhub....No escalations to CEO.
									if(notificationDetails.getIsEscalted()) {
										String grandParentQuery = "select parent from `SCHEMA_NAME`.`hr_user` where `id` in "
										+ "(select parent from `SCHEMA_NAME`.`hr_user` where (`isDeleted` is null or `isDeleted` !=1) and `id` ="+userId + ")";
										ResultSet rsGP = executeQuery(grandParentQuery.replace("SCHEMA_NAME", schema));
										if(rsGP!=null){
											if( rsGP.first() ){
												parentId = rsGP.getInt("parent");
											}
											if(parentId.equals(0) || parentId.equals(72))
												parentId = 12;
											
										
										//end
										}
										if(parentId!=-1)
											userIDFromRolesQuery = "select id from `SCHEMA_NAME`.hr_user where id in (select association.refuser from `SCHEMA_NAME`.hr_user_roles_association as association where association.refuserrole in (select id from `SCHEMA_NAME`.hr_user_roles where (isDeleted is null or isDeleted !=1) and id ="+roleArray[i]+")) "
																	+" and id=" + parentId;
									}
								}
								if(userIDFromRolesQuery!=null){
									temp = executeQuery(userIDFromRolesQuery.replace("SCHEMA_NAME", schema));
									while(temp != null && temp.next()){
										if(temp.getString("id") != null){
											if(userIDFromRoles == null || userIDFromRoles.equals(""))
												userIDFromRoles = temp.getString("id");
											else
												userIDFromRoles += ","+temp.getString("id");
										}
									}
								}
/*								temp = executeQuery(userIdFromGroupQuery.replace("SCHEMA_NAME", schema));
								if(temp != null && temp.next()){
									if(temp.getString("id") != null){
										if(userIdFromGroup == null || userIdFromGroup.equals(""))
											userIdFromGroup = temp.getString("id");
										else
											userIdFromGroup += ","+temp.getString("id");
									}
								}*/
								closeResultSetStatement(temp);
							}
							closeResultSetStatement(rsRole);
						}
					}
				
					if(userIds!= null && !userIds.equals("")){
							finalIds = userIds;					
					}	
//					if(userIdFromGroup!= null && !userIdFromGroup.equals("")){
//						if(finalIds ==null)
//							finalIds = userIdFromGroup;	
//						else
//							finalIds +=","+userIdFromGroup;	
//					}	
					if(userIDFromRoles!= null && !userIDFromRoles.equals("")){
						if(finalIds ==null)
							finalIds = userIDFromRoles;	
						else
							finalIds +=","+userIDFromRoles;	
					}	
					if(finalIds ==null){
						continue;
					}				
										
					String userQuery = "select group_concat(email) from `SCHEMA_NAME`.hr_user where (isDeleted is null or isDeleted !=1) and id in ("+finalIds+")";
					ResultSet rs1 = executeQuery(userQuery.replace("SCHEMA_NAME", schema));
					if(rs1 != null && rs1.next()){
						String tempEmail = rs1.getString("group_concat(email)");
						if(tempEmail == null || tempEmail.equals(""))
							continue;
						
						if(ccListEsclation == null || ccListEsclation.equals(""))
							ccListEsclation = tempEmail;
						else
							ccListEsclation += ","+tempEmail;
						

					}
					closeResultSetStatement(rs1);
				}
				
			}catch (Exception e) {
				e.printStackTrace();
				// TODO: handle exception
			}
			closeResultSetStatement(rs);
			notificationDetails.setEscalatedUserList(ccListEsclation);
			return;
		}
		catch(Exception ex){
			logger.error("There was an exception in getting the escalation point list for notification point" + notificationDetails.getNotificationPointsRefId());
		}
	}
	
	public synchronized boolean doCleanup() {

		Connection c;
		try{
			String sql = CLEANUP_QUERY;
			sql = sql.replace("SCHEMA_NAME", schema);
	        logger.info("SQL Query: " + sql);
			c = DBUtils.getConnection(connection, user, passwd);
			Statement st = c.createStatement();
			st.execute(sql);
			logger.info("Notification details table cleaned");
			return true;
		}
		catch(Exception e){
			logger.error("Cleanup failed with Exception: ", e);
		}
		
		return false;
	}


	/***
	 * Get the notification list for a single alert detail.
	 * 
	 * @param schemaName
	 * @param notificationDetails
	 * @param emailIds
	 *            - Used as out parameter. This will be initialized with the
	 *            list of emails.
	 * @return
	 */
	public boolean prepareNotificationRecipientsList(NotificationAlertDetails notificationDetails, Set<String> emailIds) {

		try {
			if (emailIds == null || notificationDetails == null)
				return false;

			Integer notificationPointRefId = notificationDetails
					.getNotificationPointsRefId();

			String sqlNP = "SELECT * FROM `"
					+ schema
					+ "`.`hr_notification_default_recipients` WHERE `notification_points_ref_id`="
					+ notificationPointRefId;

			ResultSet rs = executeQuery(sqlNP);
			// If there is no notification point with the given notification
			// point ref id.
			if (rs == null) return true;

//			if (NotificationManager.globalGroupLocationId == -1) {
//				// get the global group id. Normally it is '1' but can be
//				// different. The location code is a constant "GBL"
//				String globalGroupIdSQL = "SELECT dbl.`id` FROM `"
//						+ schema
//						+ "`.`dnm_branch_locations` dbl WHERE dbl.`location_code`=\"GBL\"";
//
//				ResultSet rsGlobal = executeQuery(globalGroupIdSQL);
//				try {
//					while (rsGlobal != null && rsGlobal.next()) {
//						globalGroupLocationId = rsGlobal.getInt("id");
//					}
//				} catch (SQLException ex) {
//					logger.error("SQL exception prepare recipent list", ex);
//				}
//			}
			// For each entry in default recipient table which has the
			// notificationPointRefId, get all the emails.
			while (rs.next()) {
				Integer notificationMode = rs.getInt("notification_mode");
				// if the notification mode is not "1", then this entry has been
				// disabled temporarily or permanently.
				if (notificationMode == 1) {
//					// get the role
//					Integer roleId = rs.getInt("role_ref_id");
//					// get location
//					Integer locationId = rs.getInt("branch_location_ref_id");
//					// get email id for users with role from location
//					if (roleId > 0 && locationId > 0) {
//						// NOTE: always allow global GBL group...
//						getRoleEmailIds(roleId, globalGroupLocationId, locationId, emailIds);
//						// get email id for users with group roles from location
//						getGroupRoleUserEmailIds(roleId, globalGroupLocationId, locationId, emailIds);
//					}
//
//					// 2. get the user email from user ref id
					Integer userRefId = rs.getInt("user_ref_id");
					if (userRefId > 0)
						getUserRefEmailId(userRefId, emailIds);

					// 3. get the custom recipient
//					Integer customRefId = rs.getInt("custom_notification_recipient_ref_id");
//					if (customRefId > 0)
//						getCustomNotificationRecipientEmailId(customRefId, emailIds);
				}
			}

		} catch (SQLException e) {
			logger.error("There is an exception while getting the email ids for alert detail. Exception - ", e);
		}

		return true;
	}

	/***
	 * For a given role find the group and then find the users in the group at
	 * the given location (global location always included) get the emails...
	 * 
	 * @param schemaName
	 * @param roleId
	 * @param globalLocation
	 * @param locationId
	 * @param emailIds
	 */
	private void getGroupRoleUserEmailIds(Integer roleId, Integer globalLocation, Integer locationId,
			Set<String> emailIds) {

		String roleGroupAssociationSQL = "SELECT gra.`groupID` FROM `"
				+ schema
				+ "`.`group_role_association` gra WHERE gra.`roleID`=" + roleId;

		ResultSet rs = executeQuery(roleGroupAssociationSQL);
		Set<Integer> groupIds = new HashSet<Integer>();
		try {

			while (rs != null && rs.next()) {
				Integer groupId = rs.getInt("groupID");
				if (groupId != null && groupId > 0) {
					groupIds.add(groupId);
				}
			}

		} catch (SQLException e) {
			logger.error("SQL Exception role user email ids", e);
		}

		if (groupIds.size() == 0) return;

		// find all users in the group
		String userGroupAssociationSQL = "SELECT uga.`userID` FROM `"
				+ schema + "`.`user_group_association` uga WHERE ";
		int index = 0;
		for (Integer groupId : groupIds) {
			if (index == 0) {
				userGroupAssociationSQL += "uga.`groupID`=" + groupId;
			} else {
				userGroupAssociationSQL += " OR uga.`groupID`=" + groupId;
			}
			index++;
		}

		Set<Integer> userIds = new HashSet<Integer>();
		try {
			logger.debug("User Group SQL: " + userGroupAssociationSQL);
			ResultSet rsUGA = executeQuery(userGroupAssociationSQL);

			while (rsUGA != null && rsUGA.next()) {
				Integer userId = rsUGA.getInt("userID");
				if (userId != null && userId > 0) {
					userIds.add(userId);
				}
			}
		} catch (SQLException e) {
			logger.error("SQL Exception user group association", e);
		}

		// Get user ids for given location and global location...
		try {

			String dnmUserSQL = "SELECT DISTINCT du.`email` FROM `"
					+ schema + "`.`dnm_user` du WHERE (";

			index = 0;
			for (Integer userId : userIds) {
				if (index == 0)
					dnmUserSQL += " du.`id`=" + userId;
				else
					dnmUserSQL += " OR du.`id`=" + userId;

				index++;
			}
			dnmUserSQL += ") AND (du.`location_id`=" + locationId
					+ " OR du.`location_id`=" + globalLocation + ")";

			ResultSet rsEmailIds = executeQuery(dnmUserSQL);

			while (rsEmailIds != null && rsEmailIds.next()) {
				String emailId = rsEmailIds.getString("email");
				if (emailId != null && !emailId.isEmpty())
					emailIds.add(emailId);
			}

		} catch (SQLException e) {
			logger.error("SQL Exception local/global enail ids", e);
		}

	}

	/***
	 * For a given role and location (global location always included) get the
	 * emails.
	 * 
	 * @param schemaName
	 * @param roleId
	 * @param globalLocation
	 * @param locationId
	 * @param emailIds
	 * @return
	 */
	private boolean getRoleEmailIds(Integer roleId, Integer globalLocation, Integer locationId, Set<String> emailIds) {

		String roleUserAssociationSQL = "SELECT * FROM `" + schema
				+ "`.`user_role_association` WHERE `roleId`=" + roleId;

		ResultSet rs = executeQuery(roleUserAssociationSQL);

		try {
			Set<Integer> userIds = new HashSet<Integer>();
			while (rs != null && rs.next()) {
				Integer userId = rs.getInt("userID");
				userIds.add(userId);
			}
			if (userIds.size() == 0) return true;

			String dnmUserSQL = "SELECT DISTINCT du.`email` FROM `"
					+ schema + "`.`dnm_user` du WHERE (";

			int index = 0;
			for (Integer userId : userIds) {
				if (index == 0)
					dnmUserSQL += " du.`id`=" + userId;
				else
					dnmUserSQL += " OR du.`id`=" + userId;

				index++;
			}
			dnmUserSQL += ") AND (du.`location_id`=" + locationId
					+ " OR du.`location_id`=" + globalLocation + ")";

			ResultSet rsEmailIds = executeQuery(dnmUserSQL);

			while (rsEmailIds != null && rsEmailIds.next()) {
				String emailId = rsEmailIds.getString("email");
				if (emailId != null && !emailId.isEmpty())
					emailIds.add(emailId);
			}

		} catch (SQLException e) {
			logger.error("SQL Exception role email ids", e);
		}

		return true;
	}

	/***
	 * get the email is of a user
	 * 
	 * @param schemaName
	 * @param userRefId
	 * @param emailIds
	 */
	private void getUserRefEmailId(Integer userRefId, Set<String> emailIds) {

		String userSQL = "SELECT du.`email` FROM `" + schema
				+ "`.`hr_user` du WHERE du.`id`=" + userRefId;

		ResultSet rs = executeQuery(userSQL);

		try {

			while (rs != null && rs.next()) {
				String emailId = rs.getString("email");
				if (emailId != null && !emailId.isEmpty()) {
					emailIds.add(emailId);
					break;
				}
			}

		} catch (SQLException e) {
			logger.error("SQL Exception user ref email ids", e);
		}

	}

	/***
	 * Get the email ids of custom users....
	 * 
	 * @param schemaName
	 * @param customRefId
	 * @param emailIds
	 */
	 private void getCustomNotificationRecipientEmailId(Integer customRefId, Set<String> emailIds) {

		String customSQL = "SELECT ncrd.`email` FROM `"
				+ schema
				+ "`.`notification_customised_recipient_details` ncrd WHERE `id`="
				+ customRefId;

		ResultSet rs = executeQuery(customSQL);
		try {
			while (rs != null && rs.next()) {
				String emailId = rs.getString("email");
				if (emailId != null && !emailId.isEmpty()) {
					emailIds.add(emailId);
					break;
				}
			}
		} catch (SQLException e) {
			logger.error("Exception while getting the custom users. Exception - ", e);
		}
	}

	public DnmUser getPhaseNotificationRecipient(Integer userId) {
		try {
			String sql = "SELECT `id`, `email`, `mobile` FROM `" + schema
					+ "`.`dnm_user` WHERE id=" + userId;
			logger
					.info("NotificationManager::getPhaseNotificationRecipient - SQL Query: "
							+ sql);
			DnmUser user = null;
			ResultSet rs = executeQuery(sql);
			while (rs.next()) {
				user = new DnmUser(rs.getInt("id"), rs.getString("email"), rs.getString("mobile"));
				logger.info("NotificationManager::getPhaseNotificationRecipient - Phase Notification Recipient emailId :: "
								+ user.getEmailId());
				if (user != null && user.getEmailId() != null
						&& user.getEmailId().isEmpty() == false)
					break;
			}
			return user;
		} catch (Exception e) {
			logger.error("NotificationManager::getPhaseNotification - Exception: ", e);
			return null;
		}
	}

	public boolean updateNotificationAlertParameters(NotificationAlertDetails alertDetails) {
		try {
			String sql = null;
			Integer rowsUpdated = 0;

			if (alertDetails.getAlertLeft() < 0
					|| alertDetails.getAlertSent() > alertDetails
							.getAlertTimes()) {
				return true;
			}

			if (alertDetails.getAlertLeft().equals(0)
					|| alertDetails.getAlertSent().equals(
							alertDetails.getAlertTimes())) {
				sql = "UPDATE `" + schema
						+ "`.`notification_alert_details`"
						+ " SET `alert_status` = 0" + " WHERE `id` = "
						+ alertDetails.getId();

				logger.debug("NotificationManager::updateNotificationAlertParameters - SQL Query: "
								+ sql);
				rowsUpdated = executeUpdate(sql);
				logger.debug("NotificationManager::updateNotificationAlertParameters - Notification Alert Left Rows Updated when Alert Left = 0: "
								+ rowsUpdated);
				return true;
			}

			if (alertDetails.getAlertSent() < alertDetails.getAlertTimes()) {
				sql = "UPDATE `" + schema
						+ "`.`notification_alert_details`"
						+ " SET `alert_sent` = `alert_sent` + 1"
						+ " WHERE `id` = " + alertDetails.getId();

				logger.debug("NotificationManager::updateNotificationAlertParameters - SQL Query: " + sql);
				rowsUpdated = executeUpdate(sql);
				if (rowsUpdated == 1)
					alertDetails.setAlertSent(alertDetails.getAlertSent() + 1);
			}

			int alertLeft = alertDetails.getAlertLeft();
			if (alertLeft > 0) {

				if (alertLeft == 1) {

					sql = "UPDATE `" + schema
							+ "`.`notification_alert_details`"
							+ " SET `alert_left` = 0, `alert_status` = 0"
							+ " WHERE `id` = " + alertDetails.getId();

				} else {
					sql = "UPDATE `" + schema
							+ "`.`notification_alert_details`"
							+ " SET `alert_left` = `alert_left` - 1"
							+ " WHERE `id` = " + alertDetails.getId();
				}

				logger.info("SQL Query: " + sql);

				rowsUpdated = executeUpdate(sql);
				if (rowsUpdated == 1)
					alertDetails.setAlertLeft(alertDetails.getAlertLeft() - 1);
				logger.info("Notification Alert Left Rows Updated when Alert Left > 0: "
								+ rowsUpdated);
			}

			sql = "UPDATE `" + schema + "`.`notification_alert_details`"
					+ " SET `alert_last_sent_time` = CURRENT_TIMESTAMP()"
					+ " WHERE `id` = " + alertDetails.getId();


			logger.info("SQL Query: " + sql);
			rowsUpdated = executeUpdate(sql);
			if (rowsUpdated == 1) {
				java.util.Date today = new java.util.Date();
				Timestamp timeStamp = new Timestamp(today.getTime());
				alertDetails.setAlertLastSentTime(timeStamp);
			}
			logger.info("Notification Alert Last Sent Time Rows Updated: "
					+ rowsUpdated);

			return true;
		} catch (Exception e) {
			logger.error("Exception update alert param", e);
			return false;
		}
	}

}


