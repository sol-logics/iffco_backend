package org.sol.notification.db;

import java.sql.Connection;

import java.sql.DriverManager;
import java.sql.SQLException;

import org.apache.log4j.Logger;
import java.sql.CallableStatement;


public class DBUtils  {

    private static final Logger logger = Logger.getLogger("notification");
	static Connection connection = null;
	static Boolean driverLoaded = false;
	
	public static void loadDBDriver(String driverName){
		if( DBUtils.driverLoaded)
			return;
		try
        {
            Class.forName(driverName).newInstance();
            DBUtils.driverLoaded = true;
        }
        catch(Exception e)
        {
            logger.error("*** Error : "+e.toString());
            logger.error(e.getStackTrace());
        }
	}

	
    public static Connection getConnection(String connectionStr, String user, String passwd) throws SQLException
    {
    	if(connection==null || connection.isClosed() ) {
    		logger.info("Creating connection");
    		connection = null;
    		try{
	    		connection = DriverManager.getConnection(connectionStr, user, passwd);
	    		logger.info("SQL Connection created.");

	    		
    		}
    		catch(SQLException e) {
    			logger.error("SQLException: ", e);
    			return null;
    		}
    		catch (Exception e) {
    			logger.error("SQLException: ", e);
    			return null;
			}
    	}

    	return connection;
    }
    
}
