package com.sollogics.fams.controllers;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.sollogics.fams.services.dto.AttachmentAssociationDto;
import com.sollogics.fams.services.dto.VendorsDto;
import com.sollogics.fams.services.service.VendorsService;

@RestController
@CrossOrigin
public class VendorsController {
	
	@Autowired
	private VendorsService vendorsService;
	
	@RequestMapping(value = "/users/{userId}/vendor", method = RequestMethod.POST, consumes = {MediaType.MULTIPART_FORM_DATA_VALUE, MediaType.APPLICATION_JSON_VALUE }, produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<VendorsDto> saveVendor(@PathVariable BigDecimal userId,@RequestPart("vendorsDto") String vendorsDto,@RequestPart(required = false) MultipartFile[] files) {
			VendorsDto responseDTO = vendorsService.saveVendor(userId,vendorsDto,files);
			return new ResponseEntity<VendorsDto>(responseDTO, HttpStatus.OK);
		
	}
	@RequestMapping(value = "/users/{userId}/vendor" , method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<List<VendorsDto>> getAllVendors(@PathVariable BigDecimal userId){
		List<VendorsDto> VendorsLst = vendorsService.getAllVendors(userId);
		return new ResponseEntity<List<VendorsDto>>(VendorsLst, HttpStatus.OK);
		
	}
	
	@RequestMapping(value = "/users/{userId}/vendor/{vendorId}" , method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<VendorsDto> getvendorById(@PathVariable BigDecimal userId,@PathVariable BigDecimal vendorId){
		VendorsDto vendorDto = vendorsService.getvendorById(userId,vendorId);
		return new ResponseEntity<VendorsDto>(vendorDto, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/users/{userId}/vendor/{vendorId}" , method = RequestMethod.PUT,consumes = {MediaType.MULTIPART_FORM_DATA_VALUE, MediaType.APPLICATION_JSON_VALUE }, produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<VendorsDto> updateVendors(@PathVariable BigDecimal userId,@PathVariable BigDecimal vendorId,@RequestPart("vendorsDto") String vendorsDtoStr,@RequestPart(required = false) MultipartFile[] files){
		VendorsDto vendorInfo = vendorsService.updateVendors(userId,vendorId,vendorsDtoStr,files);
		return new ResponseEntity<VendorsDto>(vendorInfo, HttpStatus.OK);
		
	}
	
	@RequestMapping(value = "/users/{userId}/vendor/{vendorId}" , method = RequestMethod.DELETE, produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<Boolean> deleteVendorById(@PathVariable BigDecimal userId,@PathVariable BigDecimal vendorId){
		Boolean result = vendorsService.deleteVendorById(userId,vendorId);
		return new ResponseEntity<Boolean>(result, HttpStatus.OK);
		
	}
	@RequestMapping(value = "/users/{userId}/vendor/{vendorId}/file" , method = RequestMethod.DELETE, produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<Boolean> deleteVendorFileByFileName(@RequestBody List<AttachmentAssociationDto> attachmentDtoLst ,@PathVariable BigDecimal userId,@PathVariable BigDecimal vendorId){
		Boolean result = vendorsService.deleteVendorFileByFileName(attachmentDtoLst,userId,vendorId);
		return new ResponseEntity<Boolean>(result, HttpStatus.OK);
		
	}

}
