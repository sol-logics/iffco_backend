package com.sollogics.fams.controllers;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.sollogics.fams.services.dto.NotificationReceiverDto;
import com.sollogics.fams.services.service.NotificationReceiverService;

@RestController
@CrossOrigin
public class NotificationReceiverController {
	
	@Autowired
	private NotificationReceiverService notificationReceiverService; 
	
	@RequestMapping(value = "/users/{userId}/notification/receiver", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<NotificationReceiverDto> saveNotificationReceiver(@PathVariable BigDecimal userId,@PathVariable BigDecimal notificationId, @RequestBody NotificationReceiverDto notificationReceiverDto) {
		NotificationReceiverDto responseDTO = notificationReceiverService.saveNotificationReceiver(userId,notificationId,notificationReceiverDto);
			return new ResponseEntity<NotificationReceiverDto>(responseDTO, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/users/{userId}/notification/receiver", method = RequestMethod.GET,produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<List<NotificationReceiverDto>> getAllNotificationReceiver(@PathVariable BigDecimal userId) {
		List<NotificationReceiverDto> responseDTO = notificationReceiverService.getAllNotificationReceiver(userId);
			return new ResponseEntity<List<NotificationReceiverDto>>(responseDTO, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/users/{userId}/notification/{notificationId}" , method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<NotificationReceiverDto> getNotificationReceiverById(@PathVariable BigDecimal userId,@PathVariable BigDecimal notificationId){
		NotificationReceiverDto responseDto = notificationReceiverService.getNotificationReceiverById(userId,notificationId);
		return new ResponseEntity<NotificationReceiverDto>(responseDto, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/users/{usersId}/notification/{notificationId}" , method = RequestMethod.PUT,consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<NotificationReceiverDto> updateNotificationReceiver(@PathVariable BigDecimal usersId,@PathVariable BigDecimal notificationId,@RequestBody NotificationReceiverDto notificationReceiverDto){
		NotificationReceiverDto notificationReceiverInfo = notificationReceiverService.updateNotificationReceiver(usersId,notificationId,notificationReceiverDto);
		return new ResponseEntity<NotificationReceiverDto>(notificationReceiverInfo, HttpStatus.OK);
		
	}
	
	@RequestMapping(value = "/users/{userId}/notification/{notificationId}" , method = RequestMethod.DELETE, produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<Boolean> deleteNotificationReceiverById(@PathVariable BigDecimal userId,@PathVariable BigDecimal notificationId){
		Boolean result = notificationReceiverService.deleteNotificationReceiverById(userId,notificationId);
		return new ResponseEntity<Boolean>(result, HttpStatus.OK);
		
	}
	

}
