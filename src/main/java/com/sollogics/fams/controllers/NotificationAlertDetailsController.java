/*package com.sollogics.fams.controllers;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.sollogics.fams.services.dto.NotificationAlertDetailsDto;
import com.sollogics.fams.services.dto.NotificationConfigDto;
import com.sollogics.fams.services.service.NotificationAlertDetailsService;

@RestController
public class NotificationAlertDetailsController {
	
	@Autowired
	private NotificationAlertDetailsService notificationAlertDetailsService;
	
	@RequestMapping(value = "/users/{userId}/notificationAlert", method = RequestMethod.POST, consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<NotificationAlertDetailsDto> saveNotificationAlertDetails(@PathVariable BigDecimal userId,@RequestBody NotificationAlertDetailsDto notificationAlertDetailsDto) {
		NotificationAlertDetailsDto responseDTO = notificationAlertDetailsService.saveNotificationAlertDetails(userId,notificationAlertDetailsDto);
			return new ResponseEntity<NotificationAlertDetailsDto>(responseDTO, HttpStatus.OK);
		
	}
	
	@RequestMapping(value = "/users/{userId}/notificationConfig" , method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<List<NotificationConfigDto>> getAllNotificationConfig(@PathVariable BigDecimal userId){
		List<NotificationConfigDto> notificationConfigLst = notificationService.getAllNotificationConfig(userId);
		return new ResponseEntity<List<NotificationConfigDto>>(notificationConfigLst, HttpStatus.OK);
		
	}
	
	@RequestMapping(value = "/users/{userId}/notificationConfig/{notificationConfigId}" , method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<NotificationConfigDto> getNotificationConfigById(@PathVariable BigDecimal userId,@PathVariable BigDecimal notificationConfigId){
		NotificationConfigDto notificationConfigDto = notificationService.getNotificationConfigById(userId,notificationConfigId);
		return new ResponseEntity<NotificationConfigDto>(notificationConfigDto, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/users/{userId}/notificationConfig/{notificationConfigId}" , method = RequestMethod.PUT,consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<NotificationConfigDto> updateNotificationConfig(@PathVariable BigDecimal userId,@PathVariable BigDecimal notificationConfigId,@RequestBody NotificationConfigDto notificationConfigDto){
		NotificationConfigDto notificationConfigInfo = notificationService.updateNotificationConfig(userId,notificationConfigId,notificationConfigDto);
		return new ResponseEntity<NotificationConfigDto>(notificationConfigInfo, HttpStatus.OK);
		
	}
	
	@RequestMapping(value = "/users/{userId}/notificationConfig/{notificationConfigId}" , method = RequestMethod.DELETE, produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<Boolean> deleteNotificationConfigById(@PathVariable BigDecimal userId,@PathVariable BigDecimal notificationConfigId){
		Boolean result = notificationService.deleteNotificationConfigById(userId,notificationConfigId);
		return new ResponseEntity<Boolean>(result, HttpStatus.OK);
		
	}

}*/
