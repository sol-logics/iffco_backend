package com.sollogics.fams.controllers.Notification;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.sollogics.fams.services.dto.NotificationAlertDetailsDto;
import com.sollogics.fams.services.service.Notification.AmcNotificationService;

@RestController
@CrossOrigin
public class AmcNotificationController {
	@Autowired
	private AmcNotificationService amcNotiService;
	
	
	@RequestMapping(value = "/users/{userId}/notification/amc", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<List<NotificationAlertDetailsDto>> amcDueDateAlert(@PathVariable BigDecimal userId) {
		List<NotificationAlertDetailsDto> notiAlertDetailsLst = amcNotiService.amcDueDateAlert(userId);
			return new ResponseEntity<List<NotificationAlertDetailsDto>>(notiAlertDetailsLst, HttpStatus.OK);
	}
}
