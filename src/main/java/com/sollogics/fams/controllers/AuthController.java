package com.sollogics.fams.controllers;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sollogics.fams.exceptionHandler.GlobalException;
import com.sollogics.fams.services.dto.ChangePassword;
import com.sollogics.fams.services.dto.CredentialDto;
import com.sollogics.fams.services.dto.ProfileDto;
import com.sollogics.fams.services.service.AuthService;

@RestController
@CrossOrigin
public class AuthController {
	
	@Autowired
	private AuthService authService;
	
	@RequestMapping(value = "/authentication", method = RequestMethod.POST, consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<ProfileDto> authentication(@RequestBody CredentialDto credentialDto) {
		if(credentialDto != null && (credentialDto.getEmail() != null && !credentialDto.getEmail().isEmpty()) &&
				credentialDto.getPassword() != null && !credentialDto.getPassword().isEmpty()) {
			ProfileDto responseDTO = authService.authenticateUser(credentialDto);
			return new ResponseEntity<ProfileDto>(responseDTO, HttpStatus.OK);
		}else {
			throw new GlobalException(401, "User not authorised");
		}
	}
	
	@RequestMapping(value = "/users/{userId}/changePassword", method = RequestMethod.PUT, consumes = {MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<Boolean> changePassword(@PathVariable BigDecimal userId, @RequestBody ChangePassword changePassword) {
		
		Boolean flag = authService.changePassword(userId, changePassword);
		return new ResponseEntity<Boolean>(flag, HttpStatus.OK);
	}
	
	/*@RequestMapping(value = "/roles", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<List<RolesDto>> getRoles(){
		List<RolesDto> response = authService.getRoles();
		return new ResponseEntity<List<RolesDto>>(response, HttpStatus.OK);
		
		
	}*/
	@RequestMapping(value = "/forgotPassword", method = RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<Boolean> forgotPassword(@RequestParam String email) {
		Boolean flag = authService.forgotPassword(email);
		return new ResponseEntity<Boolean>(flag, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/changePasswordByOtp", method = RequestMethod.PUT, consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<Boolean> changePasswordByOtp(@RequestBody ChangePassword changePassword){
		Boolean flag = authService.changePasswordByOtp(changePassword);
		return new ResponseEntity<Boolean>(flag, HttpStatus.OK);
		
	}

}