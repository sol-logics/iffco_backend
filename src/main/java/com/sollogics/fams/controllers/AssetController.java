package com.sollogics.fams.controllers;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.sollogics.fams.services.dto.AssetDto;
import com.sollogics.fams.services.dto.AssetGroupDto;
import com.sollogics.fams.services.dto.AttachmentAssociationDto;
import com.sollogics.fams.services.dto.CategoryDto;
import com.sollogics.fams.services.dto.LocationDto;
import com.sollogics.fams.services.dto.StatusDto;
import com.sollogics.fams.services.dto.TypeDto;
import com.sollogics.fams.services.service.AssetService;

@RestController
@CrossOrigin
public class AssetController {
	
	@Autowired
	private AssetService assetService;
	
	@RequestMapping(value = "/users/{userId}/asset", method = RequestMethod.POST, consumes = {
			MediaType.MULTIPART_FORM_DATA_VALUE, MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<AssetDto> saveAsset(@PathVariable BigDecimal userId, @RequestPart("assetDto") String assetDto,@RequestPart(required = false) MultipartFile[] files) {
			AssetDto responseDTO = assetService.saveAsset(userId, assetDto,files);
			return new ResponseEntity<AssetDto>(responseDTO, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/users/{userId}/asset" , method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<List<AssetDto>> getAllAssets(@PathVariable BigDecimal userId){
		List<AssetDto> assetLst = assetService.getAllAssets(userId);
		return new ResponseEntity<List<AssetDto>>(assetLst, HttpStatus.OK);
		
	}
	
	@RequestMapping(value = "/users/{userId}/asset/{assetId}" , method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<AssetDto> getAssetById(@PathVariable BigDecimal userId,@PathVariable BigDecimal assetId){
		AssetDto assetDto = assetService.getAssetById(userId,assetId);
		return new ResponseEntity<AssetDto>(assetDto, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/users/{userId}/asset/{assetId}" , method = RequestMethod.PUT,consumes = {MediaType.MULTIPART_FORM_DATA_VALUE, MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<AssetDto> updateAsset(@PathVariable BigDecimal userId,@PathVariable BigDecimal assetId,@RequestPart("assetDto") String assetDto,@RequestPart(required = false) MultipartFile[] files){
		AssetDto vendorInfo = assetService.updateAsset(userId,assetId,assetDto,files);
		return new ResponseEntity<AssetDto>(vendorInfo, HttpStatus.OK);
		
	}
	
	@RequestMapping(value = "/users/{userId}/asset/{assetId}" , method = RequestMethod.DELETE, produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<Boolean> deleteAssetById(@PathVariable BigDecimal userId,@PathVariable BigDecimal assetId){
		Boolean result = assetService.deleteAssetById(userId,assetId);
		return new ResponseEntity<Boolean>(result, HttpStatus.OK);
		
	}
	
	@RequestMapping(value = "/users/{userId}/assetGroup", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<AssetGroupDto> saveAssetGroup(@PathVariable BigDecimal userId, @RequestBody AssetGroupDto assetGroupDto) {
			AssetGroupDto responseDTO = assetService.saveAssetGroup(userId, assetGroupDto);
			return new ResponseEntity<AssetGroupDto>(responseDTO, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/users/{userId}/assetGroup" , method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<List<AssetGroupDto>> getAllAssetGroup(@PathVariable BigDecimal userId){
		List<AssetGroupDto> assetGroupLst = assetService.getAllAssetGroup(userId);
		return new ResponseEntity<List<AssetGroupDto>>(assetGroupLst, HttpStatus.OK);
		
	}
	
	@RequestMapping(value = "/users/{userId}/assetGroup/{assetGroupId}" , method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<AssetGroupDto> getAssetGroupById(@PathVariable BigDecimal userId,@PathVariable BigDecimal assetGroupId){
		AssetGroupDto assetGroupDto = assetService.getAssetGroupById(userId,assetGroupId);
		return new ResponseEntity<AssetGroupDto>(assetGroupDto , HttpStatus.OK);
	}
	
	@RequestMapping(value = "/users/{userId}/assetGroup/{assetGroupId}" , method = RequestMethod.PUT,consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<AssetGroupDto> updateAssetGroup(@PathVariable BigDecimal userId,@PathVariable BigDecimal assetGroupId,@RequestBody AssetGroupDto assetGroupDto){
		AssetGroupDto asserGroupInfo = assetService.updateAssetGroup(userId,assetGroupId,assetGroupDto);
		return new ResponseEntity<AssetGroupDto>(asserGroupInfo, HttpStatus.OK);
		
	}
	
	@RequestMapping(value = "/users/{userId}/assetGroup/{assetGroupId}" , method = RequestMethod.DELETE, produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<Boolean> deleteAssetGroupById(@PathVariable BigDecimal userId,@PathVariable BigDecimal assetGroupId){
		Boolean result = assetService.deleteAssetGroupById(userId,assetGroupId);
		return new ResponseEntity<Boolean>(result, HttpStatus.OK);
		
	}
	@RequestMapping(value = "/users/{userId}/category" , method = RequestMethod.POST,consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<CategoryDto> saveCategory(@PathVariable BigDecimal userId,@RequestBody CategoryDto categoryDto){
		CategoryDto responseDTO = assetService.saveCategory(userId,categoryDto);
		return new ResponseEntity<CategoryDto>(responseDTO, HttpStatus.OK);
		
	}
	
	@RequestMapping(value = "/users/{userId}/category" , method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<List<CategoryDto>> getAllCategory(@PathVariable BigDecimal userId){
		List<CategoryDto> categoryDtoLst = assetService.getAllCategory(userId);
		return new ResponseEntity<List<CategoryDto>>(categoryDtoLst, HttpStatus.OK);
		
	}
	
	@RequestMapping(value = "/users/{userId}/status" , method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<List<StatusDto>> getAllStatus(@PathVariable BigDecimal userId){
		List<StatusDto> statusDtoLst = assetService.getAllStatus(userId);
		return new ResponseEntity<List<StatusDto>>(statusDtoLst, HttpStatus.OK);
		
	}
	
	@RequestMapping(value = "/users/{userId}/location" , method = RequestMethod.POST,consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<LocationDto> saveLocation(@PathVariable BigDecimal userId,@RequestBody LocationDto locationDto){
		LocationDto responseDTO = assetService.saveLocation(userId,locationDto);
		return new ResponseEntity<LocationDto>(responseDTO, HttpStatus.OK);
		
	}
	
	@RequestMapping(value = "/users/{userId}/location" , method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<List<LocationDto>> getAllLocation(@PathVariable BigDecimal userId){
		List<LocationDto> locationDtoLst = assetService.getAllLocation(userId);
		return new ResponseEntity<List<LocationDto>>(locationDtoLst, HttpStatus.OK);
		
	}
	
	@RequestMapping(value = "/users/{userId}/type" , method = RequestMethod.POST,consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<TypeDto> saveType(@PathVariable BigDecimal userId,@RequestBody TypeDto typeDto){
		TypeDto responseDTO = assetService.saveType(userId,typeDto);
		return new ResponseEntity<TypeDto>(responseDTO, HttpStatus.OK);
		
	}
	
	@RequestMapping(value = "/users/{userId}/type" , method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<List<TypeDto>> getAllType(@PathVariable BigDecimal userId){
		List<TypeDto> typeDtoLst = assetService.getAllType(userId);
		return new ResponseEntity<List<TypeDto>>(typeDtoLst, HttpStatus.OK);
		
	}
	
	@RequestMapping(value = "/users/{userId}/asset/{assetId}/file" , method = RequestMethod.DELETE, produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<Boolean> deleteAssetFileByFileName(@RequestBody List<AttachmentAssociationDto> attachmentDtoLst ,@PathVariable BigDecimal userId,@PathVariable BigDecimal amcId){
		Boolean result = assetService.deleteAssetFileByFileName(attachmentDtoLst,userId,amcId);
		return new ResponseEntity<Boolean>(result, HttpStatus.OK);
		
	}
	
	/*
	 * @RequestMapping(value = "/users/{userId}/type" , method = RequestMethod.GET,
	 * produces = {MediaType.APPLICATION_JSON_VALUE}) public
	 * ResponseEntity<List<TypeDto>> getAllType(@PathVariable BigDecimal userId){
	 * List<TypeDto> typeDtoLst = assetService.getAllType(userId); return new
	 * ResponseEntity<List<TypeDto>>(typeDtoLst, HttpStatus.OK);
	 * 
	 * }
	 */
}
