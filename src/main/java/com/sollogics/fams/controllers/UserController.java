package com.sollogics.fams.controllers;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.sollogics.fams.services.dto.AttachmentAssociationDto;
import com.sollogics.fams.services.dto.CompanyDto;
import com.sollogics.fams.services.dto.ProfileDto;
import com.sollogics.fams.services.dto.RolesDto;
import com.sollogics.fams.services.service.UserService;

@RestController
@CrossOrigin
public class UserController {
	
	@Autowired 
	private UserService userService;
	
	@RequestMapping(value = "/users/{usersId}/employee" , method = RequestMethod.POST, consumes = {MediaType.MULTIPART_FORM_DATA_VALUE, MediaType.APPLICATION_JSON_VALUE }, produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<ProfileDto> saveUserProfile(@PathVariable BigDecimal usersId,@RequestPart("profileDto") String profileDto,@RequestPart(required = false) MultipartFile[] files){
			ProfileDto userProfile = userService.saveUserProfile(usersId,profileDto,files);
			return new ResponseEntity<ProfileDto>(userProfile, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/users/{usersId}/employee" , method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<List<ProfileDto>> getAllUsers(@PathVariable BigDecimal usersId){
		List<ProfileDto> profileDtoLst = userService.getAllUsers(usersId);
		return new ResponseEntity<List<ProfileDto>>(profileDtoLst, HttpStatus.OK);
		
	}
	
	@RequestMapping(value = "/users/{userId}/employee/{employeeId}" , method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<ProfileDto> getUserById(@PathVariable BigDecimal userId,@PathVariable BigDecimal employeeId){
		ProfileDto profileDto = userService.getUserById(userId,employeeId);
		return new ResponseEntity<ProfileDto>(profileDto, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/users/{usersId}/employee/{employeeId}" , method = RequestMethod.PUT,consumes = {MediaType.MULTIPART_FORM_DATA_VALUE, MediaType.APPLICATION_JSON_VALUE }, produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<ProfileDto> updateUserProfile(@PathVariable BigDecimal usersId,@PathVariable BigDecimal employeeId,@RequestPart("profileDto") String profileDto,@RequestPart(required = false) MultipartFile[] files){
		ProfileDto profileInfo = userService.updateUserProfile(usersId,employeeId,profileDto,files);
		return new ResponseEntity<ProfileDto>(profileInfo, HttpStatus.OK);
		
	}
	
	@RequestMapping(value = "/users/{usersId}/employee/{employeId}" , method = RequestMethod.DELETE, produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<Boolean> deleteUserById(@PathVariable BigDecimal usersId,@PathVariable BigDecimal employeId){
		Boolean result = userService.deleteUser(usersId,employeId);
		return new ResponseEntity<Boolean>(result, HttpStatus.OK);
		
	}
	@RequestMapping(value = "/users/{userId}/employee/{employeId}/file" , method = RequestMethod.DELETE, produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<Boolean> deleteUserFileByFileName(@RequestBody List<AttachmentAssociationDto> attachmentDtoLst ,@PathVariable BigDecimal userId,@PathVariable BigDecimal employeId){
		Boolean result = userService.deleteUserFileByFileName(attachmentDtoLst,userId,employeId);
		return new ResponseEntity<Boolean>(result, HttpStatus.OK);
		
	}
	
	@RequestMapping(value = "/users/{userId}/company" , method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<List<CompanyDto>> getAllCompany(@PathVariable BigDecimal userId){
		List<CompanyDto> companyLst = userService.getAllCompany(userId);
		return new ResponseEntity<List<CompanyDto>>(companyLst, HttpStatus.OK);
		
	}
	
	@RequestMapping(value = "/users/{userId}/company/{companyId}" , method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<CompanyDto> getCompanyById(@PathVariable BigDecimal userId,@PathVariable BigDecimal companyId){
		CompanyDto companyDto = userService.getCompanyById(userId,companyId);
		return new ResponseEntity<CompanyDto>(companyDto, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/users/{userId}/roles" , method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<List<RolesDto>> getAllRoles(@PathVariable BigDecimal userId){
		List<RolesDto> rolesLst = userService.getAllRoles(userId);
		return new ResponseEntity<List<RolesDto>>(rolesLst, HttpStatus.OK);
		
	}
	
	@RequestMapping(value = "/users/{userId}/roles/{rolesId}" , method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<RolesDto> getRolesById(@PathVariable BigDecimal userId,@PathVariable BigDecimal rolesId){
		RolesDto rolesDto = userService.getRolesById(userId,rolesId);
		return new ResponseEntity<RolesDto>(rolesDto, HttpStatus.OK);
	}

}
