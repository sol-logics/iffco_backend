package com.sollogics.fams.controllers;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.sollogics.fams.services.dto.NotificationConfigDto;
import com.sollogics.fams.services.dto.NotificationMetaDto;
import com.sollogics.fams.services.service.NotificationConfigService;

@RestController
@CrossOrigin
public class NotificationConfigController {
	
	@Autowired
	private NotificationConfigService notificationService;
	
	@RequestMapping(value = "/users/{userId}/notificationConfig", method = RequestMethod.POST, consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<NotificationConfigDto> saveNotificationConfig(@PathVariable BigDecimal userId,@RequestBody NotificationConfigDto notificationConfigDto) {
			NotificationConfigDto responseDTO = notificationService.saveNotificationConfig(userId,notificationConfigDto);
			return new ResponseEntity<NotificationConfigDto>(responseDTO, HttpStatus.OK);
		
	}
	
	@RequestMapping(value = "/users/{userId}/notificationConfig" , method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<List<NotificationConfigDto>> getAllNotificationConfig(@PathVariable BigDecimal userId){
		List<NotificationConfigDto> notificationConfigLst = notificationService.getAllNotificationConfig(userId);
		return new ResponseEntity<List<NotificationConfigDto>>(notificationConfigLst, HttpStatus.OK);
		
	}
	
	@RequestMapping(value = "/users/{userId}/notificationConfig/{notificationConfigId}" , method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<NotificationConfigDto> getNotificationConfigById(@PathVariable BigDecimal userId,@PathVariable BigDecimal notificationConfigId){
		NotificationConfigDto notificationConfigDto = notificationService.getNotificationConfigById(userId,notificationConfigId);
		return new ResponseEntity<NotificationConfigDto>(notificationConfigDto, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/users/{userId}/notificationConfig/{notificationConfigId}" , method = RequestMethod.PUT,consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<NotificationConfigDto> updateNotificationConfig(@PathVariable BigDecimal userId,@PathVariable BigDecimal notificationConfigId,@RequestBody NotificationConfigDto notificationConfigDto){
		NotificationConfigDto notificationConfigInfo = notificationService.updateNotificationConfig(userId,notificationConfigId,notificationConfigDto);
		return new ResponseEntity<NotificationConfigDto>(notificationConfigInfo, HttpStatus.OK);
		
	}
	
	@RequestMapping(value = "/users/{userId}/notificationConfig/{notificationConfigId}" , method = RequestMethod.DELETE, produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<Boolean> deleteNotificationConfigById(@PathVariable BigDecimal userId,@PathVariable BigDecimal notificationConfigId){
		Boolean result = notificationService.deleteNotificationConfigById(userId,notificationConfigId);
		return new ResponseEntity<Boolean>(result, HttpStatus.OK);
		
	}
	
	@RequestMapping(value = "/users/{userId}/notificationMeta" , method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<List<NotificationMetaDto>> getAllNotificationMeta(@PathVariable BigDecimal userId){
		List<NotificationMetaDto> notificationMetaLst = notificationService.getAllNotificationMeta(userId);
		return new ResponseEntity<List<NotificationMetaDto>>(notificationMetaLst, HttpStatus.OK);
		
	}

}
