package com.sollogics.fams.controllers;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.sollogics.fams.services.dto.AmcDto;
import com.sollogics.fams.services.dto.AttachmentAssociationDto;
import com.sollogics.fams.services.service.AmcService;

@RestController
@CrossOrigin
public class AmcController {
	
	@Autowired
	private AmcService amcService;
	
	@RequestMapping(value = "/users/{userId}/amc", method = RequestMethod.POST, consumes = {
			MediaType.MULTIPART_FORM_DATA_VALUE, MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<AmcDto> saveAmc(@PathVariable BigDecimal userId, @RequestPart("amcDto") String amcDto,@RequestPart(required = false) MultipartFile[] files){
			AmcDto responseDTO = amcService.saveAmc(userId, amcDto,files);
			return new ResponseEntity<AmcDto>(responseDTO, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/users/{userId}/amc" , method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<List<AmcDto>> getAllAmc(@PathVariable BigDecimal userId){
		List<AmcDto> amcLst = amcService.getAllAmc(userId);
		return new ResponseEntity<List<AmcDto>>(amcLst, HttpStatus.OK);
		
	}
	
	@RequestMapping(value = "/users/{userId}/amc/{amcId}" , method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<AmcDto> getAmcById(@PathVariable BigDecimal userId,@PathVariable BigDecimal amcId){
		AmcDto amcDto = amcService.getAmcById(userId,amcId);
		return new ResponseEntity<AmcDto>(amcDto, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/users/{userId}/amc/{amcId}" , method = RequestMethod.PUT,consumes = {MediaType.MULTIPART_FORM_DATA_VALUE, MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<AmcDto> updateAmc(@PathVariable BigDecimal userId,@PathVariable BigDecimal amcId,@RequestPart("amcDto") String amcDto,@RequestPart(required = false) MultipartFile[] files){
		AmcDto amcInfo = amcService.updateAmc(userId,amcId,amcDto,files);
		return new ResponseEntity<AmcDto>(amcInfo, HttpStatus.OK);
		
	}
	
	@RequestMapping(value = "/users/{userId}/amc/{amcId}" , method = RequestMethod.DELETE, produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<Boolean> deleteAmcById(@PathVariable BigDecimal userId,@PathVariable BigDecimal amcId){
		Boolean result = amcService.deleteAmcById(userId,amcId);
		return new ResponseEntity<Boolean>(result, HttpStatus.OK);
		
	}
	
	@RequestMapping(value = "/users/{userId}/renewamc/{renewuuid}" , method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<List<AmcDto>> getAmcByRenewUuid(@PathVariable BigDecimal userId,@PathVariable String renewuuid){
		List<AmcDto> amcDtoList = amcService.getAmcByRenewUuid(userId,renewuuid);
		return new ResponseEntity<List<AmcDto>>(amcDtoList, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/users/{userId}/amc/{amcId}/file" , method = RequestMethod.DELETE, produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<Boolean> deleteAmcFileByFileName(@RequestBody List<AttachmentAssociationDto> attachmentDtoLst ,@PathVariable BigDecimal userId,@PathVariable BigDecimal amcId){
		Boolean result = amcService.deleteAmcFileByFileName(attachmentDtoLst,userId,amcId);
		return new ResponseEntity<Boolean>(result, HttpStatus.OK);
		
	}
	
	/*@RequestMapping(value = "/users/{userId}/amcService", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<AmcServicesDto> saveAmcService(@PathVariable BigDecimal userId, @RequestBody AmcServicesDto amcServicesDto) {
		AmcServicesDto responseDTO = amcService.saveAmcService(userId, amcServicesDto);
			return new ResponseEntity<AmcServicesDto>(responseDTO, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/users/{userId}/amcService" , method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<List<AmcServicesDto>> getAllAmcServices(@PathVariable BigDecimal userId){
		List<AmcServicesDto> amcServicesLst = amcService.getAllAmcServices(userId);
		return new ResponseEntity<List<AmcServicesDto>>(amcServicesLst, HttpStatus.OK);
		
	}
	
	@RequestMapping(value = "/users/{userId}/amcService/{amcServiceId}" , method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<AmcServicesDto> getAmcServiceById(@PathVariable BigDecimal userId,@PathVariable BigDecimal amcServiceId){
		AmcServicesDto amcServicesDto = amcService.getAmcServiceById(userId,amcServiceId);
		return new ResponseEntity<AmcServicesDto>(amcServicesDto, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/users/{userId}/amcService/{amcServiceId}" , method = RequestMethod.PUT,consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<AmcServicesDto> updateAmcService(@PathVariable BigDecimal userId,@PathVariable BigDecimal amcServiceId,@RequestBody AmcServicesDto amcServicesDto){
		AmcServicesDto amcServicesInfo = amcService.updateAmcService(userId,amcServiceId,amcServicesDto);
		return new ResponseEntity<AmcServicesDto>(amcServicesInfo, HttpStatus.OK);
		
	}
	
	@RequestMapping(value = "/users/{userId}/amcService/{amcServiceId}" , method = RequestMethod.DELETE, produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<Boolean> deleteAmcServiceById(@PathVariable BigDecimal userId,@PathVariable BigDecimal amcServiceId){
		Boolean result = amcService.deleteAmcServiceById(userId,amcServiceId);
		return new ResponseEntity<Boolean>(result, HttpStatus.OK);
		
	}*/
}
