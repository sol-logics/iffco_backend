package com.sollogics.fams.exceptionHandler;

public class JNDIInfrastructureConfigException extends RuntimeException{

	private static final long serialVersionUID = -3338166707608147652L;
	private int errorCode;

	public JNDIInfrastructureConfigException() {
		
	}
	
	public JNDIInfrastructureConfigException(final Throwable cause) {
		super(cause);
	}
	
	public JNDIInfrastructureConfigException(final String message) {
		super(message);
	}

	public JNDIInfrastructureConfigException(final String message, final Throwable cause) {
		super(message, cause);
	}
	
	public JNDIInfrastructureConfigException(final int errorCode,final String message) {
		super(message);
		this.setErrorCode(errorCode);
	}

	public int getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}
}
