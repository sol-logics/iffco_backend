package com.sollogics.fams.exceptionHandler.advice;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.sollogics.fams.exceptionHandler.GlobalException;
import com.sollogics.fams.exceptionHandler.InvalidCredentialException;
import com.sollogics.fams.exceptionHandler.SessionException;
import com.sollogics.fams.responseEntities.ResponseDTO;

/**
 * This class is responsible for handling all application exceptions including validation exceptions raised by Spring.
 * 
 */
@ControllerAdvice
public class GlobalControllerExceptionHandler extends ResponseEntityExceptionHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(GlobalControllerExceptionHandler.class);
    
    
    @ExceptionHandler(GlobalException.class)
    public ResponseEntity<ResponseDTO<?>> handleGlobalException(final GlobalException e,final WebRequest request) {
    	LOGGER.error("Global Exception: "+ e.getCause());
    	LOGGER.error("Global Exception: "+ e.getMessage());
        ResponseDTO<?> responseDTO = new ResponseDTO<>();
        responseDTO.setSuccess(false);
        responseDTO.setCode(e.getErrorCode());
        responseDTO.setMessage(e.getMessage());
        return new ResponseEntity<>(responseDTO, HttpStatus.CONFLICT);
    }
    
    @ExceptionHandler(InvalidCredentialException.class)
    public ResponseEntity<ResponseDTO<?>> handleInvalidCredentialException(final InvalidCredentialException e,final WebRequest request) {
    	LOGGER.error("Global Exception: "+ e.getCause());
    	LOGGER.error("Global Exception: "+ e.getMessage());
        ResponseDTO<?> responseDTO = new ResponseDTO<>();
        responseDTO.setSuccess(false);
        responseDTO.setCode(e.getErrorCode());
        responseDTO.setMessage(e.getMessage());
        return new ResponseEntity<>(responseDTO, HttpStatus.UNAUTHORIZED);
    }
    
    @ExceptionHandler(SessionException.class)
    public ResponseEntity<ResponseDTO<?>> handleSessionException(final SessionException e,final WebRequest request) {
    	LOGGER.error("Global Exception: "+ e.getCause());
    	LOGGER.error("Global Exception: "+ e.getMessage());
        ResponseDTO<?> responseDTO = new ResponseDTO<>();
        responseDTO.setSuccess(false);
        responseDTO.setCode(e.getErrorCode());
        responseDTO.setMessage(e.getMessage());
        return new ResponseEntity<>(responseDTO, HttpStatus.GATEWAY_TIMEOUT);
    }
    
    @ExceptionHandler(Exception.class)
    public ResponseEntity<ResponseDTO<?>> handleAnyException(final Exception e,final WebRequest request) {
    	LOGGER.error("Error occurred while processing request: {}", e.getCause(), e.getMessage());
    	ResponseDTO<?> responseDTO = new ResponseDTO<>();
    	responseDTO.setSuccess(false);
    	responseDTO.setMessage(e.getMessage());
    	return new ResponseEntity<>(responseDTO, HttpStatus.INTERNAL_SERVER_ERROR);
    } 
    
}
