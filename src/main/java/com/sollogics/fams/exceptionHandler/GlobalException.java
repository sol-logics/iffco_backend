package com.sollogics.fams.exceptionHandler;

public class GlobalException extends RuntimeException {

	private static final long serialVersionUID = -3338166707608147652L;
	private int errorCode;

	public GlobalException() {
		
	}
	
	public GlobalException(final Throwable cause) {
		super(cause);
	}
	
	public GlobalException(final String message) {
		super(message);
	}

	public GlobalException(final String message, final Throwable cause) {
		super(message, cause);
	}
	
	public GlobalException(final int errorCode,final String message) {
		super(message);
		this.setErrorCode(errorCode);
	}

	public int getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}
}
