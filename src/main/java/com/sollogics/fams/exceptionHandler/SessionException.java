package com.sollogics.fams.exceptionHandler;

public class SessionException extends RuntimeException{


	private static final long serialVersionUID = -3338166707608147652L;
	private int errorCode;
	
	public SessionException() {
		
	}
	
	public SessionException(final Throwable cause) {
		super(cause);
	}
	
	public SessionException(final String message) {
		super(message);
	}
	
	public SessionException(final int errorCode, final String message) {
		super(message);
		this.setErrorCode(errorCode);
	}

	public SessionException(final String message, final Throwable cause) {
		super(message, cause);
	}

	public int getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}


}
