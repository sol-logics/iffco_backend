package com.sollogics.fams.exceptionHandler;

public class HttpUtilsException extends RuntimeException {

	private static final long serialVersionUID = -3338166707608147652L;
	private int errorCode;

	public HttpUtilsException() {
		
	}
	
	public HttpUtilsException(final Throwable cause) {
		super(cause);
	}
	
	public HttpUtilsException(final String message) {
		super(message);
	}

	public HttpUtilsException(final String message, final Throwable cause) {
		super(message, cause);
	}
	
	public HttpUtilsException(final int errorCode,final String message) {
		super(message);
		this.setErrorCode(errorCode);
	}

	public int getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}
}
