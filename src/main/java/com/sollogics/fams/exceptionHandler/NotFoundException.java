package com.sollogics.fams.exceptionHandler;

public class NotFoundException extends RuntimeException{

	private static final long serialVersionUID = -3338166707608147652L;
	private int errorCode;
	
	public NotFoundException() {
		
	}
	
	public NotFoundException(final Throwable cause) {
		super(cause);
	}
	
	public NotFoundException(final String message) {
		super(message);
	}
	
	public NotFoundException(final int errorCode, final String message) {
		super(message);
		this.setErrorCode(errorCode);
	}

	public NotFoundException(final String message, final Throwable cause) {
		super(message, cause);
	}

	public int getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}
}
