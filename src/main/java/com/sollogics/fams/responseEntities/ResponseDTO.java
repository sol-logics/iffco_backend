package com.sollogics.fams.responseEntities;

import java.io.Serializable;

/**
 * A part of JSON Response object which will be sent back to the client.
 * It includes the data, a success flag, validation errors if any and an
 * optional message string.
 *
 * @author Atinder
 *
 * <pre>
 * Example:
 * {
 * data: {data},
 * success: true,
 * validationErrors: null,
 * message: 'OK'
 * }
 * </pre>
 *
 * @param <T> the generic type
 */
public class ResponseDTO<T> implements Serializable {

    private static final long serialVersionUID = 6731395971331376018L;

    /** Response data which will be sent back to the client */
    private T data;

    /** Indicates success of the request by client */
    private boolean success;

    /** An optional code which can be sent back to the client */
    private int code;

    /** An optional message string which can be sent back to the client */
    private String message;

    /**
     * Instantiates a new ResponseDTO.
     */
    public ResponseDTO() {

    }

    /**
     * Instantiates a new ResponseDTO.
     *
     * @param data the data
     */
    public ResponseDTO(T data) {
        this.data = data;
        this.success = true;
    }

    /**
     * Instantiates a new ResponseDTO.
     *
     * @param data    the data
     * @param success the success
     */
    public ResponseDTO(T data, boolean success) {
        this.data = data;
        this.success = success;
    }

    /**
     * Instantiates a new ResponseDTO.
     *
     * @param data             the data
     * @param success          the success
     * @param message          the message
     */
    public ResponseDTO(T data, boolean success,String message) {
        this.data = data;
        this.success = success;

        this.message = message;
    }

    /**
     * Gets response data.
     *
     * @return the data
     */
    public T getData() {
        return data;
    }

    /**
     * Sets response data.
     *
     * @param data the data
     */
    public void setData(T data) {
        this.data = data;
    }

    /**
     * Is success.
     *
     * @return the boolean
     */
    public boolean isSuccess() {
        return success;
    }

    /**
     * Sets success of the call.
     *
     * @param success the success
     */
    public void setSuccess(boolean success) {
        this.success = success;
    }

    /**
     * Gets message (optional).
     *
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * Sets message (optional).
     *
     * @param message the message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "ResponseDTO{" +
                "data=" + data +
                ", success=" + success +
                ", code=" + code +
                ", message='" + message + '\'' +
                '}';
    }

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}
}

