package com.sollogics.fams.utils;

public interface EmailSender {

	boolean sendMail(MailContent mailContent);

}