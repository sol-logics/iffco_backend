package com.sollogics.fams.utils;

import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.sollogics.fams.exceptionHandler.GlobalException;

@Component
public class PasswordOTP {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(PasswordOTP.class);	
	
	public static String createPasswordOTP(){
		LOGGER.info("Entering in the createPasswordOTP method ");
		int len = 5;
		String Capital_chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"; 
		String Small_chars = "abcdefghijklmnopqrstuvwxyz"; 
		String numbers = "0123456789";
		String values = Capital_chars + Small_chars + numbers;
		Random rand = new Random();
		try {
			//SecureRandom rand = SecureRandom.getInstanceStrong();
			 
			char[] password = new char[len]; 

			for (int i = 0; i < len; i++) 
			{ 
				password[i] = values.charAt(rand.nextInt(values.length())); 
			} 
			String generateOTP = new String(password);
	        LOGGER.info("Exit from the createPasswordOTP method ");
	        return generateOTP; 
		}catch(Exception e) {
			LOGGER.error("OTP is not valid");
			throw new GlobalException(500, e.getMessage());
		}       
	}


}
