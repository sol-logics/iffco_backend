package com.sollogics.fams.utils;

import java.util.Date;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class GenerateOTP {

	private static final Logger LOGGER = LoggerFactory.getLogger(GenerateOTP.class);

	public static String createOTP(){
		LOGGER.info("Entering in the createOTP method ");
		int len = 5;
		Date date = new Date();
		Random rand = new Random(date.getTime());
		String generateOTP = "";
		 
        for (int i = 0; i < len; i++)
        {
        	int nextRandom = rand.nextInt(len);
            // Use of nextInt() as it is scanning the value as int
        	generateOTP = generateOTP + String.valueOf(nextRandom);        	
        }
        LOGGER.info("Exit from the createOTP method ");
        return generateOTP;        
	}


	public static char[] generatePassword(int length) {
		String capitalCaseLetters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	      String lowerCaseLetters = "abcdefghijklmnopqrstuvwxyz";
	      String specialCharacters = "!@#$";
	      String numbers = "1234567890";
	      String combinedChars = capitalCaseLetters + lowerCaseLetters + specialCharacters + numbers;
	      Random random = new Random();
	      char[] password = new char[length];

	      password[0] = lowerCaseLetters.charAt(random.nextInt(lowerCaseLetters.length()));
	      password[1] = capitalCaseLetters.charAt(random.nextInt(capitalCaseLetters.length()));
	      password[2] = specialCharacters.charAt(random.nextInt(specialCharacters.length()));
	      password[3] = numbers.charAt(random.nextInt(numbers.length()));
	   
	      for(int i = 4; i< length ; i++) {
	         password[i] = combinedChars.charAt(random.nextInt(combinedChars.length()));
	      }
	      return password;
	}

}

