package com.sollogics.fams.utils;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sollogics.fams.repositories.dao.AmcDao;

@Service
public class CommonUtils {

	@Autowired
	private AmcDao amcDao;

	public Boolean stopAmcNotification(BigDecimal objectId,String notificationName) {
		Boolean result = amcDao.stopAmcNotification(objectId,notificationName);
		return result;

	}
}
