package com.sollogics.fams.utils;

import java.time.Clock;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Date;

public class DateConversionUtil {


	public static LocalDateTime CurrentUTCTime() {
		LocalDateTime date = LocalDateTime.now(Clock.systemUTC());
		return date;
	}
	
	public static LocalDateTime CurrentUTCTimeAdd10(LocalDateTime date, Long minutes) {
		LocalDateTime dateTime = date.plus(Duration.of(minutes, ChronoUnit.MINUTES));
		return dateTime;
	}
	
	public static String dateToStringUSFormat(LocalDateTime date){
		if(date == null)
			return null;
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM-dd-yyyy");
		String date2 = date.format(formatter);
		return date2;	
	}
	
	public static LocalDateTime utcToIndiaTimeZone(LocalDateTime date){
		if(date == null)
			return null;
		ZonedDateTime ldtZoned = date.atZone(ZoneId.of("UTC"));
		ZonedDateTime utcZoned = ldtZoned.withZoneSameInstant(ZoneId.of("Asia/Kolkata"));
		return utcZoned.toLocalDateTime();	
	}
	
	public static Date dateUtcToIndiaTimeZone(LocalDateTime date){
		if(date == null)
			return null;
		ZonedDateTime ldtZoned = date.atZone(ZoneId.of("UTC"));
		ZonedDateTime utcZoned = ldtZoned.withZoneSameInstant(ZoneId.of("Asia/Kolkata"));
		Date out = Date.from(utcZoned.toLocalDateTime().atZone(ZoneId.systemDefault()).toInstant());
		return out;	
	}
	
	public static LocalDateTime convertToLocalDateTimeViaMilisecond(Date dateToConvert) {
	    return Instant.ofEpochMilli(dateToConvert.getTime())
	      .atZone(ZoneId.systemDefault())
	      .toLocalDateTime();
	}
	
	public static LocalDateTime indiaToUtcTimeZone(LocalDateTime date){
		if(date == null)
			return null;
		ZonedDateTime ldtZoned = date.atZone(ZoneId.of("Asia/Kolkata"));
		ZonedDateTime utcZoned = ldtZoned.withZoneSameInstant(ZoneId.of("UTC"));
		return utcZoned.toLocalDateTime();	
	}
	
	public static LocalDateTime indiaTimeZone(LocalDateTime date){
		if(date == null)
			return null;
		ZonedDateTime ldtZoned = date.atZone(ZoneId.of("Asia/Kolkata"));
		return ldtZoned.toLocalDateTime();	
	}
	
	public static String dateToSQLCompareFormat(LocalDateTime date){
		if(date == null)
			return null;
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		String date2 = date.format(formatter);
		return date2;	
	}
	
	public static String dateToExcelStringFormat(LocalDateTime date){
		if(date == null)
			return null;
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM-dd-yyyy hh:mm:ss a");
		String date2 = date.format(formatter);
		return date2;	
	}
	
	public static Long CurrentUTCTimeStamp() {
		Long date = LocalDateTime.now().atZone(ZoneId.of("UTC")).toInstant().toEpochMilli();
		return date;
	}
}
