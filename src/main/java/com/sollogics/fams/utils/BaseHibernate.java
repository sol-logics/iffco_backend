package com.sollogics.fams.utils;

import javax.servlet.http.HttpSession;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

public class BaseHibernate {
	@Autowired
	private SessionFactory sessionFactory;
	@Autowired
	HttpSession session;
	
	
	public Session getSession(){
		return sessionFactory.getCurrentSession();
	}

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	public void rollBack(){
		getSession().getTransaction().rollback();
	}

}

