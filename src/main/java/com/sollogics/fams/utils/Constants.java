package com.sollogics.fams.utils;

public interface Constants {

	public static final String FROM_EMAIL = "noreply@sollogics.com";
	public static final String REGISTRATION_EMAIL_SUBJECT = "Welcome to FAMS";
	public static final String FOR_REGISTRATION_USER_SUBJECT = "Approval for user registration";
	
	static final String PIPE_SEPERATOR = "|";
	
    interface URLS{
    	//static final String APP_PATH = "https://newhireroadmap.pwc.co.in/onboarding";
    	static final String FORGOT_PASSWORD_PATH = "https://newhireroadmap.pwc.co.in/#/forgot-password?";
    	//static final String FORGOT_PASSWORD_PATH = "http://d5834557.ngrok.io/onboardingPortal/#/forgot-password?";
    	//static final String PATH = "https://newhireroadmap.pwc.co.in/";
    	static final String PATH = "http://f568b85107e9.ngrok.io";
    	
    	//static final String newsFeedURL = "http://press.pwc.com/rss/GLOBAL";
    	static final String newsFeedURL = "http://feeds.feedburner.com/PwCAUMediaCentre";
    }
	
    interface SNumberCode{
		static final String ASSET_PATTERN = "EQP_"; 
	}
    
    interface RenewUuid{
    	static final String RENEW_ID_PATTERN = "R_";
    }
    
    interface SerialNumberCode{
		static final String AMC_PATTERN = "AMC_"; 
	}
	
	interface SystemRoles{
		static final String ADMIN = "Admin"; 
		static final String MANAGER = "Manager";
	}
	
	interface NotificationName{
		static final String AMC = "Amc"; 
		static final String AMC_NOTIFICATION_NAME = "AMC_DUE_DATE";
	}
	interface ColumnNames{
		static final String REF_AMC = "ref_amc";
		static final String REF_VENDOR = "ref_vendor";
		static final String REF_ASSET = "ref_asset";
		static final String REF_USER = "ref_user";
		static final String REF_NOTIFICATION_CONFIG = "ref_notification_config";
	}
	interface DATA_SOURCE_PARAMETER {
		static final int MAX_ACTIVE = 5;
		static final int INITIAL_SIZE = 5;
		static final int REMOVE_ABANDONED_TIMEOUT = 60*100;
		static final Long VALIDATION_INTERVAL = 60000L;
		static final int TIME_BETWEEN_EVICTION_RUNS_MILLIS = 60000;
		static final int MAX_WAIT = 30000;
		static final int MIN_EVICTABLE_IDLE_TIME_MILLIS = 60000;
		static final int MIN_IDLE = 60000;
	}
	interface ActionName{
		static final String POST = "post";
		static final String PUT = "put";
		static final String DELETE = "delete";
	}
	
	/*
	 * interface JournalType { static final String MANUAL = "MANUAL"; static final
	 * String AUTOMATIC = "AUTOMATIC"; }
	 * 
	 * interface Status { static final String APPROVED = "APPROVED"; static final
	 * String DRAFT = "DRAFT"; static final String SUBMITTED = "SUBMITTED"; }
	 * 
	 * interface Sequence { static final String JOURNAL_MANUAL = "JV_MJ"; static
	 * final String TRANSACTION_PREFIX = "TX"; static final String
	 * TRANSACTION_JOURNAL_PREFIX = "MJ"; }
	 */
}
