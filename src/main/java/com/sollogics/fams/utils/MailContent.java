package com.sollogics.fams.utils;

public class MailContent {
    private String[] to;
    private String from;
    private String[] cc;
    private String[] bcc;
    private String subject;
    private String attachment;
    private String mailBody;

    public MailContent() {

    }

    public String[] getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to.split(",");
    }

    public void setTo(String[] to) {
        this.to = to;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String[] getCc() {
        return cc;
    }

    public void setCc(String cc) {
        this.cc = cc.split(",");
    }

    public void setCc(String[] cc) {
        this.cc = cc;
    }

    public String[] getBcc() {
        return bcc;
    }

    public void setBcc(String bcc) {
        this.bcc = bcc.split(",");
    }

    public void setBcc(String[] bcc) {
        this.bcc = bcc;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getAttachment() {
        return attachment;
    }

    public void setAttachment(String attachment) {
        this.attachment = attachment;
    }


    public String getMailBody() {
        return mailBody;
    }

    public void setMailBody(String mailBody) {
        this.mailBody = mailBody;
    }

}

