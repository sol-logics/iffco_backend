package com.sollogics.fams.utils;

//import java.security.SecureRandom;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sollogics.fams.exceptionHandler.GlobalException;

public class MyStringRandomGen {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(MyStringRandomGen.class);

  private static final String CHAR_LIST = 
      "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890abcdefghijklmnopqrstuvwxyz";
  
  private static final String NUMBER_LIST = "1234567890";
  private static final int RANDOM_INTEGER_LENGTH = 5;
  private static final int RANDOM_STRING_LENGTH = 8;
   
  /**
   * This method generates random string
   * @return
   */
  public String generateRandomString(){
       
      StringBuffer randStr = new StringBuffer();
      for(int i=0; i<RANDOM_STRING_LENGTH; i++){
          int number = getRandomString();
          char ch = CHAR_LIST.charAt(number);
          randStr.append(ch);
      }
      return randStr.toString();
  }
  
  public String generateRandomNumber(){   
  	LOGGER.info("Enter into : generateRandomNumber() service method");
      StringBuffer randStr = new StringBuffer();
      for(int i=0; i<RANDOM_INTEGER_LENGTH; i++){
          int number = getRandomNumber();
          char ch = NUMBER_LIST.charAt(number);
          randStr.append(ch);
      }
      LOGGER.info("Exit from : generateRandomNumber() service method");
      return randStr.toString();
  }
   
  /**
   * This method generates random numbers
   * @return int
   */
  private int getRandomString() {
  	try {
  		int randomInt = 0;
  		Random randomGenerator = new Random();
			//SecureRandom randomGenerator = SecureRandom.getInstanceStrong();
			randomInt = randomGenerator.nextInt(CHAR_LIST.length());
			if (randomInt - 1 == -1) {
			    return randomInt;
			} else {
			    return randomInt - 1;
			}
  	}catch(Exception e) {
			LOGGER.error("error in getRandomString");
			throw new GlobalException(500, e.getMessage());
		}
  }
  
  private int getRandomNumber() {
  	try {
  		int randomInt = 0;
          Random randomGenerator = new Random();
  		//SecureRandom randomGenerator = SecureRandom.getInstanceStrong();
          randomInt = randomGenerator.nextInt(NUMBER_LIST.length());
          if (randomInt - 1 == -1) {
              return randomInt;
          } else {
              return randomInt - 1;
          }
  	}catch(Exception e) {
			LOGGER.error("error in getRandomNumber");
			throw new GlobalException(500, e.getMessage());
		}
  }
 
}