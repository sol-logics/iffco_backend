package com.sollogics.fams.utils;

//import static org.junit.Assert.assertEquals;

import java.io.UnsupportedEncodingException;
import java.util.Date;

import com.sollogics.fams.exceptionHandler.InvalidCredentialException;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class JwtHelper {

	static String subject = "onboarding";
	//static String secret = "faa76006-6fef-413d-9cae-a05e63170cbf";
	static String secret = "37b23863-2e56-4499-9552-53cb6cd647e2";
	static String refrsehSecret = "37b23863-2e56-4499-9552-53cb6czbzbbb";
	static final long JWT_TOKEN_REFRESH_VALIDITY = 8 * 60 * 60;//8 * 60 * 60;
	static final long JWT_TOKEN_VALIDITY = 15 * 60;
	
	String userId = null;
	String emailId = "";
	Date expireTime = null;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}	

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}	
	
	public Date getExpireTime() {
		return expireTime;
	}

	public void setExpireTime(Date expireTime) {
		this.expireTime = expireTime;
	}
	
	//check if the token has expired
	private Boolean isTokenExpired(Date expiration) {
		return expiration.before(new Date());
	}
	
	@SuppressWarnings("deprecation")
	public String getRefreshToken(String userId, String email) {
		try {
			Date d = new Date();
			Date de = d;
			de.setYear(de.getYear() + 1);
			String jwt = Jwts.builder().setSubject(subject).setExpiration(de)
					.claim("userId", userId)
					.claim("emailId", email)
					.setIssuedAt(new Date(System.currentTimeMillis()))
					.setExpiration(new Date(System.currentTimeMillis() + JWT_TOKEN_REFRESH_VALIDITY * 1000))
					.signWith(SignatureAlgorithm.HS256, refrsehSecret.getBytes("UTF-8")).compact();
			return jwt;
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public boolean validateRefreshToken(String token) {
		String jwt = token;
		Jws<Claims> claims;
		try {
			claims = Jwts.parser().setSigningKey(refrsehSecret.getBytes("UTF-8")).parseClaimsJws(jwt);
			userId = (String) claims.getBody().get("userId");
			emailId = (String) claims.getBody().get("emailId");
			expireTime = claims.getBody().getExpiration();
			if(isTokenExpired(expireTime)) {
				return false;
			}
			return true;
		} catch (ExpiredJwtException e) {
			throw new InvalidCredentialException(401, "The refresh token expired");
		} catch (Exception e) {
			throw new InvalidCredentialException(401, "The refresh token expired");
		} 
	}

	public boolean validateToken(String token) {
		String jwt = token;
		Jws<Claims> claims;
		try {
			claims = Jwts.parser().setSigningKey(secret.getBytes("UTF-8")).parseClaimsJws(jwt);
			userId = (String) claims.getBody().get("userId");
			emailId = (String) claims.getBody().get("emailId");
			expireTime = claims.getBody().getExpiration();
			if(isTokenExpired(expireTime)) {
				return false;
			}
			return true;
		} catch (ExpiredJwtException e) {
			throw new InvalidCredentialException(401, "The access token expired");
		} catch (Exception e) {
			throw new InvalidCredentialException(401, "The access token expired");
		} 
	}
	
	@SuppressWarnings("deprecation")
	public String getToken(String userId, String email) {
		try {
			Date d = new Date();
			Date de = d;
			de.setYear(de.getYear() + 1);
			String jwt = Jwts.builder().setSubject(subject).setExpiration(de)
					.claim("userId", userId)
					.claim("emailId", email)
					.setIssuedAt(new Date(System.currentTimeMillis()))
					.setExpiration(new Date(System.currentTimeMillis() + JWT_TOKEN_VALIDITY * 1000))
					.signWith(SignatureAlgorithm.HS256, secret.getBytes("UTF-8")).compact();
			return jwt;
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@SuppressWarnings("deprecation")
	public String getAccessTokenByRefreshToken(String refreshToken){
		String jwt = refreshToken;
		Jws<Claims> claims;
		try {
			claims = Jwts.parser().setSigningKey(refrsehSecret.getBytes("UTF-8")).parseClaimsJws(jwt);
			userId = (String) claims.getBody().get("userId");
			emailId = (String) claims.getBody().get("emailId");
			expireTime = claims.getBody().getExpiration();
			if(isTokenExpired(expireTime)) {
				throw new InvalidCredentialException(401, "The refresh token expired");
			}
			
			Date d = new Date();
			Date de = d;
			de.setYear(de.getYear() + 1);
			String accessToken = Jwts.builder().setSubject(subject).setExpiration(de)
					.claim("userId", userId)
					.claim("emailId", emailId)
					.setIssuedAt(new Date(System.currentTimeMillis()))
					.setExpiration(new Date(System.currentTimeMillis() + JWT_TOKEN_VALIDITY * 1000))
					.signWith(SignatureAlgorithm.HS256, secret.getBytes("UTF-8")).compact();
			return accessToken;			
		} catch (ExpiredJwtException e) {
			throw new InvalidCredentialException(401, "The refresh token expired");
		} catch (Exception e) {
			throw new InvalidCredentialException(401, "The refresh token expired");
		} 
	}

}
