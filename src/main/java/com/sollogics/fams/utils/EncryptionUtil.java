package com.sollogics.fams.utils;

import java.security.MessageDigest;

import javax.crypto.Cipher;
import javax.crypto.spec.RC2ParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Hex;

public class EncryptionUtil {
	private byte encryptionKey[] = { 8, 6, 4, 0, 2, 3, 6, 9 };
	private MessageDigest messageDigest;
	private static EncryptionUtil instance = null;

	public EncryptionUtil() {
		try {
			messageDigest = MessageDigest.getInstance("MD5");
			String tempBytes = "";
			messageDigest.update(tempBytes.getBytes());
		} catch (Exception e) {
		}
	}

	
	static public EncryptionUtil getInstance(){
		if( instance==null)
			instance = new EncryptionUtil();
		
		return instance;
	}
	 
	
	public String encode(String stringToEncode) {
		try {
			
			if (stringToEncode == null || stringToEncode.isEmpty())
				return null;

			byte[] data = stringToEncode.getBytes();
			data = doEncrypt(data);
			String encodedString = new String(Hex.encodeHex(data));
			return encodedString;
		} catch (Exception e) {
			
			return null;
		}
	}

	public String decode(String encodedString) {
		try {
			
			String decodedString;

			if (encodedString == null)
				return null;
			byte[] bytes = Hex.decodeHex(encodedString.toCharArray());
			byte[] tempBytes = doDecrypt(bytes);
			decodedString = new String(tempBytes);
			return decodedString;
		} catch (Exception e) {
			
			return null;
		}
	}
	
	private byte[] doEncrypt(byte[] data) throws Exception {
		SecretKeySpec RC2Key = new SecretKeySpec(messageDigest.digest(), "RC2");
		Cipher cipher = Cipher.getInstance("RC2/CBC/PKCS5Padding");
		cipher.init(Cipher.ENCRYPT_MODE, RC2Key, new RC2ParameterSpec(128,
				encryptionKey));
		return cipher.doFinal(data);
	}

	private byte[] doDecrypt(byte[] data) throws Exception {
		SecretKeySpec RC2Key = new SecretKeySpec(messageDigest.digest(), "RC2");
		Cipher cipher = Cipher.getInstance("RC2/CBC/PKCS5Padding");
		cipher.init(Cipher.DECRYPT_MODE, RC2Key, new RC2ParameterSpec(128,
				encryptionKey));
		return cipher.doFinal(data);
	}
	
	public static void main(String[] args) {
		EncryptionUtil encUtil = EncryptionUtil.getInstance();
		String s = encUtil.encode("admin");
		
		System.out.println("admin encrupted: " + s);
		
		String pwd=encUtil.decode("4fbee38d7647134ef76ae5f10ff0b382");
		System.out.println(pwd);
		s = encUtil.decode(s);
		//System.out.println(s);
		
		s = encUtil.encode("admin");
		//System.out.println(s);
		s = encUtil.decode(s);
		//System.out.println(s);
		
		s = encUtil.encode("admin1");
		//System.out.println(s);
		s = encUtil.decode(s);
		//System.out.println(s);
		s = encUtil.encode("admin2");
		//System.out.println(s);
		s = encUtil.decode(s);
		//System.out.println(s);

	}


}
