package com.sollogics.fams.utils;

import java.util.Properties;

import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.MimeMessage;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.env.Environment;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

@Component
public class EmailSenderImpl implements EmailSender {

	private static final Logger LOGGER = LoggerFactory.getLogger(EmailSenderImpl.class);
	
	@Autowired
	private Environment environment;
	
	protected String getProperty(String key) {
		String property = environment.getProperty(key);
		if (StringUtils.isBlank(property)) {

		}
		return property != null ? property.trim() : null;
	}

	@Lazy
	@Override
	public boolean sendMail(MailContent mailContent) {
		boolean isMailSent = false;
		try {
			LOGGER.info("Enter into : sendMail() service method");
			String username = getProperty("mail.smtp.user");
			String password = getProperty("mail.sender_pwd");
			
			// creates a new session with an authenticator
			/*
			 * Authenticator auth = new Authenticator() { public PasswordAuthentication
			 * getPasswordAuthentication() { return new PasswordAuthentication(username,
			 * password); } }; Session session = Session.getInstance(smtpProperties, auth);
			 */
		
			Properties props = new Properties();
			
			props.put("mail.smtp.auth", getProperty("mail.smtp.auth"));
			props.put("mail.smtp.host", getProperty("mail.host"));
			props.put("mail.smtp.port", getProperty("mail.port"));
			
			//when live must be comment below 3 lines
			props.put("mail.smtp.socketFactory.port", getProperty("mail.port"));
		    props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		    props.put("mail.smtp.socketFactory.fallback", "false");

			// Get the Session object.
			Session session = Session.getInstance(props, new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(username, password);
				}
			});
			
			MimeMessage mimeMessage = new MimeMessage(session);
            MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
            setMimeMessageFromMailContent(mailContent, messageHelper);
            
			// Send message
			Transport.send(mimeMessage);
			
			isMailSent = true;
     
            
			// Send message
			//Transport.send(mimeMessage);
			
			//isMailSent = true;
			LOGGER.info("Exit from : sendMail() service method");
		} catch (MessagingException e) {
			LOGGER.error("Unable to send the mail, {}", e.getMessage());
		} catch (Exception e) {
			LOGGER.error("Unable to send the mail, {}", e.getMessage());
		}
		return isMailSent;
	}
	
	private void setMimeMessageFromMailContent(MailContent mailContent, MimeMessageHelper messageHelper) throws MessagingException {

        if (mailContent.getTo() != null){
            messageHelper.setTo(mailContent.getTo());
        }
        if (mailContent.getCc() != null){
            messageHelper.setCc(mailContent.getCc());
        }
        if (mailContent.getBcc() != null){
            messageHelper.setBcc(mailContent.getBcc());
        }
        if (mailContent.getMailBody() != null){
            messageHelper.setText(mailContent.getMailBody(), true);
        }
        if (mailContent.getFrom() != null){
            messageHelper.setFrom(mailContent.getFrom());
        }
        if (mailContent.getSubject() != null){
            messageHelper.setSubject(mailContent.getSubject());
        }
    }

}
