package com.sollogics.fams.configuration;

import javax.servlet.ServletContext;

import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.ServletContextAware;

@Configuration
public class CommonConfiguration implements ServletContextAware {


	private ServletContext context;
	
//	@SuppressWarnings({ "unchecked", "rawtypes" })
//	@Bean
//	public Mapper mapper(){
//		DozerBeanMapper dozerBeanMapper = new DozerBeanMapper();
//		List<String> mappingFiles = new ArrayList();
//		mappingFiles.add("dozerJdk8Converters.xml");
//		dozerBeanMapper.setMappingFiles(mappingFiles);
//		return dozerBeanMapper;
//	}
	
	@Bean
	public ModelMapper modelMapper() {
		ModelMapper modelMapper = new ModelMapper();
		modelMapper.getConfiguration().setAmbiguityIgnored(true);
		return modelMapper;
	}
	
	/*@Bean
	public Mapper modelMapper() {
		Mapper modelMapper = new DozerBeanMapper();
		return modelMapper;
	}*/
	
    @Override
    public void setServletContext(final ServletContext servletContext) {
        this.context = servletContext;

    }
    
	@Bean
	public ServletContext servletContext() {
		return this.context;
	}
	
	@Bean
	public Mapper mapper() {
	    return new DozerBeanMapper();
	}




}
