package com.sollogics.fams.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

	// PATH - http://localhost:8084/PublishService/swagger-ui.html#/
	// PATH - http://localhost:8084/PublishService/v2/api-docs
	
	  @Bean public Docket postsApi() { return new
	  Docket(DocumentationType.SWAGGER_2) .select()
	 .apis(RequestHandlerSelectors.any()) .paths(PathSelectors.any()) .build(); }
	 

	/*@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2).select().apis(RequestHandlerSelectors.any())
				.paths(PathSelectors.any()).build().apiInfo(apiInfo()).securitySchemes(Arrays.asList(apiKey()));
	}

	private ApiInfo apiInfo() {
		return new ApiInfoBuilder().title("REST API").description("The REST API for demo swagger.")
				.termsOfServiceUrl("").contact(new Contact("RICH LEE", "", "rich.lee@gmail.com"))
				.license("Apache License Version 2.0").licenseUrl("https://www.apache.org/licenses/LICENSE-2.0")
				.version("0.0.1").build();
	}

	private ApiKey apiKey() {
		return new ApiKey("AUTHORIZATION", "AUTHORIZATION", "header");
	}

	@Bean
	SecurityConfiguration security() {
		return new SecurityConfiguration(null, null, null, // realm Needed for authenticate button to work
				null, // appName Needed for authenticate button to work
				"BEARER ", // apiKeyValue
				ApiKeyVehicle.HEADER, "AUTHORIZATION", // apiKeyName
				null);
	}

	private SecurityContext securityContext() {
		return SecurityContext.builder().securityReferences(defaultAuth()).forPaths(PathSelectors.regex("/anyPath.*"))
				.build();
	}

	List<SecurityReference> defaultAuth() {
		AuthorizationScope authorizationScope = new AuthorizationScope("global", "accessEverything");
		AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
		authorizationScopes[0] = authorizationScope;
		return Lists.newArrayList(new SecurityReference("AUTHORIZATION", authorizationScopes));
	}*/

}