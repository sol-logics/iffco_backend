package com.sollogics.fams.configuration;

import java.io.IOException;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.exception.VelocityException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.ui.velocity.VelocityEngineFactory;


@Configuration
@ComponentScan(basePackages = { "com.example.fams" })
@PropertySource(value = { "classpath:application.properties" })
public class PropertyConfiguration{
	private static final Logger LOGGER = LoggerFactory.getLogger(PropertyConfiguration.class);
	
	@Autowired
	private Environment environment;
	
	protected String getProperty(String key) {
		String property = environment.getProperty(key);
		if (StringUtils.isBlank(property)) {

		}
		return property != null ? property.trim() : null;
	}
	
	@Bean(name = "contextPath")
	public String getContextPath() {
		return getProperty("server.servlet.context-path");
	}
	
//	@Bean(name = "doNotReplyMailSender")
//	public JavaMailSender doNotReplyMailSender() {				
//		final JavaMailSenderImpl javaMailSender = new JavaMailSenderImpl();
//		javaMailSender.setHost(getProperty("mail.host"));
//		javaMailSender.setPort(Integer.parseInt(getProperty("mail.port")));
//		javaMailSender.setUsername(getProperty("mail.smtp.user"));
//		javaMailSender.setPassword(getProperty("mail.sender_pwd"));
//		Properties props = new Properties();
//		props.put("mail.smtp.starttls.enable", getProperty("mail.smtp.starttls.enable"));
//		props.put("mail.smtp.auth", getProperty("mail.smtp.auth"));
//		props.put("mail.transport.protocol", getProperty("mail.protocol"));
//		props.setProperty("mail.smtp.socketFactory.fallback", getProperty("mail.smtp.socketFactory.fallback"));
//		props.setProperty("mail.smtp.socketFactory.port", getProperty("mail.smtp.socketFactory.port"));
//		props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
//		javaMailSender.setJavaMailProperties(props);
//		return javaMailSender;		
//	}
	
	@Bean
    public VelocityEngine getVelocityEngine() throws VelocityException, IOException{
        VelocityEngineFactory factory = new VelocityEngineFactory();
        Properties props = new Properties();
        props.put("resource.loader", "class");
        props.put("class.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
        factory.setVelocityProperties(props);
        return factory.createVelocityEngine();      
    }
}
