package com.sollogics.fams.configuration;

import java.util.Properties;

import org.apache.commons.dbcp.BasicDataSource;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@ComponentScan(basePackages = { "com.example.fams" })
@PropertySource(value = { "classpath:application.properties" })
@EnableTransactionManagement
public class DataBaseConfig {
	 

	@Autowired
	private Environment environment;

	protected String getProperty(String key) {
		String property = environment.getProperty(key);
		if (StringUtils.isBlank(property)) {

		}
		return property != null ? property.trim() : null;
	}

	@Bean(name = "port")
	public String getport() {
		return getProperty("server.port");
	}

	@Bean(name = "jdbcDriverClassName")
	public String getDriverClassName() {
		return getProperty("jdbc.driverClassName");
	}

	@Bean(name = "jdbcUrl")
	public String getUrl() {
		return getProperty("jdbc.url");
	}

	@Bean(name = "jdbcUsername")
	public String getUsername() {
		return getProperty("jdbc.username");
	}

	@Bean(name = "jdbcPassword")
	public String getpassword() {
		return getProperty("jdbc.password");
	}

	@Bean(name = "hibernateDialect")
	public String gethibernateDialect() {
		return getProperty("hibernate.dialect");
	}

	@Bean(name = "hibernateShowSql")
	public String getHibernateShow_sql() {
		return getProperty("hibernate.show_sql");
	}

	@Bean(name = "hibernateHbm2ddlAuto")
	public String getHibernateHbm2ddlAuto() {
		return getProperty("hibernate.hbm2ddl.auto");
	}

	@Bean
	public LocalSessionFactoryBean sessionFactory() {
		LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
		sessionFactory.setDataSource(dataSource());
		sessionFactory.setPackagesToScan(new String[] { "com.sollogics.fams.repositories"});
		sessionFactory.setHibernateProperties(getHibernateProperties());
		return sessionFactory;
	}

	private Properties getHibernateProperties() {
		Properties prop = new Properties();
		prop.put("hibernate.format_sql", "true");
		prop.put("hibernate.show_sql", getHibernateShow_sql());
		//prop.put("hibernate.hbm2ddl.auto", getHibernateHbm2ddlAuto());
		prop.put("hibernate.dialect", gethibernateDialect());
		prop.put("hibernate.id.new_generator_mappings", "false");
		/*prop.put("hibernate.cache.use_second_level_cache", "false");
		prop.put("hibernate.cache.region.factory_class", "org.hibernate.cache.ehcache.EhCacheRegionFactory");
		prop.put("hibernate.cache.use_query_cache", "false");*/
		
//		prop.put("hibernate.transaction.auto_close_session", "false");
//		prop.put("hibernate.connection.autocommit", "false");
//		prop.put("hibernate.flushMode", "ALWAYS");
		return prop;
	}

	@Bean
	public BasicDataSource dataSource() {

		BasicDataSource ds = new BasicDataSource();
		ds.setDriverClassName(getDriverClassName());
		ds.setUrl(getUrl());
		ds.setUsername(getUsername());
		ds.setPassword(getpassword());
		
		ds.setValidationQuery("SELECT 1");
		ds.setTestWhileIdle(true);
		ds.setTimeBetweenEvictionRunsMillis(60000);
		ds.setTestOnBorrow(true);
		
		return ds;
	}

	@Bean
	@Autowired
	public HibernateTransactionManager transactionManager() {
		HibernateTransactionManager txManager = new HibernateTransactionManager();
		txManager.setSessionFactory(sessionFactory().getObject());
		return txManager;
	}

}
