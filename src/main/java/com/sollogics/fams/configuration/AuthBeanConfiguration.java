package com.sollogics.fams.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.sollogics.fams.interceptor.UrlSecurityInterceptor;

@SuppressWarnings("deprecation")
@Configuration
public class AuthBeanConfiguration extends WebMvcConfigurerAdapter {

	@Bean
	public UrlSecurityInterceptor securityInterceptor() {
		return new UrlSecurityInterceptor();
	}

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(securityInterceptor());
	}
	

}

