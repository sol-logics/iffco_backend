package com.sollogics.fams;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FamsApplication {

	public static void main(String[] args) {
		SpringApplication.run(FamsApplication.class, args);
	}
	
//	@Bean
//	public Mapper mapper() {
//	    return new DozerBeanMapper();
//	}

}
