package com.sollogics.fams.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.sollogics.fams.exceptionHandler.InvalidCredentialException;
import com.sollogics.fams.exceptionHandler.SessionException;
import com.sollogics.fams.utils.CorsData;

public class UrlSecurityInterceptor extends HandlerInterceptorAdapter {

	
	private static final Logger LOGGER = LoggerFactory.getLogger(UrlSecurityInterceptor.class);
	private static final int subStringSize = 2;

	private static final String AC_ALLOW_ORIGIN = "Access-Control-Allow-Origin";
	private static final String AC_ALLOW_METHODS = "Access-Control-Allow-Methods";
	private static final String AC_ALLOW_HEADERS = "Access-Control-Allow-Headers";
	
	private CorsData corsData;

	@SuppressWarnings("deprecation")
	@Override
	public boolean preHandle(final HttpServletRequest request, final HttpServletResponse response, final Object handler)
			throws Exception {
		try {
			this.corsData = new CorsData(request);
			if (this.corsData.isPreflighted()) {
				response.setHeader(AC_ALLOW_ORIGIN, "*");
				response.setHeader(AC_ALLOW_METHODS, "POST, GET, PUT, OPTIONS, DELETE");
				response.setHeader("Cache-Control", "No-Store");
				response.setHeader("Pragma", "no-cache");
				response.setHeader(AC_ALLOW_HEADERS, "Origin, Content-Type, X-Requested-With, accept, Access-Control-Request-Method, Access-Control-Request-Headers, Authorization");
				response.setHeader("Access-Control-Allow-Credentials", "false");
				return false;	          
	        }
			String uriPath = request.getRequestURI();
			String[] subString = uriPath.trim().split("/");
			String accessedUrl = subString[subStringSize];
			String urlContextPath = subString[1];
			System.out.println("Uri path accessed: " + uriPath);
			String adminId = "";
			String userId = "";
			String email = "";
			if(urlContextPath != null && urlContextPath.equalsIgnoreCase("fams")) {
				if(accessedUrl != null) {
					if(accessedUrl.equalsIgnoreCase("authentication") || accessedUrl.equalsIgnoreCase("authenticate") || accessedUrl.equalsIgnoreCase("changePassword") || accessedUrl.equalsIgnoreCase("forgotPassword") || accessedUrl.equalsIgnoreCase("adminforgotPassword") || accessedUrl.equalsIgnoreCase("adminchangePassword") || accessedUrl.equalsIgnoreCase("refreshToken")) {
						return true;
					}
//						else if(accessedUrl.equalsIgnoreCase("admin")){				
//						String bearer = request.getHeader("Authorization");
//						if(bearer == null)
//							bearer = request.getHeader("authorization");
//						String jwtToken = "";
//						if(bearer != null){
//							jwtToken = bearer.replace("Bearer", "");
//							jwtToken = jwtToken.trim();
//							if(!jwtToken.isEmpty()) {
//								JwtHelper jwtHelper = new JwtHelper();
//								if(jwtHelper.validateToken(jwtToken)) {
//									adminId = jwtHelper.getUserId();
//									email = jwtHelper.getEmailId();
//									String urlUserId = subString[3];
//									if((adminId != null && !adminId.isEmpty()) && (urlUserId != null && !urlUserId.isEmpty()) && adminId.equalsIgnoreCase(urlUserId)) {
//										return true;									
//									}
//								}
//							}							
//						}
//					}
				}
				return true;
			}
			LOGGER.error("Token not be null");
			response.setHeader("Access-Control-Allow-Origin", "*");
			response.setStatus(HttpStatus.SC_UNAUTHORIZED, "User not authorised");
			return false;
		}  catch (SessionException e) {
			LOGGER.error(e.getMessage());
			response.setHeader("Access-Control-Allow-Origin", "*");
			response.setStatus(HttpStatus.SC_GATEWAY_TIMEOUT);
			throw new SessionException(e.getErrorCode(), e.getMessage());
		}catch (Exception e) {
			LOGGER.error(e.getMessage());
			//response.setHeader("Access-Control-Allow-Origin", "*");
			response.setHeader("Access-Control-Allow-Origin", "*");
			response.setStatus(HttpStatus.SC_UNAUTHORIZED);
			throw new InvalidCredentialException(401, "User not authorised");
		}
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		// if any thing is required after the called method than that method will use.
		if(this.corsData.isSimple()) {
			response.setHeader(AC_ALLOW_ORIGIN, "*");
		}
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		// if any thing is required after the postHandler method than that method will
		// use.
	}	



}
