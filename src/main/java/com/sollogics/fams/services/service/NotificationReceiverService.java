package com.sollogics.fams.services.service;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.stereotype.Component;

import com.sollogics.fams.services.dto.NotificationReceiverDto;

@Component
public interface NotificationReceiverService {

	NotificationReceiverDto saveNotificationReceiver(BigDecimal userId, BigDecimal notificationId,
			NotificationReceiverDto notificationReceiverDto);

	List<NotificationReceiverDto> getAllNotificationReceiver(BigDecimal userId);

	NotificationReceiverDto getNotificationReceiverById(BigDecimal userId, BigDecimal notificationId);

	NotificationReceiverDto updateNotificationReceiver(BigDecimal usersId, BigDecimal notificationId,
			NotificationReceiverDto notificationReceiverDto);

	Boolean deleteNotificationReceiverById(BigDecimal userId, BigDecimal notificationId);


}
