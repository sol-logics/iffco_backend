package com.sollogics.fams.services.service;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import com.sollogics.fams.services.dto.AmcDto;
import com.sollogics.fams.services.dto.AttachmentAssociationDto;

@Component
public interface AmcService {

	AmcDto saveAmc(BigDecimal userId, String amcDto, MultipartFile[] files);

	List<AmcDto> getAllAmc(BigDecimal userId);

	AmcDto getAmcById(BigDecimal userId, BigDecimal amcId);

	AmcDto updateAmc(BigDecimal userId, BigDecimal amcId, String amcDto, MultipartFile[] files);

	Boolean deleteAmcById(BigDecimal userId, BigDecimal amcId);

	List<AmcDto> getAmcByRenewUuid(BigDecimal userId, String renewuuid);

	Boolean deleteAmcFileByFileName(List<AttachmentAssociationDto> attachmentDtoLst, BigDecimal userId,
			BigDecimal amcId);

	/*AmcServicesDto saveAmcService(BigDecimal userId, AmcServicesDto amcServicesDto);

	List<AmcServicesDto> getAllAmcServices(BigDecimal userId);

	AmcServicesDto getAmcServiceById(BigDecimal userId, BigDecimal amcServiceId);

	AmcServicesDto updateAmcService(BigDecimal userId, BigDecimal amcServiceId, AmcServicesDto amcServicesDto);

	Boolean deleteAmcServiceById(BigDecimal userId, BigDecimal amcServiceId);*/

}
