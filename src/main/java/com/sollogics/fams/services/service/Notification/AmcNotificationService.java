package com.sollogics.fams.services.service.Notification;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.stereotype.Component;

import com.sollogics.fams.services.dto.NotificationAlertDetailsDto;

@Component
public interface AmcNotificationService {

	List<NotificationAlertDetailsDto> amcDueDateAlert(BigDecimal userId);

}
