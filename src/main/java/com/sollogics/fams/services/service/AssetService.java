package com.sollogics.fams.services.service;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import com.sollogics.fams.services.dto.AssetDto;
import com.sollogics.fams.services.dto.AssetGroupDto;
import com.sollogics.fams.services.dto.AttachmentAssociationDto;
import com.sollogics.fams.services.dto.CategoryDto;
import com.sollogics.fams.services.dto.LocationDto;
import com.sollogics.fams.services.dto.StatusDto;
import com.sollogics.fams.services.dto.TypeDto;

@Component
public interface AssetService {

	AssetDto saveAsset(BigDecimal userId, String assetDto, MultipartFile[] files);
	
	List<AssetDto> getAllAssets(BigDecimal userId);

	AssetDto getAssetById(BigDecimal userId, BigDecimal assetId);

	AssetDto updateAsset(BigDecimal userId, BigDecimal assetId, String assetDto, MultipartFile[] files);

	Boolean deleteAssetById(BigDecimal userId, BigDecimal assetId);

	AssetGroupDto saveAssetGroup(BigDecimal userId, AssetGroupDto assetGroupDto);

	List<AssetGroupDto> getAllAssetGroup(BigDecimal userId);

	AssetGroupDto getAssetGroupById(BigDecimal userId, BigDecimal assetGroupId);

	AssetGroupDto updateAssetGroup(BigDecimal userId, BigDecimal assetGroupId, AssetGroupDto assetGroupDto);

	Boolean deleteAssetGroupById(BigDecimal userId, BigDecimal assetGroupId);

	List<CategoryDto> getAllCategory(BigDecimal userId);

	List<StatusDto> getAllStatus(BigDecimal userId);

	List<LocationDto> getAllLocation(BigDecimal userId);

	List<TypeDto> getAllType(BigDecimal userId);

	Boolean deleteAssetFileByFileName(List<AttachmentAssociationDto> attachmentDtoLst, BigDecimal userId,
			BigDecimal amcId);

	CategoryDto saveCategory(BigDecimal userId, CategoryDto categoryDto);

	LocationDto saveLocation(BigDecimal userId, LocationDto locationDto);

	TypeDto saveType(BigDecimal userId, TypeDto typeDto);

	

}
