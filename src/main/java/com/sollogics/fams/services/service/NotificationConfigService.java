package com.sollogics.fams.services.service;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.stereotype.Component;

import com.sollogics.fams.services.dto.NotificationConfigDto;
import com.sollogics.fams.services.dto.NotificationMetaDto;

@Component
public interface NotificationConfigService {

	NotificationConfigDto saveNotificationConfig(BigDecimal userId, NotificationConfigDto notificationConfigDto);

	List<NotificationConfigDto> getAllNotificationConfig(BigDecimal userId);

	NotificationConfigDto getNotificationConfigById(BigDecimal userId, BigDecimal notificationConfigId);

	NotificationConfigDto updateNotificationConfig(BigDecimal userId, BigDecimal notificationConfigId,
			NotificationConfigDto notificationConfigDto);

	Boolean deleteNotificationConfigById(BigDecimal userId, BigDecimal notificationConfigId);

	NotificationConfigDto getNotificationConfigByName(String notifactionName);

	List<NotificationMetaDto> getAllNotificationMeta(BigDecimal userId);

}
