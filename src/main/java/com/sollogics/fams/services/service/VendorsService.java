package com.sollogics.fams.services.service;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import com.sollogics.fams.services.dto.AttachmentAssociationDto;
import com.sollogics.fams.services.dto.VendorsDto;

@Component
public interface VendorsService {

	VendorsDto saveVendor(BigDecimal userId, String vendorsDto, MultipartFile[] files);

	List<VendorsDto> getAllVendors(BigDecimal usersId);

	VendorsDto getvendorById(BigDecimal userId, BigDecimal vendorId);

	VendorsDto updateVendors(BigDecimal userId, BigDecimal vendorId, String vendorsDtoStr, MultipartFile[] files);

	Boolean deleteVendorById(BigDecimal userId, BigDecimal vendorId);

	Boolean deleteVendorFileByFileName(List<AttachmentAssociationDto> attachmentDtoLst, BigDecimal userId,
			BigDecimal vendorId);

	

}
