package com.sollogics.fams.services.service;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import com.sollogics.fams.services.dto.AttachmentAssociationDto;
import com.sollogics.fams.services.dto.CompanyDto;
import com.sollogics.fams.services.dto.ProfileDto;
import com.sollogics.fams.services.dto.RolesDto;

@Component
public interface UserService {

	ProfileDto saveUserProfile(BigDecimal usersId, String profileDtoStr, MultipartFile[] files);

	List<ProfileDto> getAllUsers(BigDecimal userId);

	ProfileDto getUserById(BigDecimal userId, BigDecimal employeId);

	ProfileDto updateUserProfile(BigDecimal userId, BigDecimal employeId, String profileDtoStr, MultipartFile[] files);

	Boolean deleteUser(BigDecimal usersId, BigDecimal employeId);

	List<CompanyDto> getAllCompany(BigDecimal userId);

	CompanyDto getCompanyById(BigDecimal userId, BigDecimal companyId);

	List<RolesDto> getAllRoles(BigDecimal userId);

	RolesDto getRolesById(BigDecimal userId, BigDecimal rolesId);

	Boolean deleteUserFileByFileName(List<AttachmentAssociationDto> attachmentDtoLst, BigDecimal userId,
			BigDecimal employeId);

}
