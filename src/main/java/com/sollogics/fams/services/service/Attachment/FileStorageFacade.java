
package com.sollogics.fams.services.service.Attachment;

import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import com.sollogics.fams.services.dto.AttachmentAssociationDto;
import com.sollogics.fams.services.serviceImpl.Attachment.FileStorageService;

@Component
public class FileStorageFacade {
	private static final Logger LOGGER = LoggerFactory.getLogger(FileStorageFacade.class);

	private FileStorageService fileStorageService;

	@Autowired
	public FileStorageFacade(FileStorageService fileStorageService) {
		this.fileStorageService = fileStorageService;
	}
	
	public AttachmentAssociationDto storeFile(AttachmentAssociationDto attachmentDto, MultipartFile file,
			BigDecimal userId) {
		return fileStorageService.storeFile(attachmentDto,file,userId);
	}

	/*
	 * public AttachmentAssociationDto storeFile(AttachmentAssociationDto
	 * attachmentAssociationDto, MultipartFile file, BigDecimal userId) { return
	 * fileStorageService.storeFile(attachmentAssociationDto,file,userId); }
	 */

	public Resource loadFileAsResource(String fileName) {
		return fileStorageService.loadFileAsResource(fileName);
	}

}
