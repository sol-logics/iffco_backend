
package com.sollogics.fams.services.service.Attachment;

import java.math.BigDecimal;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import com.sollogics.fams.services.dto.AttachmentAssociationDto;

public interface FileService {

	public AttachmentAssociationDto storeFile(AttachmentAssociationDto attachmentDto, MultipartFile file,
			BigDecimal userId);
	//public AttachmentAssociationDto storeFile(AttachmentAssociationDto refattachmentAssociationDto,MultipartFile multipartFiles, BigDecimal userId);

	public Resource loadFileAsResource(String fileName);

}
