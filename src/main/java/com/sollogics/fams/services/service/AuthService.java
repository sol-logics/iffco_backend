package com.sollogics.fams.services.service;

import java.math.BigDecimal;

import org.springframework.stereotype.Component;

import com.sollogics.fams.services.dto.ChangePassword;
import com.sollogics.fams.services.dto.CredentialDto;
import com.sollogics.fams.services.dto.ProfileDto;

@Component
public interface AuthService {

	ProfileDto authenticateUser(CredentialDto credentialDto);

	Boolean changePassword(BigDecimal userId, ChangePassword changePassword);

	Boolean forgotPassword(String email);

	Boolean changePasswordByOtp(ChangePassword changePassword);

	Boolean validateAdmin(BigDecimal usersId);

	Boolean validateAdminAndManager(BigDecimal userId);

	/*List<RolesDto> getRoles();*/

}
