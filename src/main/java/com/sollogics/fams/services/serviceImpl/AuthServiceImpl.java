package com.sollogics.fams.services.serviceImpl;

import java.math.BigDecimal;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sollogics.fams.exceptionHandler.GlobalException;
import com.sollogics.fams.exceptionHandler.InvalidCredentialException;
import com.sollogics.fams.repositories.dao.AuthDao;
import com.sollogics.fams.repositories.dao.UserDao;
import com.sollogics.fams.repositories.modal.Credential;
import com.sollogics.fams.repositories.modal.Profile;
import com.sollogics.fams.repositories.modal.UserOtp;
import com.sollogics.fams.repositories.modal.Users;
import com.sollogics.fams.services.dto.ChangePassword;
import com.sollogics.fams.services.dto.CredentialDto;
import com.sollogics.fams.services.dto.ForgotPasswordEmailGenerator;
import com.sollogics.fams.services.dto.ProfileDto;
import com.sollogics.fams.services.dto.RolesDto;
import com.sollogics.fams.services.dto.UsersDto;
import com.sollogics.fams.services.service.AuthService;
import com.sollogics.fams.utils.Constants;
import com.sollogics.fams.utils.DateConversionUtil;
import com.sollogics.fams.utils.EmailSender;
import com.sollogics.fams.utils.EncryptionUtil;
import com.sollogics.fams.utils.GenerateOTP;
import com.sollogics.fams.utils.JwtHelper;
import com.sollogics.fams.utils.MailContent;

@Service
public class AuthServiceImpl implements AuthService{
//	@Autowired
//	@Qualifier("doNotReplyMailSender")
//	private JavaMailSender doNotReplyMailSender;
	
	@Autowired
	private ModelMapper mapper;
	
	@Autowired
	private ForgotPasswordEmailGenerator emailGenerator;
	
	@Autowired
	private EmailSender emailSender;
	
	@Autowired 
	private AuthDao authDao;
	
	@Autowired
	private UserDao userDao;
	
	private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

	@Override
	public ProfileDto authenticateUser(CredentialDto credentialDto) {
		LOGGER.info("Enter into : authenticateUser()");
		EncryptionUtil encryptionUtil = new EncryptionUtil();
		credentialDto.setPassword(encryptionUtil.encode(credentialDto.getPassword()));
		Credential credential = new Credential();
		credential = mapper.map(credentialDto,Credential.class);
		/*
		 * Users users = userDao.checkResetPassword(credential.getEmail());
		 * if(users.getResetPassword() == false) { changePassword(users.getId()); }
		 */
//		credential.setEmail(credentialDto.getEmail());
//		credential.setPassword(credentialDto.getPassword());
		Profile profile = authDao.authenticateUser(credential);
		if (profile != null && profile.getId()!= null) {
			ProfileDto profileDto = new ProfileDto();
			profileDto = mapper.map(profile,ProfileDto.class);
			RolesDto userRoleBo = new RolesDto();
			userRoleBo.setId(profile.getRefRoles().getId());
			userRoleBo.setType(profile.getRefRoles().getType());
			profileDto.setRefRoles(userRoleBo);
			
			/*CompanyDto companyDto = new CompanyDto();
			companyDto.setId(profile.getRefCompany().getId());
			companyDto.setName(profile.getRefCompany().getName());
			profileDto.setRefCompany(companyDto);*/
			
			UsersDto usersDto = new UsersDto();
			usersDto.setId(profile.getRefUser().getId());
			usersDto.setResetPassword(profile.getRefUser().getResetPassword());
			/*usersDto.setActive(profile.getRefUser().getActive());
			usersDto.setCreatedOn(profile.getRefUser().getCreatedOn());
			usersDto.setEmail(profile.getRefUser().getEmail());*/
			profileDto.setRefUser(usersDto);
			
			JwtHelper jwtHelper = new JwtHelper();
			credentialDto.setPassword(encryptionUtil.decode(credentialDto.getPassword()));
			profileDto.setAuthToken(jwtHelper.getToken(String.valueOf(profileDto.getRefUser().getId()), profileDto.getRefUser().getEmail()));
			LOGGER.info("Exit from : authenticateUser()");
			return profileDto;

		}else {
			throw new InvalidCredentialException(401,"Invalid email or password");
		}
		
	}

	@Override
	public Boolean changePassword(BigDecimal userId, ChangePassword changePassword) {
		LOGGER.info("Enter into : changePassword()");
		if(userId != null && changePassword != null && changePassword.getCurrentPassword() != null
				&& !changePassword.getCurrentPassword().isEmpty() && changePassword.getPreviousPassword() != null
				&& !changePassword.getPreviousPassword().isEmpty()) {
			EncryptionUtil encryptionUtil = new EncryptionUtil();
			changePassword.setPreviousPassword(encryptionUtil.encode(changePassword.getPreviousPassword()));
			changePassword.setCurrentPassword(encryptionUtil.encode(changePassword.getCurrentPassword()));
			Boolean flag = authDao.changePassword(userId, changePassword.getCurrentPassword(), changePassword.getPreviousPassword());
			if(flag) {
				LOGGER.info("Exit from : changePassword()");
				return flag;
			}
		}
			LOGGER.error("password not updated");
			throw new GlobalException(409, "Wrong previous password");
	}

	@Override
	public Boolean forgotPassword(String email) {
		LOGGER.info("Enter into : forgotPassword()");
		if(email != null && !email.isEmpty()) {
			Users users = authDao.validateEmail(email);
			if(users != null && users.getEmail() != null) {
				String createdOTP = GenerateOTP.createOTP();
				UserOtp userOtp = new UserOtp();
				userOtp.setOtp(createdOTP);
				userOtp.setRefUser(users);
				userOtp.setCreatedOn(DateConversionUtil.CurrentUTCTime());
				userOtp.setValidUpto(DateConversionUtil.CurrentUTCTimeAdd10(userOtp.getCreatedOn(), 10L));
				userOtp = authDao.saveOtpForChangedPassword(userOtp);
				LOGGER.info("Exit from : forgotPassword()");
				return mailForgotPassword(userOtp);
			}else {
				LOGGER.error("forgotPassword");
				throw new GlobalException(409, "Email not valid");
			}
		}
		LOGGER.error("forgotPassword");
		throw new GlobalException(409, "Conflict");
	}

	public Boolean mailForgotPassword(UserOtp userOtp) {
		LOGGER.info("Enter into : mailForgotPassword()");
		MailContent mailContent = null;
		boolean isMailSent = false;
		mailContent = emailGenerator.generateEmailContent(userOtp, null);
		isMailSent = emailSender.sendMail(mailContent);
		if (isMailSent == false) {
			LOGGER.error("forgotPassword");
			throw new GlobalException(409, "please resend otp");
		}
		LOGGER.info("Exit from : mailForgotPassword()");
		return isMailSent;
	}
	/*@Override
	public List<RolesDto> getRoles() {
		List<Roles> rolesLst = authDao.getRoles();
		List<RolesDto> rolesDtoLst = new ArrayList<>();
		for(Roles roles : rolesLst) {
			RolesDto rolesDto =  new RolesDto();
			rolesDto.setId(roles.getId());
			rolesDto.setType(roles.getType());
			rolesDtoLst.add(rolesDto);
		}
		return rolesDtoLst;
	}*/

	@Override
	public Boolean changePasswordByOtp(ChangePassword changePassword) {
		LOGGER.info("Enter into : changePasswordByOtp()");
		if(changePassword != null && changePassword.getEmail() !=null && !changePassword.getEmail().isEmpty() && changePassword.getOtp() != null && !changePassword.getOtp().isEmpty() && changePassword.getCurrentPassword() != null && !changePassword.getCurrentPassword().isEmpty()) {
			UserOtp userOtp = authDao.checkOtp(changePassword.getEmail(),changePassword.getOtp(),DateConversionUtil.CurrentUTCTime());
			if(userOtp != null && userOtp.getId() != null) {
				EncryptionUtil encryptionUtil = new EncryptionUtil();
				changePassword.setCurrentPassword(encryptionUtil.encode(changePassword.getCurrentPassword()));
				Boolean result = authDao.changePasswordByOtp(userOtp.getRefUser().getId(),changePassword.getCurrentPassword());
				LOGGER.info("Exit from : changePasswordByOtp()");
				return result;
			}else {
				LOGGER.error("OTP is not valid");
				throw new GlobalException(409, "OTP is not valid");
			}
		}
		LOGGER.error("password not update");
		throw new GlobalException(409, "Conflict");
	}
	
	@Override
	public Boolean validateAdmin(BigDecimal usersId) {
		LOGGER.info("Enter into : validateAdmin()");
		Profile result = authDao.validateUser(usersId);
		if(result != null && result.getRefRoles() != null && result.getRefRoles().getType() != null && !result.getRefRoles().getType().isEmpty() && result.getRefRoles().getType().equalsIgnoreCase(Constants.SystemRoles.ADMIN)) {
			LOGGER.info("Exit from : validateAdmin()");
			return true;
		}else {
			LOGGER.info("Exit from : validateAdmin()");
			return false;
		}
	}

	@Override
	public Boolean validateAdminAndManager(BigDecimal userId) {
		LOGGER.info("Enter into : validateAdminAndManager()");
		Profile result = authDao.validateUser(userId);
		if(result != null && result.getRefRoles() != null && result.getRefRoles().getType() != null && !result.getRefRoles().getType().isEmpty() && (result.getRefRoles().getType().equalsIgnoreCase(Constants.SystemRoles.ADMIN) || result.getRefRoles().getType().equalsIgnoreCase(Constants.SystemRoles.MANAGER))) {
			LOGGER.info("Exit from : validateAdminAndManager()");
			return true;
		}else {
			LOGGER.info("Exit from : validateAdminAndManager()");
			return false;
		}
	}

}
