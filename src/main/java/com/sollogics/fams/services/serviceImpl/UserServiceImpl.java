package com.sollogics.fams.services.serviceImpl;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.google.gson.Gson;
import com.sollogics.fams.controllers.Attachment.FileController;
import com.sollogics.fams.exceptionHandler.GlobalException;
import com.sollogics.fams.exceptionHandler.NotFoundException;
import com.sollogics.fams.repositories.dao.AuditTrailDao;
import com.sollogics.fams.repositories.dao.FileStorageDao;
import com.sollogics.fams.repositories.dao.NotificationReceiverDao;
import com.sollogics.fams.repositories.dao.UserDao;
import com.sollogics.fams.repositories.modal.AttachmentAssociation;
import com.sollogics.fams.repositories.modal.AuditTrail;
import com.sollogics.fams.repositories.modal.Company;
import com.sollogics.fams.repositories.modal.NotificationReceiver;
import com.sollogics.fams.repositories.modal.Profile;
import com.sollogics.fams.repositories.modal.Roles;
import com.sollogics.fams.repositories.modal.Users;
import com.sollogics.fams.services.dto.AttachmentAssociationDto;
import com.sollogics.fams.services.dto.CompanyDto;
import com.sollogics.fams.services.dto.ForgotPasswordEmailGenerator;
import com.sollogics.fams.services.dto.ProfileDto;
import com.sollogics.fams.services.dto.RolesDto;
import com.sollogics.fams.services.dto.UsersDto;
import com.sollogics.fams.services.service.AuthService;
import com.sollogics.fams.services.service.UserService;
import com.sollogics.fams.utils.Constants;
import com.sollogics.fams.utils.DateConversionUtil;
import com.sollogics.fams.utils.EmailSender;
import com.sollogics.fams.utils.EncryptionUtil;
import com.sollogics.fams.utils.GenerateOTP;
import com.sollogics.fams.utils.MailContent;

@Service
public class UserServiceImpl implements UserService {
	
	@Autowired
	private EmailSender emailSender;
	
	@Autowired
	private ForgotPasswordEmailGenerator emailGenerator;
	
	@Autowired
	private ModelMapper mapper;
	
	@Autowired
	private UserDao userDao;
	
	@Autowired
	private AuthService authService;
	
	@Autowired
	private FileStorageDao fileStorageDao;
	
	@Autowired
	private FileController fileController;
	
	@Autowired
	private NotificationReceiverDao notificationReceiverDao;
	
	@Autowired
	private AuditTrailDao auditTrailDao;
	
	private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

	@Override
	public ProfileDto saveUserProfile(BigDecimal usersId, String profileDtoStr, MultipartFile[] files) {
		LOGGER.info("Enter into : saveUserProfile()");
		ProfileDto profileDto = new ProfileDto();
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			objectMapper.registerModule(new JavaTimeModule());
			profileDto = objectMapper.readValue(profileDtoStr, ProfileDto.class);
		} catch (Exception e) {
			e.printStackTrace();
			throw new NotFoundException("Unable to parse json data");
		}
		Boolean result = authService.validateAdmin(usersId);
		if (result == true) {
			if (usersId != null && profileDto != null && profileDto.getFirstName() != null
					&& !profileDto.getFirstName().isEmpty() && profileDto.getRefCompany() != null
					&& profileDto.getRefRoles() != null && profileDto.getRefUser() != null
					&& profileDto.getRefUser().getEmail() != null && !profileDto.getRefUser().getEmail().isEmpty() && profileDto.getEmpId() != null && !profileDto.getEmpId().isEmpty()) {
				Profile profile = new Profile();
				profile = mapper.map(profileDto, Profile.class);

				Users users = new Users();
				Long emailCount = userDao.isEmailUnique(profile.getRefUser().getEmail());
				if (emailCount == 0) {
					users.setEmail(profile.getRefUser().getEmail());

					Long employeeIdCount = userDao.isEmployeeIdUnique(profile.getEmpId());
					if (employeeIdCount == 0) {
						users.setIsDeleted(false);
						users.setActive(profile.getRefUser().getActive());
						users.setMobile(profile.getRefUser().getMobile());
						char[] password = GenerateOTP.generatePassword(8);
						String stringPassword = String.valueOf(password);
						EncryptionUtil encryptionUtil = new EncryptionUtil();
						users.setPassword(encryptionUtil.encode(stringPassword));
						users.setCreatedOn(DateConversionUtil.CurrentUTCTime());
						users.setCreatedBy(usersId);
						users.setResetPassword(false);
						profile.setRefUser(users);
						profile.setIsDeleted(false);
						profile.setCreatedBy(usersId);
						profile.setCreatedOn(DateConversionUtil.CurrentUTCTime());
						profile = userDao.saveUserProfile(usersId, profile);
						
						if(files != null) {
							AttachmentAssociationDto attachmentDto = new AttachmentAssociationDto();
							attachmentDto.setRefUser(profile.getRefUser().getId());
							attachmentDto.setCreatedBy(usersId);
							attachmentDto.setCreatedDate(DateConversionUtil.CurrentUTCTime());
						List<AttachmentAssociationDto> attachmentDtoLst = fileController.uploadMultipleFiles(attachmentDto,files, usersId);
							if(attachmentDtoLst != null) {
								profileDto.setRefattachmentDtoLst(attachmentDtoLst);
								
							}
						}
						
						if (profile.getRefUser().getPassword() != null && !users.getPassword().isEmpty()) {
							sendPasswordByMail(profileDto.getRefUser(), stringPassword);
						} else {
							throw new GlobalException(409, "please provide Email Id");
						}
						profileDto.setId(profile.getId());
						
						AuditTrail auditTrail = new AuditTrail();
						auditTrail.setRefUser(profile.getRefUser().getId());
						auditTrail.setActionBy(usersId);
						auditTrail.setActionDate(DateConversionUtil.CurrentUTCTime());
						auditTrail.setAction(Constants.ActionName.POST);
						String json = null;
						try {
							Gson gson = new Gson();
							json = gson.toJson(profileDto);
						} catch (Exception e) {
							e.printStackTrace();
							throw new NotFoundException("Unable to parse json data");
						}
						auditTrail.setData(json);
						Boolean flag = auditTrailDao.saveAuditTrail(auditTrail);
						LOGGER.info("Exit from : saveUserProfile()");
						return profileDto;
					} else {
						throw new GlobalException(409, "Employee already registered");
					}
				} else {
					throw new GlobalException(409, "Email id already registered");
				}
			}
			throw new GlobalException(500, "Insufficient data");

		} else {
			throw new GlobalException(500, "Unauthorized user");
		}
	}
	
	public Boolean sendPasswordByMail(UsersDto usersDto, String password) {
		LOGGER.info("Enter into : sendPasswordByMail()");
		MailContent mailContent = null;
		boolean isMailSent = false;
		mailContent = emailGenerator.generateEmailContentForPassword(usersDto, password);
		isMailSent = emailSender.sendMail(mailContent);
		if (isMailSent == false) {
			LOGGER.error("forgotPassword");
			throw new GlobalException(409, "please resend password");
		}
		LOGGER.info("Exit from : sendPasswordByMail()");
		return isMailSent;

	}
	
	@Override
	public List<ProfileDto> getAllUsers(BigDecimal userId) {
		LOGGER.info("Enter into : getAllUsers()");
		Boolean result = authService.validateAdmin(userId);
		if (result == true) {
			List<Profile> profileLst = userDao.getAllUsers(userId);
			if (profileLst == null) {
				return null;
			}
			List<ProfileDto> profileDtoLst = new ArrayList<>();
			for (Profile profile : profileLst) {
				ProfileDto profileDto = mapper.map(profile, ProfileDto.class);

				profileDto.setRefUser(profileDto.getRefUser());
				profileDto.setRefRoles(profileDto.getRefRoles());
				profileDto.setRefCompany(profileDto.getRefCompany());
				profileDtoLst.add(profileDto);
			}
			LOGGER.info("Exit from : getAllUsers()");
			return profileDtoLst;
		} else {
			throw new GlobalException(500, "Unauthorized user");
		}
	}

	@Override
	public ProfileDto getUserById(BigDecimal userId, BigDecimal employeId) {
		LOGGER.info("Enter into : getUserById()");
		Boolean result = authService.validateAdmin(userId);
		if (result == true) {
			Profile profile = userDao.getUserById(userId, employeId);
			ProfileDto profileDto = mapper.map(profile, ProfileDto.class);
			
			List<AttachmentAssociationDto> attachmentAssociationDtoLst = getAttachmentList(employeId);
			profileDto.setRefattachmentDtoLst(attachmentAssociationDtoLst);
			LOGGER.info("Exit from : getUserById()");
			return profileDto;
		} else {
			throw new GlobalException(500, "Unauthorized user");
		}
	}
	
	public List<AttachmentAssociationDto> getAttachmentList(BigDecimal employeId){
		List<AttachmentAssociation> attachmentAssociationLst = fileStorageDao.getAttachmentList(Constants.ColumnNames.REF_USER,employeId);
		List<AttachmentAssociationDto> attachmentAssociationDtoLst = new ArrayList<>();
		for(AttachmentAssociation attachmentAssociation :attachmentAssociationLst) {
			
			AttachmentAssociationDto attachmentAssociationDto = mapper.map(attachmentAssociation,AttachmentAssociationDto.class);
			attachmentAssociationDtoLst.add(attachmentAssociationDto);
		}
		return attachmentAssociationDtoLst;
	}

	@Override
	public ProfileDto updateUserProfile(BigDecimal userId, BigDecimal employeId, String profileDtoStr, MultipartFile[] files) {
		LOGGER.info("Enter into : updateUserProfile()");
		ProfileDto profileDto = new ProfileDto();
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			objectMapper.registerModule(new JavaTimeModule());
			profileDto = objectMapper.readValue(profileDtoStr, ProfileDto.class);
		} catch (Exception e) {
			e.printStackTrace();
			throw new NotFoundException("Unable to parse json data");
		}
		Boolean result = authService.validateAdmin(userId);
		if (result != null && result == true) {
			ProfileDto profileInfo = new ProfileDto();
			if (userId != null && employeId != null && profileDto != null && profileDto.getRefCompany() != null
					&& profileDto.getRefCompany().getId() != null && profileDto.getRefUser() != null
					&& profileDto.getRefUser().getEmail() != null && !profileDto.getRefUser().getEmail().isEmpty()
					&& profileDto.getRefRoles() != null && profileDto.getRefRoles().getId() != null) {
				Profile updatedProfile = new Profile();
				Profile previousProfile = userDao.getUserById(userId, employeId);
				updatedProfile = mapper.map(profileDto, Profile.class);
				previousProfile.setDepartement(updatedProfile.getDepartement());
				previousProfile.setDesignation(updatedProfile.getDesignation());
				previousProfile.setFirstName(updatedProfile.getFirstName());
				previousProfile.setLastName(updatedProfile.getLastName());
				previousProfile.setRefCompany(updatedProfile.getRefCompany());
				previousProfile.setRefRoles(updatedProfile.getRefRoles());
				previousProfile.getRefUser().setId(updatedProfile.getRefUser().getId());
				previousProfile.getRefUser().setUpdatedOn(DateConversionUtil.CurrentUTCTime());
				previousProfile.getRefUser().setUpdatedBy(userId);
				previousProfile.setUpdatedBy(userId);
				previousProfile.setUpdatedOn(DateConversionUtil.CurrentUTCTime());
				previousProfile.getRefUser().setMobile(updatedProfile.getRefUser().getMobile());
				previousProfile.getRefUser().setActive(updatedProfile.getRefUser().getActive());
				
				if (profileDto.getRefattachmentDtoLst() != null) {
					List<AttachmentAssociationDto> attachmentAssociationDtoLst = getAttachmentList(profileDto.getRefUser().getId());
					List<BigDecimal> previousFileIdList = new ArrayList<>();
					for (AttachmentAssociationDto previousAttachmentAssociation : attachmentAssociationDtoLst) {
						previousFileIdList.add(previousAttachmentAssociation.getId());
					}
					List<BigDecimal> updatedFileIdList = new ArrayList<>();
					for (AttachmentAssociationDto updatedAttachmentAssociation : profileDto.getRefattachmentDtoLst()) {
						updatedFileIdList.add(updatedAttachmentAssociation.getId());
					}
					previousFileIdList.retainAll(updatedFileIdList);
					String commaSepFileId = null;
					for (BigDecimal fileId : previousFileIdList) {
						if (commaSepFileId == null) {
							commaSepFileId = fileId.toString();
						} else {
							commaSepFileId = commaSepFileId +","+ fileId;
						}
					}
					//if (commaSepFileId != null) {
						LocalDateTime deletedDate = DateConversionUtil.CurrentUTCTime();
						Boolean flag = fileStorageDao.updateExistingFile(deletedDate,commaSepFileId, Constants.ColumnNames.REF_USER, profileDto.getRefUser().getId(),userId);
					//}
				}
				
				Boolean isSameEmail = (previousProfile.getRefUser().getEmail()).equals(updatedProfile.getRefUser().getEmail());
				Boolean isSameEmployeeId = (previousProfile.getEmpId()).equals(updatedProfile.getEmpId());
				
				if(isSameEmployeeId && isSameEmail) {
					updatedProfile = userDao.saveUserProfile(userId, previousProfile);
				}else if(isSameEmployeeId == false && isSameEmail == false) {
						Long employeeIdCount = userDao.isEmployeeIdUnique(updatedProfile.getEmpId());
						if (employeeIdCount == 0) {
							previousProfile.setEmpId(updatedProfile.getEmpId());
							Long emailCount = userDao.isEmailUnique(updatedProfile.getRefUser().getEmail());
							if (emailCount == 0) {
								previousProfile.getRefUser().setEmail(updatedProfile.getRefUser().getEmail());
								updatedProfile = userDao.saveUserProfile(userId, previousProfile);
							}else {
								throw new GlobalException(409, "Email id already registered");
							}
						}else {
							throw new GlobalException(401, "Employee already registered");
						}
					}else if(isSameEmployeeId == true && isSameEmail == false) {
						Long emailCount = userDao.isEmailUnique(updatedProfile.getRefUser().getEmail());
						if (emailCount == 0) {
							previousProfile.getRefUser().setEmail(updatedProfile.getRefUser().getEmail());
							updatedProfile = userDao.saveUserProfile(userId, previousProfile);
						}else {
							throw new GlobalException(409, "Email id already registered");
						}
						
					}else if(isSameEmployeeId == false && isSameEmail == true) {
						Long employeeIdCount = userDao.isEmployeeIdUnique(updatedProfile.getEmpId());
						if (employeeIdCount == 0) {
							previousProfile.setEmpId(updatedProfile.getEmpId());
							updatedProfile = userDao.saveUserProfile(userId, previousProfile);
						}else {
							throw new GlobalException(401, "Employee already registered");
						}
					}		
						profileInfo = mapper.map(updatedProfile, ProfileDto.class);
						
						if(files != null) {
							AttachmentAssociationDto attachmentDto = new AttachmentAssociationDto();
							attachmentDto.setRefUser(employeId);
							attachmentDto.setUpdatedBy(userId);
							attachmentDto.setUpdatedDate(DateConversionUtil.CurrentUTCTime());
						List<AttachmentAssociationDto> attachmentDtoLst = fileController.uploadMultipleFiles(attachmentDto,files, userId);
							if(attachmentDtoLst != null) {
								profileInfo.setRefattachmentDtoLst(attachmentDtoLst);
							}
						}
						AuditTrail auditTrail = new AuditTrail();
						auditTrail.setRefUser(profileDto.getRefUser().getId());
						auditTrail.setActionBy(userId);
						auditTrail.setActionDate(DateConversionUtil.CurrentUTCTime());
						auditTrail.setAction(Constants.ActionName.PUT);
						String json = null;
						try {
							Gson gson = new Gson();
							json = gson.toJson(profileInfo);
						} catch (Exception e) {
							e.printStackTrace();
							throw new NotFoundException("Unable to parse json data");
						}
						auditTrail.setData(json);
						Boolean flag = auditTrailDao.saveAuditTrail(auditTrail);
						LOGGER.info("Exit from : updateUserProfile()");
						return profileInfo;
			} else {
				throw new GlobalException(500, "Insufficient data");
			}
		} else {
			throw new GlobalException(500, "Unauthorized user");
		}
	} 

	@SuppressWarnings("unused")
	@Override
	public Boolean deleteUser(BigDecimal usersId, BigDecimal employeId) {
		LOGGER.info("Enter into : deleteUser()");
		Boolean result = authService.validateAdmin(usersId);
		if (result == true) {
			List<NotificationReceiver> notiRcvrList = notificationReceiverDao.getNotiRcvrListByUserId(Constants.ColumnNames.REF_USER,employeId);
			//Boolean isDeleted = fileStorageDao.deleteAttachmentForCurrentObject(Constants.ColumnNames.REF_ASSET,employeId,usersId);
			Boolean flag = null;
			if(notiRcvrList.isEmpty()) {
				LocalDateTime deletedDate = DateConversionUtil.CurrentUTCTime();
				flag = userDao.deleteUser(usersId, employeId,deletedDate);
				AuditTrail auditTrail = new AuditTrail();
				auditTrail.setRefUser(employeId);
				auditTrail.setActionBy(usersId);
				auditTrail.setActionDate(DateConversionUtil.CurrentUTCTime());
				auditTrail.setAction(Constants.ActionName.DELETE);
				auditTrailDao.saveAuditTrail(auditTrail);
			}
			else {
				throw new GlobalException(409, "This user is used as notification Receiver.");
			}
			LOGGER.info("Exit from : deleteUser()");
			return flag;
		}
		else {
			throw new GlobalException(500, "Unauthorized user");
		}
	}
	
	@Override
	public List<CompanyDto> getAllCompany(BigDecimal userId) {
		LOGGER.info("Enter into : getAllCompany()");
		Boolean result = authService.validateAdminAndManager(userId);
		if(result == true) {
			List<Company> companyLst = userDao.getAllCompany();
			List<CompanyDto> companyDtoLst = new ArrayList<>();
			for(Company company : companyLst) {
				CompanyDto companyDto = mapper.map(company, CompanyDto.class);
				companyDtoLst.add(companyDto);
			}
			LOGGER.info("Exit from : getAllCompany()");
			return companyDtoLst;
		}else {
			throw new GlobalException(500, "Unauthorized user");
		}
		
	}

	@Override
	public CompanyDto getCompanyById(BigDecimal userId, BigDecimal companyId) {
		LOGGER.info("Enter into : getUserById()");
		Boolean result = authService.validateAdmin(userId);
		if(result == true) {
		Company company = userDao.getCompanyById(companyId);
		CompanyDto companyDto = mapper.map(company, CompanyDto.class);
		LOGGER.info("Exit from : getAllCompany()");
		return companyDto;
		}else {
			throw new GlobalException(500, "Unauthorized user");
		}
	}

	@Override
	public List<RolesDto> getAllRoles(BigDecimal userId) {
		LOGGER.info("Enter into : getAllRoles()");
		Boolean result = authService.validateAdminAndManager(userId);
		if(result == true) {
			List<Roles> rolesLst = userDao.getAllRoles();
			List<RolesDto> rolesDtoLst = new ArrayList<>();
			for(Roles roles : rolesLst) {
				RolesDto rolesDto = mapper.map(roles, RolesDto.class);
				rolesDtoLst.add(rolesDto);
		}
		LOGGER.info("Exit from : getAllRoles()");
		return rolesDtoLst;
	}else {
		throw new GlobalException(500, "Unauthorized user");
	}
	}

	@Override
	public RolesDto getRolesById(BigDecimal userId, BigDecimal rolesId) {
		LOGGER.info("Enter into : getRolesById()");
		Boolean result = authService.validateAdmin(userId);
		if(result == true) {
			Roles roles = userDao.getRolesById(rolesId);
			RolesDto rolesDto = mapper.map(roles, RolesDto.class);
			LOGGER.info("Exit from : getRolesById()");
			return rolesDto;
		}else {
			throw new GlobalException(500, "Unauthorized user");
		}
	}

	@Override
	public Boolean deleteUserFileByFileName(List<AttachmentAssociationDto> attachmentDtoLst, BigDecimal userId,
			BigDecimal employeId) {
		LOGGER.info("Enter into : deleteAmcFileByFileName()");
		Boolean result = authService.validateAdminAndManager(userId);
		if (result == true) {
			
			String commaSepList = null;
			for (AttachmentAssociationDto attachmentDto : attachmentDtoLst) {
				if(commaSepList==null) {
					commaSepList = attachmentDto.getFileName();
				}else {
					commaSepList = commaSepList+","+attachmentDto.getFileName();
				}
			}
			if(commaSepList!=null) {
				LocalDateTime deletedDate = DateConversionUtil.CurrentUTCTime();
				String colName = "ref_user";
				Boolean flag = fileStorageDao.deleteFileByFileName(commaSepList,employeId,deletedDate,colName);
				LOGGER.info("Exit from : deleteAmcFileByFileName()");
				return flag;
			}
			
			LOGGER.info("Exit from : deleteAmcFileByFileName()");
			return null;
		}else {
			throw new GlobalException(500, "Unauthorized user");
		}
	}
}
