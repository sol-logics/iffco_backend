/*package com.sollogics.fams.services.serviceImpl;

import java.math.BigDecimal;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sollogics.fams.repositories.dao.NotificationAlertDetailsDao;
import com.sollogics.fams.services.dto.NotificationAlertDetailsDto;
import com.sollogics.fams.services.dto.NotificationConfigDto;
import com.sollogics.fams.services.service.NotificationAlertDetailsService;
import com.sollogics.fams.services.service.NotificationConfigService;

@Service
public class NotificationAlertDetailsServiceImpl implements NotificationAlertDetailsService{
	@Autowired
	private NotificationConfigService notificationConfigService;
	@Autowired
	private NotificationAlertDetailsDao notificationAlertDetailsDao;
	
	@Autowired
	private ModelMapper mapper;
	
	private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

	@Override
	public NotificationAlertDetailsDto saveNotificationAlertDetails(BigDecimal userId,
			NotificationAlertDetailsDto notificationAlertDetailsDto) {
		LOGGER.info("Enter into : saveNotificationAlertDetails()");
		NotificationConfigDto notificationConfig = notificationConfigService.getNotificationConfigByName(notificationAlertDetailsDto.getNotifactionName());
		LOGGER.info("Exit from : saveNotificationAlertDetails()");
		return null;
	}

}
*/