package com.sollogics.fams.services.serviceImpl;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sollogics.fams.exceptionHandler.GlobalException;
import com.sollogics.fams.repositories.dao.NotificationConfigDao;
import com.sollogics.fams.repositories.dao.NotificationReceiverDao;
import com.sollogics.fams.repositories.modal.NotificationConfig;
import com.sollogics.fams.repositories.modal.NotificationMeta;
import com.sollogics.fams.repositories.modal.NotificationReceiver;
import com.sollogics.fams.repositories.modal.Users;
import com.sollogics.fams.services.dto.NotificationConfigDto;
import com.sollogics.fams.services.dto.NotificationMetaDto;
import com.sollogics.fams.services.dto.NotificationReceiverDto;
import com.sollogics.fams.services.service.NotificationConfigService;
import com.sollogics.fams.services.service.UserService;
import com.sollogics.fams.utils.DateConversionUtil;

@Service
public class NotificationConfigServiceImpl implements NotificationConfigService {
	@Autowired
	private ModelMapper mapper;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private NotificationConfigDao notificationConfigDao;
	@Autowired
	private NotificationReceiverDao notificationReceiverDao;
	
	private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

	@Transactional
	@Override
	public NotificationConfigDto saveNotificationConfig(BigDecimal userId,
			NotificationConfigDto notificationConfigDto) {
		LOGGER.info("Enter into : saveNotificationConfig()");
		if(userId !=null && notificationConfigDto != null && notificationConfigDto.getBeforeDays() != null) {
			LOGGER.info("Enter into : saveNotificationConfig()");
			NotificationConfig notificationConfig = new NotificationConfig();
			notificationConfig = mapper.map(notificationConfigDto, NotificationConfig.class);
			notificationConfig.setIsDeleted(false);
			notificationConfig.setCreatedBy(userId);
			notificationConfig.setCreatedDate(DateConversionUtil.CurrentUTCTime());
			NotificationConfig notificationConfigInfo = notificationConfigDao.saveNotificationConfig(notificationConfig);
			notificationConfigInfo.setId(notificationConfigInfo.getId());
			
			List<NotificationReceiverDto > notificationReceiverDtoLst = notificationConfigDto.getNotificationReceiverDtoLst();
			if(notificationReceiverDtoLst != null) {
			List<NotificationReceiver> notificationReceiverLst = new ArrayList<>();
			for(NotificationReceiverDto notificationReceiverDto : notificationReceiverDtoLst) {
				NotificationReceiver notificationReceiver = new NotificationReceiver();
				notificationReceiver = mapper.map(notificationReceiverDto, NotificationReceiver.class);
				NotificationConfig notiConfig = new NotificationConfig();
				notiConfig.setId(notificationConfigInfo.getId());
				notificationReceiver.setRefNotificationConfig(notiConfig);
				Users users = new Users();
				users.setId(notificationReceiver.getRefUser().getId());
				notificationReceiver.setRefUser(users);
				notificationReceiver.setCreatedBy(userId);
				notificationReceiver.setCreatedDate(DateConversionUtil.CurrentUTCTime());
				notificationReceiverLst.add(notificationReceiver);
			}
			List<NotificationReceiver> notificationReceiverLstInfo = notificationConfigDao.saveNotificationReceiver(userId,notificationReceiverLst);
			List<NotificationReceiverDto > notificationReceiverDtoLstInfo = new ArrayList<>();
			for(NotificationReceiver notificationReceiver : notificationReceiverLstInfo) {
				NotificationReceiverDto notificationReceiverDto = mapper.map(notificationReceiver, NotificationReceiverDto.class);
				notificationReceiverDtoLstInfo.add(notificationReceiverDto);
			}
			
			notificationConfigDto.setNotificationReceiverDtoLst(notificationReceiverDtoLstInfo);
			}
			LOGGER.info("Exit from : saveNotificationConfig()");
			return notificationConfigDto;
		}else {
			throw new GlobalException(401, "Insufficient data");
		}
		
	}

	@Override
	public List<NotificationConfigDto> getAllNotificationConfig(BigDecimal userId) {
		LOGGER.info("Enter into : getAllNotificationConfig()");
		List<NotificationConfig> notificationConfigLst = notificationConfigDao.getAllNotificationConfig();
		if(notificationConfigLst == null) {
			return null;
		}
		List<NotificationConfigDto> notificationConfigDtoLst = new ArrayList<>();
		for(NotificationConfig notificationConfig : notificationConfigLst) {
			NotificationConfigDto notificationConfigDto = mapper.map(notificationConfig, NotificationConfigDto.class);
			notificationConfigDto.setRefNotificationMetaDto(notificationConfigDto.getRefNotificationMetaDto());
			NotificationMetaDto notiMetaDto = new NotificationMetaDto();
			notiMetaDto.setId(notificationConfig.getRefNotificationMeta().getId());
			notiMetaDto.setMailBody(notificationConfig.getRefNotificationMeta().getMailBody());
			notiMetaDto.setMailSubject(notificationConfig.getRefNotificationMeta().getMailSubject());
			notiMetaDto.setNotificationName(notificationConfig.getRefNotificationMeta().getNotificationName());
			notificationConfigDto.setRefNotificationMetaDto(notiMetaDto);
			notificationConfigDtoLst.add(notificationConfigDto);
		}
		LOGGER.info("Enter into : getAllNotificationConfig()");
		return notificationConfigDtoLst;
	}

	@Override
	public NotificationConfigDto getNotificationConfigById(BigDecimal userId, BigDecimal notificationConfigId) {
		LOGGER.info("Enter into : getNotificationConfigById()");
		NotificationConfig notificationConfig = notificationConfigDao.getNotificationConfigById(notificationConfigId);
		NotificationConfigDto notificationConfigDto = mapper.map(notificationConfig, NotificationConfigDto.class);
		NotificationMetaDto notiMetaDto = new NotificationMetaDto();
		notiMetaDto.setId(notificationConfig.getRefNotificationMeta().getId());
		notiMetaDto.setMailBody(notificationConfig.getRefNotificationMeta().getMailBody());
		notiMetaDto.setMailSubject(notificationConfig.getRefNotificationMeta().getMailSubject());
		notiMetaDto.setNotificationName(notificationConfig.getRefNotificationMeta().getNotificationName());
		notificationConfigDto.setRefNotificationMetaDto(notiMetaDto);
		List<NotificationReceiver> notiRcvrLst = notificationConfigDao.getNotiRcvrLstByConfigId(notificationConfigId);
		if(notiRcvrLst != null) {
		List<NotificationReceiverDto> notiRcvrDtosLst = new ArrayList<>();
		for(NotificationReceiver notificationReceiver : notiRcvrLst) {
			NotificationReceiverDto notiRcvrDto = mapper.map(notificationReceiver, NotificationReceiverDto.class);
			notiRcvrDto.setrefNotiConfigId(notificationReceiver.getRefNotificationConfig().getId());
			notiRcvrDtosLst.add(notiRcvrDto);
		}
		notificationConfigDto.setNotificationReceiverDtoLst(notiRcvrDtosLst);
		}
		LOGGER.info("Exit from : getNotificationConfigById()");
		return notificationConfigDto;
	}
	
	@Override
	public NotificationConfigDto updateNotificationConfig(BigDecimal userId, BigDecimal notificationConfigId,
			NotificationConfigDto updatedValue) {
		LOGGER.info("Enter into : updateNotificationConfig()");
		NotificationConfigDto notificationConfigInfo = new NotificationConfigDto();
		NotificationConfigDto previousValues = getNotificationConfigById(userId,notificationConfigId);
		previousValues.setRefNotificationMetaDto(updatedValue.getRefNotificationMetaDto());
		previousValues.setAlertIntervals(updatedValue.getAlertIntervals());
		previousValues.setAlertTimes(updatedValue.getAlertTimes());
		previousValues.setBeforeDays(updatedValue.getBeforeDays());
		previousValues.setEndTime(updatedValue.getEndTime());
		previousValues.setStopOnAction(updatedValue.getStopOnAction());
		previousValues.setUpdatedBy(userId);
		previousValues.setUpdatedDate(DateConversionUtil.CurrentUTCTime());
		NotificationConfig notificationConfig = mapper.map(previousValues, NotificationConfig.class);
		NotificationConfig updatednotificationConfig = notificationConfigDao.saveNotificationConfig(notificationConfig);
		
		List<NotificationReceiverDto> NotiReceiverDtoLst =  previousValues.getNotificationReceiverDtoLst();
		List<NotificationReceiver> notiRcvrPreviousLst = new ArrayList<>();
		for(NotificationReceiverDto NotiReceiverDto : NotiReceiverDtoLst) {
			NotificationReceiver notificationReceiver = mapper.map(NotiReceiverDto, NotificationReceiver.class);
			notiRcvrPreviousLst.add(notificationReceiver);
		}
		List<NotificationReceiver> notiRcvrUpdatedLst = new ArrayList<>();
		for(NotificationReceiverDto notificationReceiverDto : updatedValue.getNotificationReceiverDtoLst()) {
			NotificationReceiver notificationReceiver = mapper.map(notificationReceiverDto, NotificationReceiver.class);
			notiRcvrUpdatedLst.add(notificationReceiver);
		}
		List<NotificationReceiver> notiRcvrLst = new ArrayList<>();
		
		for(NotificationReceiver notiRcvrUpdated : notiRcvrUpdatedLst) {
			Boolean found = false;
			for(NotificationReceiver notiRcvrPrevious : notiRcvrPreviousLst) {
				if(notiRcvrUpdated.getId()!=null && (notiRcvrUpdated.getId()).equals(notiRcvrPrevious.getId())) {
					found = true;
					notiRcvrPrevious.setUpdatedBy(userId);
					notiRcvrPrevious.setUpdatedDate(DateConversionUtil.CurrentUTCTime());
					notiRcvrPrevious.setRefUser(notiRcvrUpdated.getRefUser());
					notiRcvrLst.add(notiRcvrPrevious);
					break;
				}
			}
			
			if(found==false) {
				NotificationConfig notiConfig =  new NotificationConfig();
				notiConfig.setId(notificationConfigId);
				notiRcvrUpdated.setRefNotificationConfig(notiConfig);;
				notiRcvrUpdated.setCreatedBy(userId);
				notiRcvrUpdated.setCreatedDate(DateConversionUtil.CurrentUTCTime());
				notiRcvrUpdated.setIsDeleted(false);
				notiRcvrLst.add(notiRcvrUpdated);
			}
		}
		List<NotificationReceiver> notiReceiverLst = new ArrayList<>();
		notiReceiverLst = notificationConfigDao.updateNotificationReceiver(notiRcvrLst);
		String commaSepList = null;
		for (NotificationReceiver notiRcvr : notiReceiverLst) {
			if(commaSepList==null) {
				commaSepList = notiRcvr.getId().toString();
			}else {
				commaSepList = commaSepList+","+notiRcvr.getId().toString();
			}
		}
		if(commaSepList!=null) {
			Boolean flag = notificationConfigDao.deleteNotificationReceiver(commaSepList);
		}
		
		List<NotificationReceiverDto> notiRcvrDtoLst = new ArrayList<>();
		for(NotificationReceiver notiRcvr : notiReceiverLst) {
			NotificationReceiverDto notiRcvrDto = mapper.map(notiRcvr, NotificationReceiverDto.class);
			notiRcvrDtoLst.add(notiRcvrDto);
		}
		notificationConfigInfo = mapper.map(updatednotificationConfig, NotificationConfigDto.class);
		notificationConfigInfo.setNotificationReceiverDtoLst(notiRcvrDtoLst);
	LOGGER.info("Exit from : updateAmc()");
	return notificationConfigInfo;
	}

	@Override
	public Boolean deleteNotificationConfigById(BigDecimal userId, BigDecimal notificationConfigId) {
		LOGGER.info("Enter into : deleteNotificationConfigById()");
		//List<NotificationReceiver> notiRcvrList = notificationReceiverDao.getNotiRcvrListByUserId(Constants.ColumnNames.REF_NOTIFICATION_CONFIG,notificationConfigId);
		LocalDateTime deletedDate = DateConversionUtil.CurrentUTCTime();
		Boolean flag = notificationConfigDao.deleteNotificationConfigById(userId, notificationConfigId, deletedDate);
		/*
		 * Boolean flag = null; if(notiRcvrList == null) {
		 * 
		 * }else { throw new GlobalException(409,
		 * "This configuration is being used in notification Receiver."); }
		 */
		LOGGER.info("Exit from : deleteNotificationConfigById()");
		return flag;
	}

	@Override
	public NotificationConfigDto getNotificationConfigByName(String notifactionName) {
		LOGGER.info("Enter into : getNotificationConfigByName()");
		NotificationConfig notificationConfig = notificationConfigDao.getNotificationConfigById(notifactionName);
		NotificationConfigDto notificationConfigDto = mapper.map(notificationConfig, NotificationConfigDto.class);
		LOGGER.info("Exit from : getNotificationConfigByName()");
		return notificationConfigDto;
	}

	@Override
	public List<NotificationMetaDto> getAllNotificationMeta(BigDecimal userId) {
		LOGGER.info("Enter into : getAllNotificationMeta()");
			List<NotificationMeta> notificationMetaLst = notificationConfigDao.getAllNotificationMeta();
			List<NotificationMetaDto> notificationMetaDtoLst = new ArrayList<>();
			for(NotificationMeta notificationMeta : notificationMetaLst) {
				NotificationMetaDto notificationMetaDto = mapper.map(notificationMeta, NotificationMetaDto.class);
				notificationMetaDtoLst.add(notificationMetaDto);
		}
			LOGGER.info("Exit from : getAllNotificationMeta()");
			return notificationMetaDtoLst;
	}

}
