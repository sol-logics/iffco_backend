package com.sollogics.fams.services.serviceImpl.Attachment;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.sollogics.fams.controllers.Attachment.FileController;
import com.sollogics.fams.exceptionHandler.FileStorageException;
import com.sollogics.fams.exceptionHandler.MyFileNotFoundException;
import com.sollogics.fams.repositories.dao.FileStorageDao;
import com.sollogics.fams.repositories.modal.AttachmentAssociation;
import com.sollogics.fams.services.dto.AttachmentAssociationDto;
import com.sollogics.fams.services.service.Attachment.FileService;
//value = "famsTransactionManager", 
//(readOnly = true)
@Service
@Transactional
public class FileStorageService implements FileService{
	
	private Path fileStorageLocation;
//	private final String amcfileStoragePath = "../../upload/Amc";
//	private final String vendorfileStoragePath = "../../upload/Vendor";
//	private final String userfileStoragePath = "../../upload/User";
//	private final String assetfileStoragePath = "../../upload/Asset";
	
	private final String amcfileStoragePath = "D:/upload/Amc";
	private final String vendorfileStoragePath = "D:/upload/Vendor";
	private final String userfileStoragePath = "D:/upload/User";
	private final String assetfileStoragePath = "D:/upload/Asset";
	
	@Autowired
	private ModelMapper mapper;

	@Autowired
	private FileStorageDao fileStorageDao;
	/*
	 * @Autowired private Mapper mapper;
	 */
	
	/*
	 * @Autowired public FileStorageService(Mapper mapper) { this.mapper = mapper;
	 * this.fileStorageLocation = Paths.get(fileStoragePath)
	 * .toAbsolutePath().normalize();
	 * 
	 * try { Files.createDirectories(this.fileStorageLocation); } catch (Exception
	 * ex) { throw new
	 * FileStorageException("Could not create the directory where the uploaded files will be stored."
	 * , ex); } }
	 */
	
	private static final Logger logger = LoggerFactory.getLogger(FileController.class);
	 @Autowired
	    private Environment environment;
		
		private String getEnvProperty(String key) {
	        String property = environment.getRequiredProperty(key);
	        if (property == null || property.trim().isEmpty() ) {
	        	logger.error("property with key = " + key + " = " + property);
	        }
	        return property != null ? property.trim() : null;
	    }
	    
		
	@Override
	public AttachmentAssociationDto storeFile(AttachmentAssociationDto attachmentDto, MultipartFile file,
			BigDecimal userId) {
		if (attachmentDto.getRefAmc() != null) {
			fileStorageLocation = Paths.get(amcfileStoragePath).toAbsolutePath().normalize();
		}else if(attachmentDto.getRefVendor() != null) {
			fileStorageLocation = Paths.get(vendorfileStoragePath).toAbsolutePath().normalize();
		}else if(attachmentDto.getRefAsset() != null) {
			fileStorageLocation = Paths.get(assetfileStoragePath).toAbsolutePath().normalize();
		}else if(attachmentDto.getRefUser() != null) {
			fileStorageLocation = Paths.get(userfileStoragePath).toAbsolutePath().normalize();
			try {
				Files.createDirectories(fileStorageLocation);
			} catch (Exception ex) {
				throw new FileStorageException(
						"Could not create the directory where the uploaded files will be stored.", ex);
			}
		}
        // Normalize file name
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());
        fileName = fileName.substring(fileName.indexOf("."), fileName.length());
        fileName = System.currentTimeMillis() + fileName;

        try {
            // Check if the file's name contains invalid characters
            if(fileName.contains("..")) {
                throw new FileStorageException("Sorry! Filename contains invalid path sequence " + fileName);
            }

            // Copy file to the target location (Replacing existing file with the same name)
            Path targetLocation = this.fileStorageLocation.resolve(fileName);
            Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);
            String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                    .path("/downloadFile/")
                    .path(fileName)
                    .toUriString();
            String appEnv = getEnvProperty("app.env");
            logger.info("app.env {}", appEnv);
            if(appEnv != null && appEnv.equalsIgnoreCase("prod")) {
            	fileDownloadUri = fileDownloadUri.replaceAll("http", "https");
            	attachmentDto.setFileDownloadUri(fileDownloadUri);
            }else {
            	attachmentDto.setFileDownloadUri(fileDownloadUri);
            }
            attachmentDto.setFileType(file.getContentType());
            attachmentDto.setSize(file.getSize());
            attachmentDto.setFileName(fileName);
            attachmentDto.setPath(targetLocation.toString());
            AttachmentAssociation attachmentAssociation = mapper.map(attachmentDto,AttachmentAssociation.class);
            AttachmentAssociation attachmentAssociationData = fileStorageDao.saveAttachmentDetails(attachmentAssociation);
            AttachmentAssociationDto attachmentResponseDto = mapper.map(attachmentAssociationData,AttachmentAssociationDto.class);
            //attachmentResponseDto.setId(attachmentAssociationData.getId());
           
			/*
			 * return new AttachmentAssociationDto(attachmentResponseDto.getFileName(),
			 * fileDownloadUri, file.getContentType(), file.getSize());
			 */
            return attachmentResponseDto;
        } catch (IOException ex) {
            throw new FileStorageException("Could not store file " + fileName + ". Please try again!", ex);
        }
    }
	

	
	/*
	 * @Override public AttachmentAssociationDto storeFile(AttachmentAssociationDto
	 * attachmentAssociationDto, MultipartFile file, BigDecimal userId) { if
	 * (attachmentAssociationDto.getRefAmc() != null) { fileStorageLocation =
	 * Paths.get(amcfileStoragePath).toAbsolutePath().normalize(); try {
	 * Files.createDirectories(fileStorageLocation); } catch (Exception ex) { throw
	 * new FileStorageException(
	 * "Could not create the directory where the uploaded files will be stored.",
	 * ex); } } // Normalize file name String fileName =
	 * StringUtils.cleanPath(file.getOriginalFilename()); fileName =
	 * fileName.substring(fileName.indexOf("."), fileName.length()); fileName =
	 * System.currentTimeMillis() + fileName;
	 * 
	 * try { // Check if the file's name contains invalid characters
	 * if(fileName.contains("..")) { throw new
	 * FileStorageException("Sorry! Filename contains invalid path sequence " +
	 * fileName); }
	 * 
	 * // Copy file to the target location (Replacing existing file with the same
	 * name) Path targetLocation = this.fileStorageLocation.resolve(fileName);
	 * Files.copy(file.getInputStream(), targetLocation,
	 * StandardCopyOption.REPLACE_EXISTING);
	 * attachmentAssociationDto.setFileName(fileName);
	 * attachmentAssociationDto.setPath(targetLocation.toString());
	 * AttachmentAssociation attachmentAssociation =
	 * mapper.map(attachmentAssociationDto,AttachmentAssociation.class);
	 * AttachmentAssociation attachmentAssociationData =
	 * fileStorageDao.saveAttachmentDetails(attachmentAssociation);
	 * AttachmentAssociationDto attachmentDto =
	 * mapper.map(attachmentAssociationData,AttachmentAssociationDto.class);
	 * attachmentDto.setId(attachmentAssociationData.getId()); return attachmentDto;
	 * } catch (IOException ex) { throw new
	 * FileStorageException("Could not store file " + fileName +
	 * ". Please try again!", ex); } }
	 */
	 

	@Override
	public Resource loadFileAsResource(String fileName) {
        try {
            Path filePath = this.fileStorageLocation.resolve(fileName).normalize();
            Resource resource = new UrlResource(filePath.toUri());
            
            if(resource.exists()) {
                return resource;
            } else {
                throw new MyFileNotFoundException("File not found " + fileName);
            }
        } catch (MalformedURLException ex) {
            throw new MyFileNotFoundException("File not found " + fileName, ex);
        }
    }

}
