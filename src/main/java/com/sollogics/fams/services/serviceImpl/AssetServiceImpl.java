package com.sollogics.fams.services.serviceImpl;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.google.gson.Gson;
import com.sollogics.fams.controllers.Attachment.FileController;
import com.sollogics.fams.exceptionHandler.GlobalException;
import com.sollogics.fams.exceptionHandler.NotFoundException;
import com.sollogics.fams.repositories.dao.AmcDao;
import com.sollogics.fams.repositories.dao.AssetDao;
import com.sollogics.fams.repositories.dao.AuditTrailDao;
import com.sollogics.fams.repositories.dao.FileStorageDao;
import com.sollogics.fams.repositories.modal.Amc;
import com.sollogics.fams.repositories.modal.Asset;
import com.sollogics.fams.repositories.modal.AssetGroup;
import com.sollogics.fams.repositories.modal.AttachmentAssociation;
import com.sollogics.fams.repositories.modal.AuditTrail;
import com.sollogics.fams.repositories.modal.Category;
import com.sollogics.fams.repositories.modal.Location;
import com.sollogics.fams.repositories.modal.Status;
import com.sollogics.fams.repositories.modal.Type;
import com.sollogics.fams.services.dto.AssetDto;
import com.sollogics.fams.services.dto.AssetGroupDto;
import com.sollogics.fams.services.dto.AttachmentAssociationDto;
import com.sollogics.fams.services.dto.CategoryDto;
import com.sollogics.fams.services.dto.LocationDto;
import com.sollogics.fams.services.dto.StatusDto;
import com.sollogics.fams.services.dto.TypeDto;
import com.sollogics.fams.services.service.AssetService;
import com.sollogics.fams.services.service.AuthService;
import com.sollogics.fams.utils.Constants;
import com.sollogics.fams.utils.DateConversionUtil;

@Service
public class AssetServiceImpl implements AssetService{
	
	@Autowired
	private ModelMapper mapper;
	
	@Autowired
	private AuthService authService;
	
	@Autowired
	private AssetDao assetDao;
	
	@Autowired
	private FileController fileController;
	
	@Autowired
	private FileStorageDao fileStorageDao;
	
	@Autowired
	private AmcDao amcDao;
	
	@Autowired
	private AuditTrailDao auditTrailDao;

	private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

	@Override
	public AssetDto saveAsset(BigDecimal userId, String assetDtoStr, MultipartFile[] files) {
		LOGGER.info("Enter into : saveAsset()");
		AssetDto assetDto = new AssetDto();
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			objectMapper.registerModule(new JavaTimeModule());
			assetDto = objectMapper.readValue(assetDtoStr, AssetDto.class);
		} catch (Exception e) {
			e.printStackTrace();
			throw new NotFoundException("Unable to parse json data");
		}
		if (assetDto != null && assetDto.getName() != null && !assetDto.getName().isEmpty()
				&& assetDto.getModelNum() != null && !assetDto.getModelNum().isEmpty()
				&& assetDto.getSerialNum() != null && !assetDto.getSerialNum().isEmpty()) {
			Boolean result = authService.validateAdminAndManager(userId);
			if (result == true) {
				Asset asset = new Asset();
				asset = mapper.map(assetDto, Asset.class);
				Long modelNoCount = assetDao.validateModelNumber(assetDto.getModelNum());
				Long serialNumCount = assetDao.validateSerialNumber(assetDto.getSerialNum());
				if (modelNoCount == 0 && serialNumCount == 0) {
					DateFormat dateFormat = new SimpleDateFormat("yyyy");
					String dt = String.valueOf(dateFormat.format(new Date()));
					Integer latestSNoCode = assetDao.getLatestSNoCode();
					if (latestSNoCode == null) {
						asset.setsNoCount(1);
					} else {
						asset.setsNoCount(latestSNoCode + 1);
					}
					String uniqueCode = Constants.SNumberCode.ASSET_PATTERN + dt + "_" + asset.getsNoCount();
					asset.setUniqueCode(uniqueCode);
					if (assetDto.getRefLocation() != null && assetDto.getRefLocation().getId() != null) {
						Location location = new Location();
						location.setId(assetDto.getRefLocation().getId());
						asset.setRefLocation(location);
					} else {
						asset.setRefLocation(null);
					}
					if (assetDto.getRefCategory() != null && assetDto.getRefCategory().getId() != null) {
						Category category = new Category();
						category.setId(assetDto.getRefCategory().getId());
						asset.setRefCategory(category);
					} else {
						asset.setRefCategory(null);
					}
					if (assetDto.getRefStatus() != null && assetDto.getRefStatus().getId() != null) {
						Status status = new Status();
						status.setId(assetDto.getRefStatus().getId());
						asset.setRefStatus(status);
					} else {
						asset.setRefStatus(null);
					}
					if (assetDto.getRefType() != null && assetDto.getRefType().getId() != null) {
						Type type = new Type();
						type.setId(assetDto.getRefType().getId());
						asset.setRefType(type);
					} else {
						asset.setRefType(null);
					}
					if (assetDto.getRefAssetGroup() != null && assetDto.getRefAssetGroup() != null) {
						AssetGroup assetGroup = new AssetGroup();
						assetGroup.setId(assetDto.getRefAssetGroup().getId());
						asset.setRefAssetGroup(assetGroup);
					} else {
						asset.setRefAssetGroup(null);
					}
					asset.setCreatedBy(userId);
					asset.setIsDeleted(false);
					asset.setCreatedDate(DateConversionUtil.CurrentUTCTime());
					Asset assetInfo = assetDao.saveAsset(asset);
					assetDto.setId(assetInfo.getId());
					if (files != null) {
						AttachmentAssociationDto attachmentDto = new AttachmentAssociationDto();
						attachmentDto.setRefAsset(assetDto.getId());
						attachmentDto.setCreatedBy(userId);
						attachmentDto.setCreatedDate(DateConversionUtil.CurrentUTCTime());
						List<AttachmentAssociationDto> attachmentDtoLst = fileController
								.uploadMultipleFiles(attachmentDto, files, userId);
						if (attachmentDtoLst != null) {
							assetDto.setRefattachmentDtoLst(attachmentDtoLst);
						}
					}
					AuditTrail auditTrail = new AuditTrail();
					auditTrail.setRefAssets(assetDto.getId());
					auditTrail.setActionBy(userId);
					auditTrail.setActionDate(DateConversionUtil.CurrentUTCTime());
					auditTrail.setAction(Constants.ActionName.POST);
					String json = null;
					try {
						Gson gson = new Gson();
						json = gson.toJson(assetDto);
					} catch (Exception e) {
						e.printStackTrace();
						throw new NotFoundException("Unable to parse json data");
					}
					auditTrail.setData(json);
					Boolean flag = auditTrailDao.saveAuditTrail(auditTrail);
					
					LOGGER.info("Exit from : saveAsset()");
				} else if (modelNoCount > 0 && serialNumCount > 0) {
					throw new GlobalException(401, "Serial number and Model number already exist");
				} else if (modelNoCount == 0 && serialNumCount > 0) {
					throw new GlobalException(401, "Serial number already exists");

				} else {
					throw new GlobalException(401, "Model number already exists");
				}
				return assetDto;

			} else {
				throw new GlobalException(500, "Unauthorized user");
			}
		} else {
			throw new GlobalException(401, "Insufficient data");
		}
	}

	@Override
	public List<AssetDto> getAllAssets(BigDecimal userId) {
		LOGGER.info("Enter into : getAllAssets()");
		Boolean result = authService.validateAdminAndManager(userId);
		if(result == true) {
			List<Asset> assetLst = assetDao.getAllAssets();
			List<AssetDto> assetDtoLst = new ArrayList<>();
			for(Asset asset : assetLst) {
				AssetDto assetDto = mapper.map(asset, AssetDto.class);
				assetDtoLst.add(assetDto);
			}
			LOGGER.info("Exit from : getAllAssets()");
			return assetDtoLst;
		}else {
			throw new GlobalException(500, "Unauthorized user");
		}
		
	}

	@Override
	public AssetDto getAssetById(BigDecimal userId, BigDecimal assetId) {
		LOGGER.info("Enter into : getAssetById()");
		Boolean result = authService.validateAdminAndManager(userId);
		if(result == true) {
			Asset asset = assetDao.getAssetById(assetId);
			AssetDto assetDto = mapper.map(asset, AssetDto.class);
			
			List<AttachmentAssociationDto> attachmentAssociationDtoLst = getAttachmentList(assetDto.getId());
			assetDto.setRefattachmentDtoLst(attachmentAssociationDtoLst);
		
			LOGGER.info("Exit from : getAssetById()");
			return assetDto;
		}
		else {
			throw new GlobalException(500, "Unauthorized user");
		}
		
	}
	
	public List<AttachmentAssociationDto> getAttachmentList(BigDecimal amcId){
		List<AttachmentAssociation> attachmentAssociationLst = fileStorageDao.getAttachmentList(Constants.ColumnNames.REF_ASSET,amcId);
		List<AttachmentAssociationDto> attachmentAssociationDtoLst = new ArrayList<>();
		for(AttachmentAssociation attachmentAssociation :attachmentAssociationLst) {
			
			AttachmentAssociationDto attachmentAssociationDto = mapper.map(attachmentAssociation,AttachmentAssociationDto.class);
			attachmentAssociationDtoLst.add(attachmentAssociationDto);
		}
		return attachmentAssociationDtoLst;
	}

	@Override
	public AssetDto updateAsset(BigDecimal userId, BigDecimal assetId, String assetDtoStr, MultipartFile[] files) {
		LOGGER.info("Enter into : updateAsset()");
		AssetDto assetDto = new AssetDto();
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			objectMapper.registerModule(new JavaTimeModule());
			assetDto = objectMapper.readValue(assetDtoStr, AssetDto.class);
		}catch (Exception e) {
			e.printStackTrace();
			throw new NotFoundException("Unable to parse json data");
		}
		Boolean result = authService.validateAdminAndManager(userId);
		if (result == true) {
			AssetDto assetInfo = new AssetDto();
			if (userId != null && assetId != null && assetDto != null) {
				Asset updatedAsset = new Asset();
				Asset previousAsset = assetDao.getAssetById(assetId);
				updatedAsset = mapper.map(assetDto, Asset.class);

				previousAsset.setCapacity(updatedAsset.getCapacity());
				previousAsset.setDepartment(updatedAsset.getDepartment());
				previousAsset.setDescription(updatedAsset.getDescription());
				previousAsset.setManufacturer(updatedAsset.getManufacturer());
				previousAsset.setRefAssetGroup(updatedAsset.getRefAssetGroup());
				// previousAsset.setModelNum(updatedAsset.getModelNum());
				previousAsset.setName(updatedAsset.getName());
				previousAsset.setPurchaseDate(updatedAsset.getPurchaseDate());
				previousAsset.setPurchasePrice(updatedAsset.getPurchasePrice());
				// previousAsset.setSerialNum(updatedAsset.getSerialNum());
				previousAsset.setSubComponent(updatedAsset.getSubComponent());
				previousAsset.setUom(updatedAsset.getUom());
				previousAsset.setUsefulLife(updatedAsset.getUsefulLife());
				previousAsset.setWarrantyExpiryDate(updatedAsset.getWarrantyExpiryDate());
				previousAsset.setRefCategory(updatedAsset.getRefCategory());
				previousAsset.setRefLocation(updatedAsset.getRefLocation());
				previousAsset.setRefStatus(updatedAsset.getRefStatus());
				previousAsset.setRefType(updatedAsset.getRefType());
				previousAsset.setUpdatedBy(userId);
				previousAsset.setUpdatedDate(DateConversionUtil.CurrentUTCTime());
				
				if (assetDto.getRefattachmentDtoLst() != null) {
					List<AttachmentAssociationDto> attachmentAssociationDtoLst = getAttachmentList(assetDto.getId());
					List<BigDecimal> previousFileIdList = new ArrayList<>();
					for (AttachmentAssociationDto previousAttachmentAssociation : attachmentAssociationDtoLst) {
						previousFileIdList.add(previousAttachmentAssociation.getId());
					}
					List<BigDecimal> updatedFileIdList = new ArrayList<>();
					for (AttachmentAssociationDto updatedAttachmentAssociation : assetDto.getRefattachmentDtoLst()) {
						updatedFileIdList.add(updatedAttachmentAssociation.getId());
					}
					previousFileIdList.retainAll(updatedFileIdList);
					String commaSepFileId = null;
					for (BigDecimal fileId : previousFileIdList) {
						if (commaSepFileId == null) {
							commaSepFileId = fileId.toString();
						} else {
							commaSepFileId = commaSepFileId +","+ fileId;
						}
					}
						LocalDateTime deletedDate = DateConversionUtil.CurrentUTCTime();
						Boolean flag = fileStorageDao.updateExistingFile(deletedDate,commaSepFileId, Constants.ColumnNames.REF_ASSET, assetDto.getId(),userId);
				}
				Boolean isSameModelNo = (previousAsset.getModelNum()).equals(updatedAsset.getModelNum());
				Boolean isSameSerialNo = (previousAsset.getSerialNum()).equals(updatedAsset.getSerialNum());
				if (isSameModelNo && isSameSerialNo) {
					updatedAsset = assetDao.saveAsset(previousAsset);
				} else {
					if (isSameModelNo == false && isSameSerialNo == false) {
						Long modelNoCount = assetDao.validateModelNumber(updatedAsset.getModelNum());
						Long serialNumCount = assetDao.validateSerialNumber(assetDto.getSerialNum());
						if (modelNoCount == 0 && serialNumCount == 0) {
							previousAsset.setModelNum(updatedAsset.getModelNum());
							previousAsset.setSerialNum(updatedAsset.getSerialNum());
							updatedAsset = assetDao.saveAsset(previousAsset);
						} else if(modelNoCount > 0 && serialNumCount == 0){
							throw new GlobalException(401, "Model number already exists");
						}else if(modelNoCount == 0 && serialNumCount > 0) {
							throw new GlobalException(401, "Serial number already exists");
						}else {
							throw new GlobalException(401, "Serial number and Model number already exist");
						}
					} else if (isSameSerialNo == false && isSameModelNo == true) {
						Long serialNumCount = assetDao.validateSerialNumber(assetDto.getSerialNum());
						if (serialNumCount == 0) {
							previousAsset.setSerialNum(updatedAsset.getSerialNum());
							updatedAsset = assetDao.saveAsset(previousAsset);
						} else {
							throw new GlobalException(401, "Serial number already exists");
						}
					}else{
						Long modelNoCount = assetDao.validateModelNumber(updatedAsset.getModelNum());
						if (modelNoCount == 0) {
							previousAsset.setModelNum(updatedAsset.getModelNum());
							updatedAsset = assetDao.saveAsset(previousAsset);
						}else {
							throw new GlobalException(401, "Model number already exists");
						}
					}
				}
				assetInfo = mapper.map(updatedAsset, AssetDto.class);
				
				if(files != null) {
					//AttachmentAssociationDto attachmentDto = new AttachmentAssociationDto(null, null, null, 0);
					AttachmentAssociationDto attachmentDto = new AttachmentAssociationDto();
					attachmentDto.setRefAsset(assetDto.getId());
					attachmentDto.setUpdatedBy(userId);
					attachmentDto.setUpdatedDate(DateConversionUtil.CurrentUTCTime());
				List<AttachmentAssociationDto> attachmentDtoLst = fileController.uploadMultipleFiles(attachmentDto,files, userId);
					if(attachmentDtoLst != null) {
						assetInfo.setRefattachmentDtoLst(attachmentDtoLst);
						
					}
				}
				AuditTrail auditTrail = new AuditTrail();
				auditTrail.setRefAssets(assetDto.getId());
				auditTrail.setActionBy(userId);
				auditTrail.setActionDate(DateConversionUtil.CurrentUTCTime());
				auditTrail.setAction(Constants.ActionName.PUT);
				String json = null;
				try {
					Gson gson = new Gson();
					json = gson.toJson(assetInfo);
				} catch (Exception e) {
					e.printStackTrace();
					throw new NotFoundException("Unable to parse json data");
				}
				auditTrail.setData(json);
				Boolean flag = auditTrailDao.saveAuditTrail(auditTrail);
				LOGGER.info("Exit from : getAssetById()");

			} else {
				LOGGER.error("Insufficient data");
			}
			return assetInfo;
		} else {
			throw new GlobalException(500, "Unauthorized user");
		}
	}

	@SuppressWarnings("unused")
	@Override
	public Boolean deleteAssetById(BigDecimal userId, BigDecimal assetId) {
		LOGGER.info("Enter into : deleteAssetById()");
		Boolean result = authService.validateAdminAndManager(userId);
		if (result == true) {
			List<Amc> amcLst = amcDao.getAmcListForCurrentObject(Constants.ColumnNames.REF_ASSET,assetId);
			//Boolean isDeleted = fileStorageDao.deleteAttachmentForCurrentObject(Constants.ColumnNames.REF_ASSET,assetId,userId);
			Boolean flag = null;
			if(amcLst.isEmpty()) {
				LocalDateTime deletedDate = DateConversionUtil.CurrentUTCTime();
				flag = assetDao.deleteAssetById(userId,assetId,deletedDate);
				AuditTrail auditTrail = new AuditTrail();
				auditTrail.setRefAssets(assetId);
				auditTrail.setActionBy(userId);
				auditTrail.setActionDate(DateConversionUtil.CurrentUTCTime());
				auditTrail.setAction(Constants.ActionName.DELETE);
				auditTrailDao.saveAuditTrail(auditTrail);
			}
			else {
				throw new GlobalException(409, "This asset is associated with Amc.");
			}
			LOGGER.info("Exit from : deleteAssetById()");
			return flag;
		}else {
			throw new GlobalException(500, "Unauthorized user");
		}
	}

	@Override
	public AssetGroupDto saveAssetGroup(BigDecimal userId, AssetGroupDto assetGroupDto) {
		if (userId != null && assetGroupDto != null && assetGroupDto.getName() != null
				&& !assetGroupDto.getName().isEmpty() && assetGroupDto.getDescription() != null
				&& !assetGroupDto.getDescription().isEmpty()) {
			LOGGER.info("Enter into : saveAssetGroup()");
			Boolean result = authService.validateAdminAndManager(userId);
			if (result == true) {
				AssetGroup assetGroup = new AssetGroup();
				assetGroup = mapper.map(assetGroupDto, AssetGroup.class);
				AssetGroup assetGroupInfo = assetDao.saveAssetGroup(assetGroup);
				assetGroupDto.setId(assetGroupInfo.getId());
				LOGGER.info("Exit from : saveAssetGroup()");
			}
			return assetGroupDto;
		} else {
			throw new GlobalException(401, "Insufficient data");
		}
	}

	@Override
	public List<AssetGroupDto> getAllAssetGroup(BigDecimal userId) {
		LOGGER.info("Enter into : getAllAssetGroup()");
		Boolean result = authService.validateAdminAndManager(userId);
		if(result == true) {
			List<AssetGroup> asseGrouptLst = assetDao.getAllAssetGroup();
			List<AssetGroupDto> assetGroupDtoLst = new ArrayList<>();
			for(AssetGroup asset : asseGrouptLst) {
				AssetGroupDto assetGroupDto = mapper.map(asset, AssetGroupDto.class);
				assetGroupDtoLst.add(assetGroupDto);
		}
		LOGGER.info("Exit from : getAllAssetGroup()");
		return assetGroupDtoLst;
	}else {
		throw new GlobalException(500, "Unauthorized user");
	}
	}

	@Override
	public AssetGroupDto getAssetGroupById(BigDecimal userId, BigDecimal assetGroupId) {
		LOGGER.info("Enter into : getAssetGroupById()");
		Boolean result = authService.validateAdminAndManager(userId);
		if(result == true) {
			AssetGroup assetGroup = assetDao.getAssetGroupById(assetGroupId);
			AssetGroupDto assetGroupDto = mapper.map(assetGroup, AssetGroupDto.class);
			LOGGER.info("Exit from : getAssetGroupById()");
			return assetGroupDto;
		}else {
			throw new GlobalException(500, "Unauthorized user");
		}
	}

	@Override
	public AssetGroupDto updateAssetGroup(BigDecimal userId, BigDecimal assetGroupId, AssetGroupDto assetGroupDto) {
		LOGGER.info("Enter into : updateAssetGroup()");
		Boolean result = authService.validateAdminAndManager(userId);
		if(result == true) {
			AssetGroupDto assetGroupInfo = new AssetGroupDto();
			if(assetGroupId != null && assetGroupDto.getName() != null && !assetGroupDto.getName().isEmpty() && assetGroupDto.getDescription() != null ) {
				AssetGroup updatedAssetGroup = new AssetGroup();
				AssetGroup previousAsset = assetDao.getAssetGroupById(assetGroupId);
				updatedAssetGroup = mapper.map(assetGroupDto, AssetGroup.class);
				previousAsset.setDescription(updatedAssetGroup.getDescription());
				previousAsset.setName(updatedAssetGroup.getName());
				previousAsset.setUpdatedBy(userId);
				previousAsset.setUpdatedDate(DateConversionUtil.CurrentUTCTime());
				updatedAssetGroup = assetDao.saveAssetGroup(previousAsset);
				assetGroupInfo = mapper.map(updatedAssetGroup, AssetGroupDto.class);
				LOGGER.info("Exit from : updateAssetGroup()");
			}else {
				LOGGER.error("Insufficient data");
			}
			return assetGroupInfo;
		}
		 else {
				throw new GlobalException(500, "Unauthorized user");
			}
	}

	@SuppressWarnings("unused")
	@Override
	public Boolean deleteAssetGroupById(BigDecimal userId, BigDecimal assetGroupId) {
		LOGGER.info("Enter into : deleteAssetGroupById()");
		Boolean result = authService.validateAdminAndManager(userId);
		if(result == true) {
			List<Asset> assetList = assetDao.getAssetListForCurrentAssetGroup(assetGroupId);
			int assetListSize = assetList.size();
			Boolean flag;
			if(assetList == null) {
				LocalDateTime deletedDate = DateConversionUtil.CurrentUTCTime();
				flag = assetDao.deleteAssetGroupById(userId,assetGroupId,deletedDate);
			}else {
				throw new GlobalException(409, assetListSize+" asset(s) is/are associated with this assetGroup.");
			}
			
			LOGGER.info("Exit from : deleteAssetGroupById()");
			return flag;
		}else {
			throw new GlobalException(500, "Unauthorized user");
		}
	}

	@Override
	public List<CategoryDto> getAllCategory(BigDecimal userId) {
		LOGGER.info("Enter into : getAllCategory()");
		Boolean result = authService.validateAdminAndManager(userId);
		if(result == true) {
			List<Category> categoryLst = assetDao.getAllCategory();
			List<CategoryDto> categoryDtoLst = new ArrayList<>();
			for(Category category : categoryLst) {
				CategoryDto categoryDto = mapper.map(category, CategoryDto.class);
				categoryDtoLst.add(categoryDto);
		}
		LOGGER.info("Exit from : getAllCategory()");
		return categoryDtoLst;
	}else {
		throw new GlobalException(500, "Unauthorized user");
	}
	}

	@Override
	public List<StatusDto> getAllStatus(BigDecimal userId) {
		LOGGER.info("Enter into : getAllStatus()");
		Boolean result = authService.validateAdminAndManager(userId);
		if(result == true) {
			List<Status> statusLst = assetDao.getAllStatus();
			List<StatusDto> statusDtoLst = new ArrayList<>();
			for(Status status : statusLst) {
				StatusDto statusDto = mapper.map(status, StatusDto.class);
				statusDtoLst.add(statusDto);
		}
		LOGGER.info("Exit from : getAllStatus()");
		return statusDtoLst;
	}else {
		throw new GlobalException(500, "Unauthorized user");
	}
	}

	@Override
	public List<LocationDto> getAllLocation(BigDecimal userId) {
		LOGGER.info("Enter into : getAllLocation()");
		Boolean result = authService.validateAdminAndManager(userId);
		if(result == true) {
			List<Location> locationLst = assetDao.getAllLocation();
			List<LocationDto> locationDtoLst = new ArrayList<>();
			for(Location location : locationLst) {
				LocationDto locationDto = mapper.map(location, LocationDto.class);
				locationDtoLst.add(locationDto);
		}
		LOGGER.info("Exit from : getAllLocation()");
		return locationDtoLst;
	}else {
		throw new GlobalException(500, "Unauthorized user");
	}
	}

	@Override
	public List<TypeDto> getAllType(BigDecimal userId) {
		LOGGER.info("Enter into : getAllLocation()");
		Boolean result = authService.validateAdminAndManager(userId);
		if(result == true) {
			List<Type> typeLst = assetDao.getAllType();
			List<TypeDto> typeDtoLst = new ArrayList<>();
			for(Type type : typeLst) {
				TypeDto typeDto = mapper.map(type, TypeDto.class);
				typeDtoLst.add(typeDto);
		}
		LOGGER.info("Exit from : getAllLocation()");
		return typeDtoLst;
	}else {
		throw new GlobalException(500, "Unauthorized user");
	}
	}

	@Override
	public Boolean deleteAssetFileByFileName(List<AttachmentAssociationDto> attachmentDtoLst, BigDecimal userId,
			BigDecimal amcId) {

		LOGGER.info("Enter into : deleteAssetFileByFileName()");
		Boolean result = authService.validateAdminAndManager(userId);
		if (result == true) {
			
			String commaSepList = null;
			for (AttachmentAssociationDto attachmentDto : attachmentDtoLst) {
				if(commaSepList==null) {
					commaSepList = attachmentDto.getFileName();
				}else {
					commaSepList = commaSepList+","+attachmentDto.getFileName();
				}
			}
			if(commaSepList!=null) {
				LocalDateTime deletedDate = DateConversionUtil.CurrentUTCTime();
				String colName = "ref_asset";
				Boolean flag = fileStorageDao.deleteFileByFileName(commaSepList,amcId,deletedDate,colName);
				LOGGER.info("Exit from : deleteAssetFileByFileName()");
				return flag;
			}
			
			LOGGER.info("Exit from : deleteAssetFileByFileName()");
			return null;
		}else {
			throw new GlobalException(500, "Unauthorized user");
		}
	
	}

	@Override
	public CategoryDto saveCategory(BigDecimal userId, CategoryDto categoryDto) {
		if (userId != null && categoryDto != null && categoryDto.getName() != null) {
			LOGGER.info("Enter into : saveCategory()");
			Boolean result = authService.validateAdminAndManager(userId);
			if (result == true) {
				Category category = new Category();
				category = mapper.map(categoryDto, Category.class);
				Category categoryInfo = assetDao.saveCategory(category);
				categoryDto.setId(categoryInfo.getId());
				LOGGER.info("Exit from : saveCategory()");
			}
			return categoryDto;
		} else {
			throw new GlobalException(401, "Insufficient data");
		}
	}

	@Override
	public LocationDto saveLocation(BigDecimal userId, LocationDto locationDto) {
		if (userId != null && locationDto != null) {
			LOGGER.info("Enter into : saveLocation()");
			Boolean result = authService.validateAdminAndManager(userId);
			if (result == true) {
				Location location = new Location();
				location = mapper.map(locationDto, Location.class);
				Location locationInfo = assetDao.saveLocation(location);
				locationDto.setId(locationInfo.getId());
				LOGGER.info("Exit from : saveLocation()");
			}
			return locationDto;
		} else {
			throw new GlobalException(401, "Insufficient data");
		}
	}

	@Override
	public TypeDto saveType(BigDecimal userId, TypeDto typeDto) {
		if (userId != null && typeDto != null && typeDto.getName() != null) {
			LOGGER.info("Enter into : saveType()");
			Boolean result = authService.validateAdminAndManager(userId);
			if (result == true) {
				Type type = new Type();
				type = mapper.map(typeDto, Type.class);
				Type typeInfo = assetDao.saveType(type);
				typeDto.setId(typeInfo.getId());
				LOGGER.info("Exit from : saveType()");
			}
			return typeDto;
		} else {
			throw new GlobalException(401, "Insufficient data");
		}
	}
	
	
	
}