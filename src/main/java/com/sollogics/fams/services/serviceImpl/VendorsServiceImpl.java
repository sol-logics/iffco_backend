package com.sollogics.fams.services.serviceImpl;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.google.gson.Gson;
import com.sollogics.fams.controllers.Attachment.FileController;
import com.sollogics.fams.exceptionHandler.GlobalException;
import com.sollogics.fams.exceptionHandler.NotFoundException;
import com.sollogics.fams.repositories.dao.AmcDao;
import com.sollogics.fams.repositories.dao.AuditTrailDao;
import com.sollogics.fams.repositories.dao.FileStorageDao;
import com.sollogics.fams.repositories.dao.VendorsDao;
import com.sollogics.fams.repositories.modal.Amc;
import com.sollogics.fams.repositories.modal.AttachmentAssociation;
import com.sollogics.fams.repositories.modal.AuditTrail;
import com.sollogics.fams.repositories.modal.Vendors;
import com.sollogics.fams.services.dto.AttachmentAssociationDto;
import com.sollogics.fams.services.dto.VendorsDto;
import com.sollogics.fams.services.service.AuthService;
import com.sollogics.fams.services.service.VendorsService;
import com.sollogics.fams.utils.Constants;
import com.sollogics.fams.utils.DateConversionUtil;

@Service
public class VendorsServiceImpl implements VendorsService {
	
	@Autowired
	private ModelMapper mapper;
	
	@Autowired
	private VendorsDao vendorsDao;
	
	@Autowired
	private AuthService authService;
	
	@Autowired
	private FileController fileController;
	
	@Autowired
	private FileStorageDao fileStorageDao;
	
	@Autowired
	private AmcDao amcDao;
	
	@Autowired
	private AuditTrailDao auditTrailDao;
	
	private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

	@Override
	public VendorsDto saveVendor(BigDecimal userId, String vendorsDtoStr, MultipartFile[] files) {

		LOGGER.info("Enter into : saveVendor()");

		VendorsDto vendorsDto = new VendorsDto();
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			objectMapper.registerModule(new JavaTimeModule());
			vendorsDto = objectMapper.readValue(vendorsDtoStr, VendorsDto.class);
		} catch (Exception e) {
			e.printStackTrace();
			throw new NotFoundException("Unable to parse json data");
		}

		if (userId != null && vendorsDto != null && vendorsDto.getName() != null && !vendorsDto.getName().isEmpty()
				&& vendorsDto.getContactPerson() != null && !vendorsDto.getContactPerson().isEmpty()
				&& vendorsDto.getGstin() != null && !vendorsDto.getGstin().isEmpty()
				&& vendorsDto.getMobileNumber() != null && !vendorsDto.getMobileNumber().isEmpty()) {
			Boolean result = authService.validateAdminAndManager(userId);
			if (result == true) {
				Vendors vendors = new Vendors();
				vendors = mapper.map(vendorsDto, Vendors.class);
				Long emailId = vendorsDao.validateEmailId(vendorsDto.getEmail());
				if (emailId == 0) {
					Long gstin = vendorsDao.validateGstinNo(vendorsDto.getGstin());
					if (gstin == 0) {
						vendors.setEmail(vendorsDto.getEmail());
						vendors.setCreatedBy(userId);
						vendors.setCreatedDate(DateConversionUtil.CurrentUTCTime());
						vendors.setIsDeleted(false);
						vendors.setDiscontinue(false);
						Vendors vendorsInfo = vendorsDao.saveVendor(vendors);
						vendorsDto.setId(vendorsInfo.getId());

						if(files != null) {
							AttachmentAssociationDto attachmentDto = new AttachmentAssociationDto();
							attachmentDto.setRefVendor(vendorsDto.getId());
							attachmentDto.setCreatedBy(userId);
							attachmentDto.setCreatedDate(DateConversionUtil.CurrentUTCTime());
						List<AttachmentAssociationDto> attachmentDtoLst = fileController.uploadMultipleFiles(attachmentDto,files, userId);
							if(attachmentDtoLst != null) {
								vendorsDto.setRefattachmentDtoLst(attachmentDtoLst);
							}
						}
						AuditTrail auditTrail = new AuditTrail();
						auditTrail.setRefVendor(vendorsDto.getId());
						auditTrail.setActionBy(userId);
						auditTrail.setActionDate(DateConversionUtil.CurrentUTCTime());
						auditTrail.setAction(Constants.ActionName.POST);
						String json = null;
						try {
							Gson gson = new Gson();
							json = gson.toJson(vendorsDto);
						} catch (Exception e) {
							e.printStackTrace();
							throw new NotFoundException("Unable to parse json data");
						}
						auditTrail.setData(json);
						Boolean flag = auditTrailDao.saveAuditTrail(auditTrail);
						LOGGER.info("Exit from : saveVendor()");
						return vendorsDto;
					} else {
						throw new GlobalException(401, "GSTIN already exists");
					}
				} else {
					throw new GlobalException(401, "Email Id already registered");
				}
			} else {
				throw new GlobalException(500, "Unauthorized user");
			}
		} else {
			throw new GlobalException(401, "Insufficient data");
		}
	}
			

	@Override
	public List<VendorsDto> getAllVendors(BigDecimal userId) {
		LOGGER.info("Enter from : getAllVendors()");
		Boolean result = authService.validateAdminAndManager(userId);
		if(result == true) {
		List<Vendors> vendorLst = vendorsDao.getAllVendors(userId);
		List<VendorsDto> vendorDtoLst = new ArrayList<VendorsDto>();
		for(Vendors vendors : vendorLst) {
			VendorsDto vendorsDto = mapper.map(vendors, VendorsDto.class);
			vendorDtoLst.add(vendorsDto);
		}
		LOGGER.info("Exit from : getAllVendors()");
		return vendorDtoLst;
		}else {
			throw new GlobalException(500, "Unauthorized user");
		}
	}

	@Override
	public VendorsDto getvendorById(BigDecimal userId, BigDecimal vendorId) {
		LOGGER.info("Enter from : getvendorById()");
		Boolean result = authService.validateAdminAndManager(userId);
		if(result == true) {
		Vendors vendors = vendorsDao.getvendorById(userId,vendorId);
		VendorsDto vendorsDto = mapper.map(vendors, VendorsDto.class);
		List<AttachmentAssociationDto> attachmentAssociationDtoLst = getAttachmentList(vendorsDto.getId());
			vendorsDto.setRefattachmentDtoLst(attachmentAssociationDtoLst);
		
		LOGGER.info("Exit from : getvendorById()");
		return vendorsDto;
		}else {
			throw new GlobalException(500, "Unauthorized user");
		}
	}
	
	public List<AttachmentAssociationDto> getAttachmentList(BigDecimal amcId){
		List<AttachmentAssociation> attachmentAssociationLst = fileStorageDao.getAttachmentList(Constants.ColumnNames.REF_VENDOR,amcId);
		List<AttachmentAssociationDto> attachmentAssociationDtoLst = new ArrayList<>();
		for(AttachmentAssociation attachmentAssociation :attachmentAssociationLst) {
			
			AttachmentAssociationDto attachmentAssociationDto = mapper.map(attachmentAssociation,AttachmentAssociationDto.class);
			attachmentAssociationDtoLst.add(attachmentAssociationDto);
		}
		return attachmentAssociationDtoLst;
	}

	@Override
	public VendorsDto updateVendors(BigDecimal userId, BigDecimal vendorId, String vendorsDtoStr,MultipartFile[] files) {
		LOGGER.info("Enter from : updateVendors()");
		VendorsDto vendorsDto = new VendorsDto();
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			objectMapper.registerModule(new JavaTimeModule());
			vendorsDto = objectMapper.readValue(vendorsDtoStr, VendorsDto.class);
		} catch (Exception e) {
			e.printStackTrace();
			throw new NotFoundException("Unable to parse json data");
		}
		if (userId != null && vendorsDto != null) {
			Boolean result = authService.validateAdminAndManager(userId);
			if (result == true) {
				Vendors previousVendor = vendorsDao.getvendorById(userId, vendorId);
				Vendors updatedVendor = new Vendors();
				updatedVendor = mapper.map(vendorsDto, Vendors.class);
				if (vendorsDto.getDiscontinue() == true) {
					previousVendor.setDiscontinue(updatedVendor.getDiscontinue());
					previousVendor.setDiscontinueBy(userId);
					previousVendor.setDiscontinueOn(DateConversionUtil.CurrentUTCTime());
				}
				previousVendor.setName(updatedVendor.getName());
				previousVendor.setContactPerson(updatedVendor.getContactPerson());
				previousVendor.setWebsite(updatedVendor.getWebsite());
				previousVendor.setAddress(updatedVendor.getAddress());
				previousVendor.setDescription(updatedVendor.getDescription());
				previousVendor.setDesignation(updatedVendor.getDesignation());
				previousVendor.setLandline(updatedVendor.getLandline());
				previousVendor.setMobileNumber(updatedVendor.getMobileNumber());
				previousVendor.setUpdatedDate(DateConversionUtil.CurrentUTCTime());
				previousVendor.setUpdatedBy(userId);
				if(vendorsDto.getRefattachmentDtoLst() != null) {
					List<AttachmentAssociationDto> attachmentAssociationDtoLst = getAttachmentList(vendorsDto.getId());
					List<BigDecimal> previousFileIdList = new ArrayList<>();
					for(AttachmentAssociationDto previousAttachmentAssociation : attachmentAssociationDtoLst) {
						previousFileIdList.add(previousAttachmentAssociation.getId());
					}
					List<BigDecimal> updatedFileIdList = new ArrayList<>();
					for(AttachmentAssociationDto updatedAttachmentAssociation : vendorsDto.getRefattachmentDtoLst()) {
						updatedFileIdList.add(updatedAttachmentAssociation.getId());
					}
					previousFileIdList.retainAll(updatedFileIdList);
					String commaSepFileId = null;
					for (BigDecimal fileId : previousFileIdList) {
						if (commaSepFileId == null) {
							commaSepFileId = fileId.toString();
						} else {
							commaSepFileId = commaSepFileId +","+ fileId;
						}
					}
					//if(commaSepFileId!=null) {
						LocalDateTime deletedDate = DateConversionUtil.CurrentUTCTime();
						Boolean flag = fileStorageDao.updateExistingFile(deletedDate,commaSepFileId,Constants.ColumnNames.REF_VENDOR,vendorsDto.getId(),userId);
					//}
					}
				Boolean isSameEmailId = (previousVendor.getEmail()).equals(updatedVendor.getEmail());
				Boolean isSameGstin = (previousVendor.getGstin()).equals(updatedVendor.getGstin());
				if (isSameEmailId && isSameGstin) {
					updatedVendor = vendorsDao.saveVendor(previousVendor);
				} else {
					if (isSameEmailId == false) {
						Long emailId = vendorsDao.validateEmailId(updatedVendor.getEmail());
						if (emailId == 0) {
							previousVendor.setEmail(updatedVendor.getEmail());
							updatedVendor = vendorsDao.saveVendor(previousVendor);
						} else {
							throw new GlobalException(401, "Email Id already registered");
						}
					} else if (isSameGstin == false) {
						Long gstin = vendorsDao.validateGstinNo(updatedVendor.getGstin());
						if (gstin == 0) {
							previousVendor.setGstin(updatedVendor.getGstin());
							updatedVendor = vendorsDao.saveVendor(previousVendor);
						} else {
							throw new GlobalException(401, "GSTIN already exists");
						}
					}
				}
				VendorsDto vendorsInfo = mapper.map(updatedVendor, VendorsDto.class);
				if(files != null) {
					AttachmentAssociationDto attachmentDto = new AttachmentAssociationDto();
					attachmentDto.setRefVendor(vendorsDto.getId());
					attachmentDto.setUpdatedBy(userId);
					attachmentDto.setUpdatedDate(DateConversionUtil.CurrentUTCTime());
				List<AttachmentAssociationDto> attachmentDtoLst = fileController.uploadMultipleFiles(attachmentDto,files, userId);
					if(attachmentDtoLst != null) {
						vendorsInfo.setRefattachmentDtoLst(attachmentDtoLst);
						
					}
				}
				AuditTrail auditTrail = new AuditTrail();
				auditTrail.setRefVendor(vendorsDto.getId());
				auditTrail.setActionBy(userId);
				auditTrail.setActionDate(DateConversionUtil.CurrentUTCTime());
				auditTrail.setAction(Constants.ActionName.PUT);
				String json = null;
				try {
					Gson gson = new Gson();
					json = gson.toJson(vendorsInfo);
				} catch (Exception e) {
					e.printStackTrace();
					throw new NotFoundException("Unable to parse json data");
				}
				auditTrail.setData(json);
				Boolean flag = auditTrailDao.saveAuditTrail(auditTrail);
				LOGGER.info("Exit from : updateVendors()");
				return vendorsInfo;
			} else {
				throw new GlobalException(500, "Unauthorized user");
			}
		} else {
			throw new GlobalException(409, "Application User Object can not be empty");
		}
	}

	@Transactional
	@SuppressWarnings("unused")
	@Override
	public Boolean deleteVendorById(BigDecimal userId, BigDecimal vendorId) {
		LOGGER.info("Enter from : deleteVendorById()");
		Boolean result = authService.validateAdminAndManager(userId);
		if(result == true) {
			List<Amc> amcLst = amcDao.getAmcListForCurrentObject(Constants.ColumnNames.REF_VENDOR,vendorId);
			//Boolean isDeleted = fileStorageDao.deleteAttachmentForCurrentObject(Constants.ColumnNames.REF_VENDOR,vendorId,userId);
			Boolean flag = null;
			if(amcLst.isEmpty()) {
				LocalDateTime deleteDate = DateConversionUtil.CurrentUTCTime();
				flag = vendorsDao.deleteVendorById(userId,vendorId,deleteDate);
				
				AuditTrail auditTrail = new AuditTrail();
				auditTrail.setRefVendor(vendorId);
				auditTrail.setActionBy(userId);
				auditTrail.setActionDate(DateConversionUtil.CurrentUTCTime());
				auditTrail.setAction(Constants.ActionName.DELETE);
				auditTrailDao.saveAuditTrail(auditTrail);
			}
			else {
				throw new GlobalException(409, "The vendor is associated with Amc.");
			}
		LOGGER.info("Exit from : deleteVendorById()");
		return flag;
		}else {
			throw new GlobalException(500, "Unauthorized user");
		}	
	}
	
	@Override
	public Boolean deleteVendorFileByFileName(List<AttachmentAssociationDto> attachmentDtoLst, BigDecimal userId,
			BigDecimal vendorId) {
		LOGGER.info("Enter into : deleteVendorFileByFileName()");
		Boolean result = authService.validateAdminAndManager(userId);
		if (result == true) {
			
			String commaSepList = null;
			for (AttachmentAssociationDto attachmentDto : attachmentDtoLst) {
				if(commaSepList==null) {
					commaSepList = attachmentDto.getFileName();
				}else {
					commaSepList = commaSepList+","+attachmentDto.getFileName();
				}
			}
			if(commaSepList!=null) {
				LocalDateTime deletedDate = DateConversionUtil.CurrentUTCTime();
				String colName = "ref_vendor";
				Boolean flag = fileStorageDao.deleteFileByFileName(commaSepList,vendorId,deletedDate,colName);
				LOGGER.info("Exit from : deleteVendorFileByFileName()");
				return flag;
			}
			
			LOGGER.info("Exit from : deleteVendorFileByFileName()");
			return null;
		}else {
			throw new GlobalException(500, "Unauthorized user");
		}
	}


}
