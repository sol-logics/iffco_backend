package com.sollogics.fams.services.serviceImpl;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.google.gson.Gson;
import com.sollogics.fams.controllers.Attachment.FileController;
import com.sollogics.fams.exceptionHandler.GlobalException;
import com.sollogics.fams.exceptionHandler.NotFoundException;
import com.sollogics.fams.repositories.dao.AmcDao;
import com.sollogics.fams.repositories.dao.AuditTrailDao;
import com.sollogics.fams.repositories.dao.FileStorageDao;
import com.sollogics.fams.repositories.modal.Amc;
import com.sollogics.fams.repositories.modal.AmcServices;
import com.sollogics.fams.repositories.modal.Asset;
import com.sollogics.fams.repositories.modal.AttachmentAssociation;
import com.sollogics.fams.repositories.modal.AuditTrail;
import com.sollogics.fams.repositories.modal.Status;
import com.sollogics.fams.repositories.modal.Vendors;
import com.sollogics.fams.services.dto.AmcDto;
import com.sollogics.fams.services.dto.AmcServicesDto;
import com.sollogics.fams.services.dto.AttachmentAssociationDto;
import com.sollogics.fams.services.dto.StatusDto;
import com.sollogics.fams.services.service.AmcService;
import com.sollogics.fams.services.service.AuthService;
import com.sollogics.fams.utils.CommonUtils;
import com.sollogics.fams.utils.Constants;
import com.sollogics.fams.utils.DateConversionUtil;

@Service
public class AmcServiceImpl implements AmcService{
	
	@Autowired
	private CommonUtils commonUtils;
	
	@Autowired
	private ModelMapper mapper;
	
	@Autowired
	private AmcDao amcDao;
	@Autowired
	private AuthService authService;
	
	@Autowired
	private FileController fileController;
	
	@Autowired
	private FileStorageDao fileStorageDao;
	
	@Autowired
	private AuditTrailDao auditTrailDao;
	
	private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());
	
	@Override
	public AmcDto saveAmc(BigDecimal userId, String amcDtoStr,MultipartFile[] files) {
		LOGGER.info("Enter into : saveAmc()");
		AmcDto amcDto = new AmcDto();
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			objectMapper.registerModule(new JavaTimeModule());
			amcDto = objectMapper.readValue(amcDtoStr, AmcDto.class);
		}catch (Exception e) {
			e.printStackTrace();
			throw new NotFoundException("Unable to parse json data");
		}
		if(userId != null && amcDto != null && amcDto.getRefAsset() != null && amcDto.getRefVendor() != null ) {
		Boolean result = authService.validateAdminAndManager(userId);
		if(result == true) {
			Amc amc = new Amc();
			amc = mapper.map(amcDto, Amc.class);
			DateFormat dateFormat = new SimpleDateFormat("yyyy");
			String dt = String.valueOf(dateFormat.format(new Date()));
			Integer latestSNoCode = amcDao.getLatestSNoCode();
			if (latestSNoCode == null) {
				amc.setsNumberCount(1);
			} else {
				amc.setsNumberCount(latestSNoCode + 1);
			}
			if(amc.getPreviousAmcId() !=null && (amc.getRenewUuid() != null || !amc.getRenewUuid().isEmpty()) ) {
				amc.setIsLatest(true);
				Boolean flag = amcDao.updateIsLatestValue(amc.getPreviousAmcId());
				commonUtils.stopAmcNotification(amc.getPreviousAmcId(), Constants.NotificationName.AMC_NOTIFICATION_NAME);
			}
			else{
				String renewUuid = Constants.RenewUuid.RENEW_ID_PATTERN + dt + "_" + amc.getsNumberCount();
				amc.setRenewUuid(renewUuid);
			}
			String amcUniqueCode = Constants.SerialNumberCode.AMC_PATTERN + dt + "_" + amc.getsNumberCount();
			amc.setAmcUniqueCode(amcUniqueCode);
			amc.setCreatedBy(userId);
			amc.setCreatedDate(DateConversionUtil.CurrentUTCTime());
			amc.setIsDeleted(false);
			amc.setIsLatest(true);
			String statusName = "Active";
			Status status = amcDao.getActiveStatus(statusName);
			status.setId(status.getId());
			amc.setRefStatus(status);
			Asset asset = new Asset();
			asset.setId(amcDto.getRefAsset().getId());
			amc.setRefAsset(asset);
			Vendors vendors = new Vendors();
			vendors.setId(amcDto.getRefVendor().getId());
			amc.setRefVendor(vendors);
			Amc amcInfo = amcDao.saveAmc(amc);
			amcDto.setId(amcInfo.getId());
			List<AmcServicesDto > amcServicesDtoLst = amcDto.getAmcServicesDtoLst();
			if (amcServicesDtoLst != null) {
				List<AmcServices> amcServicesLst = new ArrayList<>();
				for (AmcServicesDto amcServicesDto : amcServicesDtoLst) {
					AmcServices amcService = new AmcServices();
					amcService = mapper.map(amcServicesDto, AmcServices.class);
					Amc amcObj = new Amc();
					amcObj.setId(amcInfo.getId());
					amcService.setRefAmc(amcObj);
					amcServicesLst.add(amcService);
				}
				List<AmcServices> amcServiceLstInfo = amcDao.saveAmcService(userId, amcServicesLst);
				List<AmcServicesDto> amcServicesDtoLstInfo = new ArrayList<>();
				for (AmcServices amcServices : amcServiceLstInfo) {
					AmcServicesDto amcServicesDto = new AmcServicesDto();
					amcServicesDto = mapper.map(amcServices, AmcServicesDto.class);
					amcServicesDtoLstInfo.add(amcServicesDto);
				}
				amcDto.setAmcServicesDtoLst(amcServicesDtoLstInfo);
			}
		
			
			if(files != null) {
				//AttachmentAssociationDto attachmentDto = new AttachmentAssociationDto(null, null, null, 0);
				AttachmentAssociationDto attachmentDto = new AttachmentAssociationDto();
				attachmentDto.setRefAmc(amcDto.getId());
				attachmentDto.setCreatedBy(userId);
				attachmentDto.setCreatedDate(DateConversionUtil.CurrentUTCTime());
			List<AttachmentAssociationDto> attachmentDtoLst = fileController.uploadMultipleFiles(attachmentDto,files, userId);
				if(attachmentDtoLst != null) {
					amcDto.setRefattachmentDtoLst(attachmentDtoLst);
					
				}
			}
			AuditTrail auditTrail = new AuditTrail();
			auditTrail.setRefAmc(amcDto.getId());
			auditTrail.setActionBy(userId);
			auditTrail.setActionDate(DateConversionUtil.CurrentUTCTime());
			auditTrail.setAction(Constants.ActionName.POST);
			String json = null;
			try {
				Gson gson = new Gson();
				json = gson.toJson(amcDto);
			} catch (Exception e) {
				e.printStackTrace();
				throw new NotFoundException("Unable to parse json data");
			}
			auditTrail.setData(json);
			Boolean flag = auditTrailDao.saveAuditTrail(auditTrail);
			LOGGER.info("Exit from : saveAmc()");

			return amcDto;
	}else {
		throw new GlobalException(500, "Unauthorized user");
	}
}else {
	throw new GlobalException(401, "Insufficient data");
}
		
	}

	@Override
	public List<AmcDto> getAllAmc(BigDecimal userId) {
		LOGGER.info("Enter into : getAllAmc()");
		Boolean result = authService.validateAdmin(userId);
		if(result == true) {
		List<Amc> amcLst = amcDao.getAllAmc();
		if(amcLst == null) {
			return null;
		}
		List<AmcDto> amcDtoLst = new ArrayList<>();
		for(Amc amc : amcLst) {
			AmcDto amcDto = mapper.map(amc, AmcDto.class);
			
			amcDto.setRefAsset(amcDto.getRefAsset());
			amcDto.setRefVendor(amcDto.getRefVendor());
			amcDtoLst.add(amcDto);
		}
		LOGGER.info("Exit from : getAllAmc()");
		return amcDtoLst;
		}else {
			throw new GlobalException(500, "Unauthorized user");
		}
	}

	@Override
	public AmcDto getAmcById(BigDecimal userId, BigDecimal amcId) {
		LOGGER.info("Enter into : getAmcById()");
		Boolean result = authService.validateAdmin(userId);
		if(result == true) {
			Amc amc = amcDao.getAmcById(amcId);
			AmcDto amcDto = mapper.map(amc, AmcDto.class);
			List<AmcServices> amcServicesLst = amcDao.getAmcServiceByAmc(amcId);
			List<AmcServicesDto> amcServicesDtosLst = new ArrayList<>();
			for(AmcServices amcServices : amcServicesLst) {
				AmcServicesDto amcServicesDto = mapper.map(amcServices, AmcServicesDto.class);
				amcServicesDtosLst.add(amcServicesDto);
			}
			amcDto.setAmcServicesDtoLst(amcServicesDtosLst);
			LOGGER.info("Exit from : getAmcById()");
			return amcDto;
		}else {
			throw new GlobalException(500, "Unauthorized user");
		}
	}

	@Override
	public AmcDto updateAmc(BigDecimal userId, BigDecimal amcId, String amcDtoStr, MultipartFile[] files) {
		LOGGER.info("Enter into : updateAmc()");
		AmcDto amcDto = new AmcDto();
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			objectMapper.registerModule(new JavaTimeModule());
			amcDto = objectMapper.readValue(amcDtoStr, AmcDto.class);
		} catch (Exception e) {
			e.printStackTrace();
			throw new NotFoundException("Unable to parse json data");
		}
		Boolean result = authService.validateAdmin(userId);
		if (result == true) {
			AmcDto amcInfo = new AmcDto();
			AmcDto previousAmc = getAmcById(userId, amcId);
			previousAmc.setCost(amcDto.getCost());
			previousAmc.setExpiryDate(amcDto.getExpiryDate());
			previousAmc.setStartDate(amcDto.getStartDate());
			previousAmc.setRefAsset(amcDto.getRefAsset());
			previousAmc.setRefStatus(amcDto.getRefStatus());
			previousAmc.setRefVendor(amcDto.getRefVendor());
			previousAmc.setUpdatedBy(userId);
			previousAmc.setUpdatedDate(DateConversionUtil.CurrentUTCTime());
			Amc updatedAmc = mapper.map(previousAmc, Amc.class);
			// updatedAmc = amcDao.saveAmc(updatedAmc);

			List<AmcServicesDto> amcServicesPreviousDtoLst = previousAmc.getAmcServicesDtoLst();
			List<AmcServices> amcServicesPreviousLst = new ArrayList<>();
			for (AmcServicesDto amcServicesDto : amcServicesPreviousDtoLst) {
				AmcServices amcServices = mapper.map(amcServicesDto, AmcServices.class);
				amcServicesPreviousLst.add(amcServices);
			}
			List<AmcServicesDto> amcServicesDtoUpdatedLst = amcDto.getAmcServicesDtoLst();
			List<AmcServices> amcServicesUpdatedLst = new ArrayList<>();
			for (AmcServicesDto amcServicesDto : amcServicesDtoUpdatedLst) {
				AmcServices amcServices = mapper.map(amcServicesDto, AmcServices.class);
				amcServicesUpdatedLst.add(amcServices);
			}

			List<AmcServices> amcServicesLst = new ArrayList<>();
			for (AmcServices amcServicesUpdated : amcServicesUpdatedLst) {
				Boolean found = false;
				for (AmcServices amcServicesPrevious : amcServicesPreviousLst) {
					if (amcServicesUpdated.getId() != null
							&& (amcServicesUpdated.getId()).equals(amcServicesPrevious.getId())) {
						found = true;
						amcServicesPrevious.setUpdatedBy(userId);
						amcServicesPrevious.setUpdatedDate(DateConversionUtil.CurrentUTCTime());
						amcServicesPrevious.setDescription(amcServicesUpdated.getDescription());
						amcServicesPrevious.setMonth(amcServicesUpdated.getMonth());
						amcServicesPrevious.setRefStatus(amcServicesUpdated.getRefStatus());
						amcServicesPrevious.setDay(amcServicesUpdated.getDay());
						amcServicesPrevious.setYear(amcServicesUpdated.getYear());
						amcServicesLst.add(amcServicesPrevious);
						break;
					}
				}
				if (found == false) {
					Amc amc = new Amc();
					amc.setId(amcId);
					amcServicesUpdated.setRefAmc(amc);
					amcServicesUpdated.setCreatedBy(userId);
					amcServicesUpdated.setCreatedDate(DateConversionUtil.CurrentUTCTime());
					amcServicesUpdated.setIsDeleted(false);
					amcServicesLst.add(amcServicesUpdated);
				}
			}
			List<AmcServices> amcServiceLst = new ArrayList<>();
			amcServiceLst = amcDao.updateAmcServices(amcServicesLst);
			String commaSepList = null;
			for (AmcServices amcService : amcServiceLst) {
				if (commaSepList == null) {
					commaSepList = amcService.getId().toString();
				} else {
					commaSepList = commaSepList + "," + amcService.getId().toString();
				}
			}
			if (commaSepList != null) {
				Boolean flag = amcDao.deleteAmcServices(commaSepList);
			}

			List<AmcServicesDto> amcServicesDtoLst = new ArrayList<>();
			for (AmcServices amcServices : amcServiceLst) {
				AmcServicesDto amcServicesDto = mapper.map(amcServices, AmcServicesDto.class);
				amcServicesDtoLst.add(amcServicesDto);
			}
			updatedAmc = amcDao.saveAmc(updatedAmc);
			amcInfo = mapper.map(updatedAmc, AmcDto.class);
			amcInfo.setAmcServicesDtoLst(amcServicesDtoLst);
	
			if (amcDto.getRefattachmentDtoLst() != null) {
				List<AttachmentAssociationDto> attachmentAssociationDtoLst = getAttachmentList(Constants.ColumnNames.REF_AMC,amcDto.getId());
				List<BigDecimal> previousFileIdList = new ArrayList<>();
				for (AttachmentAssociationDto previousAttachmentAssociation : attachmentAssociationDtoLst) {
					previousFileIdList.add(previousAttachmentAssociation.getId());
				}
				List<BigDecimal> updatedFileIdList = new ArrayList<>();
				for (AttachmentAssociationDto updatedAttachmentAssociation : amcDto.getRefattachmentDtoLst()) {
					updatedFileIdList.add(updatedAttachmentAssociation.getId());
				}
				previousFileIdList.retainAll(updatedFileIdList);
				String commaSepFileId = null;
				for (BigDecimal fileId : previousFileIdList) {
					if (commaSepFileId == null) {
						commaSepFileId = fileId.toString();
					} else {
						commaSepFileId = commaSepFileId +","+ fileId;
					}
				}
					LocalDateTime deletedDate = DateConversionUtil.CurrentUTCTime();
					Boolean flag = fileStorageDao.updateExistingFile(deletedDate,commaSepFileId, Constants.ColumnNames.REF_AMC, amcDto.getId(),userId);
			}
			if (files != null) {
				AttachmentAssociationDto attachmentDto = new AttachmentAssociationDto();
				attachmentDto.setRefAmc(amcDto.getId());
				attachmentDto.setUpdatedBy(userId);
				attachmentDto.setUpdatedDate(DateConversionUtil.CurrentUTCTime());
				List<AttachmentAssociationDto> attachmentDtoLst = fileController.uploadMultipleFiles(attachmentDto,
						files, userId);
				if (attachmentDtoLst != null) {
					amcInfo.setRefattachmentDtoLst(attachmentDtoLst);
				}
			}
			AuditTrail auditTrail = new AuditTrail();
			auditTrail.setRefAmc(amcDto.getId());
			auditTrail.setActionBy(userId);
			auditTrail.setActionDate(DateConversionUtil.CurrentUTCTime());
			auditTrail.setAction(Constants.ActionName.PUT);
			String json = null;
			try {
				Gson gson = new Gson();
				json = gson.toJson(amcInfo);
			} catch (Exception e) {
				e.printStackTrace();
				throw new NotFoundException("Unable to parse json data");
			}
			auditTrail.setData(json);
			Boolean flag = auditTrailDao.saveAuditTrail(auditTrail);
			LOGGER.info("Exit from : updateAmc()");
			return amcInfo;
		} else {
			throw new GlobalException(500, "Unauthorized user");
		}
	}

	@SuppressWarnings("unused")
	@Override
	public Boolean deleteAmcById(BigDecimal userId, BigDecimal amcId) {
		LOGGER.info("Enter into : deleteAmcById()");
		Boolean result = authService.validateAdminAndManager(userId);
		if (result == true) {
			//Boolean isDeleted = fileStorageDao.deleteAttachmentForCurrentObject(Constants.ColumnNames.REF_AMC, amcId,userId);
			LocalDateTime deletedDate = DateConversionUtil.CurrentUTCTime();
			Boolean flag = amcDao.deleteAmcById(userId, amcId, deletedDate);
			AuditTrail auditTrail = new AuditTrail();
			auditTrail.setRefAmc(amcId);
			auditTrail.setActionBy(userId);
			auditTrail.setActionDate(DateConversionUtil.CurrentUTCTime());
			auditTrail.setAction(Constants.ActionName.DELETE);
			auditTrailDao.saveAuditTrail(auditTrail);
			LOGGER.info("Exit from : deleteAmcById()");
			return flag;
		} else {
			throw new GlobalException(500, "Unauthorized user");
		}
	}

	@Override
	public List<AmcDto> getAmcByRenewUuid(BigDecimal userId, String renewuuid) {

		LOGGER.info("Enter into : getAmcByRenewUuid()");
		Boolean result = authService.validateAdmin(userId);
		if(result == true) {
		List<Amc> amcLst = amcDao.getAmcByRenewUuid(renewuuid);
		if(amcLst == null) {
			return null;
		}
		List<AmcDto> amcDtoLst = new ArrayList<>();
		for(Amc amc : amcLst) {
			List<AmcServices> amcServicesLst = amcDao.getAmcServiceByAmc(amc.getId());
			List<AmcServicesDto> amcServicesDtosLst = new ArrayList<>();
			for(AmcServices amcServices : amcServicesLst) {
				AmcServicesDto amcServicesDto = mapper.map(amcServices, AmcServicesDto.class);
				amcServicesDtosLst.add(amcServicesDto);
			}
			AmcDto amcDto = mapper.map(amc, AmcDto.class);
			StatusDto statusDto = new StatusDto();
			statusDto.setId(amc.getRefStatus().getId());
			statusDto.setName(amc.getRefStatus().getName());
			amcDto.setRefStatus(statusDto);
			amcDto.setAmcServicesDtoLst(amcServicesDtosLst);
			
			List<AttachmentAssociationDto> attachmentAssociationDtoLst = getAttachmentList( Constants.ColumnNames.REF_AMC, amcDto.getId());
			amcDto.setRefattachmentDtoLst(attachmentAssociationDtoLst);
			amcDtoLst.add(amcDto);
		}
		LOGGER.info("Exit from : getAmcByRenewUuid()");
		return amcDtoLst;
		}else {
			throw new GlobalException(500, "Unauthorized user");
		}
	}
	
	public List<AttachmentAssociationDto> getAttachmentList(String colName,BigDecimal amcId){
		List<AttachmentAssociation> attachmentAssociationLst = fileStorageDao.getAttachmentList(colName,amcId);
		List<AttachmentAssociationDto> attachmentAssociationDtoLst = new ArrayList<>();
		for(AttachmentAssociation attachmentAssociation :attachmentAssociationLst) {
			
			AttachmentAssociationDto attachmentAssociationDto = mapper.map(attachmentAssociation,AttachmentAssociationDto.class);
			attachmentAssociationDtoLst.add(attachmentAssociationDto);
		}
		return attachmentAssociationDtoLst;
	}

	@Override
	public Boolean deleteAmcFileByFileName(List<AttachmentAssociationDto> attachmentDtoLst, BigDecimal userId,
			BigDecimal amcId) {
		LOGGER.info("Enter into : deleteAmcFileByFileName()");
		Boolean result = authService.validateAdminAndManager(userId);
		if (result == true) {
			
			String commaSepList = null;
			for (AttachmentAssociationDto attachmentDto : attachmentDtoLst) {
				if(commaSepList==null) {
					commaSepList = attachmentDto.getFileName();
				}else {
					commaSepList = commaSepList+","+attachmentDto.getFileName();
				}
			}
			if(commaSepList!=null) {
				LocalDateTime deletedDate = DateConversionUtil.CurrentUTCTime();
				Boolean flag = fileStorageDao.deleteFileByFileName(commaSepList,amcId,deletedDate,Constants.ColumnNames.REF_AMC);
				LOGGER.info("Exit from : deleteAmcFileByFileName()");
				return flag;
			}
			
			LOGGER.info("Exit from : deleteAmcFileByFileName()");
			return null;
		}else {
			throw new GlobalException(500, "Unauthorized user");
		}
	}
}
