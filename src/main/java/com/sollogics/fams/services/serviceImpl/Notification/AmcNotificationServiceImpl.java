package com.sollogics.fams.services.serviceImpl.Notification;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sollogics.fams.exceptionHandler.GlobalException;
import com.sollogics.fams.repositories.dao.NotificationConfigDao;
import com.sollogics.fams.repositories.dao.Notification.AmcNotificationDao;
import com.sollogics.fams.repositories.modal.Amc;
import com.sollogics.fams.repositories.modal.NotificationAlertDetails;
import com.sollogics.fams.repositories.modal.NotificationConfig;
import com.sollogics.fams.repositories.modal.NotificationReceiver;
import com.sollogics.fams.services.dto.ForgotPasswordEmailGenerator;
import com.sollogics.fams.services.dto.NotificationAlertDetailsDto;
import com.sollogics.fams.services.service.Notification.AmcNotificationService;
import com.sollogics.fams.utils.Constants;
import com.sollogics.fams.utils.DateConversionUtil;
import com.sollogics.fams.utils.MailContent;
@Service
public class AmcNotificationServiceImpl implements AmcNotificationService{
//	@Autowired
//	@Qualifier("doNotReplyMailSender")
//	private JavaMailSender doNotReplyMailSender;
	
	@Autowired
	private ForgotPasswordEmailGenerator emailGenerator;
	
	@Autowired
	private ModelMapper mapper;
	
	private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());
	@Autowired
	private AmcNotificationDao amcNotificationDao;
	
	@Autowired
	private NotificationConfigDao notificationConfigDao;

	@Override
	public List<NotificationAlertDetailsDto> amcDueDateAlert(BigDecimal userId) {
		LOGGER.info("Enter into : amcDueDateAlert()");
		String notiName = Constants.NotificationName.AMC;
		List<NotificationAlertDetailsDto> notiAlertDetailsInfo = new ArrayList<>();
		NotificationConfig notiConfig = amcNotificationDao.getNotiConfigByName(notiName);
		List<Amc> amcLst = amcNotificationDao.getAmcLstByBeforeDays(notiConfig.getBeforeDays());
		if(amcLst==null || amcLst.isEmpty()) {
			LOGGER.info("No amc found");
			return notiAlertDetailsInfo;
		}
		List<NotificationReceiver> notiRcvrLst = notificationConfigDao.getNotiRcvrLstByConfigId(notiConfig.getId());
		if(notiRcvrLst != null && !notiRcvrLst.isEmpty()) {
			List<NotificationAlertDetails> notiAlertDtlsLst = new ArrayList<>();
			
			String commaSepList = null;
			for(NotificationReceiver notiRcvr : notiRcvrLst) {
				if(commaSepList==null) {
						commaSepList = notiRcvr.getRefUser().getEmail();
				}else {
					commaSepList = commaSepList+","+notiRcvr.getRefUser().getEmail();
				}
			}
			
			for(Amc amc : amcLst) {
				NotificationAlertDetails notiAlertDtls = new NotificationAlertDetails();
				notiAlertDtls.setAlertIntervals(notiConfig.getAlertIntervals());
				notiAlertDtls.setAlertLeft(notiConfig.getAlertTimes());
				notiAlertDtls.setAlertSent(0);
				notiAlertDtls.setAlertStatus(true);
				notiAlertDtls.setAlertTimes(notiConfig.getAlertTimes());
				notiAlertDtls.setCreatedBy(userId);
				notiAlertDtls.setCreatedDate(DateConversionUtil.CurrentUTCTime());
				notiAlertDtls.setEndTimes(notiConfig.getEndTime());
				notiAlertDtls.setObjectId(amc.getId());
				notiAlertDtls.setStopOnAction(notiConfig.getStopOnAction());
				notiAlertDtls.setMailSubject(notiConfig.getRefNotificationMeta().getMailSubject());
				notiAlertDtls.setAlertStartTime(DateConversionUtil.CurrentUTCTime());
				notiAlertDtls.setAlertEndTime(notiAlertDtls.getAlertStartTime().plusYears(1));
				notiAlertDtls.setCc_to(commaSepList);
				
				MailContent mailContent = setMailBodyAndSubject(notiAlertDtls,notiConfig.getBeforeDays(),amc.getExpiryDate(),amc.getRefAsset().getUniqueCode(),amc.getAmcUniqueCode());
				notiAlertDtls.setMailBody(mailContent.getMailBody());
				notiAlertDtls.setNotificationName(notiConfig.getRefNotificationMeta().getNotificationName());
				notiAlertDtlsLst.add(notiAlertDtls);
				
			}
		//NotificationAlertDetails notiAlertDetails = amcNotificationDao.saveCcTo(commaSepList);
		List<NotificationAlertDetails> notiAlertDetailsLst = amcNotificationDao.saveAmcLstInAlertDtls(notiAlertDtlsLst);		
		for(NotificationAlertDetails notificationAlertDetails : notiAlertDetailsLst) {
			NotificationAlertDetailsDto notiAlertDtlsDto = mapper.map(notificationAlertDetails, NotificationAlertDetailsDto.class);
			notiAlertDetailsInfo.add(notiAlertDtlsDto);
		}
		LOGGER.info("Exit from : amcDueDateAlert()");
		return notiAlertDetailsInfo;
		}
		else {
			throw new GlobalException(401, "Users not found");
		}
	}
	
	private MailContent setMailBodyAndSubject(NotificationAlertDetails notiAlertDtls, Integer beforeDays,
			LocalDateTime expiryDate,String uniqueCode,String amcUniqueCode) {
		LOGGER.info("Enter into : setMailBodyAndSubject()");
		MailContent mailContent = null;
		mailContent = emailGenerator.generateEmailContentForNotification(notiAlertDtls,beforeDays,expiryDate,uniqueCode,amcUniqueCode);
		LOGGER.info("Exit from : setMailBodyAndSubject()");
		return mailContent;
		
		
	}

}
