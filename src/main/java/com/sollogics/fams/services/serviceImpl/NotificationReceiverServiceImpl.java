package com.sollogics.fams.services.serviceImpl;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sollogics.fams.exceptionHandler.GlobalException;
import com.sollogics.fams.repositories.dao.NotificationReceiverDao;
import com.sollogics.fams.repositories.modal.NotificationConfig;
import com.sollogics.fams.repositories.modal.NotificationReceiver;
import com.sollogics.fams.repositories.modal.Users;
import com.sollogics.fams.services.dto.NotificationReceiverDto;
import com.sollogics.fams.services.dto.UsersDto;
import com.sollogics.fams.services.service.AuthService;
import com.sollogics.fams.services.service.NotificationReceiverService;
import com.sollogics.fams.utils.DateConversionUtil;

@Service
public class NotificationReceiverServiceImpl implements NotificationReceiverService {
	
	@Autowired
	private ModelMapper mapper;

	@Autowired
	private NotificationReceiverDao notificationReceiverDao;
	
	@Autowired
	private AuthService authService;
	
	private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());
	@Override
	public NotificationReceiverDto saveNotificationReceiver(BigDecimal userId, BigDecimal notificationId,
			NotificationReceiverDto notificationReceiverDto) {
		LOGGER.info("Enter into : saveNotificationReceiver()");
		Boolean result = authService.validateAdmin(userId);
		if(result == true) {
		if(userId != null && notificationId != null && notificationReceiverDto != null && notificationReceiverDto.getrefNotiConfigId() != null && notificationReceiverDto.getRefUser() != null ) {
			NotificationReceiver notificationReceiver = mapper.map(notificationReceiverDto, NotificationReceiver.class);
			notificationReceiver.setCreatedBy(userId);
			notificationReceiver.setCreatedDate(DateConversionUtil.CurrentUTCTime());
			Users users = new Users();
			users.setId(notificationReceiverDto.getRefUser().getId());
			notificationReceiver.setRefUser(users);
			NotificationConfig notificationConfig = new NotificationConfig();
			notificationConfig.setId(notificationReceiverDto.getrefNotiConfigId());
			notificationReceiver.setRefNotificationConfig(notificationConfig);
			notificationReceiver.setIsDeleted(false);
			NotificationReceiver notificationReceiverInfo = notificationReceiverDao.saveNotificationReceiver(notificationReceiver);
			notificationReceiverDto.setId(notificationReceiverInfo.getId());
		LOGGER.info("Exit from : saveNotificationReceiver()");
		return notificationReceiverDto;
		}else {
			throw new GlobalException(401,"Insufficient data");
		}
		}else {
			throw new GlobalException(500, "Unauthorized user");
		}
			
	}
	@Override
	public List<NotificationReceiverDto> getAllNotificationReceiver(BigDecimal userId) {
		LOGGER.info("Enter into : getAllNotificationReceiver()");
		Boolean result = authService.validateAdmin(userId);
		if(result == true) {
			List<NotificationReceiver> notificationReceiverLst = notificationReceiverDao.getAllNotificationReceiver();
			if(notificationReceiverLst == null) {
				return null;
			}
			List<NotificationReceiverDto> notificationReceiverDtoLst = new ArrayList<>();
			for(NotificationReceiver notificationReceiver : notificationReceiverLst) {
				NotificationReceiverDto notificationReceiverDto = mapper.map(notificationReceiver, NotificationReceiverDto.class);
				notificationReceiverDtoLst.add(notificationReceiverDto);
			}
			LOGGER.info("Exit from : getAllNotificationReceiver()");
			return notificationReceiverDtoLst;
		}else {
			throw new GlobalException(500, "Unauthorized user");
		}
	}
	@Override
	public NotificationReceiverDto getNotificationReceiverById(BigDecimal userId, BigDecimal notificationId) {
		LOGGER.info("Enter into : getNotificationReceiverById()");
		Boolean result = authService.validateAdmin(userId);
		if(result == true) {
			NotificationReceiver notificationReceiver = notificationReceiverDao.getNotificationReceiverById(notificationId);
			NotificationReceiverDto notificationReceiverDto = mapper.map(notificationReceiver, NotificationReceiverDto.class);
			notificationReceiverDto.setrefNotiConfigId(notificationReceiver.getRefNotificationConfig().getId());
			UsersDto userDto = new UsersDto();
			userDto.setId(notificationReceiverDto.getRefUser().getId());
			notificationReceiverDto.setRefUser(userDto);
			LOGGER.info("Exit from : getNotificationReceiverById()");
			return notificationReceiverDto;
		}else {
			throw new GlobalException(500, "Unauthorized user");
		}
		
	}

	@Override
	public NotificationReceiverDto updateNotificationReceiver(BigDecimal usersId, BigDecimal notificationId,
			NotificationReceiverDto notificationReceiverDto) {
		LOGGER.info("Enter into : updateNotificationReceiver()");
		Boolean result = authService.validateAdmin(usersId);
		if (result != null && result == true) {
			NotificationReceiverDto notificationReceiverInfo = new NotificationReceiverDto();
			if (notificationId != null && notificationReceiverDto.getrefNotiConfigId() != null
					&& notificationReceiverDto.getRefUser() != null) {
				NotificationReceiver updatedNotificationReceiver = new NotificationReceiver();
				NotificationReceiver previousNotificationReceiver = notificationReceiverDao
						.getNotificationReceiverById(notificationId);
				updatedNotificationReceiver = mapper.map(notificationReceiverDto, NotificationReceiver.class);
				previousNotificationReceiver
						.setRefNotificationConfig(updatedNotificationReceiver.getRefNotificationConfig());
				previousNotificationReceiver.setRefUser(updatedNotificationReceiver.getRefUser());
				previousNotificationReceiver.setUpdatedBy(usersId);
				previousNotificationReceiver.setUpdatedDate(DateConversionUtil.CurrentUTCTime());
				updatedNotificationReceiver = notificationReceiverDao
						.saveNotificationReceiver(previousNotificationReceiver);
				notificationReceiverInfo = mapper.map(updatedNotificationReceiver, NotificationReceiverDto.class);
				LOGGER.info("Exit from : updateNotificationReceiver()");
			} else {
				LOGGER.error("Insufficient data");
			}
			return notificationReceiverInfo;
		} else {
			throw new GlobalException(500, "Unauthorized user");
		}
	}
	@Override
	public Boolean deleteNotificationReceiverById(BigDecimal userId, BigDecimal notificationId) {
		LOGGER.info("Enter into : deleteNotificationReceiverById()");
		Boolean result = authService.validateAdmin(userId);
		if (result == true) {
			LocalDateTime deletedDate = DateConversionUtil.CurrentUTCTime();
			Boolean flag = notificationReceiverDao.deleteNotificationReceiverById(userId,notificationId,deletedDate);
			LOGGER.info("Exit from : deleteNotificationReceiverById()");
			return flag;
		}else {
			throw new GlobalException(500, "Unauthorized user");
		}
	}

}
