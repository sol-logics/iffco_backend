package com.sollogics.fams.services.dto;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import org.apache.catalina.User;

import com.sollogics.fams.repositories.modal.Amc;
import com.sollogics.fams.repositories.modal.Asset;
import com.sollogics.fams.repositories.modal.Status;
import com.sollogics.fams.repositories.modal.Vendors;

public class AuditTrailDto {
	
private BigDecimal id;
private Amc refAmc;
private Asset refAssets;
private Vendors refVendor;
private User refUser;
private String data;
private BigDecimal actionBy;
private LocalDateTime actionDate;
private Status status;

public BigDecimal getId() {
	return id;
}
public void setId(BigDecimal id) {
	this.id = id;
}
public Amc getRefAmc() {
	return refAmc;
}
public void setRefAmc(Amc refAmc) {
	this.refAmc = refAmc;
}
public Asset getRefAssets() {
	return refAssets;
}
public void setRefAssets(Asset refAssets) {
	this.refAssets = refAssets;
}
public Vendors getRefVendor() {
	return refVendor;
}
public void setRefVendor(Vendors refVendor) {
	this.refVendor = refVendor;
}
public User getRefUser() {
	return refUser;
}
public void setRefUser(User refUser) {
	this.refUser = refUser;
}
public String getData() {
	return data;
}
public void setData(String data) {
	this.data = data;
}
public BigDecimal getActionBy() {
	return actionBy;
}
public void setActionBy(BigDecimal actionBy) {
	this.actionBy = actionBy;
}
public LocalDateTime getActionDate() {
	return actionDate;
}
public void setActionDate(LocalDateTime actionDate) {
	this.actionDate = actionDate;
}
public Status getStatus() {
	return status;
}
public void setStatus(Status status) {
	this.status = status;
}
	
}
