package com.sollogics.fams.services.dto;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class AttachmentAssociationDto {
	private BigDecimal id;
	private BigDecimal refUser;
	private BigDecimal refAmc;
	private BigDecimal refAsset;
	private BigDecimal refVendor;
	private String fileName;
	private String path;
	private String fileDownloadUri;
	private String fileType;
	private Long size;
	private LocalDateTime createdDate;
	private BigDecimal createdBy;
	private LocalDateTime updatedDate;
	private BigDecimal updatedBy;
	private Boolean isDeleted;
	private LocalDateTime deletedDate;
	private BigDecimal deletedBy;
	
	/*
	 * public AttachmentAssociationDto(String fileName, String fileDownloadUri,
	 * String fileType, long size) { super(); this.fileName = fileName;
	 * this.fileDownloadUri = fileDownloadUri; this.fileType = fileType; this.size =
	 * size; }
	 */
	
	public BigDecimal getId() {
		return id;
	}
	public void setId(BigDecimal id) {
		this.id = id;
	}
	public BigDecimal getRefUser() {
		return refUser;
	}
	public void setRefUser(BigDecimal refUser) {
		this.refUser = refUser;
	}
	public BigDecimal getRefAmc() {
		return refAmc;
	}
	public void setRefAmc(BigDecimal refAmc) {
		this.refAmc = refAmc;
	}
	public BigDecimal getRefAsset() {
		return refAsset;
	}
	public void setRefAsset(BigDecimal refAsset) {
		this.refAsset = refAsset;
	}
	public BigDecimal getRefVendor() {
		return refVendor;
	}
	public void setRefVendor(BigDecimal refVendor) {
		this.refVendor = refVendor;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public String getFileDownloadUri() {
		return fileDownloadUri;
	}
	public void setFileDownloadUri(String fileDownloadUri) {
		this.fileDownloadUri = fileDownloadUri;
	}
	public String getFileType() {
		return fileType;
	}
	public void setFileType(String fileType) {
		this.fileType = fileType;
	}
	public Long getSize() {
		return size;
	}
	public void setSize(Long size) {
		this.size = size;
	}
	public LocalDateTime getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(LocalDateTime createdDate) {
		this.createdDate = createdDate;
	}
	public BigDecimal getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}
	public LocalDateTime getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(LocalDateTime updatedDate) {
		this.updatedDate = updatedDate;
	}
	public BigDecimal getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(BigDecimal updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Boolean getIsDeleted() {
		return isDeleted;
	}
	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	public LocalDateTime getDeletedDate() {
		return deletedDate;
	}
	public void setDeletedDate(LocalDateTime deletedDate) {
		this.deletedDate = deletedDate;
	}
	public BigDecimal getDeletedBy() {
		return deletedBy;
	}
	public void setDeletedBy(BigDecimal deletedBy) {
		this.deletedBy = deletedBy;
	}
	
	
}
