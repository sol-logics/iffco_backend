package com.sollogics.fams.services.dto;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

public class NotificationConfigDto {
	
private BigDecimal id;
	private Integer alertTimes;
	private Integer alertIntervals;
	private Integer beforeDays;
	private Integer endTime;
	private Boolean stopOnAction;
	private BigDecimal createdBy;
	private LocalDateTime createdDate;
	private BigDecimal updatedBy;
	private LocalDateTime updatedDate;
	private Boolean isDeleted;
	private BigDecimal deletedBy;
	private LocalDateTime deletedDate;
	private NotificationMetaDto refNotificationMetaDto;
	private List<NotificationReceiverDto> notificationReceiverDtoLst; 
	
	public List<NotificationReceiverDto> getNotificationReceiverDtoLst() {
		return notificationReceiverDtoLst;
	}
	public void setNotificationReceiverDtoLst(List<NotificationReceiverDto> notificationReceiverDtoLst) {
		this.notificationReceiverDtoLst = notificationReceiverDtoLst;
	}
	public NotificationMetaDto getRefNotificationMetaDto() {
		return refNotificationMetaDto;
	}
	public void setRefNotificationMetaDto(NotificationMetaDto refNotificationMetaDto) {
		this.refNotificationMetaDto = refNotificationMetaDto;
	}
	public BigDecimal getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(BigDecimal updatedBy) {
		this.updatedBy = updatedBy;
	}
	public LocalDateTime getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(LocalDateTime updatedDate) {
		this.updatedDate = updatedDate;
	}
	public Boolean getIsDeleted() {
		return isDeleted;
	}
	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	public BigDecimal getDeletedBy() {
		return deletedBy;
	}
	public void setDeletedBy(BigDecimal deletedBy) {
		this.deletedBy = deletedBy;
	}
	public LocalDateTime getDeletedDate() {
		return deletedDate;
	}
	public void setDeletedDate(LocalDateTime deletedDate) {
		this.deletedDate = deletedDate;
	}
	public BigDecimal getId() {
		return id;
	}
	public void setId(BigDecimal id) {
		this.id = id;
	}
	public Integer getAlertTimes() {
		return alertTimes;
	}
	public void setAlertTimes(Integer alertTimes) {
		this.alertTimes = alertTimes;
	}
	public Integer getAlertIntervals() {
		return alertIntervals;
	}
	public void setAlertIntervals(Integer alertIntervals) {
		this.alertIntervals = alertIntervals;
	}
	public Integer getBeforeDays() {
		return beforeDays;
	}
	public void setBeforeDays(Integer beforeDays) {
		this.beforeDays = beforeDays;
	}
	public Integer getEndTime() {
		return endTime;
	}
	public void setEndTime(Integer endTime) {
		this.endTime = endTime;
	}
	public Boolean getStopOnAction() {
		return stopOnAction;
	}
	public void setStopOnAction(Boolean stopOnAction) {
		this.stopOnAction = stopOnAction;
	}
	public BigDecimal getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}
	public LocalDateTime getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(LocalDateTime createdDate) {
		this.createdDate = createdDate;
	}

}
