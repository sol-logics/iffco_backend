
package com.sollogics.fams.services.dto.Attachment;

public class UploadFileResponse {
	private String fileName;
	private String fileDownloadUri;
	private String fileType;
	private long size;
	//private AttachmentAssociationDto attachmentAssociationDto;

	public UploadFileResponse(String fileName, String fileDownloadUri, String fileType, long size) {
		this.fileName = fileName;
		this.fileDownloadUri = fileDownloadUri;
		this.fileType = fileType;
		this.size = size;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileDownloadUri() {
		return fileDownloadUri;
	}

	public void setFileDownloadUri(String fileDownloadUri) {
		this.fileDownloadUri = fileDownloadUri;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public long getSize() {
		return size;
	}

	public void setSize(long size) {
		this.size = size;
	}
	/*
	 * public AttachmentAssociationDto getAttachmentAssociationDto() { return
	 * attachmentAssociationDto; }
	 * public void setAttachmentAssociationDto(AttachmentAssociationDto
	 * attachmentAssociationDto) { this.attachmentAssociationDto =
	 * attachmentAssociationDto; }
	 */


}
