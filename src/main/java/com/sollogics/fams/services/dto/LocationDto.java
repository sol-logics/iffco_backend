package com.sollogics.fams.services.dto;

import java.math.BigDecimal;

public class LocationDto {
	
	private BigDecimal id;
	private String floor;
	private String area;
	public BigDecimal getId() {
		return id;
	}
	public void setId(BigDecimal id) {
		this.id = id;
	}
	public String getFloor() {
		return floor;
	}
	public void setFloor(String floor) {
		this.floor = floor;
	}
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}

}
