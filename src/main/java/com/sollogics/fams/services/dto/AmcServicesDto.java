package com.sollogics.fams.services.dto;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class AmcServicesDto {
	private BigDecimal id;
	private BigDecimal refAmcId;
	private String description;
	private BigDecimal year;
	private BigDecimal month;
	private BigDecimal day;
	private StatusDto refStatus;
	private BigDecimal createdBy;
	private LocalDateTime createdDate;
	private BigDecimal updatedBy;
	private LocalDateTime updatedDate;
	private Boolean isDeleted;
	private BigDecimal deletedBy;
	private LocalDateTime deletedDate;

	public BigDecimal getId() {
		return id;
	}

	public void setId(BigDecimal id) {
		this.id = id;
	}
	public BigDecimal getRefAmcId() {
		return refAmcId;
	}

	public void setRefAmcId(BigDecimal refAmcId) {
		this.refAmcId = refAmcId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public BigDecimal getYear() {
		return year;
	}

	public void setYear(BigDecimal year) {
		this.year = year;
	}

	public BigDecimal getMonth() {
		return month;
	}

	public void setMonth(BigDecimal month) {
		this.month = month;
	}

	public BigDecimal getDay() {
		return day;
	}

	public void setDay(BigDecimal day) {
		this.day = day;
	}

	public StatusDto getRefStatus() {
		return refStatus;
	}

	public void setRefStatus(StatusDto refStatus) {
		this.refStatus = refStatus;
	}

	public BigDecimal getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}

	public LocalDateTime getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(LocalDateTime createdDate) {
		this.createdDate = createdDate;
	}

	public BigDecimal getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(BigDecimal updatedBy) {
		this.updatedBy = updatedBy;
	}

	public LocalDateTime getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(LocalDateTime updatedDate) {
		this.updatedDate = updatedDate;
	}

	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public BigDecimal getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(BigDecimal deletedBy) {
		this.deletedBy = deletedBy;
	}

	public LocalDateTime getDeletedDate() {
		return deletedDate;
	}

	public void setDeletedDate(LocalDateTime deletedDate) {
		this.deletedDate = deletedDate;
	}

}
