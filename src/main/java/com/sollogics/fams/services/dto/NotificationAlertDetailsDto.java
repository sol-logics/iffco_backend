package com.sollogics.fams.services.dto;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class NotificationAlertDetailsDto {
	
private BigDecimal id;

private String mailBody;
private String mailSubject;
private Boolean alertStatus;
private Integer alertTimes;
private Integer alertIntervals;
private Integer endTimes;
private BigDecimal objectId;
private String notificationName;
private Boolean stopOnAction;
private String cc_to;
private Integer alertLeft;
private BigDecimal createdBy;
private Integer alertSent;
private LocalDateTime alertStartTime;
private LocalDateTime alertEndTime;
private String attachmentDetails;

public String getAttachmentDetails() {
	return attachmentDetails;
}
public void setAttachmentDetails(String attachmentDetails) {
	this.attachmentDetails = attachmentDetails;
}
public LocalDateTime getAlertStartTime() {
	return alertStartTime;
}
public void setAlertStartTime(LocalDateTime alertStartTime) {
	this.alertStartTime = alertStartTime;
}
public LocalDateTime getAlertEndTime() {
	return alertEndTime;
}
public void setAlertEndTime(LocalDateTime alertEndTime) {
	this.alertEndTime = alertEndTime;
}
public String getNotificationName() {
	return notificationName;
}
public void setNotificationName(String notificationName) {
	this.notificationName = notificationName;
}
	public Integer getAlertSent() {
	return alertSent;
}
public void setAlertSent(Integer alertSent) {
	this.alertSent = alertSent;
}
	public BigDecimal getId() {
	return id;
}
public void setId(BigDecimal id) {
	this.id = id;
}
public String getMailBody() {
	return mailBody;
}
public void setMailBody(String mailBody) {
	this.mailBody = mailBody;
}
public String getMailSubject() {
	return mailSubject;
}
public void setMailSubject(String mailSubject) {
	this.mailSubject = mailSubject;
}
public Boolean getAlertStatus() {
	return alertStatus;
}
public void setAlertStatus(Boolean alertStatus) {
	this.alertStatus = alertStatus;
}
public Integer getAlertTimes() {
	return alertTimes;
}
public void setAlertTimes(Integer alertTimes) {
	this.alertTimes = alertTimes;
}
public Integer getAlertIntervals() {
	return alertIntervals;
}
public void setAlertIntervals(Integer alertIntervals) {
	this.alertIntervals = alertIntervals;
}
public Integer getEndTimes() {
	return endTimes;
}
public void setEndTimes(Integer endTimes) {
	this.endTimes = endTimes;
}
public BigDecimal getObjectId() {
	return objectId;
}
public void setObjectId(BigDecimal objectId) {
	this.objectId = objectId;
}
public Boolean getStopOnAction() {
	return stopOnAction;
}
public void setStopOnAction(Boolean stopOnAction) {
	this.stopOnAction = stopOnAction;
}
public String getCc_to() {
	return cc_to;
}
public void setCc_to(String cc_to) {
	this.cc_to = cc_to;
}
public Integer getAlertLeft() {
	return alertLeft;
}
public void setAlertLeft(Integer alertLeft) {
	this.alertLeft = alertLeft;
}
public BigDecimal getCreatedBy() {
	return createdBy;
}
public void setCreatedBy(BigDecimal createdBy) {
	this.createdBy = createdBy;
}
	
}
