package com.sollogics.fams.services.dto;

import com.sollogics.fams.repositories.modal.Profile;
import com.sollogics.fams.utils.MailContent;

public interface EmailBodyGenerator<T> {
	MailContent generateEmailContent(T mailComposer, String locale);
	MailContent generateEmailContentForApproval(Profile profile);
	MailContent generateEmailContentForPassword(UsersDto usersDto, String password);

}
