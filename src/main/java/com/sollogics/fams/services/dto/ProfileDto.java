package com.sollogics.fams.services.dto;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ProfileDto {
	private BigDecimal id;
	private String firstName;
	private String lastName;
	private UsersDto refUser;
	private RolesDto refRoles;
	private String empId;
	private LocalDateTime createdOn;
	private LocalDateTime updatedOn;
	private CompanyDto refCompany;
	private String authToken;
	private String designation;
	private String departement;
	private BigDecimal updatedBy;
	private BigDecimal createdBy;
	private BigDecimal deletedBy;
	private Boolean isDeleted;
	private List<AttachmentAssociationDto> refattachmentDtoLst;

	@JsonProperty("refattachmentDtoLst")
	public List<AttachmentAssociationDto> getRefattachmentDtoLst() {
		return refattachmentDtoLst;
	}
	public void setRefattachmentDtoLst(List<AttachmentAssociationDto> refattachmentDtoLst) {
		this.refattachmentDtoLst = refattachmentDtoLst;
	}
	public Boolean getIsDeleted() {
		return isDeleted;
	}
	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	public BigDecimal getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}
	public BigDecimal getDeletedBy() {
		return deletedBy;
	}
	public void setDeletedBy(BigDecimal deletedBy) {
		this.deletedBy = deletedBy;
	}
	public BigDecimal getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(BigDecimal updatedBy) {
		this.updatedBy = updatedBy;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	public String getDepartement() {
		return departement;
	}
	public void setDepartement(String departement) {
		this.departement = departement;
	}
	
	public BigDecimal getId() {
		return id;
	}
	public void setId(BigDecimal id) {
		this.id = id;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public UsersDto getRefUser() {
		return refUser;
	}
	public void setRefUser(UsersDto refUser) {
		this.refUser = refUser;
	}
	public RolesDto getRefRoles() {
		return refRoles;
	}
	public void setRefRoles(RolesDto refRoles) {
		this.refRoles = refRoles;
	}
	public String getEmpId() {
		return empId;
	}
	public void setEmpId(String empId) {
		this.empId = empId;
	}
	public LocalDateTime getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(LocalDateTime createdOn) {
		this.createdOn = createdOn;
	}
	public LocalDateTime getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(LocalDateTime updatedOn) {
		this.updatedOn = updatedOn;
	}
	public CompanyDto getRefCompany() {
		return refCompany;
	}
	public void setRefCompany(CompanyDto refCompany) {
		this.refCompany = refCompany;
	}
	public String getAuthToken() {
		return authToken;
	}
	public void setAuthToken(String authToken) {
		this.authToken = authToken;
	}
	

}
