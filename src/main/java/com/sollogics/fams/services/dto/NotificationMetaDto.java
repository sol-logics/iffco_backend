package com.sollogics.fams.services.dto;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class NotificationMetaDto {
private BigDecimal id;

private String notificationName;
private String mailBody;
private String mailSubject;
private LocalDateTime createdDate;
private LocalDateTime updatedDate;
private Boolean isDeleted;
private LocalDateTime deletedDate;

	public BigDecimal getId() {
	return id;
}
public void setId(BigDecimal id) {
	this.id = id;
}
public String getNotificationName() {
	return notificationName;
}
public void setNotificationName(String notificationName) {
	this.notificationName = notificationName;
}
public String getMailBody() {
	return mailBody;
}
public void setMailBody(String mailBody) {
	this.mailBody = mailBody;
}
public String getMailSubject() {
	return mailSubject;
}
public void setMailSubject(String mailSubject) {
	this.mailSubject = mailSubject;
}
public LocalDateTime getCreatedDate() {
	return createdDate;
}
public void setCreatedDate(LocalDateTime createdDate) {
	this.createdDate = createdDate;
}
public LocalDateTime getUpdatedDate() {
	return updatedDate;
}
public void setUpdatedDate(LocalDateTime updatedDate) {
	this.updatedDate = updatedDate;
}
public Boolean getIsDeleted() {
	return isDeleted;
}
public void setIsDeleted(Boolean isDeleted) {
	this.isDeleted = isDeleted;
}
public LocalDateTime getDeletedDate() {
	return deletedDate;
}
public void setDeletedDate(LocalDateTime deletedDate) {
	this.deletedDate = deletedDate;
}

}
