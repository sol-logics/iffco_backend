package com.sollogics.fams.services.dto;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
@JsonIgnoreProperties(ignoreUnknown = true)
public class AssetDto {
	
private BigDecimal id;

private String name;
private String manufacturer;
private String modelNum;
private String serialNum;
private String uom;
private String capacity;
private String uniqueCode;
private LocationDto refLocation;
private TypeDto refType;
private CategoryDto refCategory;
private String department;
private LocalDateTime warrantyExpiryDate;
private StatusDto refStatus;
private LocalDateTime purchaseDate;
private BigDecimal purchasePrice;
private String description;
private BigDecimal subComponent;
private String usefulLife;
private Integer sNoCount; 
private LocalDateTime updatedDate;
private BigDecimal updatedBy;
private LocalDateTime deletedDate;
private BigDecimal deletedBy;
private BigDecimal createdBy;
private LocalDateTime createdDate;
private Boolean isDeleted;
private AssetGroupDto refAssetGroup;
private List<AttachmentAssociationDto> refattachmentDtoLst;

@JsonProperty("refattachmentDtoLst")
public List<AttachmentAssociationDto> getRefattachmentDtoLst() {
	return refattachmentDtoLst;
}
public void setRefattachmentDtoLst(List<AttachmentAssociationDto> refattachmentDtoLst) {
	this.refattachmentDtoLst = refattachmentDtoLst;
}
public BigDecimal getId() {
	return id;
}
public void setId(BigDecimal id) {
	this.id = id;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getManufacturer() {
	return manufacturer;
}
public void setManufacturer(String manufacturer) {
	this.manufacturer = manufacturer;
}
public String getModelNum() {
	return modelNum;
}
public void setModelNum(String modelNum) {
	this.modelNum = modelNum;
}
public String getSerialNum() {
	return serialNum;
}
public void setSerialNum(String serialNum) {
	this.serialNum = serialNum;
}
public String getUom() {
	return uom;
}
public void setUom(String uom) {
	this.uom = uom;
}
public String getCapacity() {
	return capacity;
}
public void setCapacity(String capacity) {
	this.capacity = capacity;
}
public String getUniqueCode() {
	return uniqueCode;
}
public void setUniqueCode(String uniqueCode) {
	this.uniqueCode = uniqueCode;
}
public LocationDto getRefLocation() {
	return refLocation;
}
public void setRefLocation(LocationDto refLocation) {
	this.refLocation = refLocation;
}
public TypeDto getRefType() {
	return refType;
}
public void setRefType(TypeDto refType) {
	this.refType = refType;
}
public CategoryDto getRefCategory() {
	return refCategory;
}
public void setRefCategory(CategoryDto refCategory) {
	this.refCategory = refCategory;
}
public String getDepartment() {
	return department;
}
public void setDepartment(String department) {
	this.department = department;
}
public LocalDateTime getWarrantyExpiryDate() {
	return warrantyExpiryDate;
}
public void setWarrantyExpiryDate(LocalDateTime warrantyExpiryDate) {
	this.warrantyExpiryDate = warrantyExpiryDate;
}
public StatusDto getRefStatus() {
	return refStatus;
}
public void setRefStatus(StatusDto refStatus) {
	this.refStatus = refStatus;
}
public LocalDateTime getPurchaseDate() {
	return purchaseDate;
}
public void setPurchaseDate(LocalDateTime purchaseDate) {
	this.purchaseDate = purchaseDate;
}
public BigDecimal getPurchasePrice() {
	return purchasePrice;
}
public void setPurchasePrice(BigDecimal purchasePrice) {
	this.purchasePrice = purchasePrice;
}
public String getDescription() {
	return description;
}
public void setDescription(String description) {
	this.description = description;
}
public BigDecimal getSubComponent() {
	return subComponent;
}
public void setSubComponent(BigDecimal subComponent) {
	this.subComponent = subComponent;
}
public String getUsefulLife() {
	return usefulLife;
}
public void setUsefulLife(String usefulLife) {
	this.usefulLife = usefulLife;
}
public Integer getsNoCount() {
	return sNoCount;
}
public void setsNoCount(Integer sNoCount) {
	this.sNoCount = sNoCount;
}
public LocalDateTime getUpdatedDate() {
	return updatedDate;
}
public void setUpdatedDate(LocalDateTime updatedDate) {
	this.updatedDate = updatedDate;
}
public BigDecimal getUpdatedBy() {
	return updatedBy;
}
public void setUpdatedBy(BigDecimal updatedBy) {
	this.updatedBy = updatedBy;
}
public LocalDateTime getDeletedDate() {
	return deletedDate;
}
public void setDeletedDate(LocalDateTime deletedDate) {
	this.deletedDate = deletedDate;
}
public BigDecimal getDeletedBy() {
	return deletedBy;
}
public void setDeletedBy(BigDecimal deletedBy) {
	this.deletedBy = deletedBy;
}
public BigDecimal getCreatedBy() {
	return createdBy;
}
public void setCreatedBy(BigDecimal createdBy) {
	this.createdBy = createdBy;
}
public LocalDateTime getCreatedDate() {
	return createdDate;
}
public void setCreatedDate(LocalDateTime createdDate) {
	this.createdDate = createdDate;
}
public Boolean getIsDeleted() {
	return isDeleted;
}
public void setIsDeleted(Boolean isDeleted) {
	this.isDeleted = isDeleted;
}
public AssetGroupDto getRefAssetGroup() {
	return refAssetGroup;
}
public void setRefAssetGroup(AssetGroupDto refAssetGroup) {
	this.refAssetGroup = refAssetGroup;
}
	
}
