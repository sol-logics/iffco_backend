package com.sollogics.fams.services.dto;

import java.math.BigDecimal;

public class RolesDto {
	
	private BigDecimal id;
	private String type;
	
	public BigDecimal getId() {
		return id;
	}
	public void setId(BigDecimal id) {
		this.id = id;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}

}
