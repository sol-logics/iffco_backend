package com.sollogics.fams.services.dto;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
@JsonIgnoreProperties(ignoreUnknown = true)
public class VendorsDto {
	
	private BigDecimal id;
	
	private String name;
	private String contactPerson;
	private BigDecimal createdBy;
	private LocalDateTime createdDate;
	private LocalDateTime updatedDate;
	private Boolean isDeleted;
	private LocalDateTime deletedOn;
	private BigDecimal deletedBy;
	private Boolean discontinue;
	private LocalDateTime discontinueOn;
	private BigDecimal discontinueBy;
	private BigDecimal updatedBy;
	private String address;
	private String email;
	private String mobileNumber;
	private String landline;
	private String gstin;
	private String designation;
	private String website;
	private String description;
	private List<AttachmentAssociationDto> refattachmentDtoLst;

	@JsonProperty("refattachmentDtoLst")
	public List<AttachmentAssociationDto> getRefattachmentDtoLst() {
		return refattachmentDtoLst;
	}
	public void setRefattachmentDtoLst(List<AttachmentAssociationDto> refattachmentDtoLst) {
		this.refattachmentDtoLst = refattachmentDtoLst;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getLandline() {
		return landline;
	}
	public void setLandline(String landline) {
		this.landline = landline;
	}
	public String getGstin() {
		return gstin;
	}
	public void setGstin(String gstin) {
		this.gstin = gstin;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	public String getWebsite() {
		return website;
	}
	public void setWebsite(String website) {
		this.website = website;
	}
	public BigDecimal getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(BigDecimal updatedBy) {
		this.updatedBy = updatedBy;
	}
	public BigDecimal getId() {
		return id;
	}
	public void setId(BigDecimal id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getContactPerson() {
		return contactPerson;
	}
	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}
	public BigDecimal getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}
	public LocalDateTime getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(LocalDateTime createdDate) {
		this.createdDate = createdDate;
	}
	public LocalDateTime getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(LocalDateTime updatedDate) {
		this.updatedDate = updatedDate;
	}
	public Boolean getIsDeleted() {
		return isDeleted;
	}
	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	public LocalDateTime getDeletedOn() {
		return deletedOn;
	}
	public void setDeletedOn(LocalDateTime deletedOn) {
		this.deletedOn = deletedOn;
	}
	public BigDecimal getDeletedBy() {
		return deletedBy;
	}
	public void setDeletedBy(BigDecimal deletedBy) {
		this.deletedBy = deletedBy;
	}
	public Boolean getDiscontinue() {
		return discontinue;
	}
	public void setDiscontinue(Boolean discontinue) {
		this.discontinue = discontinue;
	}
	public LocalDateTime getDiscontinueOn() {
		return discontinueOn;
	}
	public void setDiscontinueOn(LocalDateTime discontinueOn) {
		this.discontinueOn = discontinueOn;
	}
	public BigDecimal getDiscontinueBy() {
		return discontinueBy;
	}
	public void setDiscontinueBy(BigDecimal discontinueBy) {
		this.discontinueBy = discontinueBy;
	}

}
