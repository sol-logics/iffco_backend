package com.sollogics.fams.services.dto;

import java.math.BigDecimal;

public class CompanyDto {
	 	private BigDecimal id;
	 	private String name;
		private String address;
		private String website;
		private String contact;

		public BigDecimal getId() {
			return id;
		}
		public void setId(BigDecimal id) {
			this.id = id;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getAddress() {
			return address;
		}
		public void setAddress(String address) {
			this.address = address;
		}
		public String getWebsite() {
			return website;
		}
		public void setWebsite(String website) {
			this.website = website;
		}
		public String getContact() {
			return contact;
		}
		public void setContact(String contact) {
			this.contact = contact;
		}
}
