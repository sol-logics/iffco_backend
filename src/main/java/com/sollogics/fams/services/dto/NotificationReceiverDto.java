package com.sollogics.fams.services.dto;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class NotificationReceiverDto {
private BigDecimal id;
private BigDecimal refNotiConfigId;
private UsersDto refUser;
private BigDecimal createdBy;
private LocalDateTime createdDate;
private BigDecimal updatedBy;
private LocalDateTime updatedDate;
private Boolean isDeleted;
private BigDecimal deletedBy;
private LocalDateTime deletedDate;

public BigDecimal getUpdatedBy() {
	return updatedBy;
}
public void setUpdatedBy(BigDecimal updatedBy) {
	this.updatedBy = updatedBy;
}
public LocalDateTime getUpdatedDate() {
	return updatedDate;
}
public void setUpdatedDate(LocalDateTime updatedDate) {
	this.updatedDate = updatedDate;
}
public Boolean getIsDeleted() {
	return isDeleted;
}
public void setIsDeleted(Boolean isDeleted) {
	this.isDeleted = isDeleted;
}
public BigDecimal getDeletedBy() {
	return deletedBy;
}
public void setDeletedBy(BigDecimal deletedBy) {
	this.deletedBy = deletedBy;
}
public LocalDateTime getDeletedDate() {
	return deletedDate;
}
public void setDeletedDate(LocalDateTime deletedDate) {
	this.deletedDate = deletedDate;
}
	public BigDecimal getId() {
	return id;
}
public void setId(BigDecimal id) {
	this.id = id;
}
public BigDecimal getrefNotiConfigId() {
	return refNotiConfigId;
}
public void setrefNotiConfigId(BigDecimal refNotiConfigId) {
	this.refNotiConfigId = refNotiConfigId;
}
public UsersDto getRefUser() {
	return refUser;
}
public void setRefUser(UsersDto refUser) {
	this.refUser = refUser;
}
public BigDecimal getCreatedBy() {
	return createdBy;
}
public void setCreatedBy(BigDecimal createdBy) {
	this.createdBy = createdBy;
}
public LocalDateTime getCreatedDate() {
	return createdDate;
}
public void setCreatedDate(LocalDateTime createdDate) {
	this.createdDate = createdDate;
}

}
