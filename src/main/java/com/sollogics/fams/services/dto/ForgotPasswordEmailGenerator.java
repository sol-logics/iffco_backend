package com.sollogics.fams.services.dto;

import java.io.StringWriter;
import java.time.LocalDateTime;

import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sollogics.fams.repositories.modal.NotificationAlertDetails;
import com.sollogics.fams.repositories.modal.Profile;
import com.sollogics.fams.repositories.modal.UserOtp;
import com.sollogics.fams.utils.Constants;
import com.sollogics.fams.utils.MailContent;

@Component
public class ForgotPasswordEmailGenerator implements EmailBodyGenerator<UserOtp> {
		
		 @Autowired
			private VelocityEngine velocityEngine;
		    
		    static final String UTF_8_ENCODING = "UTF-8";
		    @Override
		    public MailContent generateEmailContent(UserOtp otpForPassword, String version) {
		        MailContent mailContent = new MailContent();

		        mailContent.setTo(otpForPassword.getRefUser().getEmail());
		        mailContent.setFrom(Constants.FROM_EMAIL);
		        mailContent.setSubject(Constants.REGISTRATION_EMAIL_SUBJECT);
		       try{
		    	  VelocityContext context = new VelocityContext();
		    	  context.put("otp", otpForPassword.getOtp());
		        
		        StringWriter stringWriter = new StringWriter();
		        velocityEngine.mergeTemplate("forgot_password.vm", "UTF-8", context, stringWriter);
		        String mailBody = stringWriter.toString();
		        
		        mailContent.setMailBody(mailBody);
		       }
		       catch(Exception e){
		    	   e.printStackTrace();
		       }
		        return mailContent;
		    	
		    }
		    @Override
			public MailContent generateEmailContentForApproval(Profile profile) {
				MailContent mailContent = new MailContent();
				mailContent.setTo("shreya@sollogics.com");
				mailContent.setFrom(Constants.FROM_EMAIL);
				mailContent.setSubject(Constants.FOR_REGISTRATION_USER_SUBJECT);
				try{
				  VelocityContext context = new VelocityContext();
			    	  context.put("user",profile.getFirstName());
			    	  //context.put("role",userProfile.getRefUserRole().getName());
			        StringWriter stringWriter = new StringWriter();
			        velocityEngine.mergeTemplate("approve_user.vm", "UTF-8", context, stringWriter);
			        String mailBody = stringWriter.toString();
			        mailContent.setMailBody(mailBody);
				}
			        catch(Exception e){
			     	   e.printStackTrace();
			        }
			         return mailContent;
			}
			@Override
			public MailContent generateEmailContentForPassword(UsersDto usersDto,String password) {
				MailContent mailContent = new MailContent();

		        mailContent.setTo(usersDto.getEmail());
		        mailContent.setFrom(Constants.FROM_EMAIL);
		        mailContent.setSubject(Constants.REGISTRATION_EMAIL_SUBJECT);
		        try{
			    	  VelocityContext context = new VelocityContext();
			    	  context.put("password", password);
			        
			        StringWriter stringWriter = new StringWriter();
			        velocityEngine.mergeTemplate("generate_password.vm", "UTF-8", context, stringWriter);
			        String mailBody = stringWriter.toString();
			        
			        mailContent.setMailBody(mailBody);
			       }
			       catch(Exception e){
			    	   e.printStackTrace();
			       }
			        return mailContent;
			}

			public MailContent generateEmailContentForNotification(NotificationAlertDetails notiAlertDtls,
					Integer beforeDays, LocalDateTime expiryDate,String assetUniqueCode, String amcUniqueCode) {
				MailContent mailContent = new MailContent();
				
				mailContent.setTo(notiAlertDtls.getCc_to());
				mailContent.setFrom(Constants.FROM_EMAIL);
				mailContent.setSubject(notiAlertDtls.getMailSubject());
				  try{
			    	  VelocityContext context = new VelocityContext();
			    	  context.put("AMC_BEFORE_DAYS", beforeDays);
			    	  context.put("AMC_EXPIRY_DATE", expiryDate);
			    	  context.put("ASSET_UNIQUE_ID", assetUniqueCode);
			    	  context.put("AMC_UNIQUE_ID", amcUniqueCode);
			        
			        StringWriter stringWriter = new StringWriter();
			        velocityEngine.mergeTemplate("amc_notification.vm", "UTF-8", context, stringWriter);
			        String mailBody = stringWriter.toString();
			        
			        mailContent.setMailBody(mailBody);
			       }
			       catch(Exception e){
			    	   e.printStackTrace();
			       }
			        return mailContent;
			}

	}

