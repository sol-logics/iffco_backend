package com.sollogics.fams.services.dto;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)

public class AmcDto {
	
private BigDecimal id;

private BigDecimal createdBy;
private LocalDateTime createdDate;
private BigDecimal updatedBy;
private LocalDateTime updatedDate;
private Boolean isDeleted;
private LocalDateTime deletedDate;
private BigDecimal deletedBy;
private VendorsDto refVendor;
private AssetDto refAsset;
private LocalDateTime startDate;
private LocalDateTime expiryDate;
private BigDecimal cost;
private String amcUniqueCode;
private List<AmcServicesDto> amcServicesDtoLst;
private BigDecimal previousAmcId;
private String renewUuid;
private Boolean isLatest;
private StatusDto refStatus;
private Integer sNumberCount;
private List<AttachmentAssociationDto> refattachmentDtoLst;
@JsonProperty("refattachmentDtoLst")
public List<AttachmentAssociationDto> getRefattachmentDtoLst() {
	return refattachmentDtoLst;
}
public void setRefattachmentDtoLst(List<AttachmentAssociationDto> refattachmentDtoLst) {
	this.refattachmentDtoLst = refattachmentDtoLst;
}

/*
 * private List<UploadFileResponse> refuploadFileResponse;
 * 
 * @JsonProperty("refuploadFileResponse") public List<UploadFileResponse>
 * getRefuploadFileResponse() { return refuploadFileResponse; } public void
 * setRefuploadFileResponse(List<UploadFileResponse> refuploadFileResponse) {
 * this.refuploadFileResponse = refuploadFileResponse; }
 */
public Integer getsNumberCount() {
	return sNumberCount;
}
public void setsNumberCount(Integer sNumberCount) {
	this.sNumberCount = sNumberCount;
}
public BigDecimal getPreviousAmcId() {
	return previousAmcId;
}
public void setPreviousAmcId(BigDecimal previousAmcId) {
	this.previousAmcId = previousAmcId;
}
public String getRenewUuid() {
	return renewUuid;
}
public void setRenewUuid(String renewUuid) {
	this.renewUuid = renewUuid;
}
public Boolean getIsLatest() {
	return isLatest;
}
public void setIsLatest(Boolean isLatest) {
	this.isLatest = isLatest;
}
public StatusDto getRefStatus() {
	return refStatus;
}
public void setRefStatus(StatusDto refStatus) {
	this.refStatus = refStatus;
}
public String getAmcUniqueCode() {
	return amcUniqueCode;
}
public void setAmcUniqueCode(String amcUniqueCode) {
	this.amcUniqueCode = amcUniqueCode;
}	
	public List<AmcServicesDto> getAmcServicesDtoLst() {
	return amcServicesDtoLst;
}
public void setAmcServicesDtoLst(List<AmcServicesDto> amcServicesDtoLst) {
	this.amcServicesDtoLst = amcServicesDtoLst;
}
	public BigDecimal getId() {
	return id;
}
public void setId(BigDecimal id) {
	this.id = id;
}
public BigDecimal getCreatedBy() {
	return createdBy;
}
public void setCreatedBy(BigDecimal createdBy) {
	this.createdBy = createdBy;
}
public LocalDateTime getCreatedDate() {
	return createdDate;
}
public void setCreatedDate(LocalDateTime createdDate) {
	this.createdDate = createdDate;
}
public BigDecimal getUpdatedBy() {
	return updatedBy;
}
public void setUpdatedBy(BigDecimal updatedBy) {
	this.updatedBy = updatedBy;
}
public LocalDateTime getUpdatedDate() {
	return updatedDate;
}
public void setUpdatedDate(LocalDateTime updatedDate) {
	this.updatedDate = updatedDate;
}
public Boolean getIsDeleted() {
	return isDeleted;
}
public void setIsDeleted(Boolean isDeleted) {
	this.isDeleted = isDeleted;
}
public LocalDateTime getDeletedDate() {
	return deletedDate;
}
public void setDeletedDate(LocalDateTime deletedDate) {
	this.deletedDate = deletedDate;
}
public BigDecimal getDeletedBy() {
	return deletedBy;
}
public void setDeletedBy(BigDecimal deletedBy) {
	this.deletedBy = deletedBy;
}
public VendorsDto getRefVendor() {
	return refVendor;
}
public void setRefVendor(VendorsDto refVendor) {
	this.refVendor = refVendor;
}
public AssetDto getRefAsset() {
	return refAsset;
}
public void setRefAsset(AssetDto refAsset) {
	this.refAsset = refAsset;
}
public LocalDateTime getStartDate() {
	return startDate;
}
public void setStartDate(LocalDateTime startDate) {
	this.startDate = startDate;
}
public LocalDateTime getExpiryDate() {
	return expiryDate;
}
public void setExpiryDate(LocalDateTime expiryDate) {
	this.expiryDate = expiryDate;
}
public BigDecimal getCost() {
	return cost;
}
public void setCost(BigDecimal cost) {
	this.cost = cost;
}	

}
