package com.sollogics.fams.repositories.daoImpl;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.sollogics.fams.exceptionHandler.GlobalException;
import com.sollogics.fams.repositories.dao.FileStorageDao;
import com.sollogics.fams.repositories.modal.AttachmentAssociation;
import com.sollogics.fams.utils.BaseHibernate;

@Transactional
@Repository
public class FileStorageDaoImpl extends BaseHibernate implements FileStorageDao{
	private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

	@Override
	public AttachmentAssociation saveAttachmentDetails(AttachmentAssociation attachmentAssociation) {
		try {
			LOGGER.debug("Enter into : saveAttachmentDetails()");
				getSession().saveOrUpdate(attachmentAssociation);
				LOGGER.debug("Exit from the saveAttachmentDetails() method ");
			return attachmentAssociation;
		}catch(Exception e) {
			LOGGER.error(e.getMessage());
			throw new GlobalException(500, "Something went wrong");
		}
	}

	@Override
	public List<AttachmentAssociation> getAttachmentList(String colName, BigDecimal objectId) {
		try {
			LOGGER.debug("Enter into : getAttachmentList()");
			String query = "From AttachmentAssociation Where (isDeleted=null Or isDeleted = false) And " +colName+"="+objectId;
			List<AttachmentAssociation> attachmentAssociationLst = getSession().createQuery(query).list();
			LOGGER.debug("Exit from : getAttachmentList()");
			return attachmentAssociationLst;
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			throw new GlobalException(500, "Something went wrong");
		}
	}

	@Override
	public Boolean deleteFileByFileName(String commaSepList,BigDecimal objectId,LocalDateTime deletedDate,String colName) {
		try {
			LOGGER.debug("Enter into : deleteVendorFileByFileName()");
//			String commaSepStrong =  existingPreviousLst.stream().map(String::valueOf).collect(Collectors.joining(","));
			String query = "Update AttachmentAssociation Set isDeleted=true Where "+colName+"="+objectId+" And id IN ("+commaSepList+")";
			getSession().createQuery(query).executeUpdate();
			LOGGER.debug("Exit from : deleteVendorFileByFileName()");
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			return null;
		}
	}

	@Override
	public Boolean updateExistingFile(LocalDateTime deletedDate, String commaSepFileId,String colName, BigDecimal objectId,BigDecimal userId) {
		try {
			LOGGER.debug("Enter into : updateExistingFile()");
			String query = null;
			if(commaSepFileId != null) {
			query = "Update AttachmentAssociation Set isDeleted=true ,deletedDate='"+deletedDate+"',deletedBy="+userId+" Where "+colName+"="+objectId+" And Id Not In ("+commaSepFileId+")";
			}else {
				query = "Update AttachmentAssociation Set isDeleted=true ,deletedDate='"+deletedDate+"',deletedBy="+userId+" Where "+colName+"="+objectId;
			}
			getSession().createQuery(query).executeUpdate();
			LOGGER.debug("Exit from : updateExistingFile()");
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			return null;
		}
	}

	/*
	 * @SuppressWarnings("unchecked")
	 * 
	 * @Override public Boolean deleteAttachmentForCurrentObject(String
	 * colName,BigDecimal objectId,BigDecimal userId) { try {
	 * LOGGER.debug("Enter into : deleteAttachmentForCurrentObject()");
	 * LocalDateTime deletedDate = DateConversionUtil.CurrentUTCTime(); String query
	 * = "Update AttachmentAssociation Set isDeleted=true ,deletedDate='"
	 * +deletedDate+"',deletedBy ="+userId+" Where "+colName+"="+objectId;
	 * getSession().createQuery(query).executeUpdate();
	 * LOGGER.debug("Exit from : deleteAttachmentForCurrentObject()"); return true;
	 * }catch(Exception e) { LOGGER.error(e.getMessage()); throw new
	 * GlobalException(500, "Something went wrong"); } }
	 */
	
	
/*
 * @Override public List<AttachmentAssociation> getAttachmentList(BigDecimal
 * objectId, String colName){ try {
 * 
 * String query =
 * "From AttachmentAssociation Where (isDeleted=null Or isDeleted = false) And "
 * +colName+"="+objectId; Journal glHeader = (Journal)
 * getSession().createQuery(query).uniqueResult();
 * 
 * return null; } catch (Exception e) { LOGGER.error(e.getMessage()); throw new
 * GlobalException(500, "Something went wrong"); } }
 */
	

}
