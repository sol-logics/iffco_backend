package com.sollogics.fams.repositories.daoImpl;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.sollogics.fams.exceptionHandler.GlobalException;
import com.sollogics.fams.repositories.dao.AuditTrailDao;
import com.sollogics.fams.repositories.modal.AuditTrail;
import com.sollogics.fams.utils.BaseHibernate;

@Transactional
@Repository
public class AuditTrailDaoImpl extends BaseHibernate implements AuditTrailDao {
	private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

	@Override
	public Boolean saveAuditTrail(AuditTrail auditTrail) {
		try {
			LOGGER.debug("Enter into : saveAuditTrail()");
				getSession().saveOrUpdate(auditTrail);
				LOGGER.debug("Exit from : saveAuditTrail() method ");
			return true;
		}catch(Exception e) {
			LOGGER.error(e.getMessage());
			throw new GlobalException(500, "Something went wrong");
		}
	}

}
