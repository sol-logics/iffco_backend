package com.sollogics.fams.repositories.daoImpl;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.sollogics.fams.exceptionHandler.GlobalException;
import com.sollogics.fams.repositories.dao.NotificationConfigDao;
import com.sollogics.fams.repositories.modal.NotificationConfig;
import com.sollogics.fams.repositories.modal.NotificationMeta;
import com.sollogics.fams.repositories.modal.NotificationReceiver;
import com.sollogics.fams.utils.BaseHibernate;
import com.sollogics.fams.utils.DateConversionUtil;
@Transactional
@Repository
public class NotificationConfigDaoImpl extends BaseHibernate implements NotificationConfigDao{
	private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

	@Override
	public NotificationConfig saveNotificationConfig(NotificationConfig notificationConfig) {
		try {
			LOGGER.debug("Enter into : saveNotificationConfig()");
			getSession().saveOrUpdate(notificationConfig);
			LOGGER.debug("Exit from : saveNotificationConfig() method ");
			return notificationConfig;
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			throw new GlobalException(500, "Something went wrong");
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<NotificationConfig> getAllNotificationConfig() {
		try {
			LOGGER.debug("Enter into : getAllNotificationConfig()");
			String query = "From NotificationConfig Where isDeleted=false";
			List<NotificationConfig> notificationConfigLst = getSession().createQuery(query).list();
			LOGGER.debug("Exit from : getAllNotificationConfig()");
			return notificationConfigLst;
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			return null;
		}
	}

	@Override
	public NotificationConfig getNotificationConfigById(BigDecimal notificationConfigId) {
		
		try {
			LOGGER.debug("Enter into : getNotificationConfigById()");
			String query = "From NotificationConfig Where id="+notificationConfigId;
			NotificationConfig notificationConfig = (NotificationConfig) getSession().createQuery(query).uniqueResult();
			LOGGER.debug("Exit from : getNotificationConfigById()");
			return notificationConfig;
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			return null;
		}
	}

	@Override
	public Boolean deleteNotificationConfigById(BigDecimal userId, BigDecimal notificationConfigId,
			LocalDateTime deletedDate) {
		try {
			LOGGER.debug("Enter into : deleteNotificationConfigById()");
			String query = "Update NotificationConfig set isDeleted=true ,deletedDate='"+deletedDate+"',deletedBy="+userId+" Where id="+notificationConfigId;
			getSession().createQuery(query).executeUpdate();
			LOGGER.debug("Exit from : deleteNotificationConfigById()");
			return true;
		}catch(Exception e) {
			LOGGER.error(e.getMessage());
			throw new GlobalException(500, "Something went wrong");
		}
	}

	@Override
	public NotificationConfig getNotificationConfigById(String notifactionName) {
		try {
			LOGGER.debug("Enter into : getNotificationConfigById()");
			String query = "From NotificationConfig Where refNotificationMeta.notificationName="+notifactionName+"'";
			NotificationConfig notificationConfig = (NotificationConfig) getSession().createQuery(query).uniqueResult();
			LOGGER.debug("Exit from : getNotificationConfigById()");
			return notificationConfig;
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<NotificationReceiver> getNotiRcvrLstByConfigId(BigDecimal notificationConfigId) {
		try {
			LOGGER.debug("Enter into : getNotiRcvrLstByConfigId()");
			String query = "From NotificationReceiver Where refNotificationConfig="+notificationConfigId;
			List<NotificationReceiver> notificationReceiverLst = (List<NotificationReceiver>) getSession().createQuery(query).list();
			LOGGER.debug("Exit from : getNotiRcvrLstByConfigId()");
			return notificationReceiverLst;
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			return null;
		}
	}

	/*@Override
	public Boolean updateNotiRcvrByConfigId(BigDecimal notificationConfigId, BigDecimal userId) {
		try {
			LOGGER.debug("Enter into : updateNotiRcvrByConfigId()");
			String query = "Update NotificationReceiver Set refUser="+userId+"Where refNotificationConfig="+notificationConfigId;
			getSession().createQuery(query).executeUpdate();
			LOGGER.debug("Exit from : updateNotiRcvrByConfigId()");
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			return null;
		}
	}*/

	@Override
	public Boolean updateNotiRcvrLstByConfigId(BigDecimal notificationConfigId,List<NotificationReceiver> notiRcvrLst,List<NotificationReceiver> notiRcvrPreviousLst) {
		try {
			LOGGER.debug("Enter into : updateNotiRcvrLstByConfigId()");
			for(NotificationReceiver notificationReceiver : notiRcvrLst) {
				String query = "Update NotificationReceiver Set refUser="+notificationReceiver.getRefUser()+"Where refNotificationConfig="+notificationConfigId+" And id="+notificationReceiver.getId() ;
				getSession().createQuery(query).executeUpdate();
			}
			LOGGER.debug("Exit from : updateNotiRcvrLstByConfigId()");
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			return null;
		}
	}

	@Override
	public List<NotificationReceiver> updateNotiRcvr(BigDecimal userId,BigDecimal notificationConfigId, List<NotificationReceiver> notiRcvrUpdatedLst,
			List<NotificationReceiver> notiRcvrPreviousLst) {
		try {
			LOGGER.debug("Enter into : updateNotificationConfig()");
			for(NotificationReceiver notificationReceiverPrevious : notiRcvrPreviousLst) {
				String query = "Delete NotificationReceiver Where refNotificationConfig="+notificationConfigId+" And refUser="+notificationReceiverPrevious.getRefUser();
				getSession().createQuery(query).executeUpdate();
			}
			List<NotificationReceiver> notificationReceiverLst = new ArrayList<>();
			for(NotificationReceiver notificationReceiverUpdated : notiRcvrUpdatedLst) {
				notificationReceiverUpdated.setUpdatedBy(userId);
				notificationReceiverUpdated.setUpdatedDate(DateConversionUtil.CurrentUTCTime());
				getSession().saveOrUpdate(notificationReceiverUpdated);
				notificationReceiverLst.add(notificationReceiverUpdated);
			}
			LOGGER.debug("Exit from : updateNotificationConfig()");
			return notificationReceiverLst;
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			return null;
		}
	}

	@Override
	public List<NotificationReceiver> saveNotificationReceiver(BigDecimal userId,
			List<NotificationReceiver> notificationReceiverLst) {
		try {
			LOGGER.debug("Enter into : saveNotificationReceiver()");
			List<NotificationReceiver> notificationReceiverList = new ArrayList<>();
			for(NotificationReceiver notificationReceiver : notificationReceiverLst) {
				notificationReceiver.setCreatedBy(userId);
				notificationReceiver.setCreatedDate(DateConversionUtil.CurrentUTCTime());
				notificationReceiver.setIsDeleted(false);
				getSession().saveOrUpdate(notificationReceiver);
				notificationReceiverList.add(notificationReceiver);
			}
			
			LOGGER.debug("Exit from the saveNotificationReceiver() method ");
		return notificationReceiverList;
		}catch(Exception e) {
			LOGGER.error(e.getMessage());
			throw new GlobalException(500, "Something went wrong");
		}
	}

	@Override
	public List<NotificationReceiver> updateNotificationReceiver(List<NotificationReceiver> notiRcvrLst) {
		try {
			LOGGER.debug("Enter into : updateNotificationReceiver()");
			List<NotificationReceiver> notiReceiverLst = new ArrayList<>();
			for(NotificationReceiver notiRcvrUpdated : notiRcvrLst) {
				getSession().saveOrUpdate(notiRcvrUpdated);
				notiReceiverLst.add(notiRcvrUpdated);
			}
			LOGGER.debug("Exit from : updateNotificationReceiver()");
			return notiReceiverLst;
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			return null;
		}
	}

	@Override
	public Boolean deleteNotificationReceiver(String existingPreviousLst) {
		try {
			LOGGER.debug("Enter into : deleteNotificationReceiver()");
//			String commaSepStrong =  existingPreviousLst.stream().map(String::valueOf).collect(Collectors.joining(","));
			String query = "delete From NotificationReceiver Where id NOT IN ("+existingPreviousLst+")";
			getSession().createQuery(query).executeUpdate();
			LOGGER.debug("Exit from : deleteNotificationReceiver()");
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<NotificationMeta> getAllNotificationMeta() {
		try {
			LOGGER.debug("Enter into : getAllNotificationMeta()");
			String query = "From NotificationMeta Where isDeleted=false";
			List<NotificationMeta> notificationMetaLst = getSession().createQuery(query).list();
			LOGGER.debug("Exit from : getAllNotificationMeta()");
			return notificationMetaLst;
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			return null;
		}
	}

}
