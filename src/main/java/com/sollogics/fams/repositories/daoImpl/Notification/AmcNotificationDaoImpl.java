package com.sollogics.fams.repositories.daoImpl.Notification;

import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.sollogics.fams.exceptionHandler.GlobalException;
import com.sollogics.fams.repositories.dao.Notification.AmcNotificationDao;
import com.sollogics.fams.repositories.modal.Amc;
import com.sollogics.fams.repositories.modal.NotificationAlertDetails;
import com.sollogics.fams.repositories.modal.NotificationConfig;
import com.sollogics.fams.utils.BaseHibernate;
@Transactional
@Repository
public class AmcNotificationDaoImpl extends BaseHibernate implements AmcNotificationDao {
	private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

	@Override
	public NotificationConfig getNotiConfigByName(String notiName) {
		try {
			LOGGER.debug("Enter into : getNotiConfigByName()");
			String query = "From NotificationConfig Where refNotificationMeta.notificationName ="+notiName+",";
			NotificationConfig notificationConfig = (NotificationConfig) getSession().createQuery(query).uniqueResult();
				LOGGER.debug("Exit from the getNotiConfigByName() method ");
			return notificationConfig;
		}catch(Exception e) {
			LOGGER.error(e.getMessage());
			throw new GlobalException(500, "Something went wrong");
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Amc> getAmcLstByBeforeDays(Integer beforeDays) {
		try {
			LOGGER.debug("Enter into : getNotiConfigByName()");
			String query = "From Amc Where now() >= (expiry_date::date -'"+beforeDays+" day'::interval)";
			List<Amc> amcLst = getSession().createQuery(query).list();
				LOGGER.debug("Exit from the getNotiConfigByName() method ");
			return amcLst;
		}catch(Exception e) {
			LOGGER.error(e.getMessage());
			throw new GlobalException(500, "Something went wrong");
		}
	}

	@Override
	public List<NotificationAlertDetails> saveAmcLstInAlertDtls(List<NotificationAlertDetails> notiAlertDtlsLst) {
		try {
			LOGGER.debug("Enter into : saveAmcLstInAlertDtls()");
			getSession().saveOrUpdate(notiAlertDtlsLst);
			LOGGER.debug("Exit from : saveAmcLstInAlertDtls() method ");
			return notiAlertDtlsLst;
		}catch(Exception e) {
			LOGGER.error(e.getMessage());
			throw new GlobalException(500, "Something went wrong");
		}
	}

	/*@Override
	public NotificationAlertDetails saveCcTo(String commaSepList) {

		try {
			LOGGER.debug("Enter into : saveNotiReceivers()");
			
			//INSERT INTO users (email) VALUES ('shreya@gmail.com , dwivedi@gmail.com')

			String query = "Insert Into NotificationAlertDetails cc_to Values ('"+commaSepList+"')";
			NotificationAlertDetails notiAlertDetails = (NotificationAlertDetails) getSession().createQuery(query).uniqueResult();
			LOGGER.debug("Exit from : saveNotiReceivers()");
			return notiAlertDetails;
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			return null;
		}
	
	}*/

}
