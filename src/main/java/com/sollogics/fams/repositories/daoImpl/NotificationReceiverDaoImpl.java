package com.sollogics.fams.repositories.daoImpl;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.sollogics.fams.exceptionHandler.GlobalException;
import com.sollogics.fams.repositories.dao.NotificationReceiverDao;
import com.sollogics.fams.repositories.modal.NotificationReceiver;
import com.sollogics.fams.utils.BaseHibernate;

@Transactional
@Repository
public class NotificationReceiverDaoImpl extends BaseHibernate implements NotificationReceiverDao {
	private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

	@Override
	public NotificationReceiver saveNotificationReceiver(NotificationReceiver notificationReceiver) {
		try {
			LOGGER.debug("Enter into : saveNotificationReceiver()");
			getSession().saveOrUpdate(notificationReceiver);
			LOGGER.debug("Exit from : saveNotificationReceiver() method ");
			return notificationReceiver;
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			throw new GlobalException(500, "Something went wrong");
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<NotificationReceiver> getAllNotificationReceiver() {
		try {
			LOGGER.debug("Enter into : getAllNotificationReceiver()");
			String query = "From NotificationReceiver Where isDeleted=false";
			List<NotificationReceiver> notificationReceiverLst = getSession().createQuery(query).list();
			LOGGER.debug("Exit from : getAllNotificationReceiver()");
			return notificationReceiverLst;
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			return null;
		}
	}

	@Override
	public NotificationReceiver getNotificationReceiverById(BigDecimal notificationId) {
		try {
			LOGGER.debug("Enter into : getNotificationReceiverById()");
			String query = "From NotificationReceiver Where id="+notificationId;
			NotificationReceiver notificationReceiver = (NotificationReceiver) getSession().createQuery(query).uniqueResult();
			LOGGER.debug("Exit from : getNotificationReceiverById()");
			return notificationReceiver;
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			return null;
		}
	}

	@Override
	public Boolean deleteNotificationReceiverById(BigDecimal userId, BigDecimal notificationId,
			LocalDateTime deletedDate) {
		try {
			LOGGER.debug("Enter into : deleteNotificationReceiverById()");
			String query = "Update NotificationReceiver set isDeleted=true ,deletedDate='"+deletedDate+"',deletedBy="+userId+" Where id="+notificationId;
			getSession().createQuery(query).executeUpdate();
			LOGGER.debug("Exit from : deleteNotificationReceiverById()");
			return true;
		}catch(Exception e) {
			LOGGER.error(e.getMessage());
			throw new GlobalException(500, "Something went wrong");
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<NotificationReceiver> getNotiRcvrListByUserId(String colName,BigDecimal objectId) {
		try {
			LOGGER.debug("Enter into : getNotiRcvrListByUserId()");
			String query = "From NotificationReceiver Where (isDeleted = false Or isDeleted = null) And "+colName+"= "+objectId;
			List<NotificationReceiver> notiRcvrList = getSession().createQuery(query).list();
			LOGGER.debug("Exit from : getNotiRcvrListByUserId()");
			return notiRcvrList;
		}catch(Exception e) {
			LOGGER.error(e.getMessage());
			throw new GlobalException(500, "Something went wrong");
		}
	}

}
