package com.sollogics.fams.repositories.daoImpl;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.sollogics.fams.exceptionHandler.GlobalException;
import com.sollogics.fams.repositories.dao.AmcDao;
import com.sollogics.fams.repositories.modal.Amc;
import com.sollogics.fams.repositories.modal.AmcServices;
import com.sollogics.fams.repositories.modal.Status;
import com.sollogics.fams.utils.BaseHibernate;
import com.sollogics.fams.utils.DateConversionUtil;

@Transactional
@Repository
public class AmcDaoImpl extends BaseHibernate implements AmcDao {
	private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

	@Override
	public Amc saveAmc(Amc amc) {
		try {
			LOGGER.debug("Enter into : saveAmc()");
				getSession().saveOrUpdate(amc);
				LOGGER.debug("Exit from the saveAmc() method ");
			return amc;
		}catch(Exception e) {
			LOGGER.error(e.getMessage());
			throw new GlobalException(500, "Something went wrong");
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Amc> getAllAmc() {
		try {
			LOGGER.debug("Enter into : getAllAmc()");
			String query = "From Amc Where isDeleted=false And isLatest=true";
			List<Amc> amcLst = getSession().createQuery(query).list();
			LOGGER.debug("Exit from : getAllAmc()");
			return amcLst;
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			return null;
		}
	}

	@Override
	public Amc getAmcById(BigDecimal amcId) {
		try {
			LOGGER.debug("Enter into : getAmcById()");
			String query = "From Amc Where id="+amcId;
			Amc amc = (Amc) getSession().createQuery(query).uniqueResult();
			LOGGER.debug("Exit from : getAmcById()");
			return amc;
		}catch(Exception e) {
			LOGGER.error(e.getMessage());
			throw new GlobalException(500, "Something went wrong");
		}
		
	}

	@Override
	public Boolean deleteAmcById(BigDecimal userId, BigDecimal amcId, LocalDateTime deletedDate) {
		try {
			LOGGER.debug("Enter into : deleteAmcById()");
			String queryForAmcService = "Update AmcServices Set isDeleted=true, deletedDate='"+deletedDate+"', deletedBy="+userId+"Where refAmc="+amcId;
			getSession().createQuery(queryForAmcService).executeUpdate();
			String query = "Update Amc Set isDeleted=true ,deletedDate='"+deletedDate+"',deletedBy="+userId+" Where id="+amcId;
			getSession().createQuery(query).executeUpdate();
			LOGGER.debug("Exit from the deleteAmcById() method ");
			return true;
		}catch(Exception e) {
			LOGGER.error(e.getMessage());
			throw new GlobalException(500, "Something went wrong");
		}
	}

	@Override
	public List<AmcServices> saveAmcService(BigDecimal userId,List<AmcServices> amcServicesLst) {
		try {
			LOGGER.debug("Enter into : saveAmcService()");
			List<AmcServices> amcServicesList = new ArrayList<>();
			for(AmcServices amcServices : amcServicesLst) {
				amcServices.setCreatedBy(userId);
				amcServices.setCreatedDate(DateConversionUtil.CurrentUTCTime());
				amcServices.setIsDeleted(false);
				getSession().saveOrUpdate(amcServices);
				amcServicesList.add(amcServices);
			}
			
			LOGGER.debug("Exit from the saveAmcService() method ");
		return amcServicesList;
		}catch(Exception e) {
			LOGGER.error(e.getMessage());
			throw new GlobalException(500, "Something went wrong");
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AmcServices> getAllAmcServices() {
		try {
			LOGGER.debug("Enter into : getAllAmcServices()");
			String query = "From AmcServices Where isDeleted=false";
			List<AmcServices> amcServicesLst = getSession().createQuery(query).list();
			LOGGER.debug("Exit from : getAllAmcServices()");
			return amcServicesLst;
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			return null;
		}
	}

	@Override
	public AmcServices getAmcServiceById(BigDecimal amcServiceId) {
		try {
			LOGGER.debug("Enter into : getAmcServiceById()");
			String query = "From AmcServices Where id="+amcServiceId;
			AmcServices amcServices = (AmcServices) getSession().createQuery(query).uniqueResult();
			LOGGER.debug("Exit from : getAmcServiceById()");
			return amcServices;
		}catch(Exception e) {
			LOGGER.error(e.getMessage());
			throw new GlobalException(500, "Something went wrong");
		}
	}

	@Override
	public Boolean deleteAmcServiceById(BigDecimal userId, BigDecimal amcServiceId, LocalDateTime deletedDate) {
		try {
			LOGGER.debug("Enter into : deleteAmcServiceById()");
			String query = "Update AmcServices set isDeleted=true ,deletedDate='"+deletedDate+"',deletedBy="+userId+" Where id="+amcServiceId;
			getSession().createQuery(query).executeUpdate();
			LOGGER.debug("Exit from the deleteAmcServiceById() method ");
			return true;
		}catch(Exception e) {
			LOGGER.error(e.getMessage());
			throw new GlobalException(500, "Something went wrong");
		}
	}

	@Override
	public List<AmcServices> getAmcServiceByAmc(BigDecimal amcId) {
		try {
			LOGGER.debug("Enter into : getAmcServiceById()");
			String query = "From AmcServices Where refAmc="+amcId;
			List<AmcServices> amcServicesLst = getSession().createQuery(query).list();
			LOGGER.debug("Exit from : getAmcServiceById()");
			return amcServicesLst;
		}catch(Exception e) {
			LOGGER.error(e.getMessage());
			throw new GlobalException(500, "Something went wrong");
		}
	}

	@Override
	public List<AmcServices> updateAmcServices(List<AmcServices> amcServicesUpdatedLst) {
		try {
			LOGGER.debug("Enter into : updateAmcServices()");
			List<AmcServices> amcServicesLst = new ArrayList<>();
			for(AmcServices amcServicesUpdated : amcServicesUpdatedLst) {
				getSession().saveOrUpdate(amcServicesUpdated);
				amcServicesLst.add(amcServicesUpdated);
			}
			LOGGER.debug("Exit from : updateAmcServices()");
			return amcServicesLst;
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			return null;
		}
	}

	@Override
	public Boolean deleteAmcServices(String existingPreviousLst) {
		try {
			LOGGER.debug("Enter into : deleteAmcServices()");
//			String commaSepStrong =  existingPreviousLst.stream().map(String::valueOf).collect(Collectors.joining(","));
			String query = "delete From AmcServices Where id NOT IN ("+existingPreviousLst+")";
			getSession().createQuery(query).executeUpdate();
			LOGGER.debug("Exit from : deleteAmcServices()");
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			return null;
		}
	}

	@Override
	public Integer getLatestSNoCode() {
		try {
			LOGGER.debug("Enter into : getLatestSNoCode()");
			String query = "Select Max(sNumberCount) From Amc";
			Integer latestSNoCode = (Integer) getSession().createQuery(query).uniqueResult();
			LOGGER.debug("Exit from : getLatestSNoCode() method ");
			return latestSNoCode;
		}catch(Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			return null;
		}
	}
	@Transactional
	@Override
	public Boolean updateIsLatestValue(BigDecimal id) {
		try {
			LOGGER.debug("Enter into : updateIsLatestValue()");
			String query = "Update Amc Set isLatest=false Where id="+id;
			getSession().createQuery(query).executeUpdate();
			LOGGER.debug("Exit from : updateIsLatestValue()");
			return true;
		}catch(Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Amc> getAmcByRenewUuid(String renewUuid) {
		try {
			LOGGER.debug("Enter into : getAmcByRenewUuid()");
			String query = "From Amc Where isDeleted=false And renewUuid='"+renewUuid+"'";
			List<Amc> amcList = getSession().createQuery(query).list();
			LOGGER.debug("Exit from : getAmcByRenewUuid()");
			return amcList;
		}catch(Exception e) {
			LOGGER.error(e.getMessage());
			throw new GlobalException(500, "Something went wrong");
		}
	}

	@Override
	public Status getActiveStatus(String statusName) {
		try {
			LOGGER.debug("Enter into : getActiveStatus()");
			String query = "From Status Where name='"+statusName+"'";
			Status status = (Status) getSession().createQuery(query).uniqueResult();
			LOGGER.debug("Exit from : getActiveStatus()");
			return status;
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			throw new GlobalException(500, "Something went wrong");
		}
	}

	@Override
	public Boolean stopAmcNotification(BigDecimal objectId, String notificationName) {
		try {
			LOGGER.debug("Enter into : stopAmcNotification()");
			String query = "Update NotificationAlertDetails Set alertStatus=false Where stopOnAction=true And notificationName='"
					+ notificationName + "' And objectId=" + objectId;
			getSession().createQuery(query).executeUpdate();
			LOGGER.debug("Exit from : stopAmcNotification()");
			return true;
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			throw new GlobalException(500, "Something went wrong");
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Amc> getAmcListForCurrentObject(String colName,BigDecimal objectId) {
		try {
			LOGGER.debug("Enter into : getAmcListForCurrentVendor()");
			String query = "From Amc Where (isDeleted = false Or isDeleted = null) And "+colName+"=" +objectId;
			List<Amc> amcLst = getSession().createQuery(query).list();
			LOGGER.debug("Exit from : getAmcListForCurrentVendor()");
			return amcLst;
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			throw new GlobalException(500, "Something went wrong");
		}
	}

	
	/*
	 * @Override public Status getActiveStatus(String statusName) { try {
	 * LOGGER.debug("Enter into : getActiveStatus()"); String query =
	 * "From Status Where name='" + statusName + "'"; Status status = (Status)
	 * getSession().createQuery(query).uniqueResult();
	 * LOGGER.debug("Exit from : getActiveStatus()"); return status; } catch
	 * (Exception e) { LOGGER.error(e.getMessage()); throw new GlobalException(500,
	 * "Something went wrong"); } }
	 */
	 

}
