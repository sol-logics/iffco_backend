package com.sollogics.fams.repositories.daoImpl;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.sollogics.fams.exceptionHandler.GlobalException;
import com.sollogics.fams.repositories.dao.AssetDao;
import com.sollogics.fams.repositories.modal.Asset;
import com.sollogics.fams.repositories.modal.AssetGroup;
import com.sollogics.fams.repositories.modal.Category;
import com.sollogics.fams.repositories.modal.Location;
import com.sollogics.fams.repositories.modal.Status;
import com.sollogics.fams.repositories.modal.Type;
import com.sollogics.fams.utils.BaseHibernate;

@Transactional
@Repository
public class AssetDaoImpl extends BaseHibernate implements AssetDao{
	private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

	@Override
	public Asset saveAsset(Asset asset) {
		try {
			LOGGER.debug("Enter into : saveAsset()");
				getSession().saveOrUpdate(asset);
				LOGGER.debug("Exit from the saveAsset() method ");
			return asset;
		}catch(Exception e) {
			LOGGER.error(e.getMessage());
			throw new GlobalException(500, "Something went wrong");
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Asset> getAllAssets() {
		try {
			LOGGER.debug("Enter into : getAllAssets()");
			String query = "From Asset Where isDeleted=false";
			List<Asset> assetLst = getSession().createQuery(query).list();
			LOGGER.debug("Exit from the getAllAssets() method ");
			return assetLst;
		}catch(Exception e) {
			LOGGER.error(e.getMessage());
			throw new GlobalException(500, "Something went wrong");
		}
	}

	@Override
	public Asset getAssetById(BigDecimal assetId) {
		try {
			LOGGER.debug("Enter into : getAssetById()");
			String query = "From Asset Where id="+assetId;
			Asset asset = (Asset) getSession().createQuery(query).uniqueResult();
			return asset;
		}catch(Exception e) {
			LOGGER.error(e.getMessage());
			throw new GlobalException(500, "Something went wrong");
		}
		
	}

	@Override
	public Integer getLatestSNoCode() {
		try {
			LOGGER.debug("Enter into : getLatestSNoCode()");
			String query = "SELECT sNoCount FROM Asset ORDER BY sNoCount DESC ";
			Integer latestSNoCode = (Integer) getSession().createQuery(query).setMaxResults(1).uniqueResult();
			LOGGER.debug("Exit from the getLatestSNoCode() method ");
			return latestSNoCode;
		}catch(Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			return null;
		}
	}

	@Override
	public Long validateModelNumber(String modelNum) {
		try {
			LOGGER.debug("Enter into : validateModelNumber()");
			String query = "Select count(id) From Asset Where modelNum ='"+modelNum+"' And isDeleted=false";
			Long count = (Long) getSession().createQuery(query).uniqueResult();
			LOGGER.debug("Exit from the validateModelNumber() method ");
			return count;
		}catch(Exception e) {
			LOGGER.error(e.getMessage());
			return null;
		}
		
	}

	@Override
	public Long validateSerialNumber(String serialNum) {
		try {
			LOGGER.debug("Enter into : validateSerialNumber()");
			String query = "Select count(id) From Asset Where serialNum ='"+serialNum+"' And isDeleted=false";
			Long count = (Long) getSession().createQuery(query).uniqueResult();
			LOGGER.debug("Exit from the validateSerialNumber() method ");
			return count;
		}catch(Exception e) {
			LOGGER.error(e.getMessage());
			return null;
		}
	}

	@Override
	public Boolean deleteAssetById(BigDecimal userId, BigDecimal assetId, LocalDateTime deletedDate) {
		try {
			LOGGER.debug("Enter into : deleteAssetById()");
			String query = "Update Asset set isDeleted=true ,deletedDate='"+deletedDate+"',deletedBy="+userId+" Where id="+assetId;
			getSession().createQuery(query).executeUpdate();
			LOGGER.debug("Exit from the deleteAssetById() method ");
			return true;
		}catch(Exception e) {
			LOGGER.error(e.getMessage());
			throw new GlobalException(500, "Something went wrong");
		}
	}

	@Override
	public AssetGroup saveAssetGroup(AssetGroup assetGroup) {
		try {
			LOGGER.debug("Enter into : saveAssetGroup() method");
			getSession().saveOrUpdate(assetGroup);
			LOGGER.debug("Exit from : saveAssetGroup() method ");
			return assetGroup;
		}catch(Exception e) {
			LOGGER.error(e.getMessage());
			throw new GlobalException(500, "Something went wrong");
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AssetGroup> getAllAssetGroup() {
		try {
			LOGGER.debug("Enter into : getAllAssetGroup()");
			String query = "From AssetGroup";
			List<AssetGroup> assetGroupLst = getSession().createQuery(query).list();
			LOGGER.debug("Exit from the getAllAssetGroup() method ");
			return assetGroupLst;
		}catch(Exception e) {
			LOGGER.error(e.getMessage());
			throw new GlobalException(500, "Something went wrong");
		}
	}

	@Override
	public AssetGroup getAssetGroupById(BigDecimal assetGroupId) {
		try {
			LOGGER.debug("Enter into : getAssetGroupById() method");
			String query = "From AssetGroup Where id="+assetGroupId;
			AssetGroup assetGroup = (AssetGroup) getSession().createQuery(query).uniqueResult();
			LOGGER.debug("Exit from : getAllAssetGroup() method ");
			return assetGroup;
		}catch(Exception e) {
			LOGGER.error(e.getMessage());
			throw new GlobalException(500, "Something went wrong");
		}
	}

	@Override
	public Boolean deleteAssetGroupById(BigDecimal userId, BigDecimal assetGroupId, LocalDateTime deletedDate) {
		try {
			LOGGER.debug("Enter into : deleteAssetGroupById() method");
			String query = "Update AssetGroup set isDeleted=true ,deletedDate='"+deletedDate+"',deletedBy="+userId+" Where id="+assetGroupId;
			getSession().createQuery(query).executeUpdate();
			LOGGER.debug("Exit from the deleteAssetGroupById() method ");
			return true;
		}catch(Exception e) {
			LOGGER.error(e.getMessage());
			throw new GlobalException(500, "Something went wrong");
		}
	}

	@Override
	public List<Category> getAllCategory() {
		try {
			LOGGER.debug("Enter into : getAllCategory()");
			String query = "From Category";
			List<Category> categoryLst = getSession().createQuery(query).list();
			LOGGER.debug("Exit from the getAllCategory() method ");
			return categoryLst;
		}catch(Exception e) {
			LOGGER.error(e.getMessage());
			throw new GlobalException(500, "Something went wrong");
		}
	}

	@Override
	public List<Status> getAllStatus() {
		try {
			LOGGER.debug("Enter into : getAllStatus()");
			String query = "From Status";
			List<Status> statusLst = getSession().createQuery(query).list();
			LOGGER.debug("Exit from the getAllStatus() method ");
			return statusLst;
		}catch(Exception e) {
			LOGGER.error(e.getMessage());
			throw new GlobalException(500, "Something went wrong");
		}
	}

	@Override
	public List<Location> getAllLocation() {
		try {
			LOGGER.debug("Enter into : getAllStatus()");
			String query = "From Location";
			List<Location> locationLst = getSession().createQuery(query).list();
			LOGGER.debug("Exit from the getAllStatus() method ");
			return locationLst;
		}catch(Exception e) {
			LOGGER.error(e.getMessage());
			throw new GlobalException(500, "Something went wrong");
		}
	}

	@Override
	public List<Type> getAllType() {
		try {
			LOGGER.debug("Enter into : getAllType()");
			String query = "From Type";
			List<Type> typeLst = getSession().createQuery(query).list();
			LOGGER.debug("Exit from the getAllType() method ");
			return typeLst;
		}catch(Exception e) {
			LOGGER.error(e.getMessage());
			throw new GlobalException(500, "Something went wrong");
		}
	}

	@Override
	public List<Asset> getAssetListForCurrentAssetGroup(BigDecimal assetGroupId) {
		try {
			LOGGER.debug("Enter into : getAssetListForCurrentAssetGroup()");
			String query = "From Asset Where refAssetGroup ="+assetGroupId;
			List<Asset> assetLst = getSession().createQuery(query).list();
			LOGGER.debug("Exit from : getAssetListForCurrentAssetGroup()");
			return assetLst;
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			throw new GlobalException(500, "Something went wrong");
		}
	}

	@Override
	public Category saveCategory(Category category) {
		try {
			LOGGER.debug("Enter into : saveCategory()");
				getSession().saveOrUpdate(category);
				LOGGER.debug("Exit from the saveCategory() method ");
			return category;
		}catch(Exception e) {
			LOGGER.error(e.getMessage());
			throw new GlobalException(500, "Something went wrong");
		}
	}

	@Override
	public Location saveLocation(Location location) {
		try {
			LOGGER.debug("Enter into : saveLocation()");
				getSession().saveOrUpdate(location);
				LOGGER.debug("Exit from the saveLocation() method ");
			return location;
		}catch(Exception e) {
			LOGGER.error(e.getMessage());
			throw new GlobalException(500, "Something went wrong");
		}
	}

	@Override
	public Type saveType(Type type) {
		try {
			LOGGER.debug("Enter into : saveType()");
				getSession().saveOrUpdate(type);
				LOGGER.debug("Exit from the saveType() method ");
			return type;
		}catch(Exception e) {
			LOGGER.error(e.getMessage());
			throw new GlobalException(500, "Something went wrong");
		}
	}

}
