package com.sollogics.fams.repositories.daoImpl;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.sollogics.fams.exceptionHandler.GlobalException;
import com.sollogics.fams.repositories.dao.VendorsDao;
import com.sollogics.fams.repositories.modal.Vendors;
import com.sollogics.fams.utils.BaseHibernate;

@Transactional
@Repository
public class VendorsDaoImpl extends BaseHibernate implements VendorsDao {
	
	private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

	@Override
	public Vendors saveVendor(Vendors vendors) {
		try {
			LOGGER.debug("Enter into : saveVendor()");
				getSession().saveOrUpdate(vendors);
				LOGGER.debug("Exit from the saveVendor() method ");
			return vendors;
		}catch(Exception e) {
			LOGGER.error(e.getMessage());
			throw new GlobalException(500, "Something went wrong");
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Vendors> getAllVendors(BigDecimal userId) {
		try {
			LOGGER.debug("Enter into : getAllVendors()");
				String query = "From Vendors Where isDeleted=false";
				List<Vendors> vendorsLst = getSession().createQuery(query).list();
				LOGGER.debug("Exit from the getAllVendors() method ");
				return vendorsLst;
		}catch(Exception e) {
			LOGGER.error(e.getMessage());
			throw new GlobalException(500, "Something went wrong");
		}
	}

	@Override
	public Vendors getvendorById(BigDecimal userId, BigDecimal vendorId) {
		try {
			LOGGER.debug("Enter into : getvendorById()");
				String query = "From Vendors Where id="+vendorId;
				Vendors vendor = (Vendors) getSession().createQuery(query).uniqueResult();
				LOGGER.debug("Exit from the getvendorById() method ");
				return vendor;
		}catch(Exception e) {
			LOGGER.error(e.getMessage());
			throw new GlobalException(500, "Something went wrong");
		}
	}

	@Override
	public Boolean deleteVendorById(BigDecimal userId, BigDecimal vendorId,LocalDateTime deleteDate) {
		try {
			LOGGER.debug("Enter into : deleteVendorById()");
				String query = "Update Vendors Set isDeleted=true ,deletedOn='"+deleteDate+"',deletedBy="+userId+" Where id="+vendorId;
				getSession().createQuery(query).executeUpdate();
				LOGGER.debug("Exit from the deleteVendorById() method ");
				return true;
		}catch(Exception e) {
			LOGGER.error(e.getMessage());
			throw new GlobalException(500, "Something went wrong");
		}
	}

	@Override
	public Long validateEmailId(String email) {
		try {
			LOGGER.debug("Enter into : validateEmailId()");
				String query = "Select count(id) From Vendors Where email='"+email+"'";
				Long count = (Long) getSession().createQuery(query).uniqueResult();
				LOGGER.debug("Exit from the validateEmailId() method ");
				return count;
		}catch(Exception e) {
			LOGGER.error(e.getMessage());
			throw new GlobalException(500, "Something went wrong");
		}
	}

	@Override
	public Long validateGstinNo(String gstin) {
		try {
			LOGGER.debug("Enter into : validateGstinNo()");
				String query = "Select count(id) From Vendors Where gstin='"+gstin+"'";
				Long result = (Long) getSession().createQuery(query).uniqueResult();
				LOGGER.debug("Exit from the validateGstinNo() method ");
				return result;
		}catch(Exception e) {
			LOGGER.error(e.getMessage());
			throw new GlobalException(500, "Something went wrong");
		}
	}

}
