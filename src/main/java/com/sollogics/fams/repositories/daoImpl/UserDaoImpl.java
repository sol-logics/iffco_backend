/**
 * 
 */
package com.sollogics.fams.repositories.daoImpl;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.sollogics.fams.exceptionHandler.GlobalException;
import com.sollogics.fams.repositories.dao.UserDao;
import com.sollogics.fams.repositories.modal.Company;
import com.sollogics.fams.repositories.modal.Profile;
import com.sollogics.fams.repositories.modal.Roles;
import com.sollogics.fams.utils.BaseHibernate;

/**
 * @author sollogics
 *
 */
@Transactional
@Repository
public class UserDaoImpl extends BaseHibernate implements UserDao {
	private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

	@Override
	public Profile saveUserProfile(BigDecimal usersId, Profile profile) {
		try {
			LOGGER.debug("Enter into : saveUserProfile()");
			getSession().saveOrUpdate(profile.getRefUser());
			getSession().saveOrUpdate(profile);
			LOGGER.debug("Exit from : saveUserProfile()");
			return profile;
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			throw new GlobalException(500, "Something went wrong");
		}
	}

	//refUser.active=true And 
	@SuppressWarnings("unchecked")
	@Override
	public List<Profile> getAllUsers(BigDecimal userId) {
		try {
			LOGGER.debug("Enter into : getAllUsers()");
			String query = "From Profile Where (refUser.isDeleted=false OR refUser.isDeleted=null) And refUser.id NOT IN (" + userId + ")";
			List<Profile> profileLst = getSession().createQuery(query).list();
			LOGGER.debug("Exit from : getAllUsers()");
			return profileLst;
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			return null;
		}
	}

	@Override
	public Profile getUserById(BigDecimal userId, BigDecimal employeId) {
		try {
			//refUser.active=true And 

			LOGGER.debug("Enter into : getUserById()");
			String query = "From Profile Where refUser.id=" + employeId;
			Profile profile = (Profile) getSession().createQuery(query).uniqueResult();
			LOGGER.debug("Exit from : getUserById()");
			return profile;
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			return null;
		}
	}

	@Override
	public Boolean deleteUser(BigDecimal usersId, BigDecimal employeId,LocalDateTime deletedDate) {
		try {
			LOGGER.debug("Enter into : deleteUser()");
			String query = "Update Users Set isDeleted=true, deletedOn='"+deletedDate+"',deletedBy="+usersId+" Where id="+employeId;
			getSession().createQuery(query).executeUpdate();
			LOGGER.debug("Exit from : deleteUser()");
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Company> getAllCompany() {
		try {
			LOGGER.debug("Enter into : getAllCompany()");
			String query = "From Company";
			List<Company> companyLst = getSession().createQuery(query).list();
			LOGGER.debug("Exit from : getAllCompany()");
			return companyLst;
		}catch(Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			return null;
		}
	}

	@Override
	public Company getCompanyById(BigDecimal companyId) {
		try {
			LOGGER.debug("Enter into : getCompanyById()");
			String query = "From Company Where id="+companyId;
			Company company = (Company) getSession().createQuery(query).uniqueResult();
			LOGGER.debug("Exit from : getCompanyById()");
			return company;
		}catch(Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			return null;
		}
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Roles> getAllRoles() {
		try {
			LOGGER.debug("Enter into : getAllRoles()");
			String query = "From Roles";
			List<Roles> rolesLst = getSession().createQuery(query).list();
			LOGGER.debug("Exit from : getAllRoles()");
			return rolesLst;
		}catch(Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			return null;
		}
	}

	@Override
	public Roles getRolesById(BigDecimal rolesId) {
		try {
			LOGGER.debug("Enter into : getRolesById()");
			String query = "From Roles Where id="+rolesId;
			Roles roles = (Roles) getSession().createQuery(query).uniqueResult();
			LOGGER.debug("Exit from : getRolesById()");
			return roles;
		}catch(Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			return null;
		}
		
	}

	@Override
	public Long isEmailUnique(String email) {
		try {
			LOGGER.debug("Enter into : isEmailUnique()");
			String query = "Select count(id) From Users Where email='"+email+"' And (isDeleted=false Or isDeleted=null)";
			Long count = (Long) getSession().createQuery(query).uniqueResult();
			LOGGER.debug("Exit from : isEmailUnique()");
			return count;
		}catch(Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			return null;
		}
		
	}

	@Override
	public Long isEmployeeIdUnique(String empId) {
		try {
			LOGGER.debug("Enter into : isEmployeeIdUnique()");
			String query = "Select count(id) From Profile Where empId='"+empId+"' And (isDeleted=false Or isDeleted=null)";
			Long count = (Long) getSession().createQuery(query).uniqueResult();
			LOGGER.debug("Exit from : isEmployeeIdUnique()");
			return count;
		}catch(Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			return null;
		}
	}

}
