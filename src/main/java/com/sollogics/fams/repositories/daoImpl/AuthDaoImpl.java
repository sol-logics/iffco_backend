package com.sollogics.fams.repositories.daoImpl;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.sollogics.fams.exceptionHandler.GlobalException;
import com.sollogics.fams.repositories.dao.AuthDao;
import com.sollogics.fams.repositories.modal.Credential;
import com.sollogics.fams.repositories.modal.Profile;
import com.sollogics.fams.repositories.modal.UserOtp;
import com.sollogics.fams.repositories.modal.Users;
import com.sollogics.fams.utils.BaseHibernate;

@Transactional
@Repository
public class AuthDaoImpl extends BaseHibernate implements AuthDao {
	private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

	@Override
	public Profile authenticateUser(Credential credential) {
		LOGGER.info("Enter into : authenticateUser()");
		try {
			String query = "From Profile where refUser.email='"+credential.getEmail()+"' AND refUser.password='"+credential.getPassword()+"'";
			Profile Profile = (Profile) getSession().createQuery(query).uniqueResult();
			LOGGER.debug("Exit from the authenticateUser() method ");
			return Profile;
		}catch(Exception e) {
			LOGGER.error("User Not Found with this email = "+credential.getEmail());
			return null;
		}
	}

	@Override
	public Boolean changePassword(BigDecimal userId, String currentPassword, String previousPassword) {
		try {
			LOGGER.debug("Enter into : changePassword() method");
			String query = "Update Users Set reset_password=true ,password='"+currentPassword+"' Where id="+userId+" And password='"+previousPassword+"'";
			int result = getSession().createSQLQuery(query).executeUpdate();
			if(result > 0) {
				return true;
			}
			LOGGER.debug("Exit from the changePassword() method ");
			return false;
		}catch(Exception e) {
			LOGGER.error(e.getMessage());
			throw new GlobalException(409, "conflict");
		}
	}

	@Override
	public Users validateEmail(String email) {
			try {
				LOGGER.debug("Enter into : validateEmail() method");
				String queryString = "From Users where email='"+email+"'";
				Users user = (Users) getSession().createQuery(queryString).uniqueResult();
				LOGGER.debug("Exit from : validateEmail() method");
				return user;
			}catch(Exception e) {
				LOGGER.error("Email not valid");
				throw new GlobalException(409, "Email not valid");
			}
	}

	@Override
	public UserOtp saveOtpForChangedPassword(UserOtp userOtp) {
		try {
			LOGGER.debug("Enter into : saveOtpForChangedPassword() service method");
			String queryString = "From UserOtp where refUser="+userOtp.getRefUser().getId();
			UserOtp prevOtp = (UserOtp) getSession().createQuery(queryString).uniqueResult();
			if(prevOtp != null && prevOtp.getId() != null) {
				prevOtp.setOtp(userOtp.getOtp());
				prevOtp.setRefUser(userOtp.getRefUser());
				prevOtp.setValidUpto(userOtp.getValidUpto());
				prevOtp.setCreatedOn(userOtp.getCreatedOn());
				getSession().saveOrUpdate(prevOtp);
			}else {
				getSession().saveOrUpdate(userOtp);
			}
			LOGGER.debug("Exit from the saveOtpForChangedPassword() method ");
			return userOtp;
		}catch(Exception e) {
			LOGGER.error(e.getMessage());
			throw new GlobalException(409, "conflict");
		}
	}

	@Override
	public UserOtp checkOtp(String email, String otp, LocalDateTime validUpto) {
		try {
			LOGGER.debug("Enter into : checkOtp() service method");
			String query = "From UserOtp Where ref_user.email='"+email+"' AND OTP='"+otp+"' AND valid_upto >= '"+validUpto+"'";
			UserOtp userOtp = (UserOtp)getSession().createQuery(query).uniqueResult();
			LOGGER.debug("Exit from : checkOtp() service method");
				return userOtp;
		}catch(Exception e) {
			LOGGER.error("Email not valid");
			throw new GlobalException(409, "OTP is not valid");
		}
	}

	@Override
	public Boolean changePasswordByOtp(BigDecimal id, String currentPassword) {
		try {
			LOGGER.debug("Enter into : changePasswordByOtp() method");
			String query = "Update Users Set password='"+currentPassword+"' Where id='"+id+"'";
			int result = getSession().createSQLQuery(query).executeUpdate();
			if(result>0) {
				LOGGER.debug("Exit from : changePasswordByOtp() method");
				return true;
			}
			return false;
		}catch(Exception e) {
			LOGGER.error(e.getMessage());
			throw new GlobalException(500, "Something went wrrong");
		}
		
	}

	@Override
	public Profile validateUser(BigDecimal usersId) {
		try {
			LOGGER.debug("Enter into : validateUser() method");
			String query = "From Profile Where refUser.id="+usersId;
			Profile profile = (Profile) getSession().createQuery(query).uniqueResult();
			LOGGER.debug("Exit from : validateUser() method");
			return profile;
		}catch(Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			return null;
		}
	}
	
	
	/*@Override
	public List<Roles> getRoles() {
		try {
			LOGGER.debug("Enter into : getRoles() method");
			String query = "From Roles";
			List<Roles> rolesLst = getSession().createQuery(query).list();
			LOGGER.debug("Exit from the getRoles() method ");
			return rolesLst;
		}catch(Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getMessage());
			return null;
			
		}
	}*/
}
