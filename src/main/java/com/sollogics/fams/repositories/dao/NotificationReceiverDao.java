package com.sollogics.fams.repositories.dao;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import com.sollogics.fams.repositories.modal.NotificationReceiver;

public interface NotificationReceiverDao {

	NotificationReceiver saveNotificationReceiver(NotificationReceiver notificationReceiver);

	List<NotificationReceiver> getAllNotificationReceiver();

	NotificationReceiver getNotificationReceiverById(BigDecimal notificationId);

	Boolean deleteNotificationReceiverById(BigDecimal userId, BigDecimal notificationId, LocalDateTime deletedDate);

	List<NotificationReceiver> getNotiRcvrListByUserId(String colName,BigDecimal objectId);

}
