package com.sollogics.fams.repositories.dao;

import com.sollogics.fams.repositories.modal.AuditTrail;

public interface AuditTrailDao {

	Boolean saveAuditTrail(AuditTrail auditTrail);

}
