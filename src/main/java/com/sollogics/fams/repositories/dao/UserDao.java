package com.sollogics.fams.repositories.dao;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import com.sollogics.fams.repositories.modal.Company;
import com.sollogics.fams.repositories.modal.Profile;
import com.sollogics.fams.repositories.modal.Roles;

public interface UserDao {

	Profile saveUserProfile(BigDecimal usersId, Profile profile);

	/*Users saveUser(Users users);*/

	List<Profile> getAllUsers(BigDecimal userId);

	Profile getUserById(BigDecimal userId, BigDecimal employeId);

	Boolean deleteUser(BigDecimal usersId, BigDecimal employeId,LocalDateTime deletedDate);

	List<Company> getAllCompany();

	Company getCompanyById(BigDecimal companyId);

	List<Roles> getAllRoles();

	Roles getRolesById(BigDecimal rolesId);

	Long isEmailUnique(String email);

	Long isEmployeeIdUnique(String empId);

}
