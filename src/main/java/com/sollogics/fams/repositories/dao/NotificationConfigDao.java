package com.sollogics.fams.repositories.dao;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import com.sollogics.fams.repositories.modal.NotificationConfig;
import com.sollogics.fams.repositories.modal.NotificationMeta;
import com.sollogics.fams.repositories.modal.NotificationReceiver;

public interface NotificationConfigDao {

	NotificationConfig saveNotificationConfig(NotificationConfig notificationConfig);

	List<NotificationConfig> getAllNotificationConfig();

	NotificationConfig getNotificationConfigById(BigDecimal notificationConfigId);

	Boolean deleteNotificationConfigById(BigDecimal userId, BigDecimal notificationConfigId, LocalDateTime deletedDate);

	NotificationConfig getNotificationConfigById(String notifactionName);

	List<NotificationReceiver> getNotiRcvrLstByConfigId(BigDecimal notificationConfigId);

	//Boolean updateNotiRcvrByConfigId(BigDecimal notificationConfigId, BigDecimal userId);

	Boolean updateNotiRcvrLstByConfigId(BigDecimal notificationConfigId, List<NotificationReceiver> notiRcvrLst, List<NotificationReceiver> notiRcvrPreviousLst);

	List<NotificationReceiver> updateNotiRcvr(BigDecimal userId,BigDecimal notificationConfigId, List<NotificationReceiver> notiRcvrUpdatedLst,
			List<NotificationReceiver> notiRcvrPreviousLst);

	List<NotificationReceiver> saveNotificationReceiver(BigDecimal userId,
			List<NotificationReceiver> notificationReceiverLst);

	List<NotificationReceiver> updateNotificationReceiver(List<NotificationReceiver> notiRcvrLst);

	Boolean deleteNotificationReceiver(String existingPreviousLst);

	List<NotificationMeta> getAllNotificationMeta();

	//Boolean deletePreviouNotiRcvrLst(BigDecimal notificationConfigId, List<NotificationReceiver> notiRcvrPreviousLst);

}
