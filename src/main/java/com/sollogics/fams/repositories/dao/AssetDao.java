package com.sollogics.fams.repositories.dao;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import com.sollogics.fams.repositories.modal.Asset;
import com.sollogics.fams.repositories.modal.AssetGroup;
import com.sollogics.fams.repositories.modal.Category;
import com.sollogics.fams.repositories.modal.Location;
import com.sollogics.fams.repositories.modal.Status;
import com.sollogics.fams.repositories.modal.Type;

public interface AssetDao {

	Asset saveAsset(Asset asset);

	List<Asset> getAllAssets();

	Asset getAssetById(BigDecimal assetId);

	Integer getLatestSNoCode();

	Long validateModelNumber(String modelNum);

	Long validateSerialNumber(String serialNum);

	Boolean deleteAssetById(BigDecimal userId, BigDecimal assetId, LocalDateTime deletedDate);

	AssetGroup saveAssetGroup(AssetGroup assetGroup);

	List<AssetGroup> getAllAssetGroup();

	AssetGroup getAssetGroupById(BigDecimal assetGroupId);

	Boolean deleteAssetGroupById(BigDecimal userId, BigDecimal assetGroupId, LocalDateTime deletedDate);

	List<Category> getAllCategory();

	List<Status> getAllStatus();

	List<Location> getAllLocation();

	List<Type> getAllType();

	List<Asset> getAssetListForCurrentAssetGroup(BigDecimal assetGroupId);

	Category saveCategory(Category category);

	Location saveLocation(Location location);

	Type saveType(Type type);

}
