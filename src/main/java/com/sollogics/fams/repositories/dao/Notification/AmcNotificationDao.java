package com.sollogics.fams.repositories.dao.Notification;

import java.util.List;

import com.sollogics.fams.repositories.modal.Amc;
import com.sollogics.fams.repositories.modal.NotificationAlertDetails;
import com.sollogics.fams.repositories.modal.NotificationConfig;

public interface AmcNotificationDao {

	NotificationConfig getNotiConfigByName(String notiName);

	List<Amc> getAmcLstByBeforeDays(Integer beforeDays);

	List<NotificationAlertDetails> saveAmcLstInAlertDtls(List<NotificationAlertDetails> notiAlertDtlsLst);

	//NotificationAlertDetails saveCcTo(String commaSepList);

}
