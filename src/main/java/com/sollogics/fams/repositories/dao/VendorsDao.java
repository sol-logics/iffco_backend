package com.sollogics.fams.repositories.dao;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import com.sollogics.fams.repositories.modal.Vendors;

public interface VendorsDao {

	Vendors saveVendor(Vendors vendors);

	List<Vendors> getAllVendors(BigDecimal usersId);

	Vendors getvendorById(BigDecimal userId, BigDecimal vendorId);

	Boolean deleteVendorById(BigDecimal userId, BigDecimal vendorId, LocalDateTime deleteDate);

	Long validateEmailId(String email);

	Long validateGstinNo(String gstin);

}
