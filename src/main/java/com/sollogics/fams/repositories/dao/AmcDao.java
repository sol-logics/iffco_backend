package com.sollogics.fams.repositories.dao;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import com.sollogics.fams.repositories.modal.Amc;
import com.sollogics.fams.repositories.modal.AmcServices;
import com.sollogics.fams.repositories.modal.Status;

public interface AmcDao {

	Amc saveAmc(Amc amc);

	List<Amc> getAllAmc();

	Amc getAmcById(BigDecimal amcId);

	Boolean deleteAmcById(BigDecimal userId, BigDecimal amcId, LocalDateTime deletedDate);

	List<AmcServices> saveAmcService(BigDecimal userId,List<AmcServices> amcServicesLst);

	List<AmcServices> getAllAmcServices();

	AmcServices getAmcServiceById(BigDecimal amcServiceId);

	Boolean deleteAmcServiceById(BigDecimal userId, BigDecimal amcServiceId, LocalDateTime deletedDate);

	List<AmcServices> getAmcServiceByAmc(BigDecimal amcId);

	List<AmcServices> updateAmcServices(List<AmcServices> amcServicesLst);

	Boolean deleteAmcServices(String existingPreviousLst);

	Integer getLatestSNoCode();

	Boolean updateIsLatestValue(BigDecimal id);

	List<Amc> getAmcByRenewUuid(String renewUuid);

	Status getActiveStatus(String statusName);

	Boolean stopAmcNotification(BigDecimal objectId, String notificationName);

	//List<Amc> getAmcListForCurrentVendor(BigDecimal vendorId);

	List<Amc> getAmcListForCurrentObject(String colName,BigDecimal assetId);

	//Status getActiveStatus(String statusName);

}
