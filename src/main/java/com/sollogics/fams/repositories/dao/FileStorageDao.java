package com.sollogics.fams.repositories.dao;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import com.sollogics.fams.repositories.modal.AttachmentAssociation;

public interface FileStorageDao {

	AttachmentAssociation saveAttachmentDetails(AttachmentAssociation attachmentAssociation);

	List<AttachmentAssociation> getAttachmentList(String colName,BigDecimal objectId);

	Boolean deleteFileByFileName(String commaSepList,BigDecimal objectId,LocalDateTime deletedDate,String colName);

	Boolean updateExistingFile(LocalDateTime deletedDate, String commaSepFileId,String colName, BigDecimal objectId,BigDecimal userId);

	//List<AttachmentAssociation> attachmentListForCurrentVendor(BigDecimal vendorId);

	//Boolean deleteAttachmentForCurrentObject(String colName,BigDecimal objectID,BigDecimal userId);

}
