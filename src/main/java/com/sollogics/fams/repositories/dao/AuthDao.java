package com.sollogics.fams.repositories.dao;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import com.sollogics.fams.repositories.modal.Credential;
import com.sollogics.fams.repositories.modal.Profile;
import com.sollogics.fams.repositories.modal.UserOtp;
import com.sollogics.fams.repositories.modal.Users;

public interface AuthDao {

	Profile authenticateUser(Credential credential);

	Boolean changePassword(BigDecimal userId, String currentPassword, String previousPassword);

	Users validateEmail(String email);

	UserOtp saveOtpForChangedPassword(UserOtp userOtp);

	UserOtp checkOtp(String email, String otp, LocalDateTime localDateTime);

	Boolean changePasswordByOtp(BigDecimal id, String currentPassword);

	Profile validateUser(BigDecimal usersId);

	/*List<Roles> getRoles();*/

}
