package com.sollogics.fams.repositories.modal;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="notification_receiver")
@NamedQuery(name="NotificationReceiver.findAll", query="SELECT a FROM NotificationReceiver a")
public class NotificationReceiver {
	@Id
    @Column(name = "id", updatable = false, nullable = false)
    @SequenceGenerator(name = "SequenceIdGenerator", sequenceName = "notification_receiver_id_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SequenceIdGenerator")
	private BigDecimal id;

	@ManyToOne
	@JoinColumn(name = "ref_notification_config",referencedColumnName = "id")
	private NotificationConfig refNotificationConfig;
	@ManyToOne
	@JoinColumn(name = "ref_user",referencedColumnName = "id")
	private Users refUser;
	@Column(name = "created_by")
	private BigDecimal createdBy;
	@Column(name = "created_date")
	private LocalDateTime createdDate;
	@Column(name = "updated_by")
	private BigDecimal updatedBy;
	@Column(name = "updated_date")
	private LocalDateTime updatedDate;
	@Column(name = "is_deleted")
	private Boolean isDeleted;
	@Column(name = "deleted_by")
	private BigDecimal deletedBy;
	@Column(name = "deleted_date")
	private LocalDateTime deletedDate;
	
	public BigDecimal getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(BigDecimal updatedBy) {
		this.updatedBy = updatedBy;
	}
	public LocalDateTime getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(LocalDateTime updatedDate) {
		this.updatedDate = updatedDate;
	}
	public Boolean getIsDeleted() {
		return isDeleted;
	}
	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	public BigDecimal getDeletedBy() {
		return deletedBy;
	}
	public void setDeletedBy(BigDecimal deletedBy) {
		this.deletedBy = deletedBy;
	}
	public LocalDateTime getDeletedDate() {
		return deletedDate;
	}
	public void setDeletedDate(LocalDateTime deletedDate) {
		this.deletedDate = deletedDate;
	}
	public BigDecimal getId() {
		return id;
	}
	public void setId(BigDecimal id) {
		this.id = id;
	}
	public NotificationConfig getRefNotificationConfig() {
		return refNotificationConfig;
	}
	public void setRefNotificationConfig(NotificationConfig refNotificationConfig) {
		this.refNotificationConfig = refNotificationConfig;
	}
	public Users getRefUser() {
		return refUser;
	}
	public void setRefUser(Users refUser) {
		this.refUser = refUser;
	}
	public BigDecimal getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}
	public LocalDateTime getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(LocalDateTime createdDate) {
		this.createdDate = createdDate;
	}

}
