package com.sollogics.fams.repositories.modal;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="vendors")
@NamedQuery(name="Vendors.findAll", query="SELECT a FROM Vendors a")
public class Vendors {
	
	@Id
    @Column(name = "id", updatable = false, nullable = false)
    @SequenceGenerator(name = "SequenceIdGenerator", sequenceName = "vendors_id_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SequenceIdGenerator")
    private BigDecimal id;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "contact_person")
	private String contactPerson;
	
	@Column(name = "created_by")
	private BigDecimal createdBy;
	
	@Column(name = "created_date")
	private LocalDateTime createdDate;
	
	@Column(name = "updated_date")
	private LocalDateTime updatedDate;
	
	@Column(name ="is_deleted")
	private Boolean isDeleted;
	
	@Column(name ="deleted_on")
	private LocalDateTime deletedOn;
	
	@Column(name ="deleted_by")
	private BigDecimal deletedBy;
	
	@Column(name = "discontinue")
	private Boolean discontinue;
	
	@Column(name = "discontinue_on")
	private LocalDateTime discontinueOn;
	
	@Column(name = "discontinue_by")
	private BigDecimal discontinueBy;
	
	@Column(name = "updated_by")
	private BigDecimal updatedBy;
	
	@Column(name = "address")
	private String address;
	
	@Column(name = "email")
	private String email;
	
	@Column(name ="mobile_number")
	private String mobileNumber;
	
	@Column(name = "landline")
	private String landline;
	
	@Column(name = "gstin")
	private String gstin;
	
	@Column(name = "designation")
	private String designation;
	
	@Column(name = "website")
	private String website;
	
	@Column(name = "description")
	private String description;
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getLandline() {
		return landline;
	}

	public void setLandline(String landline) {
		this.landline = landline;
	}

	public String getGstin() {
		return gstin;
	}

	public void setGstin(String gstin) {
		this.gstin = gstin;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}
	
	public BigDecimal getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(BigDecimal updatedBy) {
		this.updatedBy = updatedBy;
	}

	public BigDecimal getId() {
		return id;
	}

	public void setId(BigDecimal id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getContactPerson() {
		return contactPerson;
	}

	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}

	public BigDecimal getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(BigDecimal userId) {
		this.createdBy = userId;
	}

	public LocalDateTime getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(LocalDateTime createdDate) {
		this.createdDate = createdDate;
	}

	public LocalDateTime getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(LocalDateTime updatedDate) {
		this.updatedDate = updatedDate;
	}

	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public LocalDateTime getDeletedOn() {
		return deletedOn;
	}

	public void setDeletedOn(LocalDateTime deletedOn) {
		this.deletedOn = deletedOn;
	}

	public BigDecimal getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(BigDecimal deletedBy) {
		this.deletedBy = deletedBy;
	}

	public Boolean getDiscontinue() {
		return discontinue;
	}

	public void setDiscontinue(Boolean discontinue) {
		this.discontinue = discontinue;
	}

	public LocalDateTime getDiscontinueOn() {
		return discontinueOn;
	}

	public void setDiscontinueOn(LocalDateTime discontinueOn) {
		this.discontinueOn = discontinueOn;
	}

	public BigDecimal getDiscontinueBy() {
		return discontinueBy;
	}

	public void setDiscontinueBy(BigDecimal discontinueBy) {
		this.discontinueBy = discontinueBy;
	}

}
