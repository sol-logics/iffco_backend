package com.sollogics.fams.repositories.modal;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="location")
@NamedQuery(name="Location.findAll", query="SELECT a FROM Location a")
public class Location {
	@Id
    @Column(name = "id", updatable = false, nullable = false)
    @SequenceGenerator(name = "SequenceIdGenerator", sequenceName = "location_id_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SequenceIdGenerator")
	private BigDecimal id;
	
	@Column(name = "floor")
	private String floor;
	
	@Column(name = "area")
	private String area;
	
	public BigDecimal getId() {
		return id;
	}

	public void setId(BigDecimal id) {
		this.id = id;
	}

	public String getFloor() {
		return floor;
	}

	public void setFloor(String floor) {
		this.floor = floor;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

}
