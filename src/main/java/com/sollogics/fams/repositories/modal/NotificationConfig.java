package com.sollogics.fams.repositories.modal;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="notification_config")
@NamedQuery(name="NotificationConfig.findAll", query="SELECT a FROM NotificationConfig a")
public class NotificationConfig {
	@Id
    @Column(name = "id", updatable = false, nullable = false)
    @SequenceGenerator(name = "SequenceIdGenerator", sequenceName = "notification_config_id_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SequenceIdGenerator")
	private BigDecimal id;
	@Column(name = "alert_times")
	private Integer alertTimes;
	@Column(name = "alert_intervals")
	private Integer alertIntervals;
	@Column(name = "before_days")
	private Integer beforeDays;
	@Column(name = "end_time")
	private Integer endTime;
	@Column(name = "stop_on_action")
	private Boolean stopOnAction;
	@Column(name = "created_by")
	private BigDecimal createdBy;
	@Column(name = "updated_by")
	private BigDecimal updatedBy;
	@Column(name = "updated_date")
	private LocalDateTime updatedDate;
	@Column(name = "is_deleted")
	private Boolean isDeleted;
	@Column(name = "deleted_by")
	private BigDecimal deletedBy;
	@Column(name = "deleted_date")
	private LocalDateTime deletedDate;
	@Column(name = "created_date")
	private LocalDateTime createdDate;
	@ManyToOne
	@JoinColumn(name = "ref_notification_meta",referencedColumnName = "id")
	private NotificationMeta refNotificationMeta;

	public NotificationMeta getRefNotificationMeta() {
		return refNotificationMeta;
	}
	public void setRefNotificationMeta(NotificationMeta refNotificationMeta) {
		this.refNotificationMeta = refNotificationMeta;
	}
	public BigDecimal getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(BigDecimal updatedBy) {
		this.updatedBy = updatedBy;
	}
	public LocalDateTime getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(LocalDateTime updatedDate) {
		this.updatedDate = updatedDate;
	}
	public Boolean getIsDeleted() {
		return isDeleted;
	}
	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	public BigDecimal getDeletedBy() {
		return deletedBy;
	}
	public void setDeletedBy(BigDecimal deletedBy) {
		this.deletedBy = deletedBy;
	}
	public LocalDateTime getDeletedDate() {
		return deletedDate;
	}
	public void setDeletedDate(LocalDateTime deletedDate) {
		this.deletedDate = deletedDate;
	}
	public BigDecimal getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}
	public LocalDateTime getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(LocalDateTime createdDate) {
		this.createdDate = createdDate;
	}
	
	public BigDecimal getId() {
		return id;
	}
	public void setId(BigDecimal id) {
		this.id = id;
	}
	public Integer getAlertTimes() {
		return alertTimes;
	}
	public void setAlertTimes(Integer alertTimes) {
		this.alertTimes = alertTimes;
	}
	public Integer getAlertIntervals() {
		return alertIntervals;
	}
	public void setAlertIntervals(Integer alertIntervals) {
		this.alertIntervals = alertIntervals;
	}
	public Integer getBeforeDays() {
		return beforeDays;
	}
	public void setBeforeDays(Integer beforeDays) {
		this.beforeDays = beforeDays;
	}
	public Integer getEndTime() {
		return endTime;
	}
	public void setEndTime(Integer endTime) {
		this.endTime = endTime;
	}
	public Boolean getStopOnAction() {
		return stopOnAction;
	}
	public void setStopOnAction(Boolean stopOnAction) {
		this.stopOnAction = stopOnAction;
	}
	

}
