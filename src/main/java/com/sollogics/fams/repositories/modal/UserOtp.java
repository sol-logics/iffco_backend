package com.sollogics.fams.repositories.modal;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="user_otp")
@NamedQuery(name="UserOtp.findAll", query="SELECT a FROM UserOtp a")
public class UserOtp {
	@Id
    @Column(name = "id", updatable = false, nullable = false)
    @SequenceGenerator(name = "SequenceIdGenerator", sequenceName = "user_otp_id_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SequenceIdGenerator")
	private BigDecimal id;
	
	@Column(name = "otp")
	private String otp;
	@Column(name = "created_on")
	private LocalDateTime createdOn;
	@Column(name = "valid_upto")
	private LocalDateTime validUpto;
	@ManyToOne
	@JoinColumn(name="ref_user",referencedColumnName="id")
	private Users refUser;
	public LocalDateTime getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(LocalDateTime createdOn) {
		this.createdOn = createdOn;
	}
	public LocalDateTime getValidUpto() {
		return validUpto;
	}
	public void setValidUpto(LocalDateTime validUpto) {
		this.validUpto = validUpto;
	}
	public Users getRefUser() {
		return refUser;
	}
	public void setRefUser(Users refUser) {
		this.refUser = refUser;
	}
	public BigDecimal getId() {
		return id;
	}
	public void setId(BigDecimal id) {
		this.id = id;
	}
	public String getOtp() {
		return otp;
	}
	public void setOtp(String otp) {
		this.otp = otp;
	}

}
