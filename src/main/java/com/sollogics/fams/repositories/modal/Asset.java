package com.sollogics.fams.repositories.modal;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="asset")
@NamedQuery(name="Asset.findAll", query="SELECT a FROM Asset a")
public class Asset {
	@Id
    @Column(name = "id", updatable = false, nullable = false)
    @SequenceGenerator(name = "SequenceIdGenerator", sequenceName = "asset_id_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SequenceIdGenerator")
	private BigDecimal id;
	
	@Column(name = "name")
	private String name;
	@Column(name = "manufacturer")
	private String manufacturer;
	@Column(name = "model_no")
	private String modelNum;
	@Column(name = "serial_no")
	private String serialNum;
	@Column(name ="uom")
	private String uom;
	@Column(name = "capacity")
	private String capacity;
	@Column(name = "unique_code")
	private String uniqueCode;
	
	@ManyToOne
	@JoinColumn(name="ref_location",referencedColumnName="id")
	private Location refLocation;
	
	@ManyToOne
	@JoinColumn(name="ref_type",referencedColumnName="id")
	private Type refType;
	
	@ManyToOne
	@JoinColumn(name="ref_category",referencedColumnName="id")
	private Category refCategory;
	
	@Column(name = "department")
	private String department;
	@Column(name = "warranty_expiry_date")
	private LocalDateTime warrantyExpiryDate;
	
	@ManyToOne
	@JoinColumn(name="ref_status",referencedColumnName="id")
	private Status refStatus;
	
	@Column(name = "purchase_date")
	private LocalDateTime purchaseDate;
	@Column(name = "purchase_price")
	private BigDecimal purchasePrice;
	@Column(name = "description")
	private String description;

	@Column(name = "sub_component")
	private BigDecimal subComponent;
	
	@Column(name = "useful_life")
	private String usefulLife;
	
	@Column(name = "s_no_count")
	private Integer sNoCount;
	
	@Column(name = "updated_date")
	private LocalDateTime updatedDate;
	
	@Column(name = "updated_by")
	private BigDecimal updatedBy;
	
	@Column(name = "deleted_date")
	private LocalDateTime deletedDate;
	
	@Column(name = "deleted_by")
	private BigDecimal deletedBy;
	
	@Column(name = "created_by")
	private BigDecimal createdBy;
	
	@Column(name = "created_date")
	private LocalDateTime createdDate;
	
	@Column(name = "is_deleted")
	private Boolean isDeleted;
	
	@ManyToOne
	@JoinColumn(name="ref_asset_group",referencedColumnName="id")
	private AssetGroup refAssetGroup;
	
	
	public AssetGroup getRefAssetGroup() {
		return refAssetGroup;
	}

	public void setRefAssetGroup(AssetGroup refAssetGroup) {
		this.refAssetGroup = refAssetGroup;
	}

	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public BigDecimal getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}

	public LocalDateTime getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(LocalDateTime createdDate) {
		this.createdDate = createdDate;
	}
	
	public LocalDateTime getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(LocalDateTime updatedDate) {
		this.updatedDate = updatedDate;
	}

	public BigDecimal getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(BigDecimal updatedBy) {
		this.updatedBy = updatedBy;
	}

	public LocalDateTime getDeletedDate() {
		return deletedDate;
	}

	public void setDeletedDate(LocalDateTime deletedDate) {
		this.deletedDate = deletedDate;
	}

	public BigDecimal getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(BigDecimal deletedBy) {
		this.deletedBy = deletedBy;
	}
	public Integer getsNoCount() {
		return sNoCount;
	}

	public void setsNoCount(Integer sNoCount) {
		this.sNoCount = sNoCount;
	}

	public BigDecimal getId() {
		return id;
	}

	public void setId(BigDecimal id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

	public String getModelNum() {
		return modelNum;
	}

	public void setModelNum(String modelNum) {
		this.modelNum = modelNum;
	}

	public String getSerialNum() {
		return serialNum;
	}

	public void setSerialNum(String serialNum) {
		this.serialNum = serialNum;
	}

	public String getUom() {
		return uom;
	}

	public void setUom(String uom) {
		this.uom = uom;
	}

	public String getCapacity() {
		return capacity;
	}

	public void setCapacity(String capacity) {
		this.capacity = capacity;
	}

	public String getUniqueCode() {
		return uniqueCode;
	}

	public void setUniqueCode(String uniqueCode) {
		this.uniqueCode = uniqueCode;
	}

	public Location getRefLocation() {
		return refLocation;
	}

	public void setRefLocation(Location refLocation) {
		this.refLocation = refLocation;
	}

	public Type getRefType() {
		return refType;
	}

	public void setRefType(Type refType) {
		this.refType = refType;
	}

	public Category getRefCategory() {
		return refCategory;
	}

	public void setRefCategory(Category refCategory) {
		this.refCategory = refCategory;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public LocalDateTime getWarrantyExpiryDate() {
		return warrantyExpiryDate;
	}

	public void setWarrantyExpiryDate(LocalDateTime warrantyExpiryDate) {
		this.warrantyExpiryDate = warrantyExpiryDate;
	}

	public Status getRefStatus() {
		return refStatus;
	}

	public void setRefStatus(Status refStatus) {
		this.refStatus = refStatus;
	}

	public LocalDateTime getPurchaseDate() {
		return purchaseDate;
	}

	public void setPurchaseDate(LocalDateTime purchaseDate) {
		this.purchaseDate = purchaseDate;
	}

	public BigDecimal getPurchasePrice() {
		return purchasePrice;
	}

	public void setPurchasePrice(BigDecimal purchasePrice) {
		this.purchasePrice = purchasePrice;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public BigDecimal getSubComponent() {
		return subComponent;
	}

	public void setSubComponent(BigDecimal subComponent) {
		this.subComponent = subComponent;
	}

	public String getUsefulLife() {
		return usefulLife;
	}

	public void setUsefulLife(String usefulLife) {
		this.usefulLife = usefulLife;
	}

	
	

}
