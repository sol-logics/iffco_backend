package com.sollogics.fams.repositories.modal;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="audit_trail")
@NamedQuery(name="AuditTrail.findAll", query="SELECT a FROM AuditTrail a")
public class AuditTrail {
	@Id
    @Column(name = "id", updatable = false, nullable = false)
    @SequenceGenerator(name = "SequenceIdGenerator", sequenceName = "audit_trail_id_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SequenceIdGenerator")
	private BigDecimal id;
	
	@Column(name = "ref_amc")
	private BigDecimal refAmc;
	
	@Column(name = "ref_assets")
	private BigDecimal refAssets;
	
	@Column(name = "ref_vendor")
	private BigDecimal refVendor;
	
	@Column(name = "ref_user")
	private BigDecimal refUser;

	@Column(name = "data")
	private String data;
	
	@Column(name = "action_by")
	private BigDecimal actionBy;
	
	@Column(name = "action_date")
	private LocalDateTime actionDate;
	
	@Column(name = "action")
	private String action;
	
	public BigDecimal getId() {
		return id;
	}

	public void setId(BigDecimal id) {
		this.id = id;
	}

	public BigDecimal getRefAmc() {
		return refAmc;
	}

	public void setRefAmc(BigDecimal refAmc) {
		this.refAmc = refAmc;
	}

	public BigDecimal getRefAssets() {
		return refAssets;
	}

	public void setRefAssets(BigDecimal refAssets) {
		this.refAssets = refAssets;
	}

	public BigDecimal getRefVendor() {
		return refVendor;
	}

	public void setRefVendor(BigDecimal refVendor) {
		this.refVendor = refVendor;
	}

	public BigDecimal getRefUser() {
		return refUser;
	}

	public void setRefUser(BigDecimal refUser) {
		this.refUser = refUser;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public BigDecimal getActionBy() {
		return actionBy;
	}

	public void setActionBy(BigDecimal actionBy) {
		this.actionBy = actionBy;
	}

	public LocalDateTime getActionDate() {
		return actionDate;
	}

	public void setActionDate(LocalDateTime actionDate) {
		this.actionDate = actionDate;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}
	
}
