package com.sollogics.fams.repositories.modal;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="attachment_association")
@NamedQuery(name="AttachmentAssociation.findAll", query="SELECT a FROM AttachmentAssociation a")
public class AttachmentAssociation {
	@Id
    @Column(name = "id", updatable = false, nullable = false)
    @SequenceGenerator(name = "SequenceIdGenerator", sequenceName = "attachment_association_id_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SequenceIdGenerator")
	private BigDecimal id;
	
	@Column(name = "ref_user")
	private BigDecimal refUser;
	@Column(name = "ref_amc")
	private BigDecimal refAmc;
	@Column(name = "ref_asset")
	private BigDecimal refAsset;
	@Column(name = "ref_vendor")
	private BigDecimal refVendor;
	
	@Column(name = "file_name")
	private String fileName;
	@Column(name = "path")
	private String path;
	@Column(name = "created_date")
	private LocalDateTime createdDate;
	@Column(name = "created_by")
	private BigDecimal createdBy;
	@Column(name = "updated_date")
	private LocalDateTime updatedDate;
	@Column(name = "updated_by")
	private BigDecimal updatedBy;
	@Column(name ="is_deleted")
	private Boolean isDeleted;
	@Column(name = "deleted_date")
	private LocalDateTime deletedDate;
	@Column(name = "deleted_by")
	private BigDecimal deletedBy;
	@Column(name = "file_download_uri")
	private String fileDownloadUri;
	@Column(name = "file_type")
	private String fileType;
	@Column(name = "size")
	private Long size;
	
	public BigDecimal getId() {
		return id;
	}
	public void setId(BigDecimal id) {
		this.id = id;
	}
	public BigDecimal getRefUser() {
		return refUser;
	}
	public void setRefUser(BigDecimal refUser) {
		this.refUser = refUser;
	}
	public BigDecimal getRefAmc() {
		return refAmc;
	}
	public void setRefAmc(BigDecimal refAmc) {
		this.refAmc = refAmc;
	}
	public BigDecimal getRefAsset() {
		return refAsset;
	}
	public void setRefAsset(BigDecimal refAsset) {
		this.refAsset = refAsset;
	}
	public BigDecimal getRefVendor() {
		return refVendor;
	}
	public void setRefVendor(BigDecimal refVendor) {
		this.refVendor = refVendor;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public LocalDateTime getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(LocalDateTime createdDate) {
		this.createdDate = createdDate;
	}
	public BigDecimal getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}
	public LocalDateTime getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(LocalDateTime updatedDate) {
		this.updatedDate = updatedDate;
	}
	public BigDecimal getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(BigDecimal updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Boolean getIsDeleted() {
		return isDeleted;
	}
	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	public LocalDateTime getDeletedDate() {
		return deletedDate;
	}
	public void setDeletedDate(LocalDateTime deletedDate) {
		this.deletedDate = deletedDate;
	}
	public BigDecimal getDeletedBy() {
		return deletedBy;
	}
	public void setDeletedBy(BigDecimal deletedBy) {
		this.deletedBy = deletedBy;
	}
	public String getFileDownloadUri() {
		return fileDownloadUri;
	}
	public void setFileDownloadUri(String fileDownloadUri) {
		this.fileDownloadUri = fileDownloadUri;
	}
	public String getFileType() {
		return fileType;
	}
	public void setFileType(String fileType) {
		this.fileType = fileType;
	}
	public Long getSize() {
		return size;
	}
	public void setSize(Long size) {
		this.size = size;
	}

}
