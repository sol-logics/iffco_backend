package com.sollogics.fams.repositories.modal;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="status")
@NamedQuery(name="Type.Status", query="SELECT a FROM Status a")
public class Status {
	@Id
    @Column(name = "id", updatable = false, nullable = false)
    @SequenceGenerator(name = "SequenceIdGenerator", sequenceName = "status_id_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SequenceIdGenerator")
	private BigDecimal id;
	@Column(name = "name")
	private String name;
	
	public BigDecimal getId() {
		return id;
	}

	public void setId(BigDecimal id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
