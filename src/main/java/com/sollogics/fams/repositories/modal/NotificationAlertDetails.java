package com.sollogics.fams.repositories.modal;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="notification_alert_details")
@NamedQuery(name="NotificationAlertDetails.findAll", query="SELECT a FROM NotificationAlertDetails a")
public class NotificationAlertDetails {
	@Id
    @Column(name = "id", updatable = false, nullable = false)
    @SequenceGenerator(name = "SequenceIdGenerator", sequenceName = "notification_alert_details_id_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SequenceIdGenerator")
	private BigDecimal id;
	
	@Column(name = "mail_body")
	private String mailBody;
	@Column(name = "mail_subject")
	private String mailSubject;
	@Column(name = "alert_status")
	private Boolean alertStatus;
	@Column(name = "alert_times")
	private Integer alertTimes;
	@Column(name = "alert_intervals")
	private Integer alertIntervals;
	@Column(name = "end_times")
	private Integer endTimes;
	@Column(name = "object_id")
	private BigDecimal objectId;
	@Column(name = "notification_name")
	private String notificationName;
	@Column(name = "stop_on_action")
	private Boolean stopOnAction;
	@Column(name = "cc_to")
	private String cc_to;
	@Column(name = "alert_left")
	private Integer alertLeft;
	@Column(name = "created_by")
	private BigDecimal createdBy;
	@Column(name = "created_date")
	private LocalDateTime createdDate;
	@Column(name = "alert_sent")
	private Integer alertSent;
	@Column(name = "alert_start_time")
	private LocalDateTime alertStartTime;
	@Column(name = "alert_end_time ")
	private LocalDateTime alertEndTime;
	@Column(name = "attachment_details")
	private String attachmentDetails;
	
	public String getAttachmentDetails() {
		return attachmentDetails;
	}
	public void setAttachmentDetails(String attachmentDetails) {
		this.attachmentDetails = attachmentDetails;
	}
	public LocalDateTime getAlertStartTime() {
		return alertStartTime;
	}
	public void setAlertStartTime(LocalDateTime alertStartTime) {
		this.alertStartTime = alertStartTime;
	}
	public LocalDateTime getAlertEndTime() {
		return alertEndTime;
	}
	public void setAlertEndTime(LocalDateTime alertEndTime) {
		this.alertEndTime = alertEndTime;
	}
	public String getNotificationName() {
		return notificationName;
	}
	public void setNotificationName(String notificationName) {
		this.notificationName = notificationName;
	}
	public Integer getAlertSent() {
		return alertSent;
	}
	public void setAlertSent(Integer alertSent) {
		this.alertSent = alertSent;
	}
	public BigDecimal getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}
	public LocalDateTime getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(LocalDateTime createdDate) {
		this.createdDate = createdDate;
	}
	public BigDecimal getId() {
		return id;
	}
	public void setId(BigDecimal id) {
		this.id = id;
	}
	public String getMailBody() {
		return mailBody;
	}
	public void setMailBody(String mailBody) {
		this.mailBody = mailBody;
	}
	public String getMailSubject() {
		return mailSubject;
	}
	public void setMailSubject(String mailSubject) {
		this.mailSubject = mailSubject;
	}
	public Boolean getAlertStatus() {
		return alertStatus;
	}
	public void setAlertStatus(Boolean alertStatus) {
		this.alertStatus = alertStatus;
	}
	public Integer getAlertTimes() {
		return alertTimes;
	}
	public void setAlertTimes(Integer alertTimes) {
		this.alertTimes = alertTimes;
	}
	public Integer getAlertIntervals() {
		return alertIntervals;
	}
	public void setAlertIntervals(Integer alertIntervals) {
		this.alertIntervals = alertIntervals;
	}
	public Integer getEndTimes() {
		return endTimes;
	}
	public void setEndTimes(Integer endTimes) {
		this.endTimes = endTimes;
	}
	public BigDecimal getObjectId() {
		return objectId;
	}
	public void setObjectId(BigDecimal objectId) {
		this.objectId = objectId;
	}
	public Boolean getStopOnAction() {
		return stopOnAction;
	}
	public void setStopOnAction(Boolean stopOnAction) {
		this.stopOnAction = stopOnAction;
	}
	public String getCc_to() {
		return cc_to;
	}
	public void setCc_to(String cc_to) {
		this.cc_to = cc_to;
	}
	public Integer getAlertLeft() {
		return alertLeft;
	}
	public void setAlertLeft(Integer alertLeft) {
		this.alertLeft = alertLeft;
	}
	
}
