package com.sollogics.fams.repositories.modal;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="amc")
@NamedQuery(name="Amc.findAll", query="SELECT a FROM Amc a")
public class Amc {
	@Id
    @Column(name = "id", updatable = false, nullable = false)
    @SequenceGenerator(name = "SequenceIdGenerator", sequenceName = "amc_id_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SequenceIdGenerator")
	private BigDecimal id;
	
	@Column(name = "created_by")
	private BigDecimal createdBy;
	
	@Column(name = "created_date")
	private LocalDateTime createdDate;
	
	@Column(name = "updated_by")
	private BigDecimal updatedBy;
	
	@Column(name = "updated_date")
	private LocalDateTime updatedDate;
	
	@Column(name = "is_deleted")
	private Boolean isDeleted;
	
	@Column(name= "deleted_date")
	private LocalDateTime deletedDate;
	
	@Column(name = "deleted_by")
	private BigDecimal deletedBy;
	
	@ManyToOne
	@JoinColumn(name="ref_vendor",referencedColumnName="id")
	private Vendors refVendor;
	
	@ManyToOne
	@JoinColumn(name="ref_asset",referencedColumnName="id")
	private Asset refAsset;
	
	@Column(name = "start_date")
	private LocalDateTime startDate;
	
	@Column(name = "expiry_date")
	private LocalDateTime expiryDate;
	
	@Column(name = "cost")
	private BigDecimal cost;
	
	@Column(name = "amc_unique_code")
	private String amcUniqueCode;
	
	@Column(name = "s_number_count")
	private Integer sNumberCount;
	
	@Column(name = "previous_amc_id")
	private BigDecimal previousAmcId;
	
	@Column(name = "renew_uuid")
	private String renewUuid;
	
	@Column(name = "is_latest")
	private Boolean isLatest;
	
	@ManyToOne
	@JoinColumn(name = "ref_status",referencedColumnName = "id")
	private Status refStatus;
	
	public BigDecimal getPreviousAmcId() {
		return previousAmcId;
	}

	public void setPreviousAmcId(BigDecimal previousAmcId) {
		this.previousAmcId = previousAmcId;
	}

	public String getRenewUuid() {
		return renewUuid;
	}

	public void setRenewUuid(String renewUuid) {
		this.renewUuid = renewUuid;
	}

	public Boolean getIsLatest() {
		return isLatest;
	}

	public void setIsLatest(Boolean isLatest) {
		this.isLatest = isLatest;
	}

	public Status getRefStatus() {
		return refStatus;
	}

	public void setRefStatus(Status refStatus) {
		this.refStatus = refStatus;
	}
	public Integer getsNumberCount() {
		return sNumberCount;
	}

	public void setsNumberCount(Integer sNumberCount) {
		this.sNumberCount = sNumberCount;
	}

	public String getAmcUniqueCode() {
		return amcUniqueCode;
	}

	public void setAmcUniqueCode(String amcUniqueCode) {
		this.amcUniqueCode = amcUniqueCode;
	}

	public BigDecimal getId() {
		return id;
	}

	public void setId(BigDecimal id) {
		this.id = id;
	}

	public BigDecimal getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}

	public LocalDateTime getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(LocalDateTime createdDate) {
		this.createdDate = createdDate;
	}

	public BigDecimal getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(BigDecimal updatedBy) {
		this.updatedBy = updatedBy;
	}

	public LocalDateTime getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(LocalDateTime updatedDate) {
		this.updatedDate = updatedDate;
	}

	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public LocalDateTime getDeletedDate() {
		return deletedDate;
	}

	public void setDeletedDate(LocalDateTime deletedDate) {
		this.deletedDate = deletedDate;
	}

	public BigDecimal getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(BigDecimal deletedBy) {
		this.deletedBy = deletedBy;
	}

	public Vendors getRefVendor() {
		return refVendor;
	}

	public void setRefVendor(Vendors refVendor) {
		this.refVendor = refVendor;
	}

	public Asset getRefAsset() {
		return refAsset;
	}

	public void setRefAsset(Asset refAsset) {
		this.refAsset = refAsset;
	}

	public LocalDateTime getStartDate() {
		return startDate;
	}

	public void setStartDate(LocalDateTime startDate) {
		this.startDate = startDate;
	}

	public LocalDateTime getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(LocalDateTime expiryDate) {
		this.expiryDate = expiryDate;
	}

	public BigDecimal getCost() {
		return cost;
	}

	public void setCost(BigDecimal cost) {
		this.cost = cost;
	}
	
	

}
