package com.sollogics.fams.repositories.modal;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="profile")
@NamedQuery(name="Profile.findAll", query="SELECT a FROM Profile a")
public class Profile {
	
	@Id
    @Column(name = "id", updatable = false, nullable = false)
    @SequenceGenerator(name = "SequenceIdGenerator", sequenceName = "profile_id_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SequenceIdGenerator")
	private BigDecimal id;
	
	@ManyToOne
	@JoinColumn(name="ref_user",referencedColumnName="id")
	private Users refUser;
	@Column(name = "first_name")
	private String firstName;
	@Column(name = "last_name")
	private String lastName;
	@ManyToOne
	@JoinColumn(name="ref_role",referencedColumnName="id")
	private Roles refRoles;
	@Column(name = "emp_id")
	private String empId;
	@Column(name = "created_on")
	private LocalDateTime createdOn;
	@Column(name = "updated_on")
	private LocalDateTime updatedOn;
	@ManyToOne
	@JoinColumn(name="ref_company",referencedColumnName="id")
	private Company refCompany;
	
	@Column(name = "designation")
	private String designation;
	
	@Column(name= "departement")
	private String departement;
	
	@Column(name = "updated_by")
	private BigDecimal updatedBy;
	
	@Column(name ="created_by")
	private BigDecimal createdBy;
	@Column(name = "deleted_by")
	private BigDecimal deletedBy;
	@Column(name = "is_deleted")
	private Boolean isDeleted;
	
	public Boolean getIsDeleted() {
		return isDeleted;
	}
	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	public BigDecimal getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}
	public BigDecimal getDeletedBy() {
		return deletedBy;
	}
	public void setDeletedBy(BigDecimal deletedBy) {
		this.deletedBy = deletedBy;
	}
	
	public BigDecimal getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(BigDecimal updatedBy) {
		this.updatedBy = updatedBy;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	public String getDepartement() {
		return departement;
	}
	public void setDepartement(String departement) {
		this.departement = departement;
	}
	public BigDecimal getId() {
		return id;
	}
	public void setId(BigDecimal id) {
		this.id = id;
	}
	public Users getRefUser() {
		return refUser;
	}
	public void setRefUser(Users refUser) {
		this.refUser = refUser;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public Roles getRefRoles() {
		return refRoles;
	}
	public void setRefRoles(Roles refRoles) {
		this.refRoles = refRoles;
	}
	public String getEmpId() {
		return empId;
	}
	public void setEmpId(String empId) {
		this.empId = empId;
	}
	public LocalDateTime getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(LocalDateTime createdOn) {
		this.createdOn = createdOn;
	}
	public LocalDateTime getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(LocalDateTime updatedOn) {
		this.updatedOn = updatedOn;
	}
	public Company getRefCompany() {
		return refCompany;
	}
	public void setRefCompany(Company refCompany) {
		this.refCompany = refCompany;
	}
	
	
}
